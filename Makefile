################################################################################
# @file Makefile
#
# @brief Device Driver Generator make.
#
# @author Copyright (C) 2003-2010 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
#
# @date Created on 29/06/2003
#
# @section license_sec License
#          Released under the GPL
################################################################################

include ./Makefile.base

CUR_DIR	 = $(shell pwd)
PSSTUFF	 = /acc/local/$(OS)
DEFFLAGS = -DTEMPLATE_ROOT=\"$(CUR_DIR)/templates\" #-D_VERBOSE_
#ADDCFLAGS = -fwritable-strings
#ADDCFLAGS = -DACCESS_XML
ADDCFLAGS = -DACCESS_DB

# libdb1.a is /acc/local/Linux/db1/libdb1.a

LIBDIRS    = $(PSSTUFF)/lib $(STANDARDLIB)
LDLIBS     = -lxml2 -ldbrt -ldb1
INCDIRS    = /usr/include/libxml2 $(PSSTUFF)/include ./include

SRCFILES = \
	commonGeneration.c \
	driverGen.c \
	drmGeneration.c \
	rwops.c \
	serviceOptions.c \
	translation.c \
	utilities.c \
	vmeGeneration.c \
	access.c \
	dbrtAccess.c \
	xml-access.c \
	dg-git-lib.c

# synchronize version
#   See http://www.gnu.org/software/sed/manual/sed.html
#+ 3.2 Selecting lines with sed (for $) &&
#+ 3.4 Often-Used Commands (for p)
version = $(shell perl -pi -e "s/(static const char dg_version\[\] = ).*/\1\"`git-tag | sed -n '$$p'`\"\;/" ./include/dg-version.h)

build:: $(version) $(OBJDIR) $(OBJDIR)/dgII dgII

$(OBJDIR)/dgII: $(OBJFILES) $(LIBS)
	$(LDEXE) -o $@ $(BASELDFLAGS) $(OBJS) $(LIBFLAGS) $(LDLIBS)

# Make symlink
dgII:
	@if [ -e "$$@" ]; then \
		rm -f $@ ; \
	echo "ln -s $(OBJDIR)/$@ $@" ; \
	ln -s $(OBJDIR)/$@ $@ ; \
	fi

clean clear::
	-rm -rf $(OBJDIR)
	find ./ -name '*~' | xargs rm -f
