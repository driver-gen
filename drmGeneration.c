/**
 * @file drmGeneration.c
 *
 * @brief Generates a specific PCI device Driver/Simulator code
 *
 * @author Copyright (C) 2002 CERN. Stuart Baird
 * @author Copyright (C) 2003 CERN. Alain Gagnaire
 * @author Copyright (C) 2003 - 2010 CERN. Georgievskiy Yury <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */
#include "driverGen.h"

static void GenerateDrmInfo(PciInfo_t *, BlockDef_t *, int);
static void BuildDrmHeaderFile(int, RegisterDef_t *, int, BlockDef_t *, int);
static void BuildDrmStatics(BlockDef_t *, int, FILE *, int);
static void BuildDrmDrvrFile(RegisterDef_t *, int, BlockDef_t *, int);
static void BuildDrmSimFile(RegisterDef_t *, int, BlockDef_t *, int);

/**
 * @brief Generates the entire driver source code from scratch, including
 *        user-defined files.
 *
 * @param registers    - register description
 * @param numRegisters - register amount
 * @param blocks       - block description
 * @param numBlocks    - block amount
 * @param pciInfo      - @e pci description
 *
 * @return void
 */
void GenerateDrmDriver(RegisterDef_t * registers, int numRegisters,
		       BlockDef_t * blocks, int numBlocks, PciInfo_t * pciInfo)
{
	char *system = TranslationGetSysLower();

	/* First, build-up major source code of the driver */
	GenerateDrmCore(registers, numRegisters, blocks, numBlocks, pciInfo);

	/* Now build-up user-defined driver files for the current module */
	printf("Building %s user entry poins.\n", system);
	BuildUserPartDrvrCode();
}

/**
 * @brief Generates major source code of the driver, excluding the generation
 *        of user-defined files.
 *
 * @param registers    - register description
 * @param numRegisters - register amoun
 * @param blocks       - block description
 * @param numBlocks    - block amoun
 * @param pciInfo      - @e pci description
 *
 * @return void
 */
void GenerateDrmCore(RegisterDef_t * registers, int numRegisters,
		     BlockDef_t * blocks, int numBlocks, PciInfo_t * pciInfo)
{
	char *system = TranslationGetSysLower();

  /*-=-=-=-=-=-=-=-=-=-=-=-=- 00. PCI driver. =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	printf("Building %s driver's source code\n", system);
	/* Header file for driver. */
	BuildDrmHeaderFile(DRIVER_FT, registers, numRegisters, blocks,
			   numBlocks);
	/* Driver source. */
	BuildDrmDrvrFile(registers, numRegisters, blocks, numBlocks);

  /*-=-=-=-=-=-=-=-=-=-=-= 01. PCI simulator. -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	printf("Building %s driver's simulator source code.\n", system);
	/* Header file for simulator. */
	BuildDrmHeaderFile(SIM_FT, registers, numRegisters, blocks, numBlocks);
	/* Simulator source. */
	BuildDrmSimFile(registers, numRegisters, blocks, numBlocks);

  /*-=-=-=-=-=-=-=-=-02. Driver/Simulator registers r/w operations-=-=-=-=-=-*/
	/* TODO. Take care of the third parameter! It's incorrect in this caseQ */
	BuildGetSetRegCH(registers, numRegisters, NULL);

  /*--------------------03. Libraries---------------------------------------*/
	printf("Building %s access library\n", system);
	BuildIoctlLibrary(registers, numRegisters, blocks, numBlocks);

	/*  */
	printf("Building %s test program\n", system);
	BuildTestProgram(registers, numRegisters, blocks, numBlocks);

  /*========XX. REGISTER DESCRIPTINON AND INFO FILE.========*/
	GenerateDrmInstallFilesOLD(pciInfo, blocks, numBlocks);

	/*  */
	printf("Building %s makefile\n", system);
	BuildMakefiles();
}

/**
 * @brief SHOULD BE REMOVED.
 *
 * @param pciInfo   - @e pci info
 * @param blocks    - block description
 * @param numBlocks - block amount
 *
 * @return void
 */
void GenerateDrmInstallFilesOLD(PciInfo_t * pciInfo, BlockDef_t * blocks,
				int numBlocks)
{
	char *system = TranslationGetModName();

	printf("Building %s essential installation files:\n", system);
	printf("	 Description files.\n");
	BuildDescsOLD(8);
	printf("	 Instance info structure.\n");
	GenerateDrmInfo(pciInfo, blocks, numBlocks);
}

/**
 * @brief Generate an info structure to be used by all drivers.
 *
 * @param pciInfo   - @e pci info
 * @param blocks    - block description
 * @param numBlocks - block amount
 *
 * As a PCI driver had no need of an info structure, a dummy one is used to
 * satisfy the LynxOS driver format.
 *
 * @return void
 */
static void GenerateDrmInfo(PciInfo_t * pciInfo, BlockDef_t * blocks,
			    int numBlocks)
{
	FILE *infoFile = OpenFile(DRIVER_FT, LOC_MODSRC, "INST/.inst");

	fwrite(pciInfo, sizeof(pciInfo), 1, infoFile);
	fclose(infoFile);
}

/**
 * @brief
 *
 * @param type         - driver or simulator
 * @param registers    - register description
 * @param numRegisters - register amount
 * @param blocks       - block description
 * @param numBlocks    - block amoun
 *
 * @return void
 */
static void BuildDrmHeaderFile(int type, RegisterDef_t * registers,
			       int numRegisters, BlockDef_t * blocks,
			       int numBlocks)
{
	FILE *headerFile;

	TranslationReset();
	headerFile = OpenFile(type, LOC_INCL, ".h");

	/* Generate the header file's head and then go to generate the rest */
	Translate(headerFile, "drm", "header/head.h");
	/* Generate the constants used for the IOCTL function */
	BuildIoctlConsts(registers, numRegisters, headerFile);

	TranslationSetPlainNum(numRegisters * 2);
	Translate(headerFile, "drm", "header/configConst.h");
	/* Generate the statics, info and block structures. */
	BuildCommonBlocks(type, registers, numRegisters, headerFile);
	BuildDrmStatics(blocks, numBlocks, headerFile, type);

	fclose(headerFile);
}

/**
 * @brief
 *
 * @param blocks     - block description
 * @param numBlocks  - block amount
 * @param headerFile - open file dscriptor
 * @param type       - driver or simulator
 *
 * @return void
 */
static void BuildDrmStatics(BlockDef_t * blocks, int numBlocks,
			    FILE * headerFile, int type)
{
	int i;

	TranslationReset();
	Translate(headerFile, "drm", "header/topologyHead.h");

	/* For each block that's been defined, place a field in the statics
	   structure.  A PCI device may have a maximum of 6 blocks defined
	   so ignore any blocks out of the range 0-5. */
	for (i = 0; i < numBlocks; i++) {
		TranslationSetFancyNum(blocks[i].blockID);
		Translate(headerFile, "common", "header/blockDef.h");
	}

	/* Close the statics structure and generate the info one. */
	TranslationSetDriverType((type == DRIVER_FT) ? "DRVR" : "SIM");
	Translate(headerFile, "common", "header/topologyFoot.h");
}

/**
 * @brief Generate the driver's source file.
 *
 * @param registers    - register description
 * @param numRegisters - register amount
 * @param blocks       - block description
 * @param numBlocks    - block amount
 *
 * @return void
 */
static void BuildDrmDrvrFile(RegisterDef_t * registers, int numRegisters,
			     BlockDef_t * blocks, int numBlocks)
{
	FILE *dfd = OpenFile(DRIVER_FT, LOC_DRVR, ".c");
	int cntr;

	TranslationReset();
	Translate(dfd, "drm", "driver/head.c");

	TranslationSetDriverType("Drvr");
	TranslationSetFancyString("DRVR");
	TranslationSetDummyString("Driver");
	Translate(dfd, "common", "driver/driverHead.c");

	/* Generate driver's open routine */
	Translate(dfd, "drm", "driver/open/drvrOpen.c");

	/* Generate driver's close routine. */
	Translate(dfd, "common", "driver/close/head.c");

	/* Generate driver's read routine. */
	Translate(dfd, "common", "driver/read/head.c");

	/* Generate driver's write routine. */
	Translate(dfd, "common", "driver/write/head.c");

	/* Generate driver's select routine. */
	Translate(dfd, "common", "driver/select/head.c");

	/* Generate driver's ioctl routine. */
	BuildDrvrSimIoctl(registers, numRegisters, dfd);

	/* Generate driver's install routine. */
	Translate(dfd, "drm", "driver/install/installHead.c");
	for (cntr = 0; cntr < numBlocks; cntr++) {
		TranslationSetPlainNum(blocks[cntr].blockID);
		TranslationSetFancyNum(blocks[cntr].blockID);
		Translate(dfd, "drm", "driver/install/installBlock.c");
	}
	Translate(dfd, "drm", "driver/install/installFoot.c");

	/* Generate driver's uninstall routine. */
	Translate(dfd, "drm", "driver/uninstall/uninstallHead.c");
	for (cntr = 0; cntr < numBlocks; cntr++) {
		TranslationSetPlainNum(blocks[cntr].blockID);
		Translate(dfd, "drm", "driver/uninstall/uninstallBlock.c");
	}
	Translate(dfd, "drm", "driver/uninstall/uninstallFoot.c");

	/* Generate an empty ISR */
	Translate(dfd, "common", "driver/isr.c");

	/* Build-up dldd structure (driver entry points) */
	TranslationSetDriverType("Drvr");
	Translate(dfd, "common", "driver/entryPoints.c");

	fclose(dfd);
}

/**
 * @brief Generate the simulator's source file.
 *
 * @param registers    - register description
 * @param numRegisters - register amount
 * @param blocks       - block description
 * @param numBlocks    - block amount
 *
 * @return void
 */
static void BuildDrmSimFile(RegisterDef_t * registers, int numRegisters,
			    BlockDef_t * blocks, int numBlocks)
{
	FILE *sfd = OpenFile(SIM_FT, LOC_DRVR, ".c");
	int cntr;

	TranslationReset();
	Translate(sfd, "drm", "driver/head.c");

	TranslationSetDriverType("Sim");
	TranslationSetFancyString("SIM");
	TranslationSetDummyString("Simulator");
	Translate(sfd, "common", "driver/driverHead.c");

	/* Generate simulator's open routine. */
	Translate(sfd, "drm", "driver/open/simOpenHead.c");
	for (cntr = 0; cntr < numBlocks; cntr++) {
		TranslationSetFancyNum(blocks[cntr].blockID);
		TranslationSetPlainNum(blocks[cntr].blockID);
		Translate(sfd, "common", "driver/install/allocateBlock.c");
	}
	Translate(sfd, "drm", "driver/open/installConfig.c");
	Translate(sfd, "drm", "driver/open/simOpenFoot.c");

	/* Generate simulator's close routine. */
	Translate(sfd, "common", "driver/close/head.c");

	/* Generate simulator's read routine. */
	Translate(sfd, "common", "driver/read/head.c");

	/* Generate simulator's write routine. */
	Translate(sfd, "common", "driver/write/head.c");

	/* Generate simulator's select routine. */
	Translate(sfd, "common", "driver/select/head.c");

	/* Generate simulator's ioctl routine. */
	BuildDrvrSimIoctl(registers, numRegisters, sfd);

	/* Generate simulator's install routine. */
	Translate(sfd, "drm", "driver/install/simInstall.c");

	/* Generate simulator's uninstall routine. */
	Translate(sfd, "drm", "driver/uninstall/simUninstallHead.c");
	for (cntr = 0; cntr < numBlocks; cntr++) {
		TranslationSetFancyNum(blocks[cntr].blockID);
		TranslationSetPlainNum(blocks[cntr].blockID);
		Translate(sfd, "common", "driver/uninstall/uninstallBlocks.c");
	}
	Translate(sfd, "common", "driver/uninstall/foot.c");

	/* Generate an empty ISR */
	Translate(sfd, "common", "driver/isr.c");

	/* Build-up dldd structure (simulator entry points) */
	TranslationSetDriverType("Sim");
	Translate(sfd, "common", "driver/entryPoints.c");

	fclose(sfd);
}
