/**
 * @file dg-git-lib.h
 *
 * @brief Git utilities function declarations are located in this module
 *
 * @author Copyright (C) 2010 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 01/02/2010
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifndef _DG_GIT_LIB_H_INCLUDE_
#define _DG_GIT_LIB_H_INCLUDE_

#include "driverGen.h"

uint8_t git_check_version(void);
uint8_t do_git_vme_driver(char *, struct mdescr *, RegisterDef_t *, int,
			  BlockDef_t *, int, VmeInfo_t *);
uint8_t do_git_pci_driver(char *, struct mdescr *, RegisterDef_t *, int,
			  BlockDef_t *, int, PciInfo_t *);



#endif	/* _DG_GIT_LIB_H_INCLUDE_ */
