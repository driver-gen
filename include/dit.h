/**
 * @file dit.h
 *
 * @brief Device Info Table description.
 *
 * This info table is generated from the DB during driver generation.
 *
 * @author Copyright (C) 2010 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 25/02/2010
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifndef _DIT_HEADER_H_INCLUDE_
#define _DIT_HEADER_H_INCLUDE_

#include <stdlib.h>
#include "driverGen.h"
#include "vme_am.h"	/* VMEbus Address Modifier Codes */

#define DBMAM 2 /* MAX allowed amount of module sub-addresses in the DB */

typedef unsigned long kaddr_t;

/*
  Debug bit filed. By setting different flags user can enable and disabe some
  Driver features. (Level of Debug information printing, register access time
  statistics, etc...)

  !NOTE! modinst and test programs should be modified and recompiled
        if you modify this enum.
*/
typedef enum _tagDBGIPL {
  DBGNONE     = 0,     /* no debug information */
  DBGERR      = 1<<0,  /* invalid debug flag */
  DBGINSTL    = 1<<1,  /* printout during driver installation procedure */
  DBGDEINSTL  = 1<<2,  /* printout during driver deinstallation procedure */
  DBGIOCTL    = 1<<3,  /* printout during driver ioctl calls */
  DBGTIMESTAT = 1<<4,  /* printout register access time statistics */
  DBGALLDI    = 1<<5,  /* printout all devinfo table during installation phase
			  but _only_ if DBGINSTL is set */
  DBGOPEN     = 1<<6,  /* printout during open drvr call */
  DBGCLOSE    = 1<<7,  /* printout during close drvr call */
  DBGREAD     = 1<<8,  /* printout during read drvr call */
  DBGWRITE    = 1<<9,  /* printout during write drvr call */
  DBGSELECT   = 1<<10, /* printout during select drvr call */
  _DBG_RES_1  = 1<<11, /* reserved */
  _DBG_RES_2  = 1<<12, /* reserved */
  _DBG_RES_3  = 1<<13, /* reserved */
  DBGMASK     = _DBG_RES_3 - 1, /* claimed debug bits mask */
  DBG_AVAL    = 14,    /* first bit, avalable for usage.
			  Other bits are used by DAL */
  DBGALL      = (DBGMASK & (~DBGERR)) /* all driver flags are enable */
} dbgIPL; /* driver debug flags */

/**
 * @brief address space information
 *
 * @param baseAddr  -- base address
 *                     ADCG__NO_ADDRESS - address space NOT defined
 * @param range     -- address range
 * @param increment -- address increment. Taking into consideration @b only
 *                     during device driver installation
 * @param dpSize    -- dataport size (16 or 32)
 * @param addrModif -- supported address modifier codes\n
 *		       0x29 (SHort)
 *		       0x39 (STandart)
 *		       0x09 (EXtended) or
 *		       0x2F (CR - Configuration Register)
 */
typedef struct {
	kaddr_t baseAddr;
	uint    range;
	int     increment;
	int     dpSize;
	vmeam_t addrModif;
} __attribute__((packed)) AddrInfo_t;

/**
 * @brief block description
 *
 * @param block       -- block sequence number, starting from 0
 * @param blkBaseAddr -- 1 for INIT and 2 for NEXT base address
 * @param offset      -- block offset from the base address
 * @param blksz_drvr  -- driver block size in bytes.
 *                       Sizes may differ for driver/simulator,
 *                       because gaps are not taken into account
 *                       for simulator. For real driver they do
 * @param blksz_sim   -- simulator block size in bytes
 * @param reg_am      -- register amount in the block

 */
typedef struct {
	short block;
	short blkBaseAddr;
	long  offset;
	int   blksz_drvr;
	int   blksz_sim;
	int   reg_am;
} __attribute__((packed)) BLKDESC;

/**
 * @brief register description
 *
 * @param regId        -- register ID
 * @param regName      -- register name
 * @param busType      -- bus type (VME, PCI, ...)
 * @param reg_regType  -- register type
 * @param regSize      -- size of each register element in bytes
 *                        (should be one of REGSIZE)
 * @param b_a          -- base address - 'addr1' or 'addr2'
 * @param bid          -- block ID (starting from zero), to which block
 *                        register belongs
 * @param regOffset    -- reg offset in bytes from the block base addr
 *                        Note, that block can have an offset itself from
 *                        his address space!
 * @param regDepth     -- register depth.
			  '-1'      => FIFO register
			  '0'       => scalar i.e. single register
			  any other => number of elements in register
 * @param regtl        -- specify a timing loop for register accessing.
			  A value of 0 disables the timing loop
 * @param regar        -- register access rights (rwe)
 * @param rwIoctlOpNum -- Each register can be read/write. ioctl
			  numbers for this operations are stored
			  here. They are masked:
			  & 0xffff0000 'read'  (or GET) ioctl num,
			  & 0x0000ffff 'write' (or SET) ioctl num.
			  IOCTL_BASE should be added after extraction.
 */
typedef struct {
	int     regId;
	char    regName[NAME_LEN];
	char    busType[MIN_STR];
	reg_t   regType;
	int     regSize;
	char    b_a[MIN_STR];
	short   bid;
	int     regOffset;
	int     regDepth;
	int     regtl;
	ARIGHTS regar;
	int     rwIoctlOpNum;
} __attribute__((packed)) REGDESC;

/**
 * @brief Device info table
 *
 * This structure is passed to the @e install driver routine during device
 * installation each time @e cdv_install() called.
 *
 * @param addr1      -- INIT address i.e. VME base address
 * @param addr2      -- NEXT address i.e. VME address of the memory part
 * @param mtn	     -- DB module type number. (IocModulType enum)
 * @param mlun       -- module Logical Unit Number
 * @param debugFlag  -- Debug Information Printing Level flag
 * @param opaque     -- dim param, intended purely for driver
 * @param iLevel     -- interrupt processing hardware level
 * @param iVector    -- interrupt vector
 * @param iVectorInc --  interrupt vector increment
 * @param chan       -- minor devices amount (number of channels)
 * @param chrindex   -- check register index (i.e. index in 'regDesc' massive)
			Register, on which a read test will be made to test
			presence of the module. Register offset and size will
			be used to compute address and size of read-test zone.
			@b -1 if there is no checking register
 * @param gen_date   -- when this info table was generated
 * @param dg_vers    -- produced by (which driverGen version)
 * @param regAmount  -- actual register amount of the module
 * @param regDesc    -- description of each register
 * @param maxRegSz   -- size in bytes of the biggest register in the module
 * @param blkAmount  -- actual block amount in the module
 * @param blkDesc    -- description of each block
 * @param checksum   -- Modulo 256 checksum must give zero
 */
typedef struct {
	AddrInfo_t addr1;
	AddrInfo_t addr2;
	/* !NOTE! add new memberes (if any) after this point */
	int	   mtn;
	int        mlun;
	dbgIPL     debugFlag;
	char	   opaque[MAX_STR];
	int	   iLevel;
	int	   iVector;
	int	   iVectorInc;
	int	   chan;
	int        chrindex;
	long       gen_date;
	char       dg_vers[NAME_LEN];
	int	   regAmount;
	REGDESC    regDesc[MAX_REG];
	int        maxRegSz;
	int        blkAmount;
	BLKDESC    blkDesc[MAX_BLK];
	char       checksum;
} __attribute__((packed)) DevInfo_t;

#ifdef USE_DIT_NEW
struct dit {
	struct addr_info *addr;
};
#endif

#endif	/* _DIT_HEADER_H_INCLUDE_ */
