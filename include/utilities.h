/**
 * @file utilities.h
 *
 * @brief Utilities used by the driverGen
 *
 * @author Copyright (C) 2004 - 2010 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifndef _UTILITIES_H_INCLUDE_
#define _UTILITIES_H_INCLUDE_

#include "dit.h"

char* itoa(int, int);
void  StrToLower(char*);
void  StrToUpper(char*);
void  PrintoutRegInfo(RegisterDef_t*, int);
int   calc_block_size(RegisterDef_t*, int);
int   calc_block_gap_amount(RegisterDef_t*);
void  set_extra_block_data(RegisterDef_t*);
DevInfo_t* read_info_file(char *);
void exploit_info_file(DevInfo_t *);
char* get_info_fn(void);
char* get_xml_fn(void);

#endif /* _UTILITIES_H_INCLUDE_ */
