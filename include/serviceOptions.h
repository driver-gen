/**
 * @file serviceOptions.h
 *
 * @brief
 *
 * @author Copyright (C) 2004 - 2010 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifndef _SERVICE_OPTIONS_H_INCLUDE_
#define _SERVICE_OPTIONS_H_INCLUDE_

/* all possible service option parameters */
typedef enum _tagSrvArg {
	SRV_VF_DRVR = 1, /* 'drvrvers' - create driver version file */
	SRV_VF_SIM,      /* 'simvers'  - create simulator version file */
	SRV_ARG_ERROR    /* wrong argument for the service option */
} srvarg;

/* container holds service option arguments */
typedef struct _tagServiceOptContainter {
	char srv_vf_dir[64];	/* version file directory name */
} srvopt_c;

int SetSrvOptArgs(srvarg, ...);
int ProcessSrvOpt(srvarg, char *);

#endif /* _SERVICE_OPTIONS_H_INCLUDE_ */
