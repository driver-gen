/**
 * @file driverGen.h
 *
 * @brief Major DriverGen definitions and function declarations are here
 *
 * @author Copyright (C) 2003 - 2010 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifndef _DRIVERGEN_H_INCLUDE_
#define _DRIVERGEN_H_INCLUDE_

#include <elf.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h> /* for standard integer types [u]intN_t */
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/param.h> /* for MAXPATHLEN */

#include "translation.h"
#include "vme_am.h"	/* for VME address modifier codes */

/* Here comes some coloring */
#define RED_CLR		"\033[0;31m"
#define WHITE_CLR	"\033[1;37m"
#define END_CLR		"\033[m"

#define WARNING_MSG "\033[0;31mWARNING->\033[m"
#define ERROR_MSG   "\033[0;31mERROR->\033[m"

#define max(A, B) ((A) > (B) ? (A) : (B))

#ifndef bool
#define bool unsigned int
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE  1
#endif

/* First entry number in 'Main Menu' that can be used */
#define TST_PROG_FIRST_FREE_ENTRY 6

/* first entry number in 'Register Sub Menus' that can be used */
#define TST_PROG_SUB_MENU_FIRST_ENTRY 3

#define SRVREGPREF "__SRV__" /* subword of all service registers */

#define FATAL_ERR()							    \
do {									    \
  fprintf(stderr, "%s %s: Function %s (line %d) shouldn't be reachable.\n", \
	  ERROR_MSG, __FILE__, __FUNCTION__, __LINE__);			    \
  exit(EXIT_FAILURE); /* 1 */						    \
} while (0)

#define ERR_POS(erF)					\
do {							\
  char posData[128];					\
  snprintf(posData, sizeof(posData), "%s %s():%d. %s",	\
	   __FILE__, __FUNCTION__, __LINE__, erF);	\
  perror(posData);					\
} while (0)

/* Function return status */
typedef enum _tagRETSTAT {
	DRIVER_GEN_OK,		/* 0 */
	DRIVER_GEN_BAD,		/* 1 */
	DRIVER_GEN_EXIST	/* 2 */
} rstat;

/* swap bytes */
static inline void __endian(const void *src, void *dest, unsigned int size)
{
  unsigned int i;
  for (i = 0; i < size; i++)
    ((unsigned char*)dest)[i] = ((unsigned char*)src)[size - i - 1];
}

/* find out current endianity */
static inline int __my_endian()
{
  static int my_endian = ELFDATANONE;
  union { short s; char c[2]; } endian_test;

  if (my_endian != ELFDATANONE)	/* already defined */
    return(my_endian);

  endian_test.s = 1;
  if (endian_test.c[1] == 1) my_endian = ELFDATA2MSB;
  else if (endian_test.c[0] == 1) my_endian = ELFDATA2LSB;
  else abort();

  return(my_endian);
}

/* assert data to Big Endian (to put it in the file afterwards) */
#define ASSERT_MSB(x)				\
({						\
  typeof(x) __x;				\
  typeof(x) __val = (x);			\
  if (__my_endian() == ELFDATA2LSB)		\
    __endian(&(__val), &(__x), sizeof(__x));	\
  else						\
    __x = __val;				\
  __x;						\
})

/*
  Used to determine a type of file that should be build. It can be either
  a 'Driver' or 'Simulator' or 'common' for Driver and Simulator or 'exact'
  i.e. file with exactly given name. Also first two definitions are used
  to distinguish between Driver and Driver Simulator.
*/
typedef enum _tagFILETYPE {
  DRIVER_FT = (1<<0),	/*  */
  SIM_FT    = (1<<1),	/*  */
  COMMON_FT = (1<<2),	/*  */
  EXACT_FT  = (1<<3)	/*  */
} FILETYPE;

#define SIZE_LEN       8   /*  */
#define MODE_LEN       4   /*  */
#define COMMENT_LEN    512 /*  */

/** @defgroup DEFS Important definitions to build-up the template files
 *
 *  If you are making any changes in them, it will influence all major
 *  Libraries that are used for device accessing, major structure sizes
 *  (such as <B>DevInfo_t</B>) and so on. So if you want to modify something
 *  here than you should keep on the consistency track (for example check
 *  <B>DevInfo_t</B> structure definition and how it's build-up)
 * @{
 */
/** Max string length */
#define MAX_STR	       256

/** Min string length */
#define MIN_STR	       8

/** Max name length */
#define NAME_LEN       32

/** Max register amount that module can have (including service registers).
    So the allowable register amount is (MAX_REG - <I>Service Register
    Amount</I>) */
#define MAX_REG	       256 /* 2010-01-20 (HBr): was 128 */

/** Max block amount that module can have */
#define MAX_BLK        64

/** Indicate address space absence */
#define NO_ADDRESS     0xFFFFFFFFUL

/** Denotes that Logical Unit Number is not defined */
#define NO_LUN	       0xfff00001

/** @} */


/* register access rights */
typedef enum _tagARIGHTS {
  AMRD  = 2, /* register can be read */
  AMWR  = 1, /* register can be write */
  AMEX  = 4  /* extraneous register (can be read and write) */
} ARIGHTS;

/* Standard DataBase sizes of register element */
typedef enum _tagREGSIZE {
	SZCHAR  = 1,	/* char  */
	SZSHORT = 2,	/* short */
	SZLONG  = 4,	/* long  */
	SZ64    = 8,	/* long long */
} REGSIZE;

/**
 * @enum reg_t
 * @brief possible register types
 */
typedef enum _tagREGTYPE {
  RT_REAL = 1,	/**<- real register */
  RT_EXTR = 2,	/**<- extraneous register */
  RT_SRVS = 4	/**<- service register */
} reg_t;

/**
 * @struct _tagSRVIOCTL
 *
 * @brief Describes service registers that grant some service
 *        operations. Enables user to control driver behavior.
 */
typedef struct _tagSRVIOCTL {
	char    *name;    /**< register name */
	char    *comment; /**< short description */
	char    *type;    /**< Valid register types are - char, short, long */
	REGSIZE regSize;  /**< register size in bytes */
	ARIGHTS rar;      /**< register access rights */
	int     depth;    /**< A register depth.
			     I.e. number of register elements */
} SRVIOCTL;



/**
 * @brief block description
 *
 * @param blockID     -- block sequence number, starting from 0
 * @param blkBaseAddr -- for INIT and 2 for NEXT base address
 * @param offset      -- block offset from the base address
 * @param blksz_drvr  -- driver block size in bytes.
 *                       Sizes may differ for driver/simulator,
 *                       because gaps are not taken into account
 *                       for simulator. For real driver they do
 * @param blksz_sim   -- simulator block size in bytes
 * @param reg_am      -- register amount in the block
 */
typedef struct _tagBlockDefT {
	short blockID;
	short blkBaseAddr;
	long  offset;
	int   blksz_drvr;
	int   blksz_sim;
	int   reg_am;
} BlockDef_t;


/**
 * @brief description of each module register
 *
 * @param blockP    -- Block to which register belongs to
 * @param regSize   -- register size in bytes
 * @param rar       -- Register Access Rights is the numerical
 *		       equivalent of mode:
 *		       @b w (write) is |= 1 @b r (read) is |= 2
 *		       @b e (extraneous) is |= 4 (set bits)
 * @param offset    -- The register's offset from the start of it's block
 * @param depth     -- A register depth of @b -1 represents a FIFO,
 *		       @b 0 a scalar register, any other positive
 *		       value is the number of elements in array
 * @param timeLoop  -- This value is used to specify a timing loop
 *		       for register accessing.  A value of 0 disables
 *		       the timing loop
 * @param size      -- Valid register sizes are @b char, @b short and @b long
 * @param mode      -- The register's access mode. (rwce)
 *		       @b r - read, @b w - write, @b c - check,
 *		       @b e - extraneous
 * @param name      -- The register's name
 * @param upperName -- The register's name in upper case
 * @param comment   -- short register description
 * @param last      -- last register in the list
 */
typedef struct _tagRegisterDefT {
	BlockDef_t *blockP;
	REGSIZE     regSize;
	ARIGHTS     rar;
	int         offset;
	int         depth;
	int         timeLoop;
	char        size[SIZE_LEN];
	char        mode[MODE_LEN];
	char        name[NAME_LEN];
	char        upperName[NAME_LEN];
	char        comment[COMMENT_LEN];
	int         last;
} RegisterDef_t;


/**
 * @struct _tagPciInfoT
 *
 * @brief Holds DbRT information about PCI module
 */
typedef struct _tagPciInfoT {
  long vendorId; /**< vendor Id */
  long deviceId; /**< device Id */
} PciInfo_t;


/**
 * @struct _tagVmeAddrInfoT
 * @brief  VME address space information
 */
typedef struct _tagVmeAddrInfoT {
  long         baseAddr;	/**< base address */
  unsigned int range;	        /**< address range */
  long         increment;	/**< address increment */
  long         dpSize;		/**< dataport size 16 (short) or 32 (long) */
  vmeam_t      addressModifier;	/**< address modifier (SH, ST, EX, CR) */
} VmeAddrInfo_t;


/**
 * @struct _tagVmeInfoT
 *
 * @brief Holds DbRT information about VME module
 */
typedef struct _tagVmeInfoT {
  VmeAddrInfo_t addr1;	   /**< 1'st addr space information table */
  VmeAddrInfo_t addr2;	   /**< 2'nd addr space information table */
  int		mtn;	   /**< module type number */
  int	        chCntr;	   /**< number of data channels (minor devices amount) */
  long		irq;	   /**< interrupt processing hardware level */
  long		vector;	   /**< interrupt vector */
  long		vectorInc; /**< interrupt vector increment */
  int	    CheckRegister; /**< regster to perform a read test. '-1' if none */
} VmeInfo_t;


/**
 * @enum dgopt
 *
 * @brief @e driverGen mutually exclusive options
 */
typedef enum _tagDgopt {
  OPT_G_IT      = (1<<0), /* -git     */
  OPT_G_ALL     = (1<<1), /* -gall    */
  OPT_G_INFO    = (1<<2), /* -ginfo   */
  OPT_G_SHARED  = (1<<3), /* -general */
  OPT_G_SERVICE = (1<<4), /* -srv     */
  OPT_COLLIDE   = (1<<5)  /* options collide with each other */
} dgopt;


/* Directory, where file will be generated */
typedef enum _tagLocation {
  LOC_MASTER,   /* master directory, where all other directories are located */
  LOC_GENERAL,  /* directory with source code, that is shared by all drivers */
  LOC_MODSRC,   /* directory with Driver/Simulator source code of a module */
  LOC_DRVR,     /* local dir for driver and install programs */
  LOC_FCLTY,    /* local dir for test program */
  LOC_INCL,     /* local dir for include files */
  LOC_LIB,      /* local dir for API libraries */
  LOC_INST,	/* local dir for installation entry */
  LOC_EXPLICIT, /* explicitly set driver directory and file name */
  LOC_LAST      /* denotes last one */
} location;


/* Next 9 definitions are predefined directory names that will be used during
   generation of driver source code */
#define DEFAULT_MASTER_DIR "genDriverDeposit" /**< Main Directory for newly
						 generatred Driver Source Code.
						 All newly generated code will be
						 located in this directory. */
#define GENERAL_DIR     "general" /**< Subword in a directory name, where source
				     code that is shared between all drivers,
				     is located. Differ for VME/PCI buses. */
#define LOCAL_DRVR_DIR  "driver" /**< Directory where driver, simulator and
				    install program will be generated */
#define LOCAL_FCLTY_DIR "test" /**< Directory in which test program will be
				  generated */
#define LOCAL_INCL_DIR  "include" /**< All '*.h' files that user will need
				     will be placed here */
#define LOCAL_LIB_DIR   "lib" /**< Directory for user API libraries, that will
				 be generated */
#define LOCAL_INST_DIR  "install" /**< user install entry point */
#define DG_INC_DIR      "dg"	/**< general include directory. Header files,
				   common to all the drivers, test programs and
				   libraries goes here */
#define DAL_DIR         "dal"	/**< DAL goes here */
#define INST_RM_DIR     "ins_rm_mod" /**< modinst/moduninst goes here */


/* here comes definitions for so-called 'user-defined' files */
#define USER_SUBWORD_LEXEME "UserDefined" /* This lexeme is a part of a
					     filename which will be generated
					     for the user entry points in the
					     driver code. */
#define LOCAL_USER_DIR "user" /* Directory(es) will hold files, intended
				 for user editing. In this dir, user can
				 add his own files. One can consider it as
				 a user entry point directory. */

/* general utility names */
#define UTIL_CP_USR_PT_GLOB "cpUsrPt"
#define UTIL_CP_USR_PT_LOC UTIL_CP_USR_PT_GLOB"_local"

/* path to the DriverGen src directory (as of 12.09.2006) */
#define DG_DIR_PATH "/acc/src/dsc/drivers/DriverGen"

/* driverGen.c */
extern bool verboseMode;   /* denotes verbose driver generation */
extern time_t mod_creation_time; /* when currently processed module
				    driver was created */

/* description of the module to generate framework for */
struct mdescr {
        uint8_t isgit; /* user request git control */
        char *mdn; /* master directory name */
};

/* commonGeneration.c */
void  BuildDescsOLD(int);
FILE* OpenFile(FILETYPE, location, char *, ...);
rstat MakeSafeDir(char *);
void  BuildIoctlConsts(RegisterDef_t*, int, FILE*);
void  BuildCommonBlocks(int, RegisterDef_t*, int, FILE*);
void  BuildIoctlLibrary(RegisterDef_t*, int, BlockDef_t*, int);
void  BuildTestProgram(RegisterDef_t*, int, BlockDef_t*, int);
void  BuildMakefiles();
char* get_mid(struct mdescr *);
char* get_msd(struct mdescr *);
void  BuildDrvrSimIoctl(RegisterDef_t*, int, FILE*);
void  BuildGetSetRegCH(RegisterDef_t*, int, VmeInfo_t*);
void  BuildGeneralCode();
void  BuildUserPartDrvrCode();
char* GenerateFilename(FILETYPE, char*, char*, char*, char*);

/* dbrtAccess.c */
int   GetRegisterConfig(char*, RegisterDef_t*, BlockDef_t*, int, char*);
int   GetBlockConfig(char*, BlockDef_t*, char*);
rstat GetPciInfo(char*, PciInfo_t*);
rstat GetVmeInfo(char*, VmeInfo_t*);

/* vmeGeneration.c */
void GenerateVmeDriver(RegisterDef_t*, int, BlockDef_t*, int, VmeInfo_t*);
void GenerateVmeCore(RegisterDef_t*, int, BlockDef_t*, int, VmeInfo_t*);
void GenerateVmeInfoFile(RegisterDef_t*, int, BlockDef_t*, int, VmeInfo_t*);
void GenerateVmeInstallFilesOLD(VmeInfo_t*, BlockDef_t*, int);

/* drmGeneration.c */
void GenerateDrmDriver(RegisterDef_t*, int, BlockDef_t*, int, PciInfo_t*);
void GenerateDrmCore(RegisterDef_t*, int, BlockDef_t*, int, PciInfo_t*);
void GenerateDrmInstallFilesOLD(PciInfo_t*, BlockDef_t*, int);

/* rwops.c */
extern SRVIOCTL srv_ioctl[];	/* all 'Service' registers */
int     PackIoctlNum(int*, ARIGHTS, bool);
void    BuildRegIdEnum(BlockDef_t*, RegisterDef_t*, int);
int     GetSrvRegNum();
char*   rar2mode(ARIGHTS);
ARIGHTS mode2rar(char*);

#endif /* _DRIVERGEN_H_INCLUDE_ */
