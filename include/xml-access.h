/**
 * @file xml-access.h
 *
 * @brief driver-gen xml library.
 *
 * Various house-keeping routines to handle xml module description
 * declared here.
 *
 * @author Copyright (C) 2010 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 25/02/2010
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifndef _XML_ACCESS_H_INCLUDE_
#define _XML_ACCESS_H_INCLUDE_

int dgxml_cerndb_info2xml(char *);

#endif	/* _XML_ACCESS_H_INCLUDE_ */
