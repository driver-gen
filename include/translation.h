/**
 * @file translation.h
 *
 * @brief Template translation functions and their description
 *
 * @author Copyright (C) 2004 - 2010 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifndef _TRANSLATION_H_INCLUDE_
#define _TRANSLATION_H_INCLUDE_

void TranslationInit(char*, char*);
void TranslationReset();
void Translate(FILE*, char*, char*, ...);

/** @defgroup TTR Template functions used for driver generation
 * @{
 */

/* <sys_lower> */
char* TranslationGetSysLower(); /* Naming Convention is following:
				   [SHARED_CIBC --> SharedCibc]
				   [BNLDSP      --> Bnldsp] */

/* <mod_name> */
char* TranslationGetModName();	/* module name _exactly_ as in the dataBase */

/* <bus_upper> */
char* TranslationGetBus();	/* VME or PCI */

/* <int_num> */
char* TranslationGetIntNum();
void  TranslationSetIntNum(int);              /* '%d'       format */

/* <plain_num> */
char* TranslationGetPlainNum();
void  TranslationSetPlainNum(int);            /* '%d'       format */

/* <hex_num> */
char* TranslationGetHexNum();
void  TranslationSetHexNum(int);              /* '0x%x'     format */

/* <fancy_num> */
char* TranslationGetFancyNum();
void  TranslationSetFancyNum(int);            /* '%02d'     format */

/* <extra_num> */
char* TranslationGetExtraNum();
void  TranslationSetExtraNum(int);            /* '0x%X'     format */

/* <dec_num> */
char* TranslationGetDecNum();
void  TranslationSetDecNum(int);              /* '%d'       format */

/* <ddd_num> */
char* TranslationGetDDDNum();
void  TranslationSetDDDNum(int);              /* '%d'       format */

/* <reg_loop> */
char* TranslationGetRegLoop();
void  TranslationSetRegLoop(int);             /* '%d'       format */

/* <reg_id> */
char* TranslationGetRegId();
void  TranslationSetRegId(char*);             /* '%s_ID'    format */

/* <reg_depth> */
char* TranslationGetRegDepth();
void  TranslationSetRegDepth(int);            /* '%d'       format */

/* <reg_name> */
char* TranslationGetRegName();
void  TranslationSetRegName(char*);           /* 'string'   format */

/* <reg_comment> */
char* TranslationGetRegComm();
void  TranslationSetRegComm(char*);           /* 'string'   format */

/* <reg_type> */
char* TranslationGetRegType();
void  TranslationSetRegType(char*);           /* 'string'   format */

/* <dg_reg_sz> */
char* TranslationGetRegSize();
void  TranslationSetRegSize(int);             /* '%d'   format */

/* <ioctl_const> */
char* TranslationGetIoctlConst();
void  TranslationSetIoctlConst(char*, char*); /* '%s_%s_%s' format for
						 normal module register
						 or
						 '%s%s' format for service
						 register */

/* <precision> */
char* TranslationGetPrecision();
void  TranslationSetPrecision(int);           /* '%d'       format */

/* <base_addr> */
char* TranslationGetBaseAddr();
void  TranslationSetBaseAddr(char*);          /* 'string'   format */

/* <fancy_str> */
char* TranslationGetFancyString();
void  TranslationSetFancyString(char*);	      /* 'string'   format */

/* <dg_dummy_str> */
char* TranslationGetDummyString();
void  TranslationSetDummyString(char*);	      /* 'string'   format */

/* <drvr_type> */
char* TranslationGetDriverType();
void  TranslationSetDriverType(char*); /* normally "Driver" or "Simulator"
					  But can be used as 'string' format */

/* <dg_comment> */
char* TranslationGetComment();
void  TranslationSetComment(char*);	      /* 'string'   format */

/* <dg_free_fmt> */
char* TranslationGetFreeFormat();
void  TranslationSetFreeFormat(char*, ...);   /* free format */

/* <dg_char> */
char* TranslationGetChar();
void  TranslationSetChar(char);   /* '%c' format */

/* <dg_string> */
char* TranslationGetString();
void  TranslationSetString(char*);	      /* 'string'   format */


/*@}*/
#endif /* _TRANSLATION_H_INCLUDE_ */
