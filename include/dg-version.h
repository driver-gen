/**
 * @file dg-version.h
 *
 * @brief Current driverGen stable version
 *
 * @author Copyright (C) 2003 - 2010 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 02/10/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifndef _DRIVER_GEN_VERSION_H_INCLUDE_
#define _DRIVER_GEN_VERSION_H_INCLUDE_

/* current driverGen release tag (set automatically during Make) */
static const char dg_version[] = "";

#endif	/* _DRIVER_GEN_VERSION_H_INCLUDE_ */
