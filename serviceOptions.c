/**
 * @file serviceOptions.c
 *
 * @brief
 *
 * @author Copyright (C) 2004 - 2010 CERN. Georgievskiy Yury <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */
#include "serviceOptions.h"
#include "driverGen.h"

static srvopt_c srv_opt_c;	/* container that holds command line args */

/**
 * @brief Arguments amount and their forman supposed to be correct! No
 *        checking is performed.
 *
 * @param cur_arg -
 * @param ...     -
 *
 * @return 0 - all OK
 * @return 1 - error occurs.
 */
int SetSrvOptArgs(srvarg cur_arg, ...)
{
	va_list ap;

	va_start(ap, cur_arg);

	switch (cur_arg) {
	case SRV_VF_DRVR:
	case SRV_VF_SIM:
		vsnprintf(srv_opt_c.srv_vf_dir, sizeof(srv_opt_c.srv_vf_dir),
			  "%s", ap);
		break;
	default:
		break;
	}

	va_end(ap);

	return 0; /* OK */
}

/**
 * @brief All service driverGen options are processed here.
 *
 * @param cur_arg --
 * @param pn      -- prog name
 *
 * To call service option you should call dgII with -srv option:
 * dgII -srv [args]
 *
 * Supported service options are:
 * 1. Creating of the version file within already existing driver directory.
 *    Arguments are:
 *    a. drvrvers/simvers
 *    b. driver directory, where to create versioin files.
 *
 * @return 0              - all OK
 * @return negative value - error occurs.
 */
int ProcessSrvOpt(srvarg cur_arg, char *pn)
{
	struct stat status;
	FILE *file;
	char file_path[128];
	char *file_name;
	FILETYPE cur_ft;	/* current file type */
	rstat cc;		/* driverGen error codes */

	switch (cur_arg) {
	case SRV_VF_DRVR:
	case SRV_VF_SIM:
		/* version files will be created only if appropriate destanation
		   directory already exist and version file is not yet created.
		   Existing version files will not be corrupted in this case.

		   Parameters for this service command are:
		   1. Module name
		   2. Bus type
		   3. Driver directory  */
		if (cur_arg == SRV_VF_DRVR) {
			cur_ft = DRIVER_FT;
			TranslationSetDriverType("DRVR");
			TranslationSetFancyString("Driver");
		} else {
			cur_ft = SIM_FT;
			TranslationSetDriverType("SIM");
			TranslationSetFancyString("Simulator");
		}

		/* first, check if driver directory exist */
		snprintf(file_path, sizeof(file_path), "%s/%s", get_mid(NULL),
			 srv_opt_c.srv_vf_dir);
		if (stat(file_path, &status)) {
			fprintf(stderr, "[%s] %s: Module driver directory"
				" doesn't exist!\n", pn, file_path);
			return -1; /* fail */
		}

		/* second, check if expected user directory is in the place */
		snprintf(file_path, sizeof(file_path), "%s/%s/%s/%s",
			 get_mid(NULL), srv_opt_c.srv_vf_dir, LOCAL_INCL_DIR,
			 LOCAL_USER_DIR);
		cc = MakeSafeDir(file_path);
		if (cc < 0)
			return cc;

		/* now check if version file is already exist */
		GenerateFilename(cur_ft, file_path, "Version", ".h", file_path);
		file_name = rindex(file_path, '/');
		++file_name;
		if (!stat(file_path, &status)) {
			fprintf(stderr, "%s Version file (%s) already exist!\n",
				(cur_arg == SRV_VF_DRVR) ? "Driver" :
				"Simulator", file_name);
			return -1; /* error */
		}

		/* ok - we are cool. Proceed... */
		file = fopen(file_path, "w");	/* create version file */
		Translate(file, "common", "driver/userPart/uepVers.h");
		fclose(file);
		break;
	default:
		break;
	}

	return 0; /* success */
}
