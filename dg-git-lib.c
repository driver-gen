/**
 * @file dg-git-lib.c
 *
 * @brief driver-gen git API.
 *
 * Driver-gen can set the driver under the git control.
 * This file contains all the utils to work with git.
 *
 * @author Copyright (C) 2010 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 01/02/2010
 *
 * @section license_sec License
 *          Released under the GPL
 */
#define _GNU_SOURCE /* asprintf rocks */
#include <sys/types.h>
#include <sys/dir.h> /* for dirent, opendir */
#include "dg-git-lib.h"

/**
 * @brief Checks if driver is already controlled by git
 *
 * @param md -- module description
 *
 * @return 0 -- no yet supervised
 * @return 1 -- already supervised
 */
static uint8_t git_already_supervised(struct mdescr *md)
{
	int rc = 1;
	char *dname = NULL;
	DIR *dir = NULL;

	asprintf(&dname, "%s%s", get_msd(NULL), "/.git");
	dir = opendir(dname);
	if (!dir)
		rc = 0;

	if (dname) free(dname);
	if (dir) closedir(dir);
	return rc;
}

/**
 * @brief Initialize empty git repository
 *
 * @param md -- module description
 *
 */
static int git_init(void)
{
	char *str;
	char cwd[1024];
	FILE *gifd = OpenFile(EXACT_FT, LOC_MODSRC, ".gitignore");

	asprintf(&str, "%s%s%s", "git-commit -sm\"", TranslationGetModName(),
		 " driver framework initial release\"");
	Translate(gifd, "common", ".gitignore");
	fclose(gifd);

	getcwd(cwd, sizeof(cwd)); /* save cwd */
	chdir(get_msd(NULL));
	system("git-init");
	system("git-add .");
	system(str);
	system("git-tag -a dg2-v001 -m\"Automatically generated Version#1\"");
	chdir(cwd);
	free(str);

	return 0;
}

/**
 * @brief Check, if driver framework repo is in the consistent state.
 *
 * @param md  -- module description
 * @param fnp -- untracked filenames goes here
 *
 * Consisten state means that there are no modified files, that are not
 * yet commited. Untracked files are allowed.
 *
 * @return  number of untracted files.
 * @return -1 -- non-constistent
 */
static int git_check_consistency(struct mdescr *md, char ***fnp)
{
	char cwd[1024];
	char status[4], fname[512];
	FILE *f;
	static char *fn[512] = { NULL };
	int i = 0;

	getcwd(cwd, sizeof(cwd)); /* save cwd */
	chdir(get_msd(NULL));

	f = popen("git-status -s 2>/dev/null", "r");
	while (fscanf(f, "%s %s\n", status, fname) != EOF) {
		if (strncmp(status, "??", 2)) {
			while (i > 0)
				free(fn[--i]);
			*fn = NULL;
			i = -1;
			break;
		}
		asprintf(&fn[i++], "%s", fname);
	}

	chdir(cwd); /* back where we start from */
	*fnp = fn;
	return i;

}

/**
 * @brief
 *
 * @param  --
 *
 * <long-description>
 *
 * @return <ReturnValue>
 */
int git_get_driver_version(void)
{
	char cwd[1024];
	FILE *f;
	uint v = 0;
	int rc;

	getcwd(cwd, sizeof(cwd)); /* save cwd */
	chdir(get_msd(NULL));

	f = popen("git-tag 2>/dev/null", "r");
	while ( (rc = fscanf(f, "dg2-v%d\n", &v)) != EOF) {
		if (!rc) { /* skip matching failure */
                        rc = fscanf(f, "%*s");
                        continue;
                }
	}
	chdir(cwd); /* back where we start from */
	return v;

}

/**
 * @brief
 *
 * @param  --
 *
 * <long-description>
 *
 */
void git_reset_user_files(void)
{
	char cwd[1024];
	char status[4], fname[512];
	FILE *f;
	char str[512];

	getcwd(cwd, sizeof(cwd)); /* save cwd */
	chdir(get_msd(NULL));

	f = popen("git-status -s 2>/dev/null", "r");
	while (fscanf(f, "%s %s\n", status, fname) != EOF) {
		if (!strncmp(status, "M", 1) && strstr(fname, "UserDefined")) {
			snprintf(str, sizeof(str), "git-checkout -- %s", fname);
			system(str);
		}
	}

	chdir(cwd); /* back where we start from */
}

/**
 * @brief Checks git version. Should be > 1.5.3
 *
 * If git was not found, or if it was,
 * but its version is < 1.5.3 -- will return an error.
 *
 * @return  0 -- git version is OK
 * @return -1 -- git was not found, or version is < 1.5.3
 */
uint8_t git_check_version(void)
{
	uint8_t rc = 0;
	uint32_t v[4] = { 0 };
	char str[2][16];
	FILE *f = popen("git --version 2>/dev/null", "r");

	fscanf(f, "%s %s %d.%d.%d.%d\n", str[0], str[1], &v[0], &v[1], &v[2], &v[3]);
	if (!v[0]) {
		printf("git not found. Sorry...\n");
		rc = -1;
	} else if (v[0]*100 + v[1]*10 + v[2] < 153) {
		printf("git version is smaller than needed (%d < %d). Sorry...\n",
		       v[0]*100 + v[1]*10 + v[2], 153);
		rc = -1;
	} else {
		printf("git version (%d) is OK...\n", v[0]*100 + v[1]*10 + v[2]);
	}

	pclose(f);
	return rc;
}

/**
 * @brief Update Driver/Simulator Version files.
 *
 * Using perl to bump the version +1.
 *
 */
static void git_upgrade_version_files(void)
{
	char *name, *cmd;

	/* simulator version */
	name = GenerateFilename(SIM_FT, "include", "Version", ".h", NULL);
	asprintf(&cmd, "perl -i -ne 'if (/(.*CURRENT_VERSION) (\\d+)/)"
		 " { $a=$2+1; print \"$1 $a\n\"; } else { print; }' %s", name);
	system(cmd);
	free(cmd);

	/* driver version */
	name = GenerateFilename(DRIVER_FT, "include", "Version", ".h", NULL);
	asprintf(&cmd, "perl -i -ne 'if (/(.*CURRENT_VERSION) (\\d+)/)"
		 " { $a=$2+1; print \"$1 $a\n\"; } else { print; }' %s", name);
	system(cmd);
	free(cmd);
}

/**
 * @brief Create new driver or commit new changes in existing repository.
 *
 * @param dn           -- driver name
 * @param md           -- module description
 * @param registers    --
 * @param numRegisters --
 * @param blocks       --
 * @param numBlocks    --
 * @param vmeInfo      --
 *
 * If driver framework already exists -- commit new changes in existing
 * repository. Commit sequence rules are:
 * 1. Check if repo is in consistent state, i.e. no modified
 *    files. Untracked files are allowed.
 * 2. Get current version from git-tag.
 *    * Versions *should* be in a simple predefined format:
 *      dg2-vXXX -- where XXX is the number, that will be bumped
 *      automatically.
 *      dg2-vXXX -- means that this driver was generated
 *      by the Driver Gen2, and that automatically generated Version
 *      of this driver is XXX).
 *    * +1 version.
 *    * If [dg2-vXXX] not found -- will be set to [dg2-v001]
 *
 * 3. Record all untracked files/dirs (so that they will not
 *    be commited)
 * 4. Switch to master branch (save previous location).
 * 5. Apply new templates.
 * 6. git-add them into staging area.
 * 7. unstage untracked files (if any).
 * 8. Commit changes into master branch.
 * 9. Switch back to original branch.
 *
 * @return <ReturnValue>
 */
uint8_t do_git_vme_driver(char *dn, struct mdescr *md,
			  RegisterDef_t *registers, int numRegisters,
			  BlockDef_t *blocks, int numBlocks,
			  VmeInfo_t *vmeInfo)
{
	char **fn; /* saved untracked filenames */
	char cwd[1024], str[1024];
	uint8_t already = git_already_supervised(md);
	int rc, i;

	if (already) { /* already under git */
		rc = git_check_consistency(md, &fn);
		if (rc == -1) {
			printf("%s git directory is NOT in a consistent state!\n"
			       "Clean it up first, then retry once more.\n", get_msd(NULL));
			return -1;
		}
		GenerateVmeDriver(registers, numRegisters, blocks,
				  numBlocks, vmeInfo);

		getcwd(cwd, sizeof(cwd)); /* save cwd */
		chdir(get_msd(NULL));
		system("git-checkout master");
		git_reset_user_files();
		git_upgrade_version_files();

		system("git-add -A");
		for (i = 0; i < rc; i++) {
			/* reset untracked files */
			snprintf(str, sizeof(str), "git-reset %s", fn[i]);
			system(str);
			free(fn[i]);
		}
		i = git_get_driver_version() + 1;

		snprintf(str, sizeof(str),
			 "git-commit -sm\"New automatically generated Version#%d\"", i);
		system(str);

		snprintf(str, sizeof(str),
			 "git-tag -a dg2-v%03d -m\"New automatically generated Version#%d\"",
			 i, i);

		system(str);
		chdir(cwd); /* go back where we start from */
		return 0;
	} else { /* this is new driver framework */
		printf("Creating   %s source code directory.\n", dn);
		GenerateVmeDriver(registers, numRegisters, blocks,
				  numBlocks, vmeInfo);
		return git_init();

	}
}

uint8_t do_git_pci_driver(char *dn, struct mdescr *md,
			  RegisterDef_t *registers, int numRegisters,
			  BlockDef_t *blocks, int numBlocks,
			  PciInfo_t *pciInfo)
{
	return -1;
}
