/**
 * @file dbrtAccess.c
 *
 * @brief DataBase access to get hw module description is here.
 *
 * @author Copyright (C) 2002 CERN. Stuart Baird
 * @author Copyright (C) 2003 - 2010 CERN. Georgievskiy Yury <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */

#ifdef ACCESS_DB

#include <stdio.h>
#include <dbrt/dbrt.h>
#include "driverGen.h"
#include "utilities.h"

static void PrintOutVmeInfo(VmeInfo_t *);
static void dbrt_error_printout();

/**
 * @brief Get register description from the DataBase.
 *
 * @param moduleName -- DataBase Module Name
 * @param defs       -- register description to put
 * @param blks       -- block description
 * @param numBlks    -- number of blocks
 * @param prog       -- program name
 *
 * Registers are prepared for the further use. If MAX allowed register amount
 * reached - printout error message and exits.
 *
 * @return number of registers, defined in the DB.
 */
int DBGetRegisterConfig(char *moduleName, RegisterDef_t *defs,
		      BlockDef_t *blks, int numBlks, char *prog)
{
	int seqno = 0;
	int idx = 0;		/* register counter */
	int maxReg;		/*  */
	int cntr;		/*  */
	DbrtRegData *record;	/*  */

	maxReg = MAX_REG - GetSrvRegNum(); /* how many reg descr we can have */

	/* pass through all register records */
	while ((record = DbrtBiscotoRD(moduleName, ++seqno)) != NULL) {
		if (idx < maxReg) {
			/* setup pointer to block information that
			   register belongs to */
			for (cntr = 0; cntr < numBlks; cntr++) {
				if (blks[cntr].blockID == record->blockno) {
					defs[idx].blockP = &blks[cntr];
					break;
				}
			}

			defs[idx].timeLoop = record->timeloop;
			defs[idx].offset = record->regoffset;
			defs[idx].depth = record->regdepth;

			snprintf(defs[idx].size, SIZE_LEN, "%s",
				 record->wordsize);
			snprintf(defs[idx].mode, MODE_LEN, "%s",
				 record->rwmode);
			snprintf(defs[idx].name, NAME_LEN, "%s",
				 record->regname);
			snprintf(defs[idx].comment, COMMENT_LEN, "%s",
				 record->description);
		}
		idx++;		/* increase register counter */
	}

	dbrt_error_printout(__FUNCTION__);

	if (idx >= maxReg) {	/* check if within range */
		fprintf(stderr,
			"%sFATAL%s %s Module %s has too many registers.\n",
			RED_CLR, END_CLR, ERROR_MSG, moduleName);
		fprintf(stderr, "\t'%s' supports MAX %d registers (including"
			" service registers).\n\tYour module has %d register"
			" definitions (plus %d service registers)\n\t'MAX_REG'"
			" should be changed.\n\tPlease report this problem to"
			" the DriverGen support!\n",
			prog, MAX_REG, idx, GetSrvRegNum());
		exit(EXIT_FAILURE);	/* 1 */
	}

	defs[idx-1].last = 1;	/* mark last register */
	return idx;
}

/**
 * @brief Get block configuration from the DataBase.
 *
 * @param moduleName -- DataBase Module Name
 * @param defs       -- results will go here
 * @param prog       -- program name
 *
 * prepare results for the further use. If MAX allowed block amount
 * reached - printout error message and exits.
 *
 * @return number of defined blocks
 */
int DBGetBlockConfig(char *moduleName, BlockDef_t * defs, char *prog)
{
	int currentBlock = -1;
	int seqno = 0;
	int idx = 0;		/* block counter */
	DbrtRegData *record;

	/* pass through all register records */
	while ((record = DbrtBiscotoRD(moduleName, ++seqno)) != NULL) {
		if (currentBlock != record->blockno) {
			currentBlock = record->blockno;
			if (idx < MAX_BLK) {
				defs[idx].blockID = (short)record->blockno;
				defs[idx].blkBaseAddr = (short)record->address;
				defs[idx].offset = (long)record->blockoffset;
			}
			idx++;	/* increase block counter */
		}
	}

	dbrt_error_printout(__FUNCTION__);

	if (idx >= MAX_BLK) {	/* check if within range */
		fprintf(stderr, "%sFATAL%s %s Module %s has too many blocks.\n",
			RED_CLR, END_CLR, ERROR_MSG, moduleName);
		fprintf(stderr, "\t'%s' supports MAX %d blocks.\n\tYour module"
			" has %d block definitions\n\t'MAX_BLK' should be"
			" changed.\n\tPlease report this problem to the"
			" DriverGen support!\n", prog, MAX_BLK, idx);
		exit(EXIT_FAILURE);	/* 1 */
	}

	return idx;
}

/**
 * @brief Get PCI board information from the database.
 *
 * @param moduleName -- DataBase Module Name
 * @param pciInfo    -- results will go here
 *
 * @return DRIVER_GEN_OK  - if PCI board info is obtained.
 * @return DRIVER_GEN_BAD - othervise.
 */
rstat DBGetPciInfo(char *moduleName, PciInfo_t * pciInfo)
{
	rstat rtnStatus = DRIVER_GEN_OK;
	DbrtModTypData *record = NULL;

	record = DbrtModulType(moduleName);

	if (record != NULL) {
		sscanf(record->vendorid, "%lx", &(pciInfo->vendorId));
		sscanf(record->deviceid, "%lx", &(pciInfo->deviceId));

		pciInfo->vendorId = ASSERT_MSB(pciInfo->vendorId);
		pciInfo->deviceId = ASSERT_MSB(pciInfo->deviceId);
	} else {
		switch (DbrtError) {
		case DBRT_NOTOPEN:
		case DBRT_BADFORMAT:
		case DBRT_BADKEYS:
			fprintf(stderr, "Failed to read pci config data.\n");
			fprintf(stderr,
				"DbrtBiscotoRD failed with error %d: %s\n",
				DbrtError, DbrtErr(DbrtError));
			rtnStatus = DRIVER_GEN_BAD;
			break;

		case DBRT_NOTFOUND:
		case DBRT_DBOK:
		default:
			break;
		}
	}
	return rtnStatus;
}

/**
 * @brief Get VME board information from the database.
 *
 * @param moduleName -- DataBase Module Name
 * @param vmeInfo    -- results will go here
 *
 * @return DRIVER_GEN_OK  - if VME board info is obtained.
 * @return DRIVER_GEN_BAD - othervise.
 */
rstat DBGetVmeInfo(char *moduleName, VmeInfo_t *vmeInfo)
{
	rstat rtnStatus = DRIVER_GEN_OK;
	DbrtModTypData *record = NULL;

	record = DbrtModulType(moduleName);

	if (record != NULL) {	/* DB record hit */
		/* setup first address space */
		vmeInfo->addr1.baseAddr = (record->initbasaddr) ?
		    record->initbasaddr : NO_ADDRESS;
		vmeInfo->addr1.range = record->initrange;
		vmeInfo->addr1.increment = record->initincrement;
		vmeInfo->addr1.dpSize = record->initdsize;

		/*
		   Supported Addresses are:
		   => SHORT        - [A16D16]
		   => STANDART     - [A24D16]
		   => EXTENDED     - [A32D32]
		   => CONFIG SPACE - [A24D08O]
		 */
		if (!strcmp(record->initasize, "SH"))
			vmeInfo->addr1.addressModifier = DG_AM_SH;
		else if (!strcmp(record->initasize, "ST"))
			vmeInfo->addr1.addressModifier = DG_AM_ST;
		else if (!strcmp(record->initasize, "EX"))
			vmeInfo->addr1.addressModifier = DG_AM_EX;
		else if (!strcmp(record->initasize, "CR"))
			vmeInfo->addr1.addressModifier = DG_AM_CR;
		else if (!strcmp(record->initasize, "-")) ;	/* not defined */
		else {
			fprintf(stderr, "Unsupported AM (%s) detected for"
				" addr1. Check DB configuration!\n",
				record->initasize);
			return DRIVER_GEN_BAD;
		}

		/* setup second address space */
		vmeInfo->addr2.baseAddr = (record->nextbasaddr) ?
		    record->nextbasaddr : NO_ADDRESS;
		vmeInfo->addr2.range = record->nextrange;
		vmeInfo->addr2.increment = record->nextincrement;
		vmeInfo->addr2.dpSize = record->nextdsize;

		if (!strcmp(record->nextasize, "SH"))
			vmeInfo->addr2.addressModifier = DG_AM_SH;
		else if (!strcmp(record->nextasize, "ST"))
			vmeInfo->addr2.addressModifier = DG_AM_ST;
		else if (!strcmp(record->nextasize, "EX"))
			vmeInfo->addr2.addressModifier = DG_AM_EX;
		else if (!strcmp(record->nextasize, "CR"))
			vmeInfo->addr2.addressModifier = DG_AM_CR;
		else if (!strcmp(record->nextasize, "-")) ;	/* not defined */
		else {
			fprintf(stderr, "Unsupported AM (%s) detected for"
				" addr2. Check DB configuration!\n",
				record->nextasize);
			return DRIVER_GEN_BAD;
		}

		/* info, common to both address spaces */
		vmeInfo->mtn = record->typeno;
		vmeInfo->chCntr = record->channelcount;
		vmeInfo->irq = record->ilevel;
		vmeInfo->vector = record->ivector;
		vmeInfo->vectorInc = record->ivectorinc;

		if (verboseMode) /* verbose driverGen */
			PrintOutVmeInfo(vmeInfo);
	} else { /* DB record miss */
		switch (DbrtError) {
		case DBRT_NOTOPEN:
		case DBRT_BADFORMAT:
		case DBRT_BADKEYS:
			fprintf(stderr, "Failed to read VME config data.\n"
				"DbrtBiscotoRD failed with error %d: %s\n",
				DbrtError, DbrtErr(DbrtError));
			rtnStatus = DRIVER_GEN_BAD;
			break;
		case DBRT_NOTFOUND:
		case DBRT_DBOK:
		default:
			break;
		}
	}
	return rtnStatus;
}

/**
 * @brief Printout VME configuration info.
 *
 * @param vmeInfo -- data to print
 *
 * @return void
 */
static void PrintOutVmeInfo(VmeInfo_t * vmeInfo)
{
	int cntr;
	VmeAddrInfo_t *ptr;

	/* general info */
	printf("General information\n");
	printf("-------------------\n");
	printf("Module type number                  => %d\n", vmeInfo->mtn);
	printf("Number of data channels             => %s\n",
	       (vmeInfo->chCntr == -1) ? "NOT_DEFINED" : itoa(vmeInfo->chCntr,
							      10));
	printf("Interrupt processing hardware level => %s\n",
	       (vmeInfo->irq == -1) ? "NOT_DEFINED" : itoa(vmeInfo->irq, 10));
	printf("Interrupt vector                    => %s\n",
	       (vmeInfo->vector == -1) ? "NOT_DEFINED" : itoa(vmeInfo->vector,
							      10));
	printf("Interrupt vector increment          => %ld\n",
	       vmeInfo->vectorInc);
	printf("\n");

	/* address space specific information */
	printf("Address space information\n");
	printf("-------------------------\n");
	ptr = &(vmeInfo->addr1);
	for (cntr = 0; cntr < 2; cntr++, ptr++) {
		if (ptr->baseAddr == NO_ADDRESS) {
			printf("\nAddress space 'Addr%d' not defined.\n",
			       cntr + 1);
			continue;
		} else {
			printf("Address space 'Addr%d' info:\n", cntr + 1);
			printf("\tBase address      => 0x%lx\n", ptr->baseAddr);
			printf("\tAddress range     => 0x%x\n", ptr->range);
			printf("\tAddress increment => 0x%lx\n",
			       ptr->increment);
			printf("\tDataport size     => %ld\n", ptr->dpSize);
			printf("\tAddress modifier  => %s\n",
			       (ptr->addressModifier ==
				16) ? "SH - short" : (ptr->addressModifier ==
						      24) ? "ST - standard" :
			       "EX - extended");
		}
	}
}

/**
 * @brief Printout DBRT error.
 *
 * @param dbrtFN -- function name
 *
 * @return void
 */
static void dbrt_error_printout(char *dbrtFN)
{
	/* see '/ps/local/Linux/dbrt/dbrt.h' file for more info about possible
	   errors and their meaning */
	switch (DbrtError) {
	case DBRT_NOTOPEN:
	case DBRT_BADFORMAT:
	case DBRT_BADKEYS:
		fprintf(stderr, "%s() failed to read Module config data from"
			" the DB with error [%d] => '%s'\n",
			dbrtFN, DbrtError, DbrtErr(DbrtError));
		break;
	case DBRT_NOTFOUND:
	case DBRT_DBOK:
	default:
		break;
	}
}

#endif
