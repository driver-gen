/**
 * @file rwops.c
 *
 * @brief Service register description and some register operations are here.
 *
 * @author Copyright (C) 2002 CERN. Stuart Baird
 * @author Copyright (C) 2003 CERN. Alain Gagnaire
 * @author Copyright (C) 2003 - 2010 CERN. Georgievskiy Yury <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */
#include "driverGen.h"

/* describes all 'service' registers, provided by 'driverGen' */
SRVIOCTL srv_ioctl[] = {
	{
	 .name = SRVREGPREF "DEBUG_FLAG",
	 .comment = "Flag Register. Enable/Disable various driver flags",
	 .type = "long",
	 .regSize = SZLONG,
	 .rar = AMRD | AMWR,
	 .depth = 1}
	,
	{
	 .name = SRVREGPREF "DEVINFO_T",
	 .comment = "Full device information (info file)",
	 .type = "long",
	 .regSize = 0,		/* irrelevant, so set it to zero */
	 .rar = AMRD,
	 .depth = 1}
	,
	{
	 /* This register consists of the driver version string plus driver version
	    number. From the user point of view - he should pass two memory
	    locations - first that will hold version number, second one that will
	    hold version string. */
	 .name = SRVREGPREF "DRVR_VERS",
	 .comment =
	 "Driver version info (current version number and version info string)",
	 .type = "char",
	 .regSize = MAX_STR + 4,	/* version string + version number */
	 .rar = AMRD,
	 .depth = 1}
	,
	{
	 .name = SRVREGPREF "DAL_CONSISTENT",
	 .comment =
	 "driver/DAL consistency checking (comparision between the creation dates)",
	 .type = "long",
	 .regSize = SZLONG,
	 .rar = AMRD,
	 .depth = 1}
	,
	/* ---------[ NOTE ON SPECIAL SERVICE REGISTERS ]-------------------
	   If you want to add a SPECIAL register, you should set reg access
	   rights (rar member) to zero. Register depth (depth member) should
	   be greater than zero. Such register will have one ioctl number */
	{
	 /* This is a SPECIAL service register.
	    Handles repetitive register r/w access. Contains no data. */
	 .name = SRVREGPREF "REP_REG_RW",
	 .comment = "Special ioctl number for repetitive register r/w access",
	 .type = 0,
	 .regSize = 0,
	 .rar = 0,		/* indicates special register type.
				   No menu entry in the test program as this register
				   contains no data */
	 .depth = 1}
	,
	{
	 /* SPECIAL service register.
	    Enable/Disable/Query r/w bounds checking in ioctl calls.
	    Write 1 - to enable, 0 - to disable, 2 - to query current state */
	 .name = SRVREGPREF "RW_BOUNDS",
	 .comment = "Special ioctl number to enable/disable r/w bounds"
	 " checking during ioctl call",
	 .type = 0,
	 .regSize = 0,
	 .rar = 0,		/* indicates special register type.
				   No menu entry in the test program as this register
				   contains no data */
	 .depth = 1}
	,
	/* Add any new description before the last element!
	   !NOTE!
	   All service register names should start with 'SRVREGPREF'
	   If you add any new service register, than you should modify
	   srvRegOps.c, testMainBegin.c and ioctl/head.c template files for
	   payload implementation of the newly defined service registers.
	 */
	{
	 .name = "LAST_SRV_REG",
	 .comment = "first user-available register ID number",
	 .type = 0,
	 .regSize = 0,
	 .rar = 0,
	 .depth = 0		/* indicates the last service reg element */
	 }
};

/**
 * @brief Pack Ioctl number
 *
 * @param opNum - number to pack. (see more in detailed description)
 * @param a_r   - register access rights
 * @param isSrv - denotes register type (service or normal)
 *
 * First parameter should be zero for the first invocation of this function.
 * This number will be properly change, so that next time user should pass the
 * previously returned number. Pay attention to the fact that if you invoke
 * this function once, then next time as a first parameter you should pass
 * the number that was returned by the previous call. If not follow this
 * rule - then discrepancy between defined ioctl numbers and driver ioctl
 * numbers will	occur.
 * Two MSB are for 'r' operation number.
 * Two LSB are for 'w' operation number.
 *	Example: XXXX|XXXX
 *		  ^    ^
 *		  |    |
 *		read  write
 *	      number  number
 *
 * @return masked operation number.
 */
int PackIoctlNum(int *opNum, ARIGHTS a_r, bool isSrv)
{
	int res = -1;		/* if r/w ioctl num is equal to 0xffff - then it means
				   that it's not defined for the given register */

	switch (a_r) {
	case AMRD:		/* 'r' */
		res = (*opNum) << 16;	/* move 'r' to the 'most significant' halfword */
		(*opNum)++;
		break;
	case AMWR:		/* 'w' */
		if (!isSrv) {	/* because of "GET_HISTORY" is possible in this case!  */
			res = (*opNum) << 16;	/* move 'r' to the 'most significant' halfword */
			(*opNum)++;
		}
		res |= (*opNum);
		(*opNum)++;
		break;
	case AMRD | AMWR:	/* 'wr' */
	case AMEX:
		res = (*opNum) << 16;	/* move 'r' to the 'most significant' halfword */
		(*opNum)++;
		res |= (*opNum);
		(*opNum)++;
		break;
	default:		/* access rights not defined for this register */
		if (isSrv) {	/* SPECIAL service reg. he's got one ioctl number!
				   (will be the same for read and write) */
			res = ((*opNum) << 16) + *opNum;
			(*opNum)++;
		} else {
			res = -1;
		}
		break;
	}
	return (res);
}

/**
 * @brief Build-up enumiration table header file for all module registers.
 *
 * @param blocks       - block description
 * @param registers    - register description
 * @param numRegisters - register amount
 *
 * Fill it in properly with the information from the database.
 *
 * @return void
 */
void BuildRegIdEnum(BlockDef_t * blocks, RegisterDef_t * registers,
		    int numRegisters)
{
	char *bus = TranslationGetBus();	/* 'VME' or 'DRM' */
	FILE *fp;
	int cntr;

	fp = OpenFile(COMMON_FT, LOC_INCL, "RegId.h");
	TranslationSetFreeFormat("%s", DG_INC_DIR);
	Translate(fp, bus, "regDesc/regIdHead.h");

	/* pass though all registers */
	for (cntr = 0; cntr < numRegisters; cntr++) {
		TranslationSetRegId(registers[cntr].upperName);
		TranslationSetRegComm(registers[cntr].comment);
		TranslationSetFancyString(registers[cntr].mode);
		if (!cntr) {
			/* first module register number should start
			   just after the last service register number */
			TranslationSetDummyString(srv_ioctl[GetSrvRegNum()].
						  name);
			Translate(fp, "common", "header/regIdEnumIndexed.h");
		} else {
			Translate(fp, "common", "header/regIdEnum.h");
		}
	}

	Translate(fp, bus, "regDesc/regIdFoot.h");
	fclose(fp);
}

/**
 * @brief Get number of all defined <E>Service Registers</E>
 *
 * @return amount of defined Service Registers.
 */
int GetSrvRegNum()
{
	SRVIOCTL *ptr = srv_ioctl;
	static int cntr = -1;

	if (cntr == -1) {	/* see if already defined */
		cntr = 0;	/* reset it */
		while (ptr->depth) {	/* 0 means no more sevice registers left */
			ptr++;
			cntr++;
		}
	}
	return (cntr);
}

/**
 * @brief Converts register access rights into register access mode.
 *
 * @param rar - register access rights
 *
 * It converts from enum representation to char	representation. Note that
 * subsequent calls modifies previously reterned string. So you should save
 * it in a local buffer before next call to this function.
 *
 * @return string pointer to register access mode.
 */
char *rar2mode(ARIGHTS rar)
{
	static char ram[4] = { 0, 0, 0, 0 };	/* register access mode (rwe) */

	/* read register */
	if (rar & AMRD)
		ram[0] = 'r';
	else
		ram[0] = '-';

	/* write register */
	if (rar & AMWR)
		ram[1] = 'w';
	else
		ram[1] = '-';

	/* extraneous register */
	if (rar & AMEX)
		ram[2] = 'e';
	else
		ram[2] = '-';

	return (ram);
}

/**
 * @brief Converts register access mode into register access rights.
 *
 * @param mode - register access mode (rwec)
 *
 * It converts from char representation to enum	representation.
 *
 * @return register access rights
 */
ARIGHTS mode2rar(char *mode)
{
	int cntr;
	ARIGHTS rar = 0;

	if (strlen(mode) > 4)
		return (rar);	/* wrong access mode string format */

	for (cntr = 0; cntr < 4; cntr++) {
		switch (mode[cntr]) {
		case 'r':
			rar |= AMRD;
			break;
		case 'w':
			rar |= AMWR;
			break;
		case 'e':
			rar |= AMEX;
			break;
		default:
			rar = 0;	/* oops... */
			return (rar);
		}
	}

	return (rar);
}
