/**
 * @file vmeGeneration.c
 *
 * @brief Generates a specific VME device Driver/Simulator code
 *
 * Generates a device driver based on a VME implementation of the
 * given hardware configuration.
 *
 * @author Copyright (C) 2002 CERN. Stuart Baird
 * @author Copyright (C) 2003 CERN. Alain Gagnaire
 * @author Copyright (C) 2003 - 2010 CERN. Georgievskiy Yury <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */
#include "driverGen.h"
#include "dg-version.h"
#include "xml-access.h"

static void BuildVmeHeaderFile(int, RegisterDef_t *, int, BlockDef_t *, int);
static void BuildVmeStatics(BlockDef_t *, int, FILE *, int);
static void BuildVmeDrvrFile(RegisterDef_t *, int, BlockDef_t *, int);
static void BuildVmeSimFile(RegisterDef_t *, int, BlockDef_t *, int);

/**
 * @brief Generates the entire driver source code from scratch, including
 *        user-defined files.
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param blocks       -- block description
 * @param numBlocks    -- block amount
 * @param vmeInfo      -- @e VME description
 *
 * @return void
 */
void GenerateVmeDriver(RegisterDef_t * registers, int numRegisters,
		       BlockDef_t * blocks, int numBlocks, VmeInfo_t * vmeInfo)
{
	char *system = TranslationGetModName();

	/* First, build-up major source code of the driver */
	GenerateVmeCore(registers, numRegisters, blocks, numBlocks, vmeInfo);

	/* Now build-up user-defined driver files for the current module */
	printf("Building   %s user entry points.\n", system);
	BuildUserPartDrvrCode();
}

/**
 * @brief Generates major source code of the driver, excluding the generation
 *        of user-defined files.
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param blocks       -- block description
 * @param numBlocks    -- block amount
 * @param vmeInfo      -- @e VME description
 *
 * @return void
 */
void GenerateVmeCore(RegisterDef_t * registers, int numRegisters,
		     BlockDef_t * blocks, int numBlocks, VmeInfo_t * vmeInfo)
{
	char *system = TranslationGetModName();

  /*-=-=-=-=-=-=-=-=-=-=-=-=- VME driver. =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	printf("Generating %s driver's source code.\n", system);
	/* Header file for driver. */
	BuildVmeHeaderFile(DRIVER_FT, registers, numRegisters, blocks,
			   numBlocks);
	/* Driver source. */
	BuildVmeDrvrFile(registers, numRegisters, blocks, numBlocks);

  /*-=-=-=-=-=-=-=-=-=-=-=- VME simulator. -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	printf("Generating %s driver's simulator source code.\n", system);
	/* Header file for simulator. */
	BuildVmeHeaderFile(SIM_FT, registers, numRegisters, blocks, numBlocks);
	/* Simulator source. */
	BuildVmeSimFile(registers, numRegisters, blocks, numBlocks);

  /*-=-=-=-=-=-=-=-=- Driver/Simulator registers r/w operations. -=-=-=-=-=-*/
	BuildGetSetRegCH(registers, numRegisters, vmeInfo);

  /*-=-=-=-=-=-=-=-=- Driver-specific Library. -=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	printf("Generating %s IoctlAccess library.\n", system);
	BuildIoctlLibrary(registers, numRegisters, blocks, numBlocks);

  /*-=-=-=-=-=-=-=-=-=- Test program. -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
	printf("Generating %s test program.\n", system);
	BuildTestProgram(registers, numRegisters, blocks, numBlocks);

  /*-=-=-=-=-=-=-=- Info file that holds 'DevInfo_t' structure. -=-=-=-=-=-=-*/
	printf("Generating %s '.info' file.\n", system);
	GenerateVmeInfoFile(registers, numRegisters, blocks, numBlocks,
			    vmeInfo);

  /*-=-=-=-=-=-=-=- Register ID of currently processed module. -=-=-=-=-=-=-=*/
	printf("Generating %s registers ID.\n", system);
	BuildRegIdEnum(blocks, registers, numRegisters);

  /*-=-=-=-=-=-=-=-=-=-=-=-=-=- Makefiles. -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	printf("Generating %s makefiles.\n", system);
	BuildMakefiles();
}

/**
 * @brief Create .info file for current module.
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param blocks       -- block description
 * @param numBlocks    -- block amount
 * @param vmeInfo      -- @e vme description
 *
 * Info file contains structure @e DevInfo_t that will be passed to the
 * module device driver during installation. All parameters that are already
 * known are set here. The rest of them will be set (or redefined) by the user
 * during installation procedure as a parameter in the command line,
 * or will be left unchanged.
 *
 * @return void
 */
void GenerateVmeInfoFile(RegisterDef_t * registers, int numRegisters,
			 BlockDef_t * blocks, int numBlocks,
			 VmeInfo_t * vmeInfo)
{
	long longVal;
	uint uintVal;
	int intVal;
	short shortVal;
	char charVal[MAX_STR];
	int cntr;
	int regCntr = 0;	/* register ID counter. Starts from zero */
	int ioctlCntr = 0;	/* ioctl number counter */

	int totalReg;
	int tmpBiggest, biggest = 0;	/* size in bytes of the
					   biggest register */
	FILE *infoF = OpenFile(COMMON_FT, LOC_MODSRC, "object_%s/.info",
			       TranslationGetSysLower());

	memset(charVal, 0, sizeof(charVal));

	/*---Fill in data of the first address space. 'addr1' member.------*/
	/* base address (NO_ADDRESS means that address space NOT defined) */
	longVal = vmeInfo->addr1.baseAddr;
	longVal = ASSERT_MSB(longVal);

	fwrite(&longVal, sizeof(longVal), 1, infoF);	/* 'baseAddr' member */

	/* address range */
	uintVal = vmeInfo->addr1.range;
	uintVal = ASSERT_MSB(uintVal);

	fwrite(&uintVal, sizeof(uintVal), 1, infoF);	/* 'range' member */

	/* address increment */
	longVal = vmeInfo->addr1.increment;
	longVal = ASSERT_MSB(longVal);

	fwrite(&longVal, sizeof(longVal), 1, infoF);	/* 'increment' member */

	/* dataport size (16 or 32) */
	longVal = vmeInfo->addr1.dpSize;
	longVal = ASSERT_MSB(longVal);

	fwrite(&longVal, sizeof(longVal), 1, infoF);	/* 'dpSize' member */

	/* address modifier (16-SH, 24-ST, 32-EX) */
	longVal = vmeInfo->addr1.addressModifier;
	longVal = ASSERT_MSB(longVal);

	fwrite(&longVal, sizeof(longVal), 1, infoF);	/* 'addrModif' member */
	/*----------- Done for the first address space ----------------------*/

	/*----- Fill in data of the second address space. 'addr2' member ----*/
	/* base address (NO_ADDRESS - address space NOT defined) */
	longVal = vmeInfo->addr2.baseAddr;
	longVal = ASSERT_MSB(longVal);

	fwrite(&longVal, sizeof(longVal), 1, infoF);	/* 'baseAddr' member */

	/* address range */
	uintVal = vmeInfo->addr2.range;
	uintVal = ASSERT_MSB(uintVal);

	fwrite(&uintVal, sizeof(uintVal), 1, infoF);	/* 'range' member */

	/* address increment */
	longVal = vmeInfo->addr2.increment;
	longVal = ASSERT_MSB(longVal);

	fwrite(&longVal, sizeof(longVal), 1, infoF);	/* 'increment' member */

	/* dataport size (16 or 32) */
	longVal = vmeInfo->addr2.dpSize;
	longVal = ASSERT_MSB(longVal);

	fwrite(&longVal, sizeof(longVal), 1, infoF);	/* 'dpSize' member */

	/* address modifier (16-SH, 24-ST, 32-EX) */
	longVal = vmeInfo->addr2.addressModifier;
	longVal = ASSERT_MSB(longVal);

	fwrite(&longVal, sizeof(longVal), 1, infoF);	/* 'addrModif' member */
	/*----------- Done for the second address space ---------------------*/

	/*---------- Fill in General Module Information ---------------------*/
	/* DataBase Module Type Number */
	intVal = vmeInfo->mtn;
	intVal = ASSERT_MSB(intVal);
	fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'mtn' member */

	/* module Logical Unit Number. Will be set by user during
	   installation. */
	intVal = NO_LUN;
	intVal = ASSERT_MSB(intVal);
	fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'mlun' member */

	/* bit field. Will be set by user during installation. */
	intVal = 0;
	intVal = ASSERT_MSB(intVal);
	fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'debugFlag' member */

	/* opaque param. Will be set by user during installation. */
	/* 'opaque' member */
	fwrite(charVal, sizeof(char), sizeof(charVal), infoF);

	/* interrupt processing hardware level */
	intVal = vmeInfo->irq;
	intVal = ASSERT_MSB(intVal);
	fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'iLevel' member */

	/* interrupt vector */
	intVal = vmeInfo->vector;
	intVal = ASSERT_MSB(intVal);
	fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'iVector' member */

	/* interrupt vector increment */
	intVal = vmeInfo->vectorInc;
	intVal = ASSERT_MSB(intVal);
	fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'iVectorInc' member */

	/* minor devices amount */
	intVal = vmeInfo->chCntr;
	intVal = ASSERT_MSB(intVal);
	fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'chan' member */

	/* check register index */
	intVal = (vmeInfo->CheckRegister == -1)
	    ? (-1)		/* no checking register */
	    : vmeInfo->CheckRegister + GetSrvRegNum(); /* check reg is present */
	intVal = ASSERT_MSB(intVal);
	fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'chrindex' member */

	/* TODO! Set good values. */
	longVal = -1;
	longVal = ASSERT_MSB(longVal);
	fwrite(&longVal, sizeof(longVal), 1, infoF);	/* 'gen_date' member */
	fwrite(charVal, sizeof(char), NAME_LEN, infoF);	/* 'dg_vers' member */

	/* actual register amount of the module */
	totalReg = intVal = GetSrvRegNum() + numRegisters;
	intVal = ASSERT_MSB(intVal);
	fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'regAmount' member */
	/*---------- Done for General Module Information --------------------*/

	/*---------- Description of each register ('regDesc' member) --------*/
	/* -----------------------------
	   00. First - Service registers
	   ----------------------------- */
	for (cntr = 0; cntr < GetSrvRegNum(); cntr++, regCntr++) {
		/* register ID */
		intVal = regCntr;
		intVal = ASSERT_MSB(intVal);
		/* 'regId' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register name */
		memset(charVal, 0, sizeof(charVal));
		strncpy(charVal, srv_ioctl[cntr].name, NAME_LEN);
		charVal[NAME_LEN] = 0;
		/* 'regName' member */
		fwrite(charVal, sizeof(char), NAME_LEN, infoF);

		/* bus type */
		memset(charVal, 0, sizeof(charVal));
		strcpy(charVal, "VME");
		/* 'busType' member */
		fwrite(charVal, sizeof(char), MIN_STR, infoF);

		/* service register */
		intVal = RT_SRVS;
		intVal = ASSERT_MSB(intVal);

		/* 'regType' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register size in bytes */
		intVal = srv_ioctl[cntr].regSize;
		intVal = ASSERT_MSB(intVal);

		/* 'regSize' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* 'addr1' or 'addr2' */
		memset(charVal, 0, sizeof(charVal));
		strcpy(charVal, "none");
		/* 'b_a' member */
		fwrite(charVal, sizeof(char), MIN_STR, infoF);

		/* block, to which register belongs */
		shortVal = -1;
		shortVal = ASSERT_MSB(shortVal);
		/* 'bid' member */
		fwrite(&shortVal, sizeof(shortVal), 1, infoF);

		/* reg offset from the base addr */
		intVal = 0;
		/* 'regOffset' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register depth */
		intVal = srv_ioctl[cntr].depth;
		intVal = ASSERT_MSB(intVal);
		/* 'regDepth' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register timing loop */
		intVal = 0;
		fwrite(&intVal, sizeof(intVal), 1, infoF); /* 'regtl' member */

		/* register access mode */
		intVal = srv_ioctl[cntr].rar;
		intVal = ASSERT_MSB(intVal);
		fwrite(&intVal, sizeof(intVal), 1, infoF); /* 'regar' member */

		/* service ioctl w/r numbers */
		intVal = PackIoctlNum(&ioctlCntr, srv_ioctl[cntr].rar,
				      TRUE/*service register */);
		intVal = ASSERT_MSB(intVal);

		/* 'rwIoctlOpNum' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* control biggest register size in bytes */
		tmpBiggest = srv_ioctl[cntr].depth * srv_ioctl[cntr].regSize;
		if (tmpBiggest > biggest)
			biggest = tmpBiggest;
	}

	/* --------------------------
	   01. Now - Module registers
	   -------------------------- */
	for (cntr = 0; cntr < numRegisters; cntr++, regCntr++) {
		int realDepth;
		/* register ID */
		intVal = regCntr;
		intVal = ASSERT_MSB(intVal);
		fwrite(&intVal, sizeof(intVal), 1, infoF); /* 'regId' member */

		/* register name */
		memset(charVal, 0, sizeof(charVal));
		strncpy(charVal, registers[cntr].name, NAME_LEN);
		charVal[NAME_LEN] = 0;
		/* 'regName' member */
		fwrite(charVal, sizeof(char), NAME_LEN, infoF);

		/* bus type */
		memset(charVal, 0, sizeof(charVal));
		strcpy(charVal, "VME");
		/* 'busType' member */
		fwrite(charVal, sizeof(char), MIN_STR, infoF);

		/* if register is Extraneous or Real */
		if (strchr(registers[cntr].mode, 'e'))
			intVal = RT_EXTR;	/* extraneous register */
		else
			intVal = RT_REAL;	/* normal register */
		intVal = ASSERT_MSB(intVal);
		/* 'regType' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register size in bytes */
		intVal = registers[cntr].regSize;
		intVal = ASSERT_MSB(intVal);
		/* 'regSize' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* 'addr1' or 'addr2' */
		memset(charVal, 0, sizeof(charVal));
		strcpy(charVal, ((registers[cntr].blockP->blkBaseAddr == 1) ?
				 "addr1" : "addr2"));
		/* 'b_a' member */
		fwrite(charVal, sizeof(char), MIN_STR, infoF);

		/* block, to which register belongs */
		shortVal = registers[cntr].blockP->blockID;
		shortVal = ASSERT_MSB(shortVal);
		/* 'bid' member */
		fwrite(&shortVal, sizeof(shortVal), 1, infoF);

		/* reg offset from the base addr */
		intVal = registers[cntr].offset;
		intVal = ASSERT_MSB(intVal);
		/* 'regOffset' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register depth */
		intVal = registers[cntr].depth;
		intVal = ASSERT_MSB(intVal);
		/* 'regDepth' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register timing loop */
		intVal = registers[cntr].timeLoop;
		intVal = ASSERT_MSB(intVal);
		fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'regtl' member */

		/* register access mode */
		intVal = registers[cntr].rar;
		intVal = ASSERT_MSB(intVal);
		fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'regar' member */

		/* ioctl w/r numbers */
		intVal = PackIoctlNum(&ioctlCntr, registers[cntr].rar,
				      FALSE /*not service register */ );
		intVal = ASSERT_MSB(intVal);
		/* 'rwIoctlOpNum' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* control biggest register size in bytes */
		if (registers[cntr].depth == 0 || registers[cntr].depth == -1)
			realDepth = 1;
		else
			realDepth = registers[cntr].depth;
		tmpBiggest = realDepth * registers[cntr].regSize;
		if (tmpBiggest > biggest)
			biggest = tmpBiggest;
	}

	/* -------------------------------------------------------------
	   02. fill in the rest (i.e. unused register slots) with zeroes
	   ------------------------------------------------------------- */
	intVal = 0;
	for (cntr = 0; cntr < MAX_REG - totalReg; cntr++) {
		/* set register ID to zero. Indicates the last one */
		fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'regId' member */

		/* register name */
		/* 'regName' member */
		fwrite(&intVal, sizeof(char), NAME_LEN, infoF);

		/* bus type */
		/* 'busType' member */
		fwrite(&intVal, sizeof(char), MIN_STR, infoF);

		/* if register is Real, Extraneous or Service */
		/* 'regType' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register size in bytes */
		/* 'regSize' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* 'addr1' or 'addr2' */
		/* 'b_a' member */
		fwrite(&intVal, sizeof(char), MIN_STR, infoF);

		/* block, to which register belongs */
		/* 'bid' member */
		fwrite(&intVal, sizeof(short), 1, infoF);

		/* reg offset from the base addr */
		/* 'regOffset' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register depth */
		/* 'regDepth' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register timing loop */
		/* 'regtl' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register access mode */
		/* 'regar' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* ioctl w/r numbers */
		/* 'rwIoctlOpNum' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);
	}
	/*------------ End of each register description -------------------*/

	/* set size in bytes of the biggest register in the module */
	biggest = ASSERT_MSB(biggest);
	fwrite(&biggest, sizeof(biggest), 1, infoF);	/* 'maxRegSz' member */

	/* actual block amount of the moudule */
	intVal = numBlocks;
	intVal = ASSERT_MSB(intVal);
	fwrite(&intVal, sizeof(intVal), 1, infoF);	/* 'blkAmount' member */

	/*-------- Description of each block 'blkDesc' member -------------*/
	/* ----------------------------
	   00. actual block description
	   ---------------------------- */
	for (cntr = 0; cntr < numBlocks; cntr++) {
		/* block sequence number, starting from 0 */
		shortVal = blocks[cntr].blockID;
		shortVal = ASSERT_MSB(shortVal);
		/* 'block' member */
		fwrite(&shortVal, sizeof(shortVal), 1, infoF);

		/* 1 for INIT and 2 for NEXT base address */
		shortVal = blocks[cntr].blkBaseAddr;
		shortVal = ASSERT_MSB(shortVal);
		/* 'blkBaseAddr' member */
		fwrite(&shortVal, sizeof(shortVal), 1, infoF);

		/* block offset from the base address */
		longVal = blocks[cntr].offset;
		longVal = ASSERT_MSB(longVal);
		/* 'offset' member */
		fwrite(&longVal, sizeof(longVal), 1, infoF);

		/* driver block size in bytes */
		intVal = blocks[cntr].blksz_drvr;
		intVal = ASSERT_MSB(intVal);
		/* 'blksz_drvr' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* simulator block size in bytes */
		intVal = blocks[cntr].blksz_sim;
		intVal = ASSERT_MSB(intVal);
		/* 'blksz_sim' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register amount in the block */
		intVal = blocks[cntr].reg_am;
		intVal = ASSERT_MSB(intVal);
		/* 'reg_am' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

	}

	/* --------------------------------
	   01. fill in the rest with zeroes
	   -------------------------------- */
	shortVal = longVal = intVal = 0;
	for (cntr = 0; cntr < MAX_BLK - numBlocks; cntr++) {
		/* block sequence number, starting from 0 */
		/* 'block' member */
		fwrite(&shortVal, sizeof(shortVal), 1, infoF);

		/* 1 for INIT and 2 for NEXT base address */
		/* 'blkBaseAddr' member */
		fwrite(&shortVal, sizeof(shortVal), 1, infoF);

		/* block offset from the base address */
		/* 'offset' member */
		fwrite(&longVal, sizeof(longVal), 1, infoF);

		/* driver block size in bytes */
		/* 'blksz_drvr' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* simulator block size in bytes */
		/* 'blksz_sim' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);

		/* register amount in the block */
		/* 'reg_am' member */
		fwrite(&intVal, sizeof(intVal), 1, infoF);
	}
	/*-----------------End of each block description-------------------*/

	/* TODO! compute checksum */
	memset(charVal, 0, sizeof(charVal));
	fwrite(charVal, sizeof(char), 1, infoF);	/* 'checksum' member */
	fclose(infoF);

	/* produce .xml file */
	dgxml_cerndb_info2xml(NULL);
}

/**
 * @brief Construct driver/simulator header files.
 *
 * @param type         -- driver or simulator
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param blocks       -- block description
 * @param numBlocks    -- block amount
 */
static void BuildVmeHeaderFile(int type, RegisterDef_t * registers,
			       int numRegisters, BlockDef_t * blocks,
			       int numBlocks)
{
	FILE *headerFile;
	char *bus = TranslationGetBus();

	TranslationReset();
	headerFile = OpenFile(type, LOC_INCL, ".h");

	/* Generate the header file's head and then go to generate the rest */
	TranslationSetDriverType((type == DRIVER_FT) ? "DRVR" : "SIM");
	TranslationSetChar(*(TranslationGetModName()));
	TranslationSetFreeFormat("%s", DG_INC_DIR);
	Translate(headerFile, bus, "header/DrvrSimHeader.h");

	/* Generate the constants used for the IOCTL function */
	BuildIoctlConsts(registers, numRegisters, headerFile);

	/* Generate the statics, info and block structures */
	BuildCommonBlocks(type, registers, numRegisters, headerFile);
	BuildVmeStatics(blocks, numBlocks, headerFile, type);

	fclose(headerFile);
}

/**
 * @brief Construct the driver's statics and info structures.
 *
 * @param blocks     -- block description
 * @param numBlocks  -- block amount
 * @param headerFile -- open file descriptor
 * @param type       -- driver or simulator
 *
 * @return void
 */
static void BuildVmeStatics(BlockDef_t * blocks, int numBlocks,
			    FILE * headerFile, int type)
{
	int cntr;
	char *bus = TranslationGetBus();

	TranslationReset();

	/* Generate the topographic structure's head */
	Translate(headerFile, bus, "header/topologyHead.h");

	/* For each block that's been defined, put a field in the topology
	   structure. */
	for (cntr = 0; cntr < numBlocks; cntr++) {
		TranslationSetFancyNum(blocks[cntr].blockID);
		Translate(headerFile, "common", "header/blockDef.h");
	}

	/* Close the statics structure and generate the info one. */
	TranslationSetDriverType((type == DRIVER_FT) ? "DRVR" : "SIM");
	Translate(headerFile, "common", "header/topologyFoot.h");
}

/**
 * @brief Generate the driver's source file.
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param blocks       -- block description
 * @param numBlocks    -- block amount
 *
 * @return void
 */
static void BuildVmeDrvrFile(RegisterDef_t * registers, int numRegisters,
			     BlockDef_t * blocks, int numBlocks)
{
	char *bus = TranslationGetBus();
	FILE *dfd = OpenFile(DRIVER_FT, LOC_DRVR, ".c");
	int cntr;
	char *tstr = ctime(&mod_creation_time);	/* time string */

	tstr[strlen(tstr) - 1] = 0;	/* get rid of '\n' */

	TranslationReset();

	TranslationSetDriverType("Drvr");
	TranslationSetFancyString("DRVR");
	TranslationSetDummyString("Driver");
	TranslationSetFreeFormat("%s", DG_INC_DIR);
	/* set driver generation time */
	TranslationSetHexNum(mod_creation_time);
	TranslationSetComment(tstr);
	TranslationSetString((char *)dg_version);
	Translate(dfd, "common", "driver/driverHead.c");

	/* Generate driver's OPEN routine */
	Translate(dfd, bus, "driver/open/head.c");

	/* Generate driver's CLOSE routine. */
	Translate(dfd, "common", "driver/close/head.c");

	/* Generate driver's READ routine. */
	Translate(dfd, "common", "driver/read/head.c");

	/* Generate driver's WRITE routine. */
	Translate(dfd, "common", "driver/write/head.c");

	/* Generate driver's SELECT routine */
	Translate(dfd, "common", "driver/select/head.c");

	/* Generate driver's IOCTL routine. */
	BuildDrvrSimIoctl(registers, numRegisters, dfd);

	/* generate statics table cleanup routine */
	TranslationSetDriverType("Driver");
	Translate(dfd, "common", "driver/uninstall/cleanupHead.c");
	Translate(dfd, "common", "driver/uninstall/cleanupFoot.c");

	/* Generate driver's INSTALL routine */
	/* device checking should be done in case of the real driver */
	Translate(dfd, bus, "driver/install/checkDev.c");
	Translate(dfd, bus, "driver/install/head.c");

	Translate(dfd, bus, "driver/install/infoPrnt.c");
	Translate(dfd, bus, "driver/install/doCheck.c"); /* device checking */

	Translate(dfd, bus, "driver/install/body.c");
	for (cntr = 0; cntr < numBlocks; cntr++) {
		TranslationSetPlainNum(blocks[cntr].blockID);
		TranslationSetFancyNum(blocks[cntr].blockID);
		TranslationSetIntNum(cntr);

		if (blocks[cntr].blkBaseAddr == 1)
			TranslationSetBaseAddr("addr1");
		else
			TranslationSetBaseAddr("addr2");

		Translate(dfd, bus, "driver/install/assignBlock.c");
	}
	Translate(dfd, bus, "driver/install/foot.c");

	/* Generate driver's UNINSTALL routine */
	Translate(dfd, bus, "driver/uninstall/head.c");
	Translate(dfd, bus, "driver/uninstall/unmap.c");
	Translate(dfd, bus, "driver/uninstall/foot.c");

	/* Build-up dldd structure (driver entry points) */
	TranslationSetDriverType("Drvr");
	Translate(dfd, "common", "driver/entryPoints.c");

	fclose(dfd);
}

/**
 * @brief Generate the simulator's source file.
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param blocks       -- block description
 * @param numBlocks    -- block amount
 *
 * @return void
 */
static void BuildVmeSimFile(RegisterDef_t * registers, int numRegisters,
			    BlockDef_t * blocks, int numBlocks)
{
	char *bus = TranslationGetBus();
	FILE *sfd = OpenFile(SIM_FT, LOC_DRVR, ".c");
	int cntr;
	char *tstr = ctime(&mod_creation_time);	/* time string */

	tstr[strlen(tstr) - 1] = 0;	/* get rid of '\n' */

	TranslationReset();

	TranslationSetDriverType("Sim");
	TranslationSetFancyString("SIM");
	TranslationSetDummyString("Simulator");
	TranslationSetFreeFormat("%s", DG_INC_DIR);
	/* set driver generation time */
	TranslationSetHexNum(mod_creation_time);
	TranslationSetComment(tstr);
	TranslationSetString((char *)dg_version);
	Translate(sfd, "common", "driver/driverHead.c");

	/* Generate simulator's OPEN routine. */
	Translate(sfd, bus, "driver/open/head.c");

	/* Generate simulator's CLOSE routine. */
	Translate(sfd, "common", "driver/close/head.c");

	/* Generate simulator's READ routine. */
	Translate(sfd, "common", "driver/read/head.c");

	/* Generate simulator's WRITE routine. */
	Translate(sfd, "common", "driver/write/head.c");

	/* Generate simulator's SELECT routine */
	Translate(sfd, "common", "driver/select/head.c");

	/* Generate simulator's IOCTL routine */
	BuildDrvrSimIoctl(registers, numRegisters, sfd);

	/* generate statics table cleanup routine */
	TranslationSetDriverType("Simulator");
	Translate(sfd, "common", "driver/uninstall/cleanupHead.c");
	for (cntr = 0; cntr < numBlocks; cntr++) {
		TranslationSetFancyNum(blocks[cntr].blockID);
		TranslationSetPlainNum(blocks[cntr].blockID);
		Translate(sfd, "common", "driver/uninstall/uninstallBlocks.c");
	}
	Translate(sfd, "common", "driver/uninstall/cleanupFoot.c");

	/* Generate simulator's INSTALL routine */
	Translate(sfd, bus, "driver/install/head.c");
	Translate(sfd, bus, "driver/install/infoPrnt.c");

	Translate(sfd, bus, "driver/install/body.c");
	for (cntr = 0; cntr < numBlocks; cntr++) {
		TranslationSetFancyNum(blocks[cntr].blockID);
		TranslationSetPlainNum(blocks[cntr].blockID);
		Translate(sfd, "common", "driver/install/allocateBlock.c");
	}
	Translate(sfd, bus, "driver/install/foot.c");

	/* Generate simulator's UNINSTALL routine */
	Translate(sfd, bus, "driver/uninstall/head.c");
	Translate(sfd, bus, "driver/uninstall/foot.c");

	/* Build-up dldd structure (simulator entry points) */
	TranslationSetDriverType("Sim");
	Translate(sfd, "common", "driver/entryPoints.c");

	fclose(sfd);
}
