/**
 * @file utils.c
 *
 * @brief
 *
 * <long description>
 *
 * @author Copyright (C) 2009 CERN CO/HT Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 15/06/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/dir.h>		/* for struct direct and readdir */

#ifdef __Lynx__
#include <io.h> /* for majordev definition */
#else		/* __linux__ */
#include <sys/sysmacros.h>	/* for major, minor, makedev */
/* minor/major handling */
#define minordev minor
#define majordev major
#endif

#include "utils.h"

/*
   TODO list.
   0x0. close /dev/mem fd. Who shoul do this???
*/

/**
 * @brief Convert a string to lower case.
 *
 * @param string -- string to convert
 *
 * String can be _STOULSZ letters max.
 * User-provided string is not changed.
 * Returned string will be changed by subsequent calls.
 *
 * @return pointer to converted string - if OK
 * @return NULL - if failed
 */
char* StrToLower(const char *string)
{
	static char _lower[_STOULSZ];
	int i = strlen(string);

	if (i > _STOULSZ)
		return NULL;

	strncpy(_lower, string, _STOULSZ);
	while (--i >= 0)
		_lower[i] = (char)tolower(_lower[i]);

	return _lower;
}

/**
 * @brief Convert a string to upper case.
 *
 * @param string -- string to convert
 *
 * String can be _STOULSZ letters max.
 * User-provided string is not changed.
 * Returned string will be changed by subsequent calls.
 *
 * @return pointer to converted string - if OK
 * @return NULL - if failed
 */
char* StrToUpper(const char *string)
{
	static char _upper[_STOULSZ];
	int i = strlen(string);

	if (i > _STOULSZ)
		return NULL;

	strncpy(_upper, string, _STOULSZ);
	while (--i >= 0)
		_upper[i] = (char)toupper(_upper[i]);

	return _upper;
}


/**
 * @brief Container for the @b exact DataBase Module Name.
 *
 * @param nm      -- module name @b exactly as in the DataBase
 * @param checkNm -- should i check module name?
 *
 * Due to the DB spec - it iss not possible for module name contain any
 * lower-case letters. If this is the case -- it means that user provide
 * wrong module name for module installation/deinstallation program.
 * Error info is printed out in stderr.
 *
 * @return -1 - if provided module name 'nm' is spurious i.e. contains
 *		any lower-case letters
 * @return DB module name - if success
 */
char *db_mod_name(char *nm, uint checkNm)
{
	static char modNm[DDNMSZ] = { 0 };	/* module name container */

	if (nm /* name is provided */  && !modNm[0] /* not set yet */ ) {
		if (checkNm && !MODULE_NAME_OK(nm)) {
			fprintf(stderr, "Spurious Module Name '%s'.\nNormally"
				" _should not_ contain any lowercase"
				" letters!\nCheck with the DataBase if"
				" provided module name is correct.\nIT"
				" SHOULD BE _EXACTLY_ AS IN THE DATABASE!\n",
				nm);
			return ( char *)-1;
		}
		strncpy(modNm, nm, sizeof(modNm)); /* copy to local buffer */
	}

	return modNm;
}


/**
 * @brief Container for the upper case bus name
 *
 * @param nm -- bus type
 *
 * @return Upper case bus name. @e VME or @e PCI.
 */
char* __attribute__((unused)) GetBusName(char *nm)
{
	static char busNm[DDNMSZ] = { 0 };	/* bus name container */

	if (nm /* name is provided */  && !busNm[0] /* not set yet */ )
		/* copy to local buffer */
		strncpy(busNm, StrToUpper(nm), sizeof(busNm));

	return busNm;
}

/**
 * @brief Build-up and return name of the driver/simulator.
 *
 * @param nmp -- name part. _DRVR_ or _SIML_
 * @param ptr -- if not NULL then driver name is copied here.
 *
 * Based on the @e nmp parameter, driver/simulator name will be build.
 *
 * Examples:
 * SHAREDCIBC  --> SharedcibcD.ko [ Driver    ]
 * SHAREDCIBC  --> SharedcibcS.ko [ Simulator ]
 * RF_IRQ_TEST --> RfIrqTestS.ko  [ Simulator ]
 * etc...
 *
 * @return build-up driver name
 */
char *build_drvr_nm(char *nmp, char *ptr)
{
	static char drvrNm[DDNMSZ];	/* build-up driver name */
	char *tmp = _ncf(db_mod_name(NULL, FALSE));

	snprintf(drvrNm, sizeof(drvrNm), DRVR_NAME_FMT, tmp, nmp);

	if (ptr)
		strncpy(ptr, drvrNm, sizeof(drvrNm));

	free(tmp);
	return drvrNm;
}

/**
 * @brief Build-up and return name of the driver/simulator device.
 *
 * @param lun -- logical unit number
 * @param ptr -- if not NULL, build-up device name will go here.
 *
 * Based on the @e lun parameter, info file name will be build.
 * Examples:
 * SHAREDCIBC  --> SharedcibcS03.info
 * SHAREDCIBC  --> SharedcibcD02.info
 * RF_IRQ_TEST --> RfIrqTestD10.info
 *
 * @return build-up device name
 */
char *BuildDevNm(int lun, char *ptr)
{
	static char devNm[DDNMSZ];	/* build-up device name */
	char *tmp = _ncf(db_mod_name(NULL, FALSE));

	snprintf(devNm, sizeof(devNm), DEVICE_NAME_FMT,
		 tmp, (lun < 0) ? _SIML_ : _DRVR_, abs(lun));

	if (ptr)
		strncpy(ptr, devNm, sizeof(devNm));

	free(tmp);
	return devNm;
}

/**
 * @brief Build-up and return name of the driver/simulator node in /dev
 *
 * @param lun     -- logical unit number
 * @param chanNum -- channel number
 * @param ptr     -- if not NULL, node will be copied here
 *
 * Based on the parameters.
 * Examples:
 * SHARED_CIBC --> SharedCibcD.l00.c01
 * SHARED_CIBC --> SharedCibcS.l14.c09
 * TRIC        --> TricD.l01.c00
 *
 * @return build-up node name
 */
char *BuildNodeNm(int lun, int chanNum, char *ptr)
{
	static char nodeNm[DDNMSZ];	/* build-up node name */
	char *tmp = _ncf(db_mod_name(NULL, FALSE));

	snprintf(nodeNm, sizeof(nodeNm), NODE_NAME_FMT,
		 tmp, (lun < 0) ? _SIML_ : _DRVR_, abs(lun), chanNum);

	if (ptr)
		strncpy(ptr, nodeNm, sizeof(nodeNm));

	free(tmp);
	return nodeNm;
}

/**
 * @brief Search for device nodes in /dev
 *
 * @param devID  - major device ID to find nodes for
 * @param nodeNm - found device node names (full pathnames) will be place here.
 * @param nnsz   - buffer size
 *
 * @return Number of found device nodes.
 */
int FindDevNodes(int devID, char (*nodeNm)[NODENMSZ], int nnsz)
{
	DIR *dir;
	struct direct *direntry;
	int nodeCntr = 0;
	char fname[64];
	struct stat fstat;
	int elAm = nnsz / sizeof(*nodeNm);	/* buffer element amount */

	dir = opendir("/dev");

	while ( (direntry = readdir(dir)) ) {
		snprintf(fname, sizeof(fname), "/dev/%s", direntry->d_name);
		stat(fname, &fstat);
		if (majordev(fstat.st_rdev) == devID) {	/* bingo */
			if (nodeNm && (nodeCntr < elAm))
				strncpy(nodeNm[nodeCntr], fname,
					sizeof(nodeNm[nodeCntr]));
			nodeCntr++;
		}
	}

	closedir(dir);
	return  nodeCntr;
}

/**
 * @brief Parsing device name 'dn' to obtain it's LUN.
 *
 * @param dn - name to parse
 *
 * @return Logical Unit Nunber - if success.
 * @return NO_LUN              - otherwise.
 */
int GetDevLunFromName(char *dn)
{
	char *ssptr;
	char num[3] = { 0, 0, 0 };
	int ret = NO_LUN;

	ssptr = strstr(dn, _DRVR_);

	if (ssptr) {
		ssptr += strlen(_DRVR_);
		strncpy(num, ssptr, 2);
		ret = atoi(num);
		return  ret;
	}

	ssptr = strstr(dn, _SIML_);

	if (ssptr) {
		ssptr += strlen(_SIML_);
		strncpy(num, ssptr, 2);
		ret = atoi(num) * (-1);
		return  ret;
	}

	return  ret;
}

/**
 * @brief Exploit cwd and try to set default Module Name.
 *
 * @param res -- module name is placed here in first two cases,
 *               0 --  in third case (see explanation below)
 *
 * Search && check cwd for *.ko and *.info files.
 *
 * 1. Normal cwd for @b delivered driver should have one *.info file
 *    and 2 *.ko objects (driver and simulator).
 *    Example:
 *    Module is [ RF_IRQ_TEST ]
 *    [1] RfIrqTest.info [2] RfIrqTestS.ko [3] RfIrqTestD.ko
 *
 * 2. If driver is compiled locally and is @b not delivered, then normal
 *    cwd should have 1 *.info file and N (divisible by 2) *.ko objects,
 *    which are symlinks in this case.
 *    Example:
 *    Module is [ CTC ]
 *    [1] Ctc.info
 *    [2] CtcD.2.6.29.4-rt15.ko [3] CtcS.2.6.29.4-rt15.ko -- for Linux
 *    [4] CtcD.4.0.0.ko         [5] CtcD.4.0.0.ko         -- for Lynx
 *
 * 3. .ko names with 'uname -r' part in their names and without it -- can't
 *    co-exist. Directory considered to be a non-standard one.
 *
 * 4. If cwd doesn't meet two above rules -- it considered to be a
 *    @b non-standard directory
 *
 * In first 2 cases Module Names (exactly as in the DB) will be successfully
 * derived and returned to the caller in @ref res param.
 *
 * @return -1 -- *.ko filename ambiguity, means that there are names with
 *               'uname -r' part in them, and there are one without it.
 */
void set_def_mn(char *res)
{
	DIR *dir;
	struct direct *direntry = NULL;
	char ifn[256] = { 0 }; /* *.info name  (without *.info suffix) */
	int ic = 0;    /* *.info counter */
	char *ptr;

	dir = opendir("./");
	/* first look for .info files */
	while ( (direntry = readdir(dir)) ) {
		ptr = strstr(direntry->d_name, ".info");
		if (ptr) {
			if (ic == 1) {
				/* more then one .info -- non-standard */
				*res = 0;
				goto out;
			}
			strncpy(ifn, direntry->d_name,
				(int)(ptr - direntry->d_name));
			++ic;
		}
	}
	closedir(dir);

	dir = opendir("./");
	/* now pass through all .ko files */
	while ( (direntry = readdir(dir)) ) {
		ptr = strstr(direntry->d_name, ".ko");
		if (ptr) {
			if (strncmp(ifn, direntry->d_name, strlen(ifn))) {
				/* info name doesn't match ko -- non-standard */
				*res = 0;
				goto out;
			}
		}
	}

	/* convert to DB format */
	ptr = _ncf(ifn);
	strcpy(res, ptr);
	free(ptr);
 out:
	closedir(dir);
	return;
}
