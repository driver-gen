/**
 * @file utils.h
 *
 * @brief
 *
 * <long description>
 *
 * @author Copyright (C) 2009 CERN CO/HT Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 15/06/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */

#ifndef _UTILITIES_H_INCLUDE_
#define _UTILITIES_H_INCLUDE_

#include <stddef.h>		/* for size_t */
#include <sys/types.h>		/* for uint */
#include "GeneralHeader.h"

/* to suppress 'implicit declaration' warnings, and 'undefined vars' errors */
extern int snprintf(char *str, size_t size, const char *format, ...);
extern int dr_uninstall(int);
extern int cdv_uninstall(int);
extern char *optarg; /* for getopt() calls */


#define ISSUE_ERR() \
fprintf(stderr, "\n%sfile '%s' line %d. Function %s.\n", ERR_MSG, __FILE__, __LINE__, __FUNCTION__)

#define DDNMSZ    32  /* device/driver name size in bytes. Same as in Lynx */
#define NODENMSZ  40  /* device/driver node name size in bytes. */
#define MAX_NODES 255 /* max minor device amount */
#define MAXMODNUM 256 /* max module definition supported */

/* device types */
#define CHAR_DEV  0
#define BLOCK_DEV 1

/* driver description */
typedef struct _tagDRIVDESC {
  int  driv_id;	          /* driver ID */
  char driv_name[DDNMSZ]; /* driver name
			     as seen in /proc/devices in case of Linux
			     as seen by 'drivers' command in case of Lynx
			  */
  int  usecount;	  /* number of devices using driver */
} driv_desc;

/* device description */
typedef struct _tagDEVDESC {
  int  dev_id;	         /* device ID */
  char dev_name[DDNMSZ]; /* device name */
  int  dev_type;	 /* block (BLOCK_DEV) or char (CHAR_DEV) device */
  int  dev_drvrid;       /* driver ID, that device is using */
} dev_desc;

extern char progName[DDNMSZ];	/* program name */

#define _STOULSZ 128 /* max allowed string size for StrToLower/Upper calls */
char* StrToLower(const char*);
char* StrToUpper(const char*);

char* db_mod_name(char*, uint);
char* GetBusName(char*);
char* build_drvr_nm(char*, char*);
char* BuildDevNm(int, char*);
char* BuildNodeNm(int, int, char*);
int   find_driver(char*, driv_desc*, int);
int   find_devices(int, dev_desc*, int);
int   FindDevNodes(int, char (*)[], int);
int   GetDevLunFromName(char*);
void  set_def_mn(char *);

#endif /* _UTILITIES_H_INCLUDE_ */
