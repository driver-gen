#ifndef _EXTRA_LINUX_H_INCLUDE_
#define _EXTRA_LINUX_H_INCLUDE_

#include "ModuleHeader.h"	   /* for DevInfo_t && friends */

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))
#endif

char*      extract_driver_name(char *);
int        proc_dev_find_by_name(char *, int *, int);
char*      proc_dev_find_by_major(int);
DevInfo_t* convert_dit(DevInfo_t *, DevInfo_t *);
int        find_device_nodes(int, char (*)[32], int);
char*      device2drvr(char *);
int        open_srv_drvr_node(int);
int        get_module_amount(int);
int        disable_kernel_hotplug(char **);
int        set_kernel_hotplug(char *);

#endif	/* _EXTRA_LINUX_H_INCLUDE_ */
