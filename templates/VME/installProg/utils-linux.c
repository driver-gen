/**
 * @file utils-linux.c
 *
 * @brief
 *
 * <long description>
 *
 * @author Copyright (C) 2009 CERN CO/HT Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 15/06/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <include/general_ioctl.h> /* for _GIOCTL_* && co. ioctls */
#include "utils.h"
#include "extra-linux.h"

/* _GIOCTL_GET_DG_DEV_INFO ioctl argument
   Keep syncronize with one from the driver! (in cdcm-driver-gen-vme.c) */
struct dg_module_info {
	int dev_major;		/* device major number */
	int drvr_major;		/* driver major number */
	char name[DDNMSZ];	/* its name */
};

/**
 * @brief Get module information from the driver
 *
 * @param maj -- driver major number
 * @param mam -- module amount
 *
 * Handy routine, ioctl-ing the driver.
 * Returned value should be freed by the caller afterwards.
 *
 * @return module info pointer -- if OK
 * @return 0                   -- if FAILED
 */
struct dg_module_info* get_module_info(int maj, int mam)
{
	struct dg_module_info *param; /* _GIOCTL_GET_DG_DEV_INFO ioctl param */
	int dfd = open_srv_drvr_node(maj); /* driver file descriptor */

	if (dfd < 0)
		return NULL;

	param = calloc(mam, sizeof(struct dg_module_info));
	if (!param) {
		perror("Can't allocate memory for _GIOCTL_GET_DG_DEV_INFO"
		       " ioctl call");
		param = NULL;
		goto out;
	}

	/* get info from the driver */
	if (ioctl(dfd, _GIOCTL_GET_DG_DEV_INFO, param)) {
		perror("Can't get device info from the driver");
		free(param);
		param = NULL;
		goto out;
	}

 out:
	close(dfd);
	return param;
}

/**
 * @brief Search for driver devices.
 *
 * @param drId     -- driver ID (major number), to get devices for
 *                    -1 -- special case. See below.
 * @param devDescr -- found device description will be placed in this buffer.
 *                    Can be NULL. See below.
 * @param ddsz     -- buffer size in bytes
 *
 * If @ref drId is not -1 and @ref devDescr is not NULL, then it points to
 * the buffer, where description of found driver devices will go.
 *  @ref ddsz is the size in bytes of this buffer.
 *
 * If all that user wants - is to find out how many devices are installed in
 * the system for the given driver, then second parameter @ref devDescr should
 * be NULL.
 *
 * When @ref drId is -1 -- will search for specific device in the driver.
 * In this case second parameter should contain the name
 * (in @ref devDescr[0].dev_name) of the device to look for.
 *
 * @return number of devices found in the system and their data in @e devDescr
 * @return -1 - if internal error occurs. Error info is printed to the
 *		standard output.
 */
int find_devices(int drId, dev_desc *devDescr, int ddsz)
{
	int mam = 0, cntr;
	char *dname = NULL;
	struct dg_module_info *param = NULL; /* _GIOCTL_GET_DG_DEV_INFO ioctl
						param */
	int ret;			    /* return code */
	int elAm = ddsz / sizeof(dev_desc); /* buffer element amount */
	int drmajor;

	if (drId != -1) {
		dname = proc_dev_find_by_major(drId);
		if (!dname) {
			fprintf(stderr, "Can't find driver with major %d\n",
				drId);
			ret = -1;
			goto out;
		}
		drmajor = drId;
	} else {
		dname = device2drvr(devDescr->dev_name);
		if (!dname) {
			fprintf(stderr, "Wrong device name [%s] detected\n",
				devDescr->dev_name);
			ret = -1;
			goto out;
		}

		if (!proc_dev_find_by_name(dname, &drmajor, 1)) {
			fprintf(stderr, "Can't find driver for %s deivce\n",
				devDescr->dev_name);
			ret = -1;
			goto out;
		}
	}

	/* now get all devices, driver controls */
	mam = get_module_amount(drmajor);
	if (mam < 0) {
		perror("Can't get module amount");
		ret = -1;
		goto out;
	}

	ret = mam; /* set return code to module amount driver controls */
	if (!mam)
		goto out;

	if (drId != -1 && !devDescr)
		/* user just wants device amount,
		   without their description */
		goto out;

	param = get_module_info(drmajor, mam);
	if (!param)
		goto out;

	if (drId != -1) {
		/* caller wants description of all driver devices */
		if (mam > elAm) {
			fprintf(stderr, "\nProvided device description"
				" buffer (%d descr MAX) is too small.\n"
				"Drvr has info on %d devices.\n",
				elAm, mam);
			ret = -1;
			goto out;
		}

		/* fill in caller buffer */
		for (cntr = 0; cntr < mam; cntr++) {
			devDescr[cntr].dev_id = param[cntr].dev_major;
			strncpy(devDescr[cntr].dev_name,
				param[cntr].name, DDNMSZ);
			devDescr[cntr].dev_type   = CHAR_DEV;
			devDescr[cntr].dev_drvrid =
				param[cntr].drvr_major;
		}
	} else {
		/* we should search for specific device
		   and tell caller the result */
		ret = 0;
		for (cntr = 0; cntr < mam; cntr++) {
			if (!strncmp(devDescr->dev_name,
				     param[cntr].name,
				     strlen (devDescr->dev_name))) {
				devDescr[ret].dev_id =
					param[cntr].dev_major;
				strncpy(devDescr[ret].dev_name,
					param[cntr].name, DDNMSZ);
				devDescr[ret].dev_type = CHAR_DEV;
				devDescr[ret].dev_drvrid =
					param[cntr].drvr_major;
				++ret;
			}
		}
	}

 out:
	if (dname) free(dname);
	if (param) free(param);
	return ret;
}

/**
 * @brief Get information about the driver @e dnam
 *
 * @param dnam -- name of the driver to find (*.ko), or NULL.
 * @param dd   -- found driver description will be placed in this buffer.
 * @param ddsz -- buffer size in bytes
 *
 * In LynxOS - more than one driver [with the same name] can be installed in
 * the system at the same time.
 *
 * In Linux it's not possible.
 *
 * If @ref dd is not NULL, then it points to the buffer, where found driver
 * ID's will go.
 * @ref ddsz is the size of this buffer. If all that user wants - is to find
 * out how many drivers with the name @ref dnam are currently installed in the
 * system, then second parameter (i.e. @ref dd) should be NULL.
 *
 * Special case, when @ref dnam is a NULL pointer is possible. In this case
 * second parameter should contain the ID of the driver that should be find in
 * the system in 'dd[0].driv_id' field. So in this case searching is based
 * on the provided driver ID, but not on the driver name.
 *
 * @return number of found drivers [0, 1] - if success
 * @return negative value                 - if internal error occurs.
 *                                          Error info is printed in stderr.
 */
int find_driver(char *dnam, driv_desc *dd, int ddsz)
{
	int did;
	char *name = NULL;
	int ret;

	if (dnam) {		/* we should find driver major */
		name = extract_driver_name(dnam); /* get driver name */

		if (!name) {
			printf("Wrong driver name %s\n", dnam);
			ret = -1;
			goto out;
		}

		/* search in /proc/devices for the driver major */
		if (!proc_dev_find_by_name(name, &did, 1)) {
			/* not found */
			ret = 0;
			goto out;
		}
	} else {
		if (!dd || !ddsz) { /* sane check */
			ret = -1;
			goto out;
		}

		did = dd->driv_id;
		name = proc_dev_find_by_major(did); /* get driver name */
		if (!name) {
			/* not found */
			ret = 0;
			goto out;
		}
	}

	/* get controlled module amount */
	ret = get_module_amount(did);
	if (ret < 0) {
		perror("Can't get module amount");
		ret = -1;
		goto out;
	}

	/* fill in caller buffer, if needed
	   in case of Linux we can have _only_ one driver */
	if (dd) {
		dd->driv_id = did;
		strncpy(dd->driv_name, name, strlen(name));
		dd->usecount = ret;
	}

	ret = 1;	/* driver is installed and only one */
 out:
	if (name) free(name);
	return ret;
}
