/**
 * @file utils-lynx.c
 *
 * @brief
 *
 * <long description>
 *
 * @author Copyright (C) 2009 CERN CO/HT Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 15/06/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */
#include <stdio.h>
#include <info.h>		/* for obtaining info from /dev/mem */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

/* we need to define __LYNXOS for cdevsw_entry, driver_entry, etc... */
#define __LYNXOS
#include <io.h>
#undef __LYNXOS

#include "utils.h"

#define _I_DIRECT -1		/* for /dev/mem seeking */

/**
 * @brief Repositions the offset of the @e /dev/mem file descriptor
 *
 * @param what -- if _I_DIRECT - do nothing but returns /dev/mem file descriptor
 *
 * Seeking to the system memory address corresponding to the given value of
 * 'what'. Open actual operating system table by opening the memory
 * pseudo-device.
 *
 * @return @e /dev/mem file descr with reposition offset.
 * @return -1 - otherwise. Error info is printed to the standard err output.
 */
static int seek_lynx_os_tab(int what)
{
	static int fd = (-1);
	static char filename[] = "/dev/mem";

	if (fd == (-1)) {
		if ((fd = open(filename, O_RDONLY, 0)) == (-1)) {
			fprintf(stderr, "\n%s: %s[ %s] open /dev/mem failed.\n",
				progName, ERR_MSG, strerror(errno));
#ifdef DEBUG_VERBOSE
			ISSUE_ERR();
#endif
			return -1;
		}
	}

	if (what == _I_DIRECT)	/* just return /dev/mem file descriptor */
		return fd;

	if ((lseek(fd, info(what), SEEK_SET) == (-1))) {
		fprintf(stderr, "\n%s: %s[ %s] lseek system memory failed.\n",
			progName, ERR_MSG, strerror(errno));
#ifdef DEBUG_VERBOSE
		ISSUE_ERR();
#endif
		return -1;
	}

	return fd;
}

/**
 * @brief Obtains all the devices of the driver with ID @ref drId, that
 *        currently are installed in the system.
 *
 * @param drId     -- driver ID, to get devices for, or -1
 * @param devDescr -- found device description will be placed in this buffer.
 * @param ddsz     -- buffer size in bytes
 *
 * If @ref devDescr is not NULL, then it points to the buffer, where
 * description of found devices will go. @ref ddsz is the size of this buffer.
 * If all that user wants - is to find out how many devices are installed in
 * the system for the given driver, then second parameter @ref devDescr should
 * be NULL.
 * Special case, when @ref drId equals to -1 is possible. In this case
 * second parameter should contain the name of the device that should be found
 * in the system in @ref devDescr[0].dev_name field. So in this case searching
 * is based on the provided device name, but not on the driver ID.
 *
 * @return number of devices found in the system and their data in @e devDescr
 *         massive.
 * @return -1 - if internal error occurs. Error info is printed to the
 *		standard output.
 */
int find_devices(int drId, dev_desc *devDescr, int ddsz)
{
	int devmem = 0;		/* operating system table file descriptor */

	long ncdevs = info(_I_NCDEVSW);	/* size of char device table */
	struct cdevsw_entry chde;	/* character device entry */

	long nbdevs = info(_I_NBDEVSW);	/* size of block device table */
	struct bdevsw_entry blkde;	/* block device entry */

	long dtbl = info(_I_DRIVERS);	/* addr of driver tbl */
	long drvrAddr;		/* driver table real address */

	int cntr = 0;
	int devCntr = 0;	/* found device counter */
	int elAm = ddsz / sizeof(dev_desc);	/* buffer element amount */

	if (drId != -1)		/* compute driver table physical address */
		drvrAddr = dtbl + sizeof(struct driver_entry) * drId;

  /*=================First, investigate character devices:===================*/
	if ((devmem = seek_lynx_os_tab(_I_CDEVSW)) == -1)
		return -1;

	/* read character device info */
	do {
		if (read(devmem, &chde, sizeof(struct cdevsw_entry)) == -1) {
			printf("\n%s: %s[ %s] read character device"
			       " table entry failed.\n",
			     progName, ERR_MSG, strerror(errno));
#ifdef DEBUG_VERBOSE
			ISSUE_ERR();
#endif
			return  -1;
		}

		if (((drId != -1) ? ((long)chde.driver == drvrAddr) :
		     (!strncmp(devDescr[0].dev_name, chde.name,
			       sizeof(chde.name)))) && (long)chde.driver != 0)
			{	/* bingo! */
			if (devDescr && (devCntr < elAm)) {
				devDescr[devCntr].dev_id = cntr;
				strncpy(devDescr[devCntr].dev_name, chde.name,
					sizeof(devDescr[devCntr].dev_name));
				devDescr[devCntr].dev_type = CHAR_DEV;
				devDescr[devCntr].dev_drvrid =
				    ((long)chde.driver -
				     dtbl) / sizeof(struct driver_entry);
			}
			devCntr++;
		}
	} while (++cntr < ncdevs);

  /*====================Now, investigate block devices:======================*/
	cntr = 0;		/* reset counter */
	if ((devmem = seek_lynx_os_tab(_I_BDEVSW)) == -1)
		return  -1;

	/* read block device info */
	do {
		if (read(devmem, &blkde, sizeof(struct bdevsw_entry)) == -1) {
			printf("\n%s: %s[ %s] read block device table entry"
			       " failed.\n",
			     progName, ERR_MSG, strerror(errno));
#ifdef DEBUG_VERBOSE
			ISSUE_ERR();
#endif
			return  -1;
		}

		if (((drId != -1) ? ((long)blkde.driver == drvrAddr) :
		     (!strncmp((char *)devDescr, blkde.name,
			       sizeof(blkde.name)))) &&
		    (long)blkde.driver != 0) {	/* bingo! */
			if (devDescr && (devCntr < elAm)) {
				devDescr[devCntr].dev_id = cntr;
				strncpy(devDescr[devCntr].dev_name, blkde.name,
					sizeof(devDescr[devCntr].dev_name));
				devDescr[devCntr].dev_type = BLOCK_DEV;
				devDescr[devCntr].dev_drvrid =
				    ((long)blkde.driver -
				     dtbl) / sizeof(struct driver_entry);
			}
			devCntr++;
		}
	} while (++cntr < nbdevs);

	return  devCntr;
}

/**
 * @brief Get information about the driver @e dnam
 *
 * @param dnam -- name of the driver to find (*.ko), or NULL.
 * @param dd   -- found driver description will be placed in this buffer.
 * @param ddsz -- buffer size in bytes
 *
 * In LynxOS - more than one driver [with the same name] can be installed in
 * the system at the same time.
 *
 * In Linux it's not possible.
 *
 * If @ref dd is not NULL, then it points to the buffer, where found driver
 * ID's will go.
 * @ref ddsz is the size of this buffer. If all that user wants - is to find
 * out how many drivers with the name @ref dnam are currently installed in the
 * system, then second parameter (i.e. @ref dd) should be NULL.
 *
 * Special case, when @ref dnam is a NULL pointer is possible. In this case
 * second parameter should contain the ID of the driver that should be find in
 * the system in 'dd[0].driv_id' field. So in this case searching is based
 * on the provided driver ID, but not on the driver name.
 *
 * @return number of found drivers - if success
 * @return -1                      - if internal error occurs.
 *                                   Error info is printed in stderr.
 */
int find_driver(char *dnam, driv_desc *dd, int ddsz)
{
	int devmem = 0;		/* operating system table file descriptor */

	long dtbl = info(_I_DRIVERS);	/* direct addr of driver tbl */
	long devAddr;		/* device table real address */

	long ndrivs = info(_I_NDRIVERS);	/* size of driver table */
	struct driver_entry dde;	/* device driver entry */

	int dcntr = 0;		/* found driver counter */
	int cntr = 0;
	int elAm = ddsz / sizeof(driv_desc);	/* buffer element amount */

	/* open operating system table by opening the memory pseudo-device */
	if ((devmem = seek_lynx_os_tab((!dnam) ? _I_DIRECT : _I_DRIVERS)) == -1)
		return  -1;

	if (!dnam)		/* compute driver table physical address */
		devAddr = dtbl + sizeof(struct driver_entry) * dd[0].driv_id;

	/* read driver info */
	do {
		if (read(devmem, &dde, sizeof(struct driver_entry)) == -1) {
			fprintf(stderr, "\n%s: %s[ %s] read driver table"
				" entry failed.\n", progName, ERR_MSG,
				strerror(errno));
#ifdef DEBUG_VERBOSE
			ISSUE_ERR();
#endif
			return  -1;
		}

		if (((!dnam) ? (devAddr == (long)dde.next - sizeof(dde))
		     : (!strcmp(dde.name, basename(dnam))))
		    && (dde.dev_type != -1)) {
			if (dd && (dcntr < elAm)) {
				dd[dcntr].driv_id = cntr;
				strncpy(dd[dcntr].driv_name, dde.name,
					sizeof(dd[dcntr].driv_name));
				dd[dcntr].usecount = dde.usecount;
			}

			dcntr++;

			if (!dnam)
				break;
		}
	} while (++cntr < ndrivs);

	return dcntr;
}
