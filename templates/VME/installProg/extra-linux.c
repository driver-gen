/**
 * @file extra-linux.c
 *
 * @brief
 *
 * <long description>
 *
 * @author Copyright (C) 2009 CERN CO/HT Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 15/06/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */
#define _GNU_SOURCE /* asprintf rocks */
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/dir.h>		/* for struct direct and readdir */
#include <sys/ioctl.h>
#include <string.h>
#include <fcntl.h>
#include <libgen.h>
#include <errno.h>
#include <elf.h>    /* for endianity business */

#include <include/general_ioctl.h> /* for _GIOCTL_* && co. ioctls */
#include "ModuleHeader.h"	   /* for DevInfo_t && friends */
#include "utils.h"
#include "extra-linux.h"

#define streq(a,b) (strcmp((a),(b)) == 0)

/* calls system call */
extern long init_module(void *, unsigned long, const char *);
extern long delete_module(const char *, unsigned int);

/**
 * @brief We use error numbers in a loose translation...
 *
 * @param err
 *
 * @return
 */
static const char* moderror(int err)
{
	switch (err) {
	case ENOEXEC:
		return "Invalid module format";
	case ENOENT:
		return "Unknown symbol in module";
	case ESRCH:
		return "Module has wrong symbol version";
	case EINVAL:
		return "Invalid parameters";
	default:
		return strerror(err);
	}
}

/**
 * @brief Read .ko and place it in the local buffer
 *
 * @param filename -- .ko file name
 * @param size     -- its size will be placed here
 *
 * @return NULL               - if error
 * @return locally copied .ko - if success
 */
static void* grab_file(const char *filename, unsigned long *size)
{
	unsigned int max = 16384;
	int ret, fd;
	void *buffer = malloc(max);
	if (!buffer)
		return NULL;

	if (streq(filename, "-"))
		fd = dup(STDIN_FILENO);
	else
		fd = open(filename, O_RDONLY, 0);

	if (fd < 0)
		return NULL;

	*size = 0;
	while ((ret = read(fd, buffer + *size, max - *size)) > 0) {
		*size += ret;
		if (*size == max)
			buffer = realloc(buffer, max *= 2);
	}
	if (ret < 0) {
		free(buffer);
		buffer = NULL;
	}
	close(fd);
	return buffer;
}

/**
 * @brief Extract Driver name from the .ko filename
 *
 * @param koptr -- .ko filename
 *
 * @b NOTE Returned pointer should be freed by the caller afterwards.
 *
 * @b Examples: /path/to/driver/TricD.ko -->  TricD
 *              ./RfIrqTestD.ko          -->  RfIrqTestD
 *              TricS.ko                 -->  TricS
 *              TricSko                  -->  [ WRONG ]
 *
 * @return driver name - if OK
 * @return NULL        - failed
 */
char* extract_driver_name(char *koptr)
{
	char *ptr;
	int am;

	/* get rid of path if any */
	koptr = basename(koptr);

	/* should have suffix */
	ptr = strchr(koptr, '.');
	if (!ptr)
		return NULL;

	/* get rid of suffix and create driver name */
	am = ptr - koptr;
	asprintf(&ptr, "%.*s", am, koptr);

	return ptr;
}

/**
 * @brief Find device(s) by name
 *
 * @param name -- device name
 * @param res  -- storage. major numbers goes here. Can be NULL
 * @param sz   -- storage size in elements
 *
 * If @ref res is NULL - only number of found devices returned.
 * Can be more then one device with the same name.
 *
 * @return how many devices with the @e name found
 */
int proc_dev_find_by_name(char *name, int *res, int sz)
{
	char buff[32], *ptr;
	int major, cntr = 0;
	FILE *procfd = fopen("/proc/devices", "r");

	if (!procfd) {
		perror("Can't open /proc/devices");
		return 0;
	}

	while (fgets(buff, sizeof(buff), procfd)) {
		ptr = buff;
		/* get major && name */
		major = atoi(strsep(&ptr, " "));
		if (!ptr)
			continue;
		if (!strncmp(name, ptr, strlen(name)) &&
		    strlen(name) == strlen(ptr) - 1 /*exclude \n*/) {
			/* bingo! */
			cntr++;
			if (res && cntr <= sz) { /* give them to the user */
				*res = major;
				res++;
			}
		}
	}

	fclose(procfd);
	return cntr;
}

/**
 * @brief Find device by major
 *
 * @param major -- device major number to search for
 *
 * @b NOTE Returned pointer should be freed by the caller afterwards.
 *
 * @return NULL        - not found
 * @return device name - found
 */
char* proc_dev_find_by_major(int major)
{
	char buff[64];
	char *ptr = NULL;
	int _major = 0;
	FILE *procfd = fopen("/proc/devices", "r");

	if (!procfd) {
		perror("Can't open /proc/devices");
		return NULL;
	}

	while (fgets(buff, sizeof(buff), procfd)) {
		ptr = buff;
		/* get major && name */
		_major = atoi(strsep(&ptr, " "));
		if (_major == major) {
			asprintf(&ptr, "%.*s", strlen(ptr)-1/*get rid of \n*/,
				 ptr);
			goto found; /* found. no need to search more */
		}
	}

	major = -1; /* not found */
	ptr = NULL;

 found:
	fclose(procfd);
	return ptr;
}

/**
 * @brief Lynx-like driver installation routine.
 *
 * @param drvr_fn -- .ko object
 * @param type    -- @ref BLOCKDRIVER or @ref CHARDRIVER
 *
 * Will install the driver in the system.
 * Same driver _cannot_ be installed several times.
 *
 * @return driver ID (major number) - if successful
 * @return -1                       - if not
 */
int dr_install(char *drvr_fn, int type)
{
	char *dnm = NULL;	/* driver name */
	int rc = 0;		/* return code */
	char *node_nm = NULL;	/* driver node name */

	void *dfile = NULL; /* our .ko */
	unsigned long len;
	char *options = NULL; /* driver option */
	long int ret;
	int major_num = 0;
	int devn; /* major/minor device number */
	char *hotplug = NULL;

	dnm = extract_driver_name(drvr_fn);
	if (!dnm) {
		fprintf(stderr, "Wrong [.ko] object file name\n");
		return -1;
	}

	/* put .ko in the local buffer */
	dfile = grab_file(drvr_fn, &len);
	if (!dfile) {
		fprintf(stderr, "Can't read '%s': %s\n",
			drvr_fn, strerror(errno));
		rc = -1;
		goto dr_install_exit;
	}

	/* driver options */
	asprintf(&options, "dname=%s drivergen=1", dnm);

	disable_kernel_hotplug(&hotplug); /* move mdev out-of-the-way */

	/* insert module in the kernel */
	if ( (ret = init_module(dfile, len, options)) != 0 ) {
		fprintf(stderr, "Error inserting '%s' module: %li %s\n",
			drvr_fn, ret, moderror(errno));
		rc = -1;
		goto dr_install_exit;
	}

	/* check if he made it */
	proc_dev_find_by_name(dnm, &major_num, 1);

	if (!major_num) {
		/* we fucked */
		errno = ENODEV;
		rc = -1;
		goto dr_install_exit;
	}

	/* create service entry point */
	asprintf(&node_nm, "/dev/%s", dnm);
	unlink(node_nm); /* if already exist delete it */
	devn = makedev(major_num, 0);
	if (mknod(node_nm, S_IFCHR | 0666, devn)) {
		perror("Can't create service node");
		rc = -1;
		goto dr_install_exit;
	}


	/* driverID (major num) will be used by the cdv_install() */
	rc = major_num;

 dr_install_exit:
	if (hotplug) {
		/* hotplug was disabled -- re-enable it back */
		set_kernel_hotplug(hotplug);
		free(hotplug);
	}
	if (dnm)     free(dnm);
	if (options) free(options);
	if (dfile)   free(dfile);
	if (node_nm) free(node_nm);
	return rc;
}

/**
 * @brief Lynx call wrapper. Will unload the driver from the kernel
 *
 * @param id -- driver ID to uninstall (major number)
 *
 * @return zero on success or a negative number on failure
 */
int dr_uninstall(int id)
{
	FILE *module_list = NULL;
	char line[10240], name[64];
	unsigned long size, refs;
	int scanned;
	int mam;
	int ret = 0;		/* return code. good by default */
	char *modname = proc_dev_find_by_major(id);

	if (!modname) {
		fprintf(stderr, "%s(): Can't find driver for deivce"
			" [major %d]\n", __func__, id);
		ret = -1;
		goto outbad;
	}

	/* get all devices, driver controls */
	mam = get_module_amount(id);
	if (mam < 0) {
		perror("Can't get module amount");
		ret = -1;
		goto outbad;
	}

	if (mam) {
		/* driver is still controlling some modules */
		ret = -1;
		goto outbad;
	}

	module_list = fopen("/proc/modules", "r");
	if (!module_list) {
		fprintf(stderr, "can't open /proc/modules: %s\n",
			strerror(errno));
		ret = -1;
		goto outbad;
	}

	while (fgets(line, sizeof(line)-1, module_list) != NULL) {

		if (strchr(line, '\n') == NULL) {
			fprintf(stderr, "V. v. long line broke rmmod.\n");
			ret = -1;
			goto outbad;
		}

		scanned = sscanf(line, "%s %lu %lu", name, &size, &refs);
		if (!strstr(name, modname))
			continue;

		if (scanned < 2) {
			fprintf(stderr,
				"Unknown format in /proc/modules: %s\n", line);
			ret = -1;
			goto outbad;
		}

		if (scanned == 2) {
			fprintf(stderr,
				"Kernel does not have unload support.\n");
			ret = -1;
			goto outbad;
		}

		if (refs != 0) {
			/* module is in use */
			errno = EBUSY;
			ret = -1;
			goto outbad;
		}
		goto outgood;
	}

	fprintf(stderr, "Module %s does not exist in /proc/modules\n", modname);
	refs = -1;
	goto outbad;

 outgood:
	ret = delete_module(name, O_NONBLOCK|O_EXCL);
 outbad:
	if (module_list) fclose(module_list);
	if (modname) free(modname);
	return ret;
}


/**
 * @brief Lynx call wrapper for character device installation.
 *
 * @param path      -- info file
 * @param driver_id -- driver ID (major number) returned by @e dr_install()
 * @param extra     -- extra parameter for the driver. not used
 *
 * Called for each device driver will control.
 *
 * In LynxOS, each time @e cdv_install() or @e bdv_install() are called by the
 * user-space installation programm, driver installation vector from @b dldd
 * structure is called. It returns statics table, that in turn is used in every
 * entry point of @b dldd vector table.
 *
 * @return major device ID - if successful
 * @return -1              - if not.
 */
int cdv_install(char *path, int driver_id, int extra)
{
	int dfd  = -1;	 /* driver file descriptor */
	int maj_num = -1;
	char nn[32]; /* device node name */
	char *hp;

	if (!find_device_nodes(driver_id, &nn, 1)) {
		fprintf(stderr, "Can't find driver node (maj %d)\n",
			driver_id);
		return -1;
	}

	if ((dfd = open(nn, O_RDWR)) == -1) {
		perror("Can't open driver node");
		return -1;
	}

	disable_kernel_hotplug(&hp); /* move mdev out-of-the-way */

	maj_num = ioctl(dfd, _GIOCTL_CDV_INSTALL, path);
	if (maj_num < 0) {
		perror("_GIOCTL_CDV_INSTALL ioctl fails."
		       " Can't get major number");
		maj_num = -1;
	}

	if (hp) { set_kernel_hotplug(hp); free(hp); }
	close(dfd);
	return maj_num;
}

int bdv_uninstall(int did)
{
	return -1; /* we don't work with block devices yet */
}

/**
 * @brief Driver uninstallation procedure
 *
 * @param dev_id -- device id (major number)
 *
 * @return zero on success or a negative number on failure
 */
int cdv_uninstall(int dev_id)
{
	char *dev_name = proc_dev_find_by_major(dev_id);
	int drid;		/* driver ID */
	char *drvr_name = NULL;
	char nn[32]; /* service node name */
	int dfd = -1;     /* driver file descriptor */
	int ret = 0;

	if (!dev_name) {
		fprintf(stderr, "%s(): Can't find driver for deivce"
			" [major %d]\n", __func__, dev_id);
		ret = -1;
		goto out;
	}

	/* get driver name from device name */
	drvr_name = device2drvr(dev_name);
	if (!drvr_name) {
		fprintf(stderr, "Wrong device name [%s] detected\n",
			dev_name);
		ret = -1;
		goto out;
	}

	/* get driver major number */
	if (!proc_dev_find_by_name(drvr_name, &drid, 1)) {
		fprintf(stderr, "Can't find driver for %s deivce\n",
			dev_name);
		ret = -1;
		goto out;
	}

	/* get service entry */
	if (!find_device_nodes(drid, &nn, 1)) {
		fprintf(stderr, "Can't find driver service node (maj %d)\n",
			drid);
		ret = -1;
		goto out;
	}

	if ((dfd = open(nn, O_RDWR)) == -1) {
		perror("Can't open service driver node");
		ret = -1;
		goto out;
	}

	if (ioctl(dfd, _GIOCTL_CDV_UNINSTALL, &dev_id)) {
		perror("_GIOCTL_CDV_UNINSTALL failed");
		ret = -1;
		goto out;
	}

 out:
	if (dev_name)  free(dev_name);
	if (drvr_name) free(drvr_name);
	if (dfd != -1) close(dfd);
	return ret;
}


//!< swap bytes
static __inline__ void __endian(const void *src, void *dest, unsigned int size)
{
  unsigned int i;
  for (i = 0; i < size; i++)
    ((unsigned char*)dest)[i] = ((unsigned char*)src)[size - i - 1];
}

/**
 * @brief To find out current endianity.
 *
 * @param none
 *
 * @return current endianity - in case of success,
 *         or generate an abnormal process abort - in case of failure.
 */
static inline int __my_endian(void)
{
  static int my_endian = ELFDATANONE;
  union { short s; char c[2]; } endian_test;

  if (my_endian != ELFDATANONE)	/* already defined */
    return(my_endian);

  endian_test.s = 1;
  if (endian_test.c[1] == 1) my_endian = ELFDATA2MSB;
  else if (endian_test.c[0] == 1) my_endian = ELFDATA2LSB;
  else abort();

  return(my_endian);
}

/*! @name BE <-> LE convertion
 */
//@{
#define _ENDIAN(x)				\
({						\
  typeof(x) __x;				\
  typeof(x) __val = (x);			\
    __endian(&(__val), &(__x), sizeof(__x));	\
  __x;						\
})
//@}

/**
 * @brief Converts Device Info Table from BE to LE
 *
 * @param src -- source. Device info table to convert
 * @param dst -- destanation. If not NULL - converted table goes here.
 *               If NULL - @ref src will be used as destanation.
 *
 * Info file stores all data in the BE format. It should be converted it LE
 * in order to be able to use it.
 *
 * @e NOTE
 * If dsc is NULL - current info table (in @ref src parameter) will be
 * overwritten!
 *
 * @return pointer to LE-converted info table
 */
DevInfo_t* convert_dit(DevInfo_t *src, DevInfo_t *dsc)
{
	int i;
	DevInfo_t *res = (dsc)?dsc:src; /* where results will go */

	/* addr1 member */
	res->addr1.baseAddr  = _ENDIAN(src->addr1.baseAddr);
	res->addr1.range     = _ENDIAN(src->addr1.range);
	res->addr1.increment = _ENDIAN(src->addr1.increment);
	res->addr1.dpSize    = _ENDIAN(src->addr1.dpSize);
	res->addr1.addrModif = _ENDIAN(src->addr1.addrModif);

	/* addr2 member */
	res->addr2.baseAddr  = _ENDIAN(src->addr2.baseAddr);
	res->addr2.range     = _ENDIAN(src->addr2.range);
	res->addr2.increment = _ENDIAN(src->addr2.increment);
	res->addr2.dpSize    = _ENDIAN(src->addr2.dpSize);
	res->addr2.addrModif = _ENDIAN(src->addr2.addrModif);

	res->mtn        = _ENDIAN(src->mtn);
	res->mlun       = _ENDIAN(src->mlun);
	res->debugFlag  = _ENDIAN(src->debugFlag);
	/* 'opaque' is char */
	res->iLevel     = _ENDIAN(src->iLevel);
	res->iVector    = _ENDIAN(src->iVector);
	res->iVectorInc = _ENDIAN(src->iVectorInc);
	res->chan       = _ENDIAN(src->chan);
	res->chrindex   = _ENDIAN(src->chrindex);
	res->gen_date   = _ENDIAN(src->gen_date);
	/* 'dg_vers' is char */
	res->regAmount  = _ENDIAN(src->regAmount);

	for (i = 0; i < res->regAmount; i++) {
	res->regDesc[i].regId        = _ENDIAN(src->regDesc[i].regId);
	/* 'regName' is char */
	/* 'busType' is char */
	res->regDesc[i].regType      = _ENDIAN(src->regDesc[i].regType);
	res->regDesc[i].regSize      = _ENDIAN(src->regDesc[i].regSize);
	/* 'b_a' is char */
	res->regDesc[i].bid          = _ENDIAN(src->regDesc[i].bid);
	res->regDesc[i].regOffset    = _ENDIAN(src->regDesc[i].regOffset);
	res->regDesc[i].regDepth     = _ENDIAN(src->regDesc[i].regDepth);
	res->regDesc[i].regtl        = _ENDIAN(src->regDesc[i].regtl);
	res->regDesc[i].regar        = _ENDIAN(src->regDesc[i].regar);
	res->regDesc[i].rwIoctlOpNum = _ENDIAN(src->regDesc[i].rwIoctlOpNum);
	}

	res->maxRegSz   = _ENDIAN(src->maxRegSz);
	res->blkAmount  = _ENDIAN(src->blkAmount);

	for (i = 0; i < res->blkAmount; i++) {
	res->blkDesc[i].block       = _ENDIAN(src->blkDesc[i].block);
	res->blkDesc[i].blkBaseAddr = _ENDIAN(src->blkDesc[i].blkBaseAddr);
	res->blkDesc[i].offset      = _ENDIAN(src->blkDesc[i].offset);
	res->blkDesc[i].blksz_drvr  = _ENDIAN(src->blkDesc[i].blksz_drvr);
	res->blkDesc[i].blksz_sim   = _ENDIAN(src->blkDesc[i].blksz_sim);
	res->blkDesc[i].reg_am      = _ENDIAN(src->blkDesc[i].reg_am);
	}

	/* 'checksum' is char */

	return res;
}

/**
 * @brief Search for device nodes by major in /dev directory.
 *
 * @param maj -- major device ID to find nodes for
 * @param nn  -- massive of the strings to store found device node names.
 *               Each string in this massive @b should be 32 bytes long.
 * @param sz  -- massive size, i.e. how many strings of 32 bytes long are in
 *               this massive.
 *
 * @return Number of found device nodes.
 */
int find_device_nodes(int maj, char (*nn)[32], int sz)
{
	DIR *dir = opendir("/dev");
	struct direct *direntry = NULL;
	int nodeCntr = 0;
	char fname[64];
	struct stat fstat;

	while ( (direntry = readdir(dir)) ) {
		snprintf(fname, sizeof(fname), "/dev/%s", direntry->d_name);
		stat(fname, &fstat);
		/* st_rdev holds [Major | Minor] numbers */
		if (major(fstat.st_rdev) == maj) { /* bingo */
			if (nn && (nodeCntr < sz))
				strncpy(nn[nodeCntr], fname,
					sizeof(nn[nodeCntr]));
			++nodeCntr;
		}
	}

	closedir(dir);
	return nodeCntr;
}

/**
 * @brief Convert device node names to driver node name
 *
 * @param dev_name -- device node name
 *
 * @b NOTE Returned pointer should be freed by the caller afterwards.
 *
 * Examples:
 * SharedCibcD13.info --> SharedCibcD
 * RfIrqTestS5.info   --> RfIrqTestS
 * TricD37.info       --> TricD
 *
 * @return driver node name - if OK
 * @return NULL             - wrong device name was provided by the caller
 */
char* device2drvr(char *dev_name)
{
	char *ptr;
	int am;

	/* get rid of path if any */
	dev_name = basename(dev_name);

	/* find first delim */
	ptr = strchr(dev_name, '.');
	if (!ptr)
		return NULL;
	--ptr;
	/* skip LUN */
	while (isdigit(*ptr)) --ptr;

	am = (ptr+1) - dev_name;
	asprintf(&ptr, "%.*s", am, dev_name);

	return ptr;
}

/**
 * @brief Open (O_RDWR) service driver node based on driver major number
 *
 * @param maj -- driver major number
 *
 * User should close it afterwards...
 *
 * @return opened file descriptor -- if OK
 * @return -1                     -- if FAILED
 */
int open_srv_drvr_node(int maj)
{
	char nn[32];  /* node name */
	int dfd; /* driver file descriptor */

	/* get service entry */
	if (!find_device_nodes(maj, &nn, 1)) {
		fprintf(stderr, "Can't find driver service node (maj %d)\n",
			maj);
		return -1;
	}

	dfd = open(nn, O_RDWR);
	if (dfd == -1)
		perror("Can't open service driver node");

	return dfd;
}

/**
 * @brief Get module amount, currently controlled by the driver
 *
 * @param maj -- driver major number
 *
 * Handy routine, ioctl-ing the driver.
 *
 * @return module amount, currently controlled by the driver (can be 0)
 * @return negative value -- if failed
 */
int get_module_amount(int maj)
{
	int ret;   /* return code */
	int dfd = open_srv_drvr_node(maj); /* driver file descriptor */

	if (dfd < 0)
		return dfd;

	ret = ioctl(dfd, _GIOCTL_GET_MOD_AM);

	close(dfd);
	return ret;
}

/**
 * @brief Disable udev/mdev
 *
 * @param ptr -- pointer address will hold disabled hotplug prog name
 *               (normally mdev/udev)
 *
 * If failed (i.e. returned -1) ptr is set to NULL
 *
 * @return  0 -- hotplug disabled
 * @return -1 -- failed to disable hotplug
 */
int disable_kernel_hotplug(char **ptr)
{
	char buf[32] = { 0 };
	int fd = open("/proc/sys/kernel/hotplug", O_RDWR);

	*ptr = NULL;

	if (fd == -1) {
		perror("Can't open /proc/sys/kernel/hotplug");
		return -1;
	}

	if (read(fd, buf, sizeof(buf)) == -1) {
		close(fd);
		perror("Can't read /proc/sys/kernel/hotplug");
		return -1;
	}

	asprintf(ptr, "%s", buf);

	strcpy(buf, ""); /* disable the usermode helper hotplug mechanism */
	if (write(fd, buf, 2) == -1) {
		close(fd);
		free(*ptr);
		perror("Can't write /proc/sys/kernel/hotplug");
		*ptr = NULL;
		return -1;
	}

	close(fd);
	return 0;
}

/**
 * @brief Enable kernel hotplug helper
 *
 * @param hppath -- what to put as a hotplug helper for the kernel
 *                  (mdev/udev normally)
 *
 * <long-description>
 *
 * @return  0 -- all OK
 * @return -1 -- FAILED
 */
int set_kernel_hotplug(char *hppath)
{
	int fd = open("/proc/sys/kernel/hotplug", O_WRONLY);

	if (fd == -1) {
		perror("Can't open /proc/sys/kernel/hotplug");
		return -1;
	}

	if (write(fd, hppath, strlen(hppath)) == -1) {
		close(fd);
		perror("Can't write /proc/sys/kernel/hotplug");
		return -1;
	}

	close(fd);
	return 0;
}
