/**
 * @file modinst.h
 *
 * @brief
 *
 * @author Copyright (C) 2009 CERN CO/HT Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 15/06/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifndef _MODULE_INSTALL_H_INCLUDE_
#define _MODULE_INSTALL_H_INCLUDE_

#include "ModuleHeader.h"
#include "utils.h"
#include "lklist.h" /* Linux kernel list */

/* max amount of minor numbers */
#define MI_MAX_CHAN 255

/* function return status */
typedef enum _tagResults {
  RES_OK   = 0,	/*  */
  RES_FAIL = 1	/*  */
} rslts;

/* Module description */
struct mod_descr {
  struct list_head mod_list; /* module linked list */
  char      name[DDNMSZ]; /* path to device info */
  int       lun;     /* Logical Unit Number assigned to the module */
  uint      isDrvr;  /* Driver or Simulator */
  int       chan;    /* how many file handles (channels) should be created */
  DevInfo_t infoTab; /* device information table */
};

#endif /* _MODULE_INSTALL_H_INCLUDE_ */
