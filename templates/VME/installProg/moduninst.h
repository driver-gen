/**
 * @file moduninst.h
 *
 * @brief
 *
 * <long description>
 *
 * @author Copyright (C) 2009 CERN CO/HT Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 15/06/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */

#ifndef _MODULE_DE_INSTALL_H_INCLUDE_
#define _MODULE_DE_INSTALL_H_INCLUDE_

#include "lklist.h" /* Linux kernel list */

/* Module description */
typedef struct _tagModuleDescr {
  int       lun;       /* module Logical Unit Number */
  uint      isDrvr;    /* Driver or Simulator */
} ModuleDescr;

/* Structure contains general information about VME crate */
typedef struct _tagCrateConfig {
  int	      maxNb;              /* MAX number of modules */
  int	      actualNb;           /* actual number of modules */
  ModuleDescr modDesc[MAXMODNUM]; /* module information */
} CrateConfig;

#endif /* _MODULE_DE_INSTALL_H_INCLUDE_ */
