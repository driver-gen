
/**
 * @brief Entry point function. Initializes minor devices.
 *
 * @param statPtr -- statics table pointer
 * @param devno   -- contains major/minor dev numbers
 * @param flp     -- file pointer
 *
 * @return OK     -- if succeed.
 * @return SYSERR -- in case of failure.
 */
int <sys_lower>_open(register <mod_name>Statics_t *statPtr,
		     int devno, struct file *flp)
{
	int minN = minor(devno); /* get minor device number */
	int proceed;	/* if standard code execution should be
			   proceed after call to
			   user entry point function  */
	int usrcoco;	/* completion code of user entry point function */

	if (minN >= <mod_name>_MAX_NUM_CARDS) {
		kkprintf("%sMinor number (%d) exceeds MAX allowed"
			 " cards number (%d)\n",
			 ERR_MSG, minN, <mod_name>_MAX_NUM_CARDS);
		pseterr(ENXIO);
		return SYSERR; /* -1 */
	}

	kkprintf("<sys_lower>: Open Logical Unit %d.\n", statPtr->info->mlun);

	/* user entry point function call */
	usrcoco = <sys_lower>UserOpen(&proceed, statPtr, devno, flp);
	if (!proceed) /* all done by user */
		return usrcoco;

	kkprintf("<sys_lower>: Done\n\n");

	return OK; /* 0 */
}
