
/**
 * @brief Driver uninstallation.
 *
 * @param statPtr -- statics table
 *
 * Deallocates shared memory and clears used interrupt vectors.
 *
 * @return OK     - if succeed.
 * @return SYSERR - in case of failure.
 */
int <sys_lower>_uninstall(<mod_name>Statics_t *statPtr)
{
	int rc;

	/*
	  Should we disable interrupts?
	  int ps;
	  disable(ps);
	*/

	kkprintf("<sys_lower>: Uninstall...\n");

	/* user entry point function call */
	rc = <sys_lower>UserUnInst(NULL, statPtr);
	if (rc == SYSERR )
		return rc; /* -1 */

