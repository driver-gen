	/* For device driver resources, resource allocation must be done during
	   the installation routine (e.g.: decriptor of system call interface:
	   semaphore, system thread, etc.) */

	/* 0x0 */
	kkprintf("<sys_lower>: Statics data structure allocation - ");
	if(!(statPtr = (<mod_name>Statics_t*)
	     sysbrk((long)sizeof(<mod_name>Statics_t)))) {
		/* Horrible, impossible failure. */
		kkprintf("FAILED\n");
		pseterr(ENOMEM);
		INST_FAIL();
		return (char*)SYSERR; /* -1 */
	}
	kkprintf("OK\n");

	/* 0x1 */
	kkprintf("<sys_lower>: Hardware topology structure allocation - ");
	if( !(statPtr->card = (<mod_name>Topology_t*)
	      sysbrk((long)sizeof(<mod_name>Topology_t))) ) {
		/* Horrible, impossible failure. */
		kkprintf("FAILED\n");
		pseterr(ENOMEM);
		INST_FAIL();
		return (char*)SYSERR; /* -1 */
	}
	bzero((char*)statPtr->card, sizeof(<mod_name>Topology_t));
	kkprintf("OK\n");

	/* 0x2 */
	if (sizeof(<mod_name>Writeonly_t) > 0) {
		kkprintf("<sys_lower>: Writeonly structure allocation - ");
		if ( !(statPtr->card->wo = (<mod_name>Writeonly_t*)
		       sysbrk((long)sizeof(<mod_name>Writeonly_t))) ) {
			/* Horrible, impossible failure. */
			kkprintf("FAILED\n");
			pseterr(ENOMEM);
			INST_FAIL();
			return (char*)SYSERR;	/* -1 */
		}
		bzero((char*)statPtr->card->wo, sizeof(<mod_name>Writeonly_t));
		kkprintf("OK\n");
	} else {
		statPtr->card->wo = NULL;
	}

	/* 0x3 */
	if (sizeof(<mod_name>Extraneous_t) > 0) {
		kkprintf("<sys_lower>: Extraneous structure allocation - ");
		if ( !(statPtr->card->ex = (<mod_name>Extraneous_t*)
		       sysbrk((long)sizeof(<mod_name>Extraneous_t))) ) {
			/* Horrible, impossible failure. */
			kkprintf("FAILED\n");
			pseterr(ENOMEM);
			INST_FAIL();
			return (char*)SYSERR;	/* -1 */
		}
		bzero((char*)statPtr->card->ex, sizeof(<mod_name>Extraneous_t));
		kkprintf("OK\n");
	} else {
		statPtr->card->ex = NULL;
	}

	/* 0x4 */
