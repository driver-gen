	if (info->chrindex == -1)
		kkprintf("<sys_lower>: %sModule presence will NOT be"
			 " checked!\n", WARNING_MSG);
	else
		kkprintf("<sys_lower>: Module presence will be checked during"
			 " device address space mapping by reading '%s'"
			 " register @0x%x offset.\n",
			 info->regDesc[info->chrindex].regName,
			 info->regDesc[info->chrindex].regOffset);

	/* Check hardware module and abort installation if hardware not OK. */
	kkprintf("<sys_lower>: Mapping device address space - ");
	res = MapAndCheckDevice(info);
	if (res < 0) {
		kkprintf("FAILED\n");
		pseterr(res*(-1));	/* set error number for user */
		INST_FAIL();
		return (char*)SYSERR; /* -1 */
	} else if (res > 0) {
		kkprintf("OK\n");
	} else
		kkprintf("Nothing to map (no Addr1 and Addr2 defined)\n");

