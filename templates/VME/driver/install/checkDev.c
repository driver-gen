/* to suppress implisit declaration warnings */
#ifdef __Lynx__
extern unsigned long find_controller _AP((unsigned long vmeaddr, unsigned long len, unsigned long am, unsigned long offset, unsigned long size, struct pdparam_master  *param));
#endif

/**
 * @brief Free mapped VME resources
 *
 * @param dit -- info table
 *
 * @return  0 - on success
 * @return -1 - if fails
 */
int __attribute__((weak)) unmapVmeAddr(DevInfo_t *dit)
{
	int i, rc = 0;
	AddrInfo_t *aip = &dit->addr1;

	for (i = 0; i < ASA; i++) {
		if (aip->baseAddr != ADCG__NO_ADDRESS)
			if (return_controller(aip->baseAddr, aip->range))
				rc = -1;
		aip++;
	}

	return rc;
}

/*
 * In lynx, find_controller accepts 0 as @size as a way of avoiding the
 * test access after setting up the mapping. In Linux, however,
 * passing 0 as @size will always fail, since this parameter is in fact
 * the one that provides the data width for the whole mapping, and
 * will therefore be detected as invalid; We deal with this by taking
 * the data width from the mapping's description.
 */
#ifdef __linux__
static inline void check_datawidth(unsigned long *data_width, AddrInfo_t *map)
{
	if (!*data_width)
		*data_width = map->dpSize / 8;
}
#else
static inline void check_datawidth(unsigned long *data_width, AddrInfo_t *map)
{
}
#endif /* __linux__ */

/**
 * @brief Maps VME address space into PCI address space.
 *
 * @param info     -- dev info table
 * @param map	   -- mapping descriptor
 * @param err      -- error number will go here in case of error
 *
 * @return -1			 - if FAILS.
 * @return mapped device address - if SUCCEED.
 */
static kaddr_t MapVMEaddress(DevInfo_t *info, AddrInfo_t *map, int *err)

{
	struct pdparam_master param; /* CES structure containing VME access
					parameters */
	int		offst = 0;  /* check address offset */
	unsigned long	size  = 0;  /* size in bytes of read-test zone
				       (0 to disable read test). If more
				       than 4 bytes, then value will be >> 2
				       and obtained number of elements
				       (4 bytes each) will be read from
				       memory */
	kaddr_t k_addr;  /*  */
	kaddr_t		infoBase	= map->baseAddr;
	unsigned long	len		= map->range;
	vmeam_t		am		= map->addrModif;

	/* Compute address in system space */
	/* CES: build an address window (64 kbyte) for
	   VME A16-D16 or A24-D32 or A32-D32 A24-D08 accesses
	   (depend on config) */
	memset(&param, 0, sizeof(param)); /* safety zeroing */
	param.iack   = 1; /* VME iack. 0: IACK pages, 1: not IACK pages */
	param.rdpref = 0; /* VME read prefetch option. 0: disable, 1: enable */
	param.wrpost = 0; /* VME write posting option. 0: disable, 1: enable */
	param.swap   = SINGLE_AUTO_SWAP; /* VME swap option: one of
					    SINGLE_NO_SWAP,
					    SINGLE_AUTO_SWAP,
					    SINGLE_WORD_SWAP,
					    SINGLE_BYTEWORD_SWAP */
	param.sgmin  = 0;  /* reserved, _must_ be 0  */
	param.dum[0] = VME_PG_SHARED; /* page qualifier (shared/private)
					 In this case - window is sharable */
	param.dum[1] = 0;             /* XPC ADP-type on RIO3 */
	param.dum[2] = 0;             /* reserved, _must_ be 0 */

	/* check VME Address Modifier code */
	switch (am) {
	case DG_AM_SH: /* short    */
	case DG_AM_ST: /* standard */
	case DG_AM_EX: /* extended */
	case DG_AM_CR: /* Configuration Register */
		break; /* we cool */
	default:
		kkprintf("%sUNKNOWN address modifier (%d)", ERR_MSG, am);
		*err = EINVAL;
		return -1;
	}

	/* module checking will be performed */
	if (info->chrindex != -1) {
		offst = info->regDesc[info->chrindex].regOffset;
		size  = info->regDesc[info->chrindex].regSize;
	}

	if (info->debugFlag & DBGINSTL) {
		kkprintf("\nfind_controller() call parameters are:\n");
		kkprintf("+--------------------------------------\n");
		kkprintf("| Base Addr  => 0x%x\n", infoBase);
		kkprintf("| Addr Range => 0x%lx\n", len);
		kkprintf("| Addr Modifier => '%s' (0x%x)\n",
			 (am == DG_AM_SH)?"SHort":(am == DG_AM_ST)?"STandard":
			 (am == DG_AM_EX)?"EXtended":(am == DG_AM_CR)?
			 "Configuration Register":"UNKNOWN", am);
		if (info->chrindex == -1)
			kkprintf("| Module checking will NOT be performed!\n");
		else {
			kkprintf("| Offset from VME Base Address at which a"
				 " read test will be made is 0x%x bytes.\n",
				 offst);
			kkprintf("| Size of read-test zone is %ld bytes"
				 " long.\n", size);
		}
		kkprintf("+--------------------------------------\n\n");
	}

	/*
	 * the behaviour of @size in find_controller differs
	 * among platforms: make sure anything blows up
	 */
	check_datawidth(&size, map);

	if (info->debugFlag & DBGINSTL)
		kkprintf("find_controller() call - ");
	k_addr = find_controller((u_long)infoBase, len, am, offst, size,
				 &param);
	if (k_addr == -1) {	/* error */
		if (info->debugFlag & DBGINSTL) {
			kkprintf("FAILED.\n");
			kkprintf("  (VME device @0x%x. Address range is"
				 " 0x%lx bytes.)\n", infoBase, len);
		}
		*err = ENXIO;
		return -1;
	}

	if (info->debugFlag & DBGINSTL) {
		kkprintf("OK.\n");
		kkprintf("  (VME device @0x%x has been mapped @0x%x)\n",
			 infoBase, k_addr);
	}
	return k_addr;
}


/**
 * @brief Maps specified part of the VME address space into PCI address space.
 *
 * @param info -- complete device information
 *
 * Also checks if device is installed in the crate by means of reading at given
 * address. If read test address is not provided -  then @b no module checking
 * will be perfomed. If there is no base address (i.e. address is
 * ADCG__NO_ADDRESS), then no mapping and no module checking will be performed.
 *
 * @return -errno - some negative value, if address mapping fails
 * @return 1, 2   - i.e. number of mapped addresses in case of success
 */
static int MapAndCheckDevice(DevInfo_t *info)
{
	kaddr_t mapped_addr; /* mapped address */
	int		cntr;
	AddrInfo_t	*aitptr;
	int		res = 0;

	if (info->debugFlag & DBGINSTL) {
		kkprintf("\n\n%s() parameters are:\n", __FUNCTION__);
		kkprintf("+-----------------------------------\n");
		if (info->addr1.baseAddr != ADCG__NO_ADDRESS)
			kkprintf("| addr1 => 0x%x\n", info->addr1.baseAddr);
		else
			kkprintf("| addr1 => NOT defined.\n");

		if (info->addr2.baseAddr != ADCG__NO_ADDRESS)
			kkprintf("| addr2 => 0x%x\n", info->addr2.baseAddr);
		else
			kkprintf("| addr2 => NOT defined.\n");

		for (cntr = 0; cntr < info->blkAmount; cntr++)
			kkprintf("| Block %02d offset: 0x%lx\n",
				 cntr, info->blkDesc[cntr].offset);
		kkprintf("+-----------------------------------\n\n");
	}

	aitptr = &(info->addr1);	/* init pointer */
	for (cntr = 0; cntr < ASA; cntr++) {
		/* move through all address spaces */
		if (aitptr->baseAddr != ADCG__NO_ADDRESS) {
			mapped_addr = MapVMEaddress(info, aitptr, &res);
			if (mapped_addr != -1) {
				/* overwrite with mapped base address */
				aitptr->baseAddr = mapped_addr;
				if (info->debugFlag & DBGINSTL)
					kkprintf("  addr%1d succesfully"
						 " mapped.\n", cntr+1);
				res++; /* increase counter */
			} else {
				if (info->debugFlag & DBGINSTL)
					kkprintf("  addr%1d mapping fails!"
						 " (errcode is %d)\n",
						 cntr+1, res);
				res *= -1;
			}
		}
		aitptr++; /* move pointer to the next addr space */
	}

	return res;
}
