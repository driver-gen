

/**
 * @brief Entry point function.
 *
 * @param info --
 *
 * Initializes the hardware and allocates shared memory buffers. Invoked each
 * time the device driver is installed for a major device (i.e. each time
 * when @b cdv_install() or @b bdv_install() functions are called).
 *
 * @return pointer to a statics data structure - if succeed.
 * @return SYSERR                              - in case of failure.
 */
#define INST_OK()						\
do {								\
  kkprintf("<sys_lower>: <drvr_type> (LUN %d) ", info->mlun);	\
  kkprintf(" installation Complete. ");				\
  DisplayVersion(NULL);						\
  kkprintf("\n\n");						\
} while (0)

#define INST_FAIL()						\
do {								\
  kkprintf("<sys_lower>: <drvr_type> (LUN %d) ", info->mlun);	\
  kkprintf(" installation Fails. ");				\
  DisplayVersion(NULL);						\
  kkprintf("\n\n");						\
} while (0)

char* <sys_lower>_install(void *infofile)
{
	register <mod_name>Statics_t *statPtr;
	char *usrcoco; /* completion code of user entry point function */
	int cntr;
	AddrInfo_t *aiptr;
	DevInfo_t *info;
	int res __attribute__((unused));

	kkprintf("<sys_lower>: Device info table structure allocation - ");
	info = (DevInfo_t *)sysbrk(sizeof(DevInfo_t));
	if (!info) {
		/* Horrible failure */
		kkprintf("FAILED\n");
		pseterr(ENOMEM);
		INST_FAIL();
		return (char *)SYSERR; /* -1 */
	}
	kkprintf("OK\n");

	kkprintf("<sys_lower>: Copying info table from user space - ");
	memcpy(info, infofile, sizeof(DevInfo_t));
	kkprintf("OK\n");

