	/* 0x5 save info table pointer */
	statPtr->info = (void*)info;

	/* 0x6 */
	if (sizeof(<mod_name>UserStatics_t) > 0) { /* only if it's not empty */
		kkprintf("<sys_lower>: User-defined statics data table"
			 " allocation - ");
		if( !(statPtr->usrst = (<mod_name>UserStatics_t*)
		      sysbrk( (long)sizeof(<mod_name>UserStatics_t) )) ) {
			/* Horrible, impossible failure. */
			kkprintf("FAILED\n");
			pseterr(ENOMEM);
			INST_FAIL();
			return (char*)SYSERR; /* -1 */
		}
		bzero((char*)statPtr->usrst, sizeof(<mod_name>UserStatics_t));
		kkprintf("OK\n");
	} else {
		statPtr->usrst = NULL;
	}

	/* user entry point function call */
	usrcoco = <sys_lower>UserInst(NULL, info, statPtr);
	if (usrcoco == (char*)SYSERR ) {
		INST_FAIL();
		<sys_lower>MemCleanup(statPtr); /* free allocated memory */
		return (char*)SYSERR; /* -1 */
	}

	INST_OK();
	return (char*)statPtr;
}

