
  if (info->debugFlag & DBGINSTL) {	/* only in case of debugging */
    aiptr = &(info->addr1);
    kkprintf("\n\n<drvr_type> install procedure parameters are:\n");
    kkprintf("+-----------------------------------------\n");
    /* info about 2 address spaces */
    for (cntr = 0; cntr < 2; cntr++) {
      kkprintf("|    Addr%d info:\n", cntr+1);
      kkprintf("|    -----------\n");
      if (aiptr->baseAddr != ADCG__NO_ADDRESS) {
	kkprintf("|     Base address      => 0x%x\n", aiptr->baseAddr);
	kkprintf("|     Address range     => 0x%x\n", aiptr->range);
	kkprintf("|     Address increment => 0x%x\n", aiptr->increment);
	kkprintf("|     Dataport size     => %d\n", aiptr->dpSize);
	kkprintf("|     Address modifier  => %s (0x%x)\n|\n", (aiptr->addrModif == DG_AM_SH)?"SH":(aiptr->addrModif == DG_AM_ST)?"ST":(aiptr->addrModif == DG_AM_EX)?"EX":(aiptr->addrModif == DG_AM_CR)?"CR":"UNKNOWN", aiptr->addrModif);
      } else kkprintf("|     Addr%d NOT defined.\n|\n", cntr+1);
      aiptr++;
    }

    kkprintf("| Debug flag                 => 0x%x\n", info->debugFlag);
    kkprintf("| Opaque parameter           => '%s'\n", info->opaque);
    kkprintf("| Interrupt level            => %d\n", info->iLevel);
    kkprintf("| Interrupt vector           => %d\n", info->iVector);
    kkprintf("| Interrupt vector increment => %d\n", info->iVectorInc);
    kkprintf("| Number of channels         => %d\n", info->chan);
    kkprintf("+-----------------------------------------\n\n");

    if (info->debugFlag & DBGALLDI) {
      /* user wants really everything, so let him have it! */
      REGDESC *rdp = info->regDesc;
      BLKDESC *bdp = info->blkDesc;
      aiptr = &(info->addr1);

      /* 2 address space info */
      for (cntr = 0; cntr < 2; cntr++) {
	kkprintf("+-----------------------------\n");
	kkprintf("| Addr[%d].baseAddr  => 0x%X\n", cntr, aiptr->baseAddr);
	kkprintf("| Addr[%d].range     => 0x%X\n", cntr, aiptr->range);
	kkprintf("| Addr[%d].increment => 0x%X\n", cntr, aiptr->increment);
	kkprintf("| Addr[%d].dpSize    => 0x%d\n", cntr, aiptr->dpSize);
	kkprintf("| Addr[%d].addrModif => 0x%d\n", cntr, aiptr->addrModif);
	kkprintf("+-----------------------------\n\n");
	aiptr++;
      }

      /* general info */
      kkprintf("+-----------------------------\n");
      kkprintf("| 'mtn'        => %d\n",   info->mtn);
      kkprintf("| 'debugFlag'  => 0x%X\n", info->debugFlag);
      kkprintf("| 'opaque'     => '%s'\n", info->opaque);
      kkprintf("| 'iLevel'     => %d\n",   info->iLevel);
      kkprintf("| 'iVector'    => %d\n",   info->iVector);
      kkprintf("| 'iVectorInc' => %d\n",   info->iVectorInc);
      kkprintf("| 'chan'       => %d\n",   info->chan);
      kkprintf("| 'chrindex'   => %d\n",   info->chrindex);
      kkprintf("| 'gen_date'   => %ld\n",  info->gen_date);
      kkprintf("| 'dg_vers'    => '%s'\n", info->dg_vers);
      kkprintf("| 'regAmount'  => %d\n",   info->regAmount);
      kkprintf("| 'maxRegSz'   => %d\n",   info->maxRegSz);
      kkprintf("| 'checksum'   => %d\n",   info->checksum);
      kkprintf("+-----------------------------\n\n");

      /* all block info */
      for (cntr = 0; cntr < info->blkAmount; cntr++) {
	kkprintf("+---------------------------------------------\n");
	kkprintf("| Blk[%d] 'block'       => %d\n", cntr, bdp[cntr].block);
	kkprintf("| Blk[%d] 'blkBaseAddr' => %d\n", cntr,
		 bdp[cntr].blkBaseAddr);
	kkprintf("| Blk[%d] 'offset'      => 0x%lX\n", cntr, bdp[cntr].offset);
	kkprintf("| Blk[%d] 'blksz_drvr'  => %d\n", cntr, bdp[cntr].blksz_drvr);
	kkprintf("| Blk[%d] 'blksz_sim'   => %d\n", cntr, bdp[cntr].blksz_sim);
	kkprintf("| Blk[%d] 'reg_am'      => %d\n", cntr, bdp[cntr].reg_am);
	kkprintf("+---------------------------------------------\n\n");
      }

      /* all registers info */
      for (cntr = 0; cntr < info->regAmount; cntr++) {
	kkprintf("+---------------------------------------------\n");
	kkprintf("| Reg[%d] 'ID'           => %d\n", cntr, rdp[cntr].regId);
	kkprintf("| Reg[%d] 'regName'      => %s\n", cntr, rdp[cntr].regName);
	kkprintf("| Reg[%d] 'busType'      => %s\n", cntr, rdp[cntr].busType);
	kkprintf("| Reg[%d] 'regType'      => %s (%d)\n", cntr, (rdp[cntr].regType == RT_REAL)?"RT_REAL":(rdp[cntr].regType == RT_EXTR)?"RT_EXTR":"RT_SRVS", rdp[cntr].regType);
	kkprintf("| Reg[%d] 'regSize'      => %d\n", cntr, rdp[cntr].regSize);
	kkprintf("| Reg[%d] 'b_a'          => %s\n", cntr, rdp[cntr].b_a);
	kkprintf("| Reg[%d] 'bid'          => %d\n", cntr, rdp[cntr].bid);
	kkprintf("| Reg[%d] 'regOffset'    => 0x%X\n", cntr, rdp[cntr].regOffset);
	kkprintf("| Reg[%d] 'regDepth'     => %d\n", cntr, rdp[cntr].regDepth);
	kkprintf("| Reg[%d] 'regtl'        => %d\n", cntr, rdp[cntr].regtl);
	kkprintf("| Reg[%d] 'regar'        => %d\n", cntr, rdp[cntr].regar);
	kkprintf("| Reg[%d] 'rwIoctlOpNum' => 0x%x\n", cntr, rdp[cntr].rwIoctlOpNum);
	kkprintf("+---------------------------------------------\n\n");
      }
    }
  }

  kkprintf("<sys_lower>: <drvr_type> (LUN %d) installation begins...\n", info->mlun);

#ifdef __Lynx__
  GlobCalVal = CalibrateTime(info); /* do timing calibration */
  if (info->debugFlag & DBGTIMESTAT) /* time statistics is enabled */
     kkprintf("<sys_lower>: Calibrated timing is %dns per access.\n",
	      GlobCalVal);
#endif

  /*
     Should we disable interrupts here?
     int ps;
     disable(ps);
  */

