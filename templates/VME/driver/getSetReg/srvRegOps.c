
/*****************************************************************************/
/*                                                                           */
/* First that comes - are service routines (with __SRV__ subword), provided  */
/* by default for all automatically generated drivers. Normally they are     */
/* used by the Driver Access Library (aka DAL) only.                         */
/*                                                                           */
/*****************************************************************************/

/**
 * @brief Service function used in ioctl entry point.
 *
 * For repetitive r/w register access.
 */
#ifdef __Lynx__

#define srv_func___SRV__REP_REG_RW(_s, _arg, _crw, _drw)		\
({									\
	r_rw = 1;	/* set repetitive flag on */			\
	com = (int)*_arg; /* set ioctl number */			\
									\
	_arg += sizeof(unsigned long); /* move argument pointer */	\
	goto rep_ioctl; /* repeat ioctl switch */			\
	r_rw;								\
})

#else  /* __linux__ */

#define srv_func___SRV__REP_REG_RW(_s, _arg, _crw, _drw)		\
({									\
	r_rw = 1;	/* set repetitive flag on */			\
									\
	if (get_user(com, (int*)_arg))					\
		return -EFAULT;						\
									\
	_arg += sizeof(unsigned long); /* move argument pointer */	\
	goto rep_ioctl; /* repeat ioctl switch */			\
	r_rw;								\
})

#endif


/**
 * @brief Service function used in ioctl entry point.
 *
 * Enable/Disable/Query r/w bounds checking during register access.
 * Write 1 - to enable, 0 - to disable, 2 - to query current state
 * @e NOTE. Valid _ONLY_ in case of Lynx!
 */
#ifdef __Lynx__

#define srv_func___SRV__RW_BOUNDS(_s, _arg, _crw, _drw)			\
({									\
	int _usrv = (int)*_arg;						\
									\
	if (_usrv != 2)	/* usr wants to change current check state */	\
		c_rwb = _usrv;						\
									\
	c_rwb; /* return current state */				\
})

#else  /* __linux__ */

#define srv_func___SRV__RW_BOUNDS(_s, _arg, _crw, _drw)			\
({									\
	int _usrv;							\
									\
	if (get_user(_usrv, (int*)_arg))				\
		return -EFAULT;						\
									\
	if (_usrv != 2)	/* usr wants to change current check state */	\
		c_rwb = _usrv;						\
									\
	c_rwb; /* return current state */				\
})

#endif

/**
 * @brief Service Register. Get current debug flag.
 *
 * @param s     -- statics table
 * @param uaddr -- data goes here
 *                 type is [ulong] in the user space
 * @param crwb  -- check r/w bounds (1 - check, 0 - don't)
 * @param r_rw  -- repeated r/w reg access (1 - yes, 0 - no). ignored
 *
 * @return SYSERR - if fails
 * @return OK     - otherwise
 */
int get___SRV__DEBUG_FLAG(register <mod_name>Statics_t *s,
			  char *uaddr, int crwb, int r_rw)
{
	char *uaddr_back; /* backup user-space address */

	DBG_BEG(__FUNCTION__);

	/* check (and prepare in case of Linux) user buffer */
	if (assert_wbounds(s, crwb, &uaddr, sizeof(long), &uaddr_back))
		return SYSERR;

	/* give data to the user */
	*(unsigned long*)uaddr = s->info->debugFlag;

	return end_read(s, uaddr, sizeof(long), uaddr_back);
}


/**
 * @brief Service Register. Set miscellaneous driver flags.
 *
 * @param s     -- statics table
 * @param uaddr -- new driver flags
 *                 type is [ulong] in the user space
 * @param crwb  -- check r/w bounds (1 - check, 0 - don't)
 * @param r_rw  -- repeated r/w reg access (1 - yes, 0 - no). ignored
 *
 * Enable/disable debug info printout, etc...
 * Previous flag register state returned to the user.
 *
 * @return SYSERR - if fails
 * @return OK     - otherwise
 */
int set___SRV__DEBUG_FLAG(register <mod_name>Statics_t *s,
			  char *uaddr, int crwb, int r_rw)
{
	char *uaddr_back; /* backup user-space address */
	dbgIPL dbg_prev = s->info->debugFlag; /* save current flag */

	DBG_BEG(__FUNCTION__);

	/* check (and prepare in case of Linux) usr buffer */
	if (assert_rbounds(s, crwb, &uaddr, sizeof(long), &uaddr_back))
		return SYSERR;

	/* set new dbg flag */
	s->info->debugFlag = *(unsigned long*)uaddr;

	/* give previous flag value to the user */
	*(unsigned long*)uaddr = dbg_prev;

	return end_read(s, uaddr, sizeof(long), uaddr_back);
}


/**
 * @brief Service Register. Get device info table
 *
 * @param s     -- statics table
 * @param uaddr -- data goes here
 *                 type is [DevInfo_t] in the user space
 * @param crwb  -- check r/w bounds (1 - check, 0 - don't)
 * @param r_rw  -- repeated r/w reg access (1 - yes, 0 - no). ignored
 *
 * @return SYSERR - if fails
 * @return OK     - otherwise
 */
int get___SRV__DEVINFO_T(register <mod_name>Statics_t *s,
			 char *uaddr, int crwb, int r_rw)
{
	char *uaddr_back; /* backup user-space address */

	DBG_BEG(__FUNCTION__);

	/* check (and prepare in case of Linux) usr buffer */
	if (assert_wbounds(s, crwb, &uaddr, sizeof(DevInfo_t), &uaddr_back))
		return SYSERR;

	/* give data to the user */
	memcpy((void*)uaddr, (void*)(s->info), sizeof(DevInfo_t));

	return end_read(s, uaddr, sizeof(DevInfo_t), uaddr_back);
}


/**
 * @brief Service Register. Give driver version info to the user.
 *
 * @param s        -- statics table
 * @param ioctlarg -- ioctl arguments are:
 *                    two usr-space addresses packed in unsigned long arg[3]
 *                    for ioctl call.
 *                    only arg[0] and arg[1] are valid. They are:
 *                    1. addr[0] -- address, where driver version in
 *                                  [int] format will go
 *                    2. addr[1] -- adsress, where driver version string
 *                                  in [char*] format will go
 * @param crwb     -- check r/w bounds (1 - check, 0 - don't)
 * @param r_rw     -- repeated r/w reg access (1 - yes, 0 - no). ignored
 *
 * @return SYSERR - if fails
 * @return OK     - otherwise
 */
int get___SRV__DRVR_VERS(register <mod_name>Statics_t *s,
			 char *ioctlarg, int crwb, int r_rw)
{
	char *vers_str;		/* version string */
	int drvr_vers;		/* current driver version */

	int *usr_drvr_vers;	/* user space */
	char *usr_drvr_vers_back; /* backup user-space address */

	char *usr_vers_str;	/* user space */
	char *usr_vers_str_back; /* backup user-space address */

	int dum;

	DBG_BEG(__FUNCTION__);

	drvr_vers = DisplayVersion(&vers_str); /* get version string */

	/* unpack arguments */
	UNPACK_ARGS(ioctlarg, usr_drvr_vers, usr_vers_str, dum);

	/* check (and prepare in case of Linux) user buffers */
	if (assert_wbounds(s, crwb, (char**)&usr_drvr_vers, sizeof(int),
			   (char**)&usr_drvr_vers_back))
		return SYSERR;
	if (assert_wbounds(s, crwb, (char**)&usr_vers_str, strlen(vers_str)+1,
			   (char**)&usr_vers_str_back)) {
#ifdef __linux__
		sysfree((char*)usr_drvr_vers, 0);
#endif
		return SYSERR;
	}

	/* give it to the user */
	memcpy((void*)usr_vers_str, (void*)vers_str, strlen(vers_str)+1);
	*(int*)usr_drvr_vers = drvr_vers;

#ifdef __linux__
	/* TODO Check copy_to_user completion code */
	dum = copy_to_user(usr_drvr_vers_back, usr_drvr_vers, sizeof(int));
	dum = copy_to_user(usr_vers_str_back, usr_vers_str, strlen(vers_str)+1);
	sysfree((char*)usr_drvr_vers, 0);
	sysfree((char*)usr_vers_str, 0);
#endif
	FINISH_TIME_STAT(); /* timing measurements */
	DBG_OK();
	return OK;
}


/**
 * @brief Service Register. Current driver/DAL consistency checking.
 *
 * @param s     -- statics table
 * @param uaddr -- driver generation time goes here
 *                 type is [time_t] in the user space
 * @param crwb  -- check r/w bounds (1 - check, 0 - don't)
 * @param r_rw  -- repeated r/w reg access (1 - yes, 0 - no). ignored
 *
 * If driver generation date is older than @e DAL generation date, then new
 * @e DAL features are not available for the user. In this case driver should
 * be regenerated using the newest version of the @b driverGen
 *
 * @return SYSERR - if fails
 * @return OK     - otherwise
 */
int get___SRV__DAL_CONSISTENT(register <mod_name>Statics_t *s,
			      char *uaddr, int crwb, int r_rw)
{
	char *uaddr_back; /* backup user-space address */

	DBG_BEG(__FUNCTION__);

	/* check (and prepare in case of Linux) usr buffer */
	if (assert_wbounds(s, crwb, &uaddr, <int_num>, &uaddr_back))
		return SYSERR;

	/* all OK - put data in the user space */
	*(unsigned <reg_type>*)uaddr = <mod_name>_GENERATION_TIME_HEX;

	return end_read(s, uaddr, <int_num>, uaddr_back);
}
/*===================== end of service routines =============================*/

