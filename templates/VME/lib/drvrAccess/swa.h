/**
 * @file swa.h
 *
 * @brief Adopted linux/swab.h
 *
 * Needed by DAL DMA module.
 *
 * Exported functions are:
 * A. from linux/swab.h
 *    1. swab16s
 *    2. swab32s
 *    3. swahw32s
 *    4. swab64s
 *
 * B. home-made:
 *    5. swahl64s
 *    6. swahw64s
 *
 * @author Copyright (C) 2009 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 03/12/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifndef _SWA_H_INCLUDE_
#define _SWA_H_INCLUDE_

/*
 * casts are necessary for constants, because we never know how for sure
 * how U/UL/ULL map to uint16_t, uint32_t, uint64_t. At least not in a portable way.
 */
#define ___constant_swab16(x) ((uint16_t)(\
	(((uint16_t)(x) & (uint16_t)0x00ffU) << 8) |\
	(((uint16_t)(x) & (uint16_t)0xff00U) >> 8)))

#define ___constant_swab32(x) ((uint32_t)(\
	(((uint32_t)(x) & (uint32_t)0x000000ffUL) << 24) |\
	(((uint32_t)(x) & (uint32_t)0x0000ff00UL) <<  8) |\
	(((uint32_t)(x) & (uint32_t)0x00ff0000UL) >>  8) |\
	(((uint32_t)(x) & (uint32_t)0xff000000UL) >> 24)))

#define ___constant_swab64(x) ((uint64_t)(\
	(((uint64_t)(x) & (uint64_t)0x00000000000000ffULL) << 56) |\
	(((uint64_t)(x) & (uint64_t)0x000000000000ff00ULL) << 40) |\
	(((uint64_t)(x) & (uint64_t)0x0000000000ff0000ULL) << 24) |\
	(((uint64_t)(x) & (uint64_t)0x00000000ff000000ULL) <<  8) |\
	(((uint64_t)(x) & (uint64_t)0x000000ff00000000ULL) >>  8) |\
	(((uint64_t)(x) & (uint64_t)0x0000ff0000000000ULL) >> 24) |\
	(((uint64_t)(x) & (uint64_t)0x00ff000000000000ULL) >> 40) |\
	(((uint64_t)(x) & (uint64_t)0xff00000000000000ULL) >> 56)))

#define ___constant_swahw32(x) ((uint32_t)(\
	(((uint32_t)(x) & (uint32_t)0x0000ffffUL) << 16) |\
	(((uint32_t)(x) & (uint32_t)0xffff0000UL) >> 16)))

#define ___constant_swahb32(x) ((uint32_t)(\
	(((uint32_t)(x) & (uint32_t)0x00ff00ffUL) << 8) |\
	(((uint32_t)(x) & (uint32_t)0xff00ff00UL) >> 8)))

/**
 * __swab16 - return a byteswapped 16-bit value
 * @x: value to byteswap
 */
#define __swab16(x) ___constant_swab16(x)

/**
 * __swab32 - return a byteswapped 32-bit value
 * @x: value to byteswap
 */
#define __swab32(x) ___constant_swab32(x)


/**
 * __swab64 - return a byteswapped 64-bit value
 * @x: value to byteswap
 */
#define __swab64(x) ___constant_swab64(x)

/**
 * __swahw32 - return a word-swapped 32-bit value
 * @x: value to wordswap
 *
 * __swahw32(0x12340000) is 0x00001234
 */
#define __swahw32(x) ___constant_swahw32(x)

/**
 * __swahb32 - return a high and low byte-swapped 32-bit value
 * @x: value to byteswap
 *
 * __swahb32(0x12345678) is 0x34127856
 */
#define __swahb32(x) ___constant_swahb32(x)

/**
 * __swab16p - return a byteswapped 16-bit value from a pointer
 * @p: pointer to a naturally-aligned 16-bit value
 */
static inline uint16_t __swab16p(const uint16_t *p)
{ return __swab16(*p); }

/**
 * __swab16s - byteswap a 16-bit value in-place
 * @p: pointer to a naturally-aligned 16-bit value
 */
static inline void __swab16s(uint16_t *p)
{ *p = __swab16p(p); }


/**
 * __swab32p - return a byteswapped 32-bit value from a pointer
 * @p: pointer to a naturally-aligned 32-bit value
 */
static inline uint32_t __swab32p(const uint32_t *p)
{ return __swab32(*p); }

/**
 * __swab32s - byteswap a 32-bit value in-place
 * @p: pointer to a naturally-aligned 32-bit value
 */
static inline void __swab32s(uint32_t *p)
{ *p = __swab32p(p); }



/**
 * __swahw32p - return a wordswapped 32-bit value from a pointer
 * @p: pointer to a naturally-aligned 32-bit value
 *
 * See __swahw32() for details of wordswapping.
 */
static inline uint32_t __swahw32p(const uint32_t *p)
{ return __swahw32(*p); }

/**
 * __swahw32s - wordswap a 32-bit value in-place
 * @p: pointer to a naturally-aligned 32-bit value
 *
 * See __swahw32() for details of wordswapping
 */
static inline void __swahw32s(uint32_t *p)
{ *p = __swahw32p(p); }


/**
 * __swab64p - return a byteswapped 64-bit value from a pointer
 * @p: pointer to a naturally-aligned 64-bit value
 */
static inline uint64_t __swab64p(const uint64_t *p)
{ return __swab64(*p); }

/**
 * __swab64s - byteswap a 64-bit value in-place
 * @p: pointer to a naturally-aligned 64-bit value
 */
static inline void __swab64s(uint64_t *p)
{ *p = __swab64p(p); }


#define swab16s __swab16s
#define swab32s __swab32s
#define swahw32s __swahw32s
#define swab64s __swab64s

/* home-made stuff */
/**
 * __swahl64 - return a long dword-swapped 64-bit value
 * @x: value to long-dwordswap
 *
 * __swahl64(0x1234 5678 12ab cdef) is 0x12ab cdef 1234 5678
 */
#define __swahl64(x) ((uint64_t)(\
        (((uint64_t)(x) & (uint64_t)0x00000000ffffffffULL) << 32) |\
        (((uint64_t)(x) & (uint64_t)0xffffffff00000000ULL) >> 32)))

/**
 * __swahw64 - return a dword-swapped 64-bit value
 * @x: value to dwordswap
 *
 * __swahw64(0x1234 5678 12ab cdef) is 0xcdef 12ab 5678 1234
 */
#define __swahw64(x) ((uint64_t)(\
        (((uint64_t)(x) & (uint64_t)0x000000000000ffffULL) << 48) |\
        (((uint64_t)(x) & (uint64_t)0x00000000ffff0000ULL) << 32) |\
        (((uint64_t)(x) & (uint64_t)0x0000ffff00000000ULL) >> 32) |\
        (((uint64_t)(x) & (uint64_t)0xffff000000000000ULL) >> 48)))


static inline uint64_t __swahl64p(const uint64_t *p)
{ return __swahl64(*p); }

static inline uint64_t __swahw64p(const uint64_t *p)
{ return __swahw64(*p); }

/**
 * swahl64s - dwordswap a 64-bit value in-place
 * @p: pointer to a naturally-aligned 64-bit value
 *
 * See __swahl64() for details of high and low dword swapping
 */
static inline void swahl64s(uint64_t *p)
{ *p = __swahl64p(p); }

/**
 * swahw64s - wordswap a 64-bit value in-place
 * @p: pointer to a naturally-aligned 64-bit value
 *
 * See __swahw64() for details of word swapping
 */
static inline void swahw64s(uint64_t *p)
{ *p = __swahw64p(p); }


#endif	/* _SWA_H_INCLUDE_ */
