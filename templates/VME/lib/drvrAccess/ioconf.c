/**
 * @file ioconf.c
 *
 * @brief mmap I/O access for Lynx
 *
 * IoConfig library is used to setup HW device mapping.
 *
 * @author Copyright (C) 2009 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 02/12/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#ifdef __Lynx__
#include <st.h>			/* for getstid */
#include "port_ops_lynx.h"	/* for __rep_inb_delay, __inb etc... */
#else /* __linux__ */
#include <string.h>
#include <sys/syscall.h>
#define _DG_SIM
#include "port_ops_linux.h"
#define getstid gettid
#endif

#include "dal.h"

extern int snprintf(char *str, size_t size, const char *format, ...);

#ifdef __linux__
/* man gettid:
   Glibc does not provide a wrapper for this system call;
   call it using syscall(2). */
static pid_t getstid(void)
{
	return syscall(__NR_gettid);
}
#endif

/**
 * @brief Mapped Memory Register Access.
 *
 * @param usrH  -- current user handle
 * @param rdP   -- register description
 * @param opt   -- operation to perfom (read or write).
 * @param elNum -- amount of elements to read/write from the register.
 *                 Minimum one (1)
 * @param fEl   -- first element index to get (starting from 0)
 * @param rba   -- address space base address
 * @param arg   -- result will be put here, or get from here.
 *                 Depends on operation type in @e opt parameter
 *
 * This routine access VME module registers using direct I/O access to module
 * memory, which was previously mapped on the kernel memory space by means of
 * @b ioconfig library. Register accessing is accomplished by reading/writing
 * directly into this memory.
 *
 * @return   0 - if required r/w operation complete successfully.
 * @return < 0 - if r/w operation failed for some reason.
 */
int MMRegAccess(_dal_h *usrH, REGDESC *rdP, REGOPTYPE opt, int elNum, int fEl,
		char *rba, char *arg)
{
	int ret = 0;		/* OK return code */
	int fd;
	char msg[0xff];

	/* mapped address of the element */
	/* TODO Support 64bit addresses */
	__port_addr_t port = (__port_addr_t)
		((unsigned long) rba +      /* address space Base Address */
		 usrH->dit.blkDesc[rdP->bid].offset + /* block offset from
							 the Base Address */
		 rdP->regOffset +      /* register offset from the
					  beginning of the Block */
		 fEl * rdP->regSize);


	/* debug printout */
	if (usrH->dal_flag & DA_DBG_DMA_RW_OPS) {
		/* console open will succeed only with the root rights */
		fd = open("/dev/console", O_WRONLY);
		if (fd != -1) {
			snprintf(msg, sizeof(msg),
				 "(pid %d) (tid %d) mmap@0x%x [%s] %s - ",
				 getpid(), getstid(), (uint)port, rdP->regName,
				 (opt == OPRD) ? "read" : (opt == OPWR) ?
				 "write" : (opt == OPRDR) ? "repetitive read" :
				 "repetitive write");
			write(fd, msg, strlen(msg));
		}
	}

	switch (opt) {
	case OPRD:
	     /*============00. user wants to READ register===============*/
		switch (rdP->regSize) {	/* register size */
		case SZCHAR:
			__rep_inb_delay_shift(port, (unsigned char *)arg, elNum,
					      rdP->regtl);
			break;
		case SZSHORT:
			__rep_inw_delay_shift(port, (unsigned short *)arg,
					      elNum, rdP->regtl);
			break;
		case SZLONG:
			__rep_inl_delay_shift(port, (unsigned long *)arg, elNum,
					      rdP->regtl);
			break;
		default:
			/* error. wrong register size */
			ret = -1;
			break;
		}
		break;
	case OPWR:
	     /*===============01. user wants to WRITE register=============*/
		switch (rdP->regSize) {	/* register size */
		case SZCHAR:
			__rep_outb_delay_shift(port, (unsigned char *)arg,
					       elNum, rdP->regtl);
			break;
		case SZSHORT:
			__rep_outw_delay_shift(port, (unsigned short *)arg,
					       elNum, rdP->regtl);
			break;
		case SZLONG:
			__rep_outl_delay_shift(port, (unsigned long *)arg,
					       elNum, rdP->regtl);
			break;
		default:
			/* error. wrong register size */
			ret = -1;
			break;
		}
		break;
	case OPRDR:
	      /*============02. user wants REPTTITIVE READ register==========*/
		switch (rdP->regSize) {	/* register size */
		case SZCHAR:
			__rep_inb(port, (unsigned char *)arg, elNum);
			break;
		case SZSHORT:
			__rep_inw(port, (unsigned short *)arg, elNum);
			break;
		case SZLONG:
			__rep_inl(port, (unsigned long *)arg, elNum);
			break;
		default:
			/* error. wrong register size */
			ret = -1;
			break;
		}
		break;
	case OPWRR:
	      /*============03. user wants to REPETITIVE WRITE register======*/
		switch (rdP->regSize) {	/* register size */
		case SZCHAR:
			__rep_outb(port, (unsigned char *)arg, elNum);
			break;
		case SZSHORT:
			__rep_outw(port, (unsigned short *)arg, elNum);
			break;
		case SZLONG:
			__rep_outl(port, (unsigned long *)arg, elNum);
			break;
		default:
			/* error. wrong register size */
			ret = -1;
			break;
		}
		break;
	default:
		/* error. wrong operation type */
		ret = -1;
		break;
	}

	/* debug printout */
	if (usrH->dal_flag & DA_DBG_DMA_RW_OPS) {
		if (fd != -1) {
			sprintf(msg, "OK.\n");
			write(fd, msg, strlen(msg));
			close(fd);	/* close console */
		}
	}

	return ret;
}
