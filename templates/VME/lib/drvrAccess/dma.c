/**
 * @file dma.c
 *
 * @brief DMA support through TSI148 chip
 *
 * <long description>
 *
 * @author Copyright (C) 2009 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 02/12/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifdef __linux__
#define _GNU_SOURCE /* asprintf rocks */
#endif
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "dal.h"

#ifdef __Lynx__
/* stubs in case of Lynx */
int init_dma(_dal_h *h) { return -1; }
int do_dma(_dal_h *h, REGDESC *rd, REGOPTYPE t, int eln, int fel,
	   void *buf, int sz)
{ return -1; }

#else

#include <vmebus.h>

/* for byte swapping */
#define _DG_SIM
#include <port_ops_linux.h>
#include <swa.h>

/* error handling */
#define TSI148_LCSR_DSTA_VBE           (1<<28)  /* Error */
#define TSI148_LCSR_DSTA_ABT           (1<<27)  /* Abort */
#define TSI148_LCSR_DSTA_PAU           (1<<26)  /* Pause */
#define TSI148_LCSR_DSTA_DON           (1<<25)  /* Done */
#define TSI148_LCSR_DSTA_BSY           (1<<24)  /* Busy */
#define TSI148_LCSR_DSTA_ERRS          (1<<20)  /* Error Source */
#define TSI148_LCSR_DSTA_ERT           (3<<16)  /* Error Type */
#define TSI148_LCSR_DSTA_MASK          0x1f130000

/**
 * @brief init data for Linux DMA access.
 *
 * @param h -- library handle pointer
 *
 * Extra data, needed for Linux DMA should be set.
 *
 * @return < 0 -- FAILED
 * @return   0 -- OK
 */
int init_dma(_dal_h *h)
{
#define VME_DMA_DEV "/dev/vme_dma"
#define VME_WINDOWS "/proc/vme/windows"
	int rc;
	void *va, *pcia, *vmea;	/* data containers */
	char str[128];		/* data containers */
	uint32_t sz, w;		/* data containers */
	FILE *fd = fopen(VME_WINDOWS, "r");

	if (fd < 0) {
		perror("init_dma() open");
		return -1;
	}

	/* get VME address */
	while ( (rc = fscanf(fd, "%d: %p %p %p %x - %s\n",
			     &w, &va, &pcia, &vmea, &sz, str)) != EOF) {
		if (rc != 6) { /* skip matching failure */
			rc = fscanf(fd, "%*[^\n]");
			continue;
		}

		if (va == (void *) h->dit.addr1.baseAddr)
			h->dma.addr[0] = vmea;
		else if (va == (void *) h->dit.addr2.baseAddr)
			h->dma.addr[1] = vmea;
		else
			continue;
	}
	fclose(fd);
	if (!h->dma.addr[0] && !h->dma.addr[1])
		return -1; /* didn't find any */

	/* open vmebridge dma node */
	h->dma.fd = open(VME_DMA_DEV, O_RDWR);
        if (h->dma.fd < 0) {
		h->dma.fd = 0;
                perror("init_dma()  open");
                return -1;
        }
	return 0;
}

/**
 * @brief Setup DMA description
 *
 * @param h   -- lib handle
 * @param rdP -- register description
 * @param dir -- r/w operation type
 * @param eln -- number of register elements to r/w
 * @param fel -- register element idx to do r/w
 * @param buf -- user-space buffer to get/put data from/to
 * @param d   -- description table to init
 *
 * <long-description>
 *
 */
static void setup_vme_desc(_dal_h *h, REGDESC *rdP, REGOPTYPE dir, int eln,
			   int fel, void *buf, struct vme_dma *d)
{
	ulong vmeaddr;
	AddrInfo_t *aip;
	DevInfo_t  *dip = &h->dit;

	/* set correct Base Address table pointer */
	if (rdP->b_a[4] == '2') {
                aip = &dip->addr2;
		vmeaddr = (ulong) h->dma.addr[1];
	} else {
                aip = &dip->addr1;
		vmeaddr = (ulong) h->dma.addr[0];
	}

	memset(d, 0, sizeof(*d));

        if (dir & OPRD) { /* read */
		d->dir = VME_DMA_FROM_DEVICE;
                d->src.data_width = aip->dpSize;
                d->src.am = aip->addrModif;
                d->src.addrl = (unsigned int) vmeaddr +
                        dip->blkDesc[rdP->bid].offset + rdP->regOffset +
			fel * rdP->regSize;
                d->dst.addrl = (unsigned int) buf;
        } else { /* write */
		d->dir = VME_DMA_TO_DEVICE;
                d->dst.data_width = aip->dpSize;
                d->dst.am = aip->addrModif;
                d->src.addrl = (unsigned int) buf;
		d->dst.addrl = (unsigned int) vmeaddr +
                        dip->blkDesc[rdP->bid].offset + rdP->regOffset +
			fel * rdP->regSize;
        }
        d->length = eln * rdP->regSize; /* number of bytes to r/w */
        d->novmeinc = dir & OPREP;	/* see if will r/w FIFO */

        d->ctrl.pci_block_size   = VME_DMA_BSIZE_4096;
        d->ctrl.pci_backoff_time = VME_DMA_BACKOFF_0;
        d->ctrl.vme_block_size   = VME_DMA_BSIZE_4096;
        d->ctrl.vme_backoff_time = VME_DMA_BACKOFF_0;
}

static inline void dg_swab16(void **addr)
{ swab16s((uint16_t *)*addr);  *addr += 2; }

static inline void dg_swab32(void **addr)
{ swab32s((uint32_t *)*addr);  *addr += 4; }
static inline void dg_swaw32(void **addr)
{ swahw32s((uint32_t *)*addr); *addr += 4; }

static inline void dg_swab64(void **addr)
{ swab64s((uint64_t *)*addr);  *addr += 8; }
static inline void dg_swaw64(void **addr)
{ swahw64s((uint64_t *)*addr); *addr += 8; }
static inline void dg_swal64(void **addr)
{ swahl64s((uint64_t *)*addr); *addr += 8; }

/**
 * @brief Select swap data function. For solving NUXI problem.
 *
 * @param rsz -- register size in bits
 * @param dps -- Data Port Size in bits
 *
 * @return swap data vector
 */
static inline void (*select_vector(int rsz, int dps)) (void **)
{
	switch (dps) {
	case 8:
		switch (rsz) {
		case 16:
			return dg_swab16;
		case 32:
			return dg_swab32;
		case 64:
			return dg_swab64;
		}
		break;
	case 16:
		switch (rsz) {
		case 32:
			return dg_swaw32;
		case 64:
			return dg_swaw64;
		}
		break;
	case 32:
		switch (rsz) {
		case 64:
			return dg_swal64;
		}
		break;
	}

	return NULL; /* not reachable */
}

/**
 * @brief If register size is bigger then DPS -- register data should be swapped
 *
 * @param h   -- library handle
 * @param rd  -- register description
 * @param eln -- number of elements to swap
 * @param buf -- data to be swapped
 *
 * Avoid NUXI problem
 *
 */
static void swap_data(_dal_h *h, REGDESC *rd, int eln, void *buf)
{
	int i;
	void (*vector)(void **);
	AddrInfo_t *aip = (rd->b_a[4] == '2') ? &h->dit.addr2 : &h->dit.addr1;

	/* *only* if register is bigger then DPS -- we should swap
	   data within the register */
	if ((rd->regSize / (aip->dpSize/8)) > 1) {
		vector = select_vector(rd->regSize * 8, aip->dpSize);
	for (i = 0; i < eln; i++)
		vector(&buf);
	}
}

/**
 * @brief BE <-> LE converter
 *
 * @param rd  -- register description
 * @param eln -- number of elements to swap
 * @param buf -- data to be swapped
 *
 * <long-description>
 *
 */
static void swap_bytes(REGDESC *rd, int eln, void *buf)
{
	uint8_t  *b;
	uint16_t *w;
	uint32_t *dw;
	uint64_t *llw;

	switch (rd->regSize) {
	case SZCHAR:
		b = (uint8_t *) buf;
		while (eln--) { *b = _ENDIAN(*b); b++; }
		break;
	case SZSHORT:
		w = (uint16_t *) buf;
		while (eln--) { *w = _ENDIAN(*w); w++; }
		break;
	case SZLONG:
		dw = (uint32_t *) buf;
		while (eln--) { *dw = _ENDIAN(*dw); dw++; }
		break;
	case SZ64:
		llw = (uint64_t *) buf;
		while (eln--) { *llw = _ENDIAN(*llw); llw++; }
		break;
	}
}

/**
 * @brief Decode TSI148 DMA errors. Should go into vmebus lib.
 *
 * @param desc -- VME DMA description table
 * @param ptr  -- if not NULL, error string will go here
 *
 * @note ptr (if not NULL) should be freed afterwards by the caller
 *
 * @return  0 -- no DMA error
 * @return -1 -- DMA error occurs
 */
static int vme_dma_error_decode(struct vme_dma *desc, char **ptr)
{
	char str[256];
	char *p = str;
	int rc = 0, i;
	/* Do not care about masked stuff */
	int stat = desc->status & TSI148_LCSR_DSTA_MASK;

	if (stat & TSI148_LCSR_DSTA_VBE) {
		i = sprintf(p, "%s", "VME DMA error\n");
		p += i;
		rc = -1;
		if ((stat & TSI148_LCSR_DSTA_ERRS) == 0) {
			switch (stat & TSI148_LCSR_DSTA_ERT) {
			case (0 << 16):
				i = sprintf(p, "%s", "Bus error: SCT, BLT,"
					    " MBLT, 2eVME even data, 2eSST\n");
				p += i;
				break;
			case (1 << 16):
				i = sprintf(p, "%s",
					    "Bus error: 2eVME odd data\n");
				p += i;
				break;
			case (2 << 16):
				i = sprintf(p, "%s", "Slave termination:"
					    " 2eVME even data, 2eSST read\n");
				p += i;
				break;
			case (3 << 16):
				i = sprintf(p, "%s", "Slave termination:"
					        " 2eVME odd data, 2eSST read"
					    " last word invalid\n");
				p += i;
				break;
			default:
				break;
			}
		} else {
			i = sprintf(p, "%s", "PCI/X Bus Error\n");
			p += i;
		}
	}

	if (stat & TSI148_LCSR_DSTA_ABT) {
		i = sprintf(p, "%s", "VME DMA aborted\n");
		p += i;
		rc = -1;
	}

	if (stat & TSI148_LCSR_DSTA_PAU) {
		i = sprintf(p, "%s", "DMA paused\n");
		p += i;
		rc = -1;
	}

	if (stat & TSI148_LCSR_DSTA_DON) {
		i = sprintf(p, "%s", "DMA done\n");
		p += i;
		rc = 0;
	}

	if (stat & TSI148_LCSR_DSTA_BSY) {
		i = sprintf(p, "%s", "DMA busy\n");
		p += i;
		rc = -1;
	}

	*p = '\0';

	if (ptr)
		asprintf(ptr, "%s", str);

	return rc;
}

/**
 * @brief Perform DMA access
 *
 * @param h   -- library handle
 * @param rd  -- register description
 * @param t   -- r/w operation type
 * @param eln -- number of elements to r/w
 * @param fel -- register element index to start r/w from
 * @param buf -- user-space buffer to get/put data from/to
 * @param sz  -- buffer size in bytes
 *
 * @return -1 -- if FAILED
 * @return  0 -- if OK
 */
int do_dma(_dal_h *h, REGDESC *rd, REGOPTYPE t, int eln, int fel,
	   void *buf, int sz)
{
	struct vme_dma d;
	char *errstr;

	if (t & OPWR) {
		/* should swap in case of write operation */
		swap_data(h, rd, eln, buf);
		swap_bytes(rd, eln, buf);
	}

	setup_vme_desc(h, rd, t, eln, fel, buf, &d);

	if (ioctl(h->dma.fd, VME_IOCTL_START_DMA, &d) < 0) {
                perror("VME DMA access");
                return -1;
        }

	if (vme_dma_error_decode(&d, &errstr)) {
		fprintf(stderr, "%s\n", errstr);
		free(errstr);
		return -1;
	}

	free(errstr);

	/* always swap data.
	   In case of read  -- BE will come from VME
	   In case of write -- it was swapped by us at the beginning,
	   so swap it back */
	swap_bytes(rd, eln, buf);
	swap_data(h, rd, eln, buf);

	return 0;
}

#endif	/* __linux__ */
