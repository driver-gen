#ifndef _DRIVER_ACCESS_H_INCLUDE_
#define _DRIVER_ACCESS_H_INCLUDE_

#include <time.h>		/* for time_t */
#include "GeneralHeader.h"
#include "RegDesc.h"
#include "ModuleHeader.h"	/* for 'DevInfo_t' and 'dbgIPL' */
#include "lklist.h"

/* library handle */
typedef long HANDLE;

/* how to access registers (access method) */
typedef enum _tagMETHOD {
	IOCTL  = 1,	/* through 'ioctl' calls */
	IOMMAP = 2,	/* I/O through mmaped memory */
	IODMA  = 4,     /* DMA access. Valid for Linux only.
                           If to use DMA on TSI148 */
	IOLAST = 8	/* SPECIAL CASE. ONLY for enabling all debug flags
			   in 'DaEnableAccess' call */
} METHOD;

/* operation to perform on the register */
typedef enum _tagREGOPTYPE {
	OPRD  = 1 << 0, /* read register */
	OPWR  = 1 << 1, /* write register */
	OPREP = 1 << 2,	/* repetitive bit */
	OPRDR = 5,	/* repetitive read register */
	OPWRR =	6	/* repetitive write register */
} REGOPTYPE;

/* debug flags to tune DAL behavior
   DBG_AVAL is the last one used by the driver flags */
typedef enum _tagDA_DBG {
	DA_DBG_ERR_INFO = 1 << (DBG_AVAL + 0),	 /* printout DAL error
						    information on the stderr */
	DA_DBG_DMA_RW_OPS = 1 << (DBG_AVAL + 1), /* trace dma read/write
						    operations */
	DA_DBG_LAST = DBG_AVAL + 1,	/* last bit used */
	DA_DBG_MASK = (DA_DBG_ERR_INFO | DA_DBG_DMA_RW_OPS),
	DA_DBG_ALL = DA_DBG_MASK	/* all DAL flags are enabled */
} da_dbg;

/* predefined DAL errors */
typedef enum _tagDA_ERR {
	DA_ERR_ALL_OK     = 0,	/* we fucken cool */
	DA_ERR_BAD_HANDLE = 1,	/* library handle is bad */
	DA_ERR_NO_REG     = 2,	/* no such register */
	DA_ERR_INT_ERR    = 3,	/* internal error. info is printed out
				   on stderr if DA_DBG_ERR_INFO debug flag
				   is set */
	DA_ERR_REG_BOUNDS = 4,	/* required elements are beyond register
				   bounds */
	DA_ERR_IOCTL_FAIL = 5,	/* ioctl call fails */
	DA_ERR_BAD_AM     = 6,	/* bad register access method */
	DA_ERR_NOT_INIT   = 7,	/* library was _not_ initialized.
				   i.e. DaEnableAccess() was not called */
	DA_ERR_B2S        = 8,		/* provided buffer is too small */
	DA_ERR_NOT_ALLOW  = 9,	/* requested operation not allowed */
	DA_ERR_INCOMPAT   = 10,	/* DAlib <-> driver are incompatible.
				   Driver was generated BEFORE current DAlib
				   and SHOULD be re-generated using
				   driverGen! */
	DA_ERR_ERR        = 11,	/* General DAL error. Depending on the function,
				   can have different meanings */
	DA_BAD_PARAM      = 12,	/* provided parameters are bad */
	DA_SYS_FAIL       = 13,	/* system call failed */
	DA_ERR_LAST
} da_err_t;

/* human error representation */
typedef struct _tagDA_ERR_STR {
	char da_err_code[32];	/* enum name */
	char da_err_str[128];	/* error meaning */
} da_err_str_t;

struct dal_dma {
        int fd; /* open file descriptor */
        void *addr[2]; /* VME address */
};

/* DAL user handle. Contains all needed module info */
typedef struct _tagDALH {
	struct list_head hndl_list;	/* handle linked list */
	HANDLE hID;		/* library handle (address of hID itself) */
	int drvrFd;		/* driver/simulator node descriptor */
        struct dal_dma dma;     /* for TSI148 DMA */
	int lun;		/* Logical Unit Number. Negative in case of
				   Simulator */
	METHOD defAm;		/* current default access method to module
				   registers */
	METHOD reqAm;		/* requested during EnableAccess()
				   Access Method */
	char *ma1;		/* mapped address pointer for Direct Register
				   Access
				   of 1'st address space. (-1 if none) */
	char *ma2;		/* mapped address pointer for Direct Register
				   Access
				   of 2'nd address space. (-1 if none) */
	char *uh_vers_info_str;	/* driver version information string */
	int uh_drvr_vers;	/* current driver version */
	time_t uh_dgd;		/* Driver Generation Date */
	DevInfo_t dit;		/* Device info table */
	REGDESC **reg_hash_tbl;	/* reg desc hash table */
	da_dbg dal_flag;	/* current debug flag */
} _dal_h;

#ifdef __cplusplus
extern "C" {
#endif
	HANDLE DaEnableAccess(char *, METHOD, int, int);
	da_err_t DaDisableAccess(HANDLE);
	da_err_t DaSetDefAM(HANDLE, METHOD);
	METHOD DaGetDefAM(HANDLE);
	int DaGetNodeFd(HANDLE);
	int DaGetRegister(HANDLE, int, void *, int);
	int DaGetRegChunk(HANDLE, int, int, unsigned int, void *, int);
	int DaSetRegister(HANDLE, int, void *, int);
	int DaSetRegChunk(HANDLE, int, int, unsigned int, void *, int);
	int DaGetRegRep(HANDLE, int, int, int, void *, int);
	int DaSetRegRep(HANDLE, int, int, int, void *, int);
	int DaGetRegDepth(HANDLE, int);
	int DaGetRegSize(HANDLE, int);
	ARIGHTS DaGetRegAccessRights(HANDLE, int);
	char *DaGetRegName(HANDLE, int);
	int DaDrvrVers(HANDLE, const char **, time_t *);
	int DaLibVers(HANDLE, const char **, time_t *);
	int DaDbgFlag(HANDLE, int, REGOPTYPE);
	da_err_str_t *DaGetErrCode(da_err_t);
	int DaRwBounds(HANDLE, int);

#ifdef __Lynx__ /* export handy functions for Lynx */
	int strnlen(const char *, size_t);
	int asprintf(char **, const char *, ...);
	int mkstemp(char *);
#endif /* __Lynx__ */

#ifdef __cplusplus
}
#endif
#endif	/* _DRIVER_ACCESS_H_INCLUDE_ */
