#ifndef _<mod_name>_IOCTL_ACCESS_H_INCLUDE_
#define _<mod_name>_IOCTL_ACCESS_H_INCLUDE_

#include "<sys_lower>UserDefinedAccess.h"

/* this API is obsolete! */
#warning WARNING! Deprecated library. Use DrvrAccess library instead.
