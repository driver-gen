#ifndef _<mod_name>_<drvr_type>_H_INCLUDE_
#define _<mod_name>_<drvr_type>_H_INCLUDE_

#ifdef __LYNXOS
/* only for kernel */
#include <dldd.h>
#include <kernel.h>
#include <kern_proto.h>

#include <errno.h>
#include <sys/types.h>
#include <conf.h>
#include <io.h>
#include <sys/ioctl.h>
#include <time.h>
#include <param.h>
#include <string.h>
#include <proto.h>
#include <proto_int.h>
#include <fcntl.h>
#include <dir.h>
#include <string.h>
#include <unistd.h>
#include <param.h>
#include <sys/stat.h>
#include <stdio.h>
#include <signal.h>
#include <sys/file.h>
#include <termio.h>
#include <termios.h>
#include <ces/vmelib.h>	  /* for 'pdparam_xxx' structure */
#include <inode.h>

/* system thread */
#include <conf.h>
#include <st.h>
#endif	/* __LYNXOS */

#include "<dg_free_fmt>/vme_am.h" /* for VMEbus Address Modifier Codes */
#include "<dg_free_fmt>/ServiceRegIoctl.h"
#include "<dg_free_fmt>/ModuleHeader.h"

#if defined(__linux__) && defined (__KERNEL__)
#undef sel
#define sel poll_table_struct	/* for select entry point */
/* only for Linux kernel */
#include <cdcm/cdcm.h>
#include "<dg_free_fmt>/swab-extra-linux.h"
#endif

/* for kernel only */
#if defined(__LYNXOS) || defined (__KERNEL__)
/* to be able to use CDCM utils inside the driver */
#include <cdcm/cdcmBoth.h>
#endif

/* Provides debug information printing. */
#define DBG_BEG(f_nm)							\
do {									\
  if (s->info->debugFlag & DBGIOCTL) {					\
    swait(&sem_drvr, SEM_SIGIGNORE); /* lock calling thread */		\
    cprintf("(pid %d) (tid %d) %s() ", getpid(), st_getstid(), f_nm);	\
  }									\
} while (0)

/* information about the address that will be read/write */
#define DBG_ADDR(ra, idx, en)					   \
do {								   \
  if (s->info->debugFlag & DBGIOCTL) {				   \
    unsigned addr = (unsigned)ra;				   \
    cprintf("[(elIdx %d) (elAm %d) @ 0x%x] ", idx, (int)en, addr); \
  }								   \
} while (0)

/* ioctl completion tag */
#define DBG_OK()							\
do {									\
	if (s->info->debugFlag & DBGIOCTL) {				\
		cprintf(" - OK.\n");					\
		ssignal(&sem_drvr);	/* wake up waiting thread */	\
	}								\
 } while (0)

/* ioctl completion tag */
#define DBG_FAIL()					\
do {							\
  if (s->info->debugFlag & DBGIOCTL) {			\
    cprintf(" - FAIL.\n");				\
    ssignal(&sem_drvr);	/* wake up waiting thread */	\
  }							\
} while (0)

/* start timing measurements. Different in case of Lynx/Linux */
#ifdef __Lynx__

#define START_TIME_STAT()								\
do {											\
  if (s->info->debugFlag & DBGTIMESTAT) {						\
    /* because of the spatial locality reference phenomenon, fill in cache		\
       memory so that it will keep recently accessed values (in our case		\
       it is 'nanotime' sys function stuff), thus trying to avoid additional		\
       time loss induced by cache miss.							\
       Normally, '__builtin_prefetch' gcc function should be used to minimize		\
       cache-miss latency by moving data into a cache before it is accessed.		\
       But with currently used Lynx gcc (version 2.95.3 20010323 (Lynx) as		\
       of 24.04.06) it's not possible, as it's probably not supported.			\
       For more info see gcc 4.0.1 reference manual at:					\
       http://gcc.gnu.org/onlinedocs/gcc-4.0.1/gcc/Other-Builtins.html#Other-Builtins	\
       So let's wait until newer gcc will be installed and then try the new		\
       approach. */									\
    GlobStartN = nanotime(&GlobStartS);							\
											\
    /* wait for the better times to implement... */					\
    /*__builtin_prefetch(&GlobStartN, 1, 1); */						\
											\
    /* now we can start real timing measurements */					\
    GlobStartN = nanotime(&GlobStartS);							\
  }											\
} while (0)

#else  /* linux */

#define START_TIME_STAT() \
	if (s->info->debugFlag & DBGTIMESTAT) getnstimeofday(&_t_beg)

#endif /* defined __Lynx__ */

/* printout measured time statistics */
#define FINISH_TIME_STAT()						\
do {									\
	if (s->info->debugFlag & DBGTIMESTAT) {				\
		unsigned int retN; /* for timing measurements */	\
		retN = ComputeTime(GlobTimeStatS);			\
		if (retN == -1)						\
			cprintf("Done in %s\n", GlobTimeStatS);		\
		else							\
			cprintf("Done in %dns (%s)\n", retN, GlobTimeStatS); \
	}								\
 } while (0)


#define <mod_name>_MAX_NUM_CARDS 21 /* VME crates have 21 slots max */

/* <mod_name> module ioctl numbers */
#define <mod_name>_IOCTL_MAGIC '<dg_char>'

#define <mod_name>_IO(nr)      _IO(<mod_name>_IOCTL_MAGIC, nr)
#define <mod_name>_IOR(nr,sz)  _IOR(<mod_name>_IOCTL_MAGIC, nr, sz)
#define <mod_name>_IOW(nr,sz)  _IOW(<mod_name>_IOCTL_MAGIC, nr, sz)
#define <mod_name>_IOWR(nr,sz) _IOWR(<mod_name>_IOCTL_MAGIC, nr, sz)

