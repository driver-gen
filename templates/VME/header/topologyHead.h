
/* <mod_name> module master topology */
typedef struct {
  <mod_name>Writeonly_t  *wo; /* write only registers last history */
  <mod_name>Extraneous_t *ex; /* extraneous registers */
