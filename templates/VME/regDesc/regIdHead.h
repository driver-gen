#ifndef _<mod_name>_REG_ID_H_INCLUDE_
#define _<mod_name>_REG_ID_H_INCLUDE_

#include "<dg_free_fmt>/ServiceRegId.h"

/* <mod_name> module registers ID. Used by DAL */
typedef enum _tag_<mod_name>_rid {
