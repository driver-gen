    default:
      printf("Please enter a valid menu item.\n\n<enter> to continue");
      getchar();
      break;
    }
  } /* end of 'for' */

  free(curModName);
  DaDisableAccess(handle); /* disconnect from the DAL library */
  return(EXIT_SUCCESS);	/* 0 */
}
