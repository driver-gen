#include <kernel.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "<sys_lower>Drvr.h"
#include "<sys_lower>Access.h"

#define _DRVR_NM_ "<mod_name>"


/*-----------------------------------------------------------------------------
 * FUNCTION:    LIBMatchEndian.
 * DESCRIPTION: This function converts data from Big Endian (MSB comes first)
 *		to Little Endian (MSB comes last).
 * RETURNS:	pointer to converted data.
 *-----------------------------------------------------------------------------
 */
static char*
LIBMatchEndian(
	       char *data,    /* pointer to data that should be converted */
	       int typeSize,  /* size of each element in bytes */
	       int dataDepth) /* number of elements */
{
  int  i, j;
  int  count;
  char *buffer = (char*)malloc(typeSize);

  if (dataDepth == 0)
    dataDepth++;

  for (j = 0; j < dataDepth; j++) {
    memcpy(buffer, data + (j * typeSize), typeSize);
    count = 0;
    for (i = typeSize - 1; i >= 0; i--)
      data[(j * typeSize) + count++] = buffer[i];
  }

  free(buffer);
  return(data);
}


/*-----------------------------------------------------------------------------
 * FUNCTION:    <sys_lower>GetConfig.
 * DESCRIPTION:
 * RETURNS:
 *-----------------------------------------------------------------------------
 */
int
<sys_lower>GetConfig(
		     int fd,		  /*  */
		     PciConfig_t *result) /*  */
{
  return(ioctl(fd, <mod_name>_GET_CONFIG, (char*)result));
}
