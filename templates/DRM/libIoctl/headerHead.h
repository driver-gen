#ifndef _<mod_name>_IOCTL_ACCESS_H_INCLUDE_
#define _<mod_name>_IOCTL_ACCESS_H_INCLUDE_

#include "<sys_lower>UserDefinedAccess.h"

/*  */
typedef struct {
  unsigned short deviceId;	/*  */
  unsigned short vendorId;	/*  */
  unsigned short status;	/*  */
  unsigned short command;	/*  */
  unsigned char	 classCode[3];	/*  */
  unsigned char	 revision;	/*  */
  unsigned char	 bist;		/*  */
  unsigned char	 headerType;	/*  */
  unsigned char	 latencyTimr;	/*  */
  unsigned char	 cacheLine;	/*  */
  unsigned long	 baseAddrs[6];	/*  */
  unsigned long	 cisPointer;	/*  */
  unsigned short subDeviceId;	/*  */
  unsigned short subVendorId;	/*  */
  unsigned long	 romAddr;	/*  */
  unsigned long	 reserved[2];	/*  */
  unsigned char	 maxLat;	/*  */
  unsigned char	 minGnt;	/*  */
  unsigned char	 intPin;	/*  */
  unsigned char	 intLine;	/*  */
} PciConfig_t;

