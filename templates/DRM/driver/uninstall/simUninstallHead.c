/*-----------------------------------------------------------------------------
 * FUNCTION:    <sys_lower>_uninstall.
 * DESCRIPTION: Entry point function. Deallocates shared memory and clears
 *		used interrupt vectors.
 * RETURNS:	OK     - if succeed.
 *		SYSERR - in case of failure.
 *-----------------------------------------------------------------------------
 */
int
<sys_lower>_uninstall(
		      <mod_name>Statics_t *s) /*  */
{
  int ps, i;

  /*  disable(ps); */

  kkprintf("<sys_lower>: Uninstall\n");

  for (i = 0; i < <mod_name>_MAX_NUM_CARDS; i++) {
    if (s->card[i] != NULL) {
      kkprintf("<sys_lower>:   Cleaning up Logical Unit %d\n", i);

      if (s->card[i]->wo != NULL) {
        kkprintf("<sys_lower>:     Writeonly structure deallocation - ");
        sysfree((char*)s->card[i]->wo, (long)sizeof(<mod_name>Writeonly_t));
        kkprintf("OK\n");
      }

      if (s->card[i]->ex != NULL) {
        kkprintf("<sys_lower>:     Extraneous structure deallocation - ");
        sysfree((char*)s->card[i]->ex, (long)sizeof(<mod_name>Extraneous_t));
        kkprintf("OK\n");
      }

      if (s->card[i]->config != NULL) {
        kkprintf("<sys_lower>:     PCI Config deallocation - ");
        sysfree((char*)s->card[i]->config, (long)sizeof(LowResPciConfig_t));
        kkprintf("OK\n");
      }

