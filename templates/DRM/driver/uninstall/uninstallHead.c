/*-----------------------------------------------------------------------------
 * FUNCTION:    <sys_lower>_uninstall.
 * DESCRIPTION: Entry point function. Deallocates shared memory and clears
 *		used interrupt vectors.
 * RETURNS:	OK     - if succeed.
 *		SYSERR - in case of failure.
 *-----------------------------------------------------------------------------
 */
int
<sys_lower>_uninstall(
		      register <mod_name>Statics_t *s) /*  */
{
  int ps;
  int result, i;

  disable(ps);

  kkprintf("<sys_lower>: Uninstall\n");

  for (i = 0; i < <mod_name>_MAX_NUM_CARDS; i++) {
    if (s->card[i] != NULL) {
      kkprintf("<sys_lower>:   Cleaning up Unit %d\n", i);

      kkprintf("<sys_lower>:     ISR decoupling - ");
      if (drm_unregister_isr(s->card[i]->handle) == SYSERR)
        kkprintf("FAILED\n");
      else
        kkprintf("OK\n");

