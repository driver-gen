      kkprintf("<sys_lower>Drvr:     PCI Config deallocation - ");
      sysfree((char*)s->card[i]->config, (long)sizeof(LowResPciConfig_t));
      kkprintf("OK\n");

      if (s->card[i]->ex != NULL) {
        kkprintf("<sys_lower>Drvr:     Extraneous structure deallocation - ");
        sysfree((char*)s->card[i]->ex, (long)sizeof(<mod_name>Extraneous_t));
        kkprintf("OK\n");
      }

      if (s->card[i]->wo != NULL) {
        kkprintf("<sys_lower>Drvr:     Writeonly structure deallocation - ");
        sysfree((char*)s->card[i]->wo, (long)sizeof(<mod_name>Writeonly_t));
        kkprintf("OK\n");
      }

      kkprintf("<sys_lower>Drvr:     Topology structure deallocation - ");
      sysfree((char*)s->card[i], (long)sizeof(<mod_name>Topology_t));
      kkprintf("OK\n");
    }
  }

  kkprintf("<sys_lower>Drvr:   All Unit specific resources have been deallocated\n");

  kkprintf("sharedTester:   Hardware array deallocation - ");
  sysfree((char*)s->card, (long)sizeof(<mod_name>Topology_t*) * <mod_name>_MAX_NUM_CARDS);
  kkprintf("OK\n");

  kkprintf("<sys_lower>Drvr:   Statics structure deallocation - ");
  sysfree((char*)s, (long)sizeof(<mod_name>Statics_t));
  kkprintf("OK\n");

  /* restore(ps); */
  kkprintf("<sys_lower>Drvr: Done.\n\n");

  return(OK);
}


