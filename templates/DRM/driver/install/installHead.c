/*-----------------------------------------------------------------------------
 * FUNCTION:    <sys_lower>_install.
 * DESCRIPTION: Entry point function. Initializes the hardware and allocates
 *		shared memory buffers.
 * RETURNS:	SYSERR - in case of failure.
 *-----------------------------------------------------------------------------
 */
char*
<sys_lower>_install(
		    register <mod_name>Info_t *info) /*  */
{
  register <mod_name>Statics_t *s;
  int cardsFound = -1;
  int badRead = 0;
  long pAddr, vAddr, size, sp, val;

  /*
    int ps;
    disable(ps);
  */

  DBG_install(("<sys_lower>: Install\n"));

  DBG_install(("<sys_lower>:   Statics structure allocation - "));
  if (!(s = (<mod_name>Statics_t *)sysbrk((long) sizeof(<mod_name>Statics_t))))
    {
      /* Horrible, impossible failure. */
      DBG_install(("FAILED\n"));
      pseterr(ENOMEM);
      return((char*)SYSERR); /* -1 */
    }
  bzero((char*)s, sizeof(<mod_name>Statics_t));
  DBG_install(("OK\n"));


  DBG_install(("<sys_lower>:   Hardware array allocation - "));
  if (!(s->card = (<mod_name>Topology_t**)sysbrk((long) sizeof(<mod_name>Topology_t *) * <mod_name>_MAX_NUM_CARDS)))
    {
      /* Horrible, impossible failure. */
      DBG_install(("FAILED\n"));
      pseterr(ENOMEM);
      return((char*)SYSERR); /* -1 */
    }

  bzero((char*)s->card, sizeof(<mod_name>Topology_t*) * <mod_name>_MAX_NUM_CARDS);
  DBG_install(("OK\n"));


  DBG_install(("<sys_lower>:   Scanning PCI\n"));


  while ((cardsFound < <mod_name>_MAX_NUM_CARDS) && drm_get_handle(PCI_BUSLAYER, info->vendorId, info->deviceId, &(s->card[cardsFound]->handle)))
  {
    DBG_install(("<sys_lower>:   Unit %d found\n", ++cardsFound));


    DBG_install(("<sys_lower>:     Topology structure allocation - "));
    if (!(s->card[cardsFound] = (<mod_name>Topology_t*)sysbrk((long)sizeof(<mod_name>Topology_t))))
    {
      /* Horrible, impossible failure. */
      DBG_install(("FAILED\n"));
      pseterr(ENOMEM);
      return((char*)SYSERR); /* -1 */
    }
    bzero((char*)s->card[cardsFound], sizeof(<mod_name>Topology_t));
    DBG_install(("OK\n"));


    /* Enable IO Space, Mem Space and Bus Mastering */

    DBG_install(("<sys_lower>:      Reading from CMDREG - "));
    if (!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_CMDREG, 0, 0, &val))
    {
      DBG_install(("FAILED\n"));
      pseterr(EFAULT);
      return((char*)SYSERR); /* -1 */
    }
    else
      DBG_install(("OK\n"));


    val |= (PCIHDR_CMDREG_IOSP | PCIHDR_CMDREG_MEMSP | PCIHDR_CMDREG_MASTR);

    DBG_install(("<sys_lower>:     Enabling of IO & Mem Space and Bus Mastering - "));
    if (!drm_device_write(s->card[cardsFound]->handle, PCI_RESID_CMDREG, 0, 0, &val))
    {
      DBG_install(("FAILED\n"));
      pseterr(EFAULT);
      return((char*)SYSERR); /* -1 */
    }
    else
      DBG_install(("OK\n"));


    if (sizeof(<mod_name>Writeonly_t) > 0) {
      DBG_install(("<sys_lower>:     Writeonly structure allocation - "));
      if (!(s->card[cardsFound]->wo = (<mod_name>Writeonly_t*)sysbrk((long)sizeof(<mod_name>Writeonly_t))))
      {
	/* Horrible, impossible failure. */
        DBG_install(("FAILED\n"));
        pseterr(ENOMEM);
        return((char *)SYSERR); /* -1 */
      }
      bzero((char*)s->card[cardsFound]->wo, sizeof(<mod_name>Writeonly_t));
      DBG_install(("OK\n"));
    }
    else
      s->card[cardsFound]->wo = NULL;


    if (sizeof(<mod_name>Extraneous_t) > 0) {
      DBG_install(("<sys_lower>:     Extraneous structure allocation - "));
      if (!(s->card[cardsFound]->ex = (<mod_name>Extraneous_t*)sysbrk((long)sizeof(<mod_name>Extraneous_t))))
	{
	  /* Horrible, impossible failure. */
	  DBG_install(("FAILED\n"));
	  pseterr(ENOMEM);
	  return((char *)SYSERR);	/* -1 */
	}
      bzero((char*)s->card[cardsFound]->ex, sizeof(<mod_name>Extraneous_t));
      DBG_install(("OK\n"));
    }
    else
      s->card[cardsFound]->ex = NULL;


    DBG_install(("<sys_lower>:     PCI Config structure allocation - "));
    if (!(s->card[cardsFound]->config = (LowResPciConfig_t *)sysbrk((long)sizeof(LowResPciConfig_t))))
      {
	/* Horrible, impossible failure. */
	DBG_install(("FAILED\n"));
	pseterr(ENOMEM);
	return((char *)SYSERR);	/* -1 */
      }
    bzero((char*)s->card[cardsFound]->config, sizeof(LowResPciConfig_t));
    DBG_install(("OK\n"));


    DBG_install(("<sys_lower>:     Reading of PCI Config data - "));

    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0x0, 4, &(s->card[cardsFound]->config->id))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0x1, 4, &(s->card[cardsFound]->config->status_command))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0x2, 4, &(s->card[cardsFound]->config->class))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0x3, 4, &(s->card[cardsFound]->config->special))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0x4, 4, &(s->card[cardsFound]->config->io_addr))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0x5, 4, &(s->card[cardsFound]->config->mem_addr))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0x6, 4, &(s->card[cardsFound]->config->reserved[0]))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0x7, 4, &(s->card[cardsFound]->config->reserved[1]))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0x8, 4, &(s->card[cardsFound]->config->reserved[2]))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0x9, 4, &(s->card[cardsFound]->config->reserved[3]))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0xa, 4, &(s->card[cardsFound]->config->reserved[4]))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0xb, 4, &(s->card[cardsFound]->config->reserved[5]))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0xc, 4, &(s->card[cardsFound]->config->reserved[6]))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0xd, 4, &(s->card[cardsFound]->config->reserved[7]))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0xe, 4, &(s->card[cardsFound]->config->reserved[8]))){badRead = 1;}
    if(!drm_device_read(s->card[cardsFound]->handle, PCI_RESID_REGS, 0xf, 4, &(s->card[cardsFound]->config->interrupt))){badRead = 1;}

    if(badRead)
      DBG_install(("FAILED\n"));
    else
      DBG_install(("OK\n"));


