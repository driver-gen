/*-----------------------------------------------------------------------------
 * FUNCTION:    <sys_lower>_install.
 * DESCRIPTION: Entry point function. Initializes the hardware and allocates
 *		shared memory buffers.
 * RETURNS:	SYSERR - in case of failure.
 *-----------------------------------------------------------------------------
 */
char*
<sys_lower>_install(
		    <mod_name>Info_t *info) /*  */
{
  register <mod_name>Statics_t *s;
  int ps;

  /* disable(ps); */

  kkprintf("<sys_lower>Drvr: Install\n");

  kkprintf("<sys_lower>Drvr:   Statics structure allocation - ");
  if (!(s = (<mod_name>Statics_t *)sysbrk((long)sizeof(<mod_name>Statics_t))))
    {
      /* Horrible, impossible failure. */
      kkprintf("FAILED\n");
      pseterr(ENOMEM);
      return((char*)SYSERR); /* -1 */
    }
  kkprintf("OK\n");


  kkprintf("<sys_lower>Drvr:   Hardware array allocation - ");
  if(!(s->card = (<mod_name>Topology_t**)sysbrk((long)sizeof(<mod_name>Topology_t *) * <mod_name>_MAX_NUM_CARDS)))
    {
      /* Horrible, impossible failure. */
      kkprintf("FAILED\n");
      pseterr(ENOMEM);
      return((char*)SYSERR); /* -1 */
    }
  bzero((char*)s->card, sizeof(<mod_name>Topology_t*) * <mod_name>_MAX_NUM_CARDS);
  kkprintf("OK\n");

  kkprintf("<sys_lower>Drvr: Done\n\n");

  return((char*)s);
}


