/*
    kkprintf("<sys_lower>Drvr:     ISR mapping - ");
    if(drm_register_isr(s->card[cardsFound]->handle, <sys_lower>Isr, (void *) s) == SYSERR)
      kkprintf("FAILED\n");

    kkprintf("OK\n");
*/

  }

  if (cardsFound == -1) {
    kkprintf("<sys_lower>Drvr:   No devices found\n");
    pseterr(ENODEV);
    return((char*)SYSERR); /* -1 */
  }

  kkprintf("<sys_lower>Drvr:  Scan complete\n");

  eieio();
  kkprintf("<sys_lower>Drvr: Done\n\n");

  return((char*)s);
}


