/* Service Register. Get device's PCI config structure. */
static int
getSRV__Config(
	  register <mod_name>Statics_t *s, /*  */
	  int  lun,	/*  */
	  char *result)	/*  */
{
  DBG_BEG(__FUNCTION__);

  if (wbounds((unsigned long) result) < (sizeof(DevInfo_t))) {
    DBG_FAIL();
    pseterr(EFAULT);
    return(SYSERR); /* -1 */
  }

  DBG_ADDR(&(s->card[lun]->config), 0, 1, 0); /* info to the user */

  memcpy(result, s->card[lun]->config, sizeof(LowResPciConfig_t));

  DBG_OK();
  return(OK); /* 0 */
}
