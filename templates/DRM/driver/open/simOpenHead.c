/*-----------------------------------------------------------------------------
 * FUNCTION:    <sys_lower>_open.
 * DESCRIPTION: Entry point function. Initializes minor devices.
 * RETURNS:	OK     - if succeed.
 *		SYSERR - in case of failure.
 *-----------------------------------------------------------------------------
 */
int
<sys_lower>_open(
		 register <mod_name>Statics_t *statP, /*  */
		 int devno, /* contains major/minor dev numbers */
		 struct file *flp) /* file pointer */
{
  int lun = minor(devno); /* get minor device number */

  if (lun >= <mod_name>_MAX_NUM_CARDS) {
    pseterr(ENXIO);
    return(SYSERR); /* -1 */
  }

  if (statP->card[lun] == NULL) {
    kkprintf("<sys_lower>: Open Logical Unit %d\n", lun);
    kkprintf("<sys_lower>: Topology structure allocation - ");
    if (!(statP->card[lun] = (<mod_name>Topology_t *)sysbrk((long)sizeof(<mod_name>Topology_t))))
      {
	/* Horrible, impossible failure. */
	kkprintf("FAILED\n");
	pseterr(ENOMEM);
	return(SYSERR);		/* -1 */
      }

    kkprintf("OK\n");

