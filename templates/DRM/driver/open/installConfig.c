    kkprintf("<sys_lower>: PCI Config allocation - ");
    if(!(statP->card[lun]->config = (LowResPciConfig_t *)sysbrk((long) sizeof(LowResPciConfig_t))))
    {
      /* Horrible, impossible failure. */
      kkprintf("FAILED\n");
      pseterr(ENOMEM);
      return(SYSERR); /* -1 */
    }
    bzero((char*)statP->card[lun]->config, sizeof(LowResPciConfig_t));
    kkprintf("OK\n");

