
    if (sizeof(<mod_name>Writeonly_t) > 0) {
      kkprintf("<sys_lower>Drvr: Writeonly structure allocation - ");
      if (!(statP->card[lun]->wo = (<mod_name>Writeonly_t*)sysbrk((long)sizeof(<mod_name>Writeonly_t))))
	{
	  /* Horrible, impossible failure. */
	  kkprintf("FAILED\n");
	  pseterr(ENOMEM);
	  return(SYSERR); /* -1 */
	}
      bzero((char*)statP->card[lun]->wo, sizeof(<mod_name>Writeonly_t));
      kkprintf("OK\n");
    }
    else
      statP->card[lun]->wo = NULL;


    if (sizeof(<mod_name>Extraneous_t) > 0) {
      kkprintf("<sys_lower>Drvr: Extraneous structure allocation - ");
      if (!(s->card[lun]->ex = (<mod_name>Extraneous_t*)sysbrk((long)sizeof(<mod_name>Extraneous_t))))
	{
	  /* Horrible, impossible failure. */
	  kkprintf("FAILED\n");
	  pseterr(ENOMEM);
	  return(SYSERR);	/* -1 */
	}
      bzero((char*)statP->card[lun]->ex, sizeof(<mod_name>Extraneous_t));
      kkprintf("OK\n");
    }
    else
      statP->card[lun]->ex = NULL;

    kkprintf("<sys_lower>Drvr: Done\n\n");
  }
  return(OK); /* 0 */
}


