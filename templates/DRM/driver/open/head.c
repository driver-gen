/**
 * @brief Entry point function. Initializes minor devices.
 *
 * @param statPtr - statics table pointer
 * @param devno   - contains major/minor dev numbers
 * @param flp     - file pointer
 *
 * @return OK     - if succeed.
 * @return SYSERR - in case of failure.
 */
int <sys_lower>_open(register <mod_name>Statics_t *statPtr, int devno, struct file *flp)
{
  int lun = minor(devno); /* get minor device number */
  int proceed;	/* if standard code execution should be proceed after call to
		   user entry point function  */
  int usrcoco;	/* completion code of user entry point function */

  if (lun >= <mod_name>_MAX_NUM_CARDS) {
    pseterr(ENXIO);
    return(SYSERR); /* -1 */
  }

  if (statPtr->card[lun] == NULL) {
    pseterr(ENXIO);
    return(SYSERR); /* -1 */
  }

  /* user entry point function call */
  usrcoco = <sys_lower>UserOpen(&proceed, statP, devno, flp);
  if (!proceed) /* all done by user */
    return(usrcoco);

  return(OK); /* 0 */
}


