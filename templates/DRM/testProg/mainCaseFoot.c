      case (<plain_num> + 1):
        pciConfig(<sys_lower>);
      break;
      default:
        printf("Please enter a valid menu item.\n\n<enter> to continue");
        getchar();
      break;
    }
  } /* end of 'for' */

  free(curModName);
  DaDisableAccess(handle); /* disconnect from the DAL library */
  return(EXIT_SUCCESS);	/* 0 */
}


/*-----------------------------------------------------------------------------
 * FUNCTION:    pciConfig.
 * DESCRIPTION:
 * RETURNS:
 *-----------------------------------------------------------------------------
 */
int
pciConfig(
          int lun) /*  */
{

  PciConfig_t config;
  int i;

  <sys_lower>GetConfig(lun, &config);

  printf("%s<mod_name> %s <V. %d> Test Program - PCI Config\n[%s]\n", ClearScreen, (lun < 0)?"Simulator":"Driver", g_drvrVers, g_drvrVersInfo);
  for (i = 0; i < screenWidth(); i++)
    printf("-");

  printf("\n\nVendor ID     = 0x%04X\n", config.vendorId);
  printf("Device ID     = 0x%04X\n", config.deviceId);
  printf("Command       = 0x%04X\n", config.command);
  printf("Status        = 0x%04X\n", config.status);
  printf("Revision ID   = 0x%04X\n", config.revision);

  printf("Class Code\n");
  printf("\tBase Class = 0x%02X\n", config.classCode[0]);
  printf("\tSub Class  = 0x%02X\n", config.classCode[1]);
  printf("\tInterface  = 0x%02X\n\n", config.classCode[2]);

  printf("Cache Line    = 0x%02X\n", config.cacheLine);
  printf("Latency Timer = 0x%02X\n", config.latencyTimr);
  printf("Header Type   = 0x%02X\n", config.headerType);
  printf("BIST          = 0x%02X\n", config.bist);

  printf("Base Addresses\n");
  printf("\tBlock 0 = 0x%08X\n", config.baseAddrs[0]);
  printf("\tBlock 1 = 0x%08X\n", config.baseAddrs[1]);
  printf("\tBlock 2 = 0x%08X\n", config.baseAddrs[2]);
  printf("\tBlock 3 = 0x%08X\n", config.baseAddrs[3]);
  printf("\tBlock 4 = 0x%08X\n", config.baseAddrs[4]);
  printf("\tBlock 5 = 0x%08X\n\n", config.baseAddrs[5]);

  printf("CIS Pointer   = 0x%08X\n", config.cisPointer);
  printf("Sub Vendor ID = 0x%4X\n", config.subVendorId);
  printf("Sub Device ID = 0x%4X\n", config.subDeviceId);
  printf("ROM Base Addr = 0x%08x\n", config.romAddr);

  printf("Reserved\n");
  printf("\tReg 0 = 0x%08X\n", config.reserved[0]);
  printf("\tReg 1 = 0x%08X\n\n", config.reserved[1]);

  printf("Int Line      = 0x%02X\n", config.intLine);
  printf("Int Pin       = 0x%02X\n", config.intPin);
  printf("Min_Gnt       = 0x%02X\n", config.minGnt);
  printf("Max_Lat       = 0x%02X\n", config.maxLat);

  printf("\n<enter> to continue\n");
  getchar();
  return(1);
}


