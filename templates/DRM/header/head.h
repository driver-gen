#define <mod_name>_MAX_NUM_CARDS 8

/*  */
typedef struct {
   long id;			/*  */
   long status_command;		/*  */
   long class;			/*  */
   long special;		/*  */
   long io_addr;		/*  */
   long mem_addr;		/*  */
   long reserved[9];		/*  */
   long interrupt;		/*  */
} LowResPciConfig_t;

/* <mod_name> module ioctl numbers */
