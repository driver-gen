
/*  */
typedef struct {
  long vendorId;
  long deviceId;
} <mod_name>Info_t;


/*  */
typedef struct {
  struct drm_node_s *handle;	/*  */
  LowResPciConfig_t *config;	/*  */
  <mod_name>Writeonly_t  *wo;	/*  */
  <mod_name>Extraneous_t *ex;	/*  */
