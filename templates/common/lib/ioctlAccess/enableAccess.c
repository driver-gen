
/**
 * @brief Open device driver node
 *
 * @param lun   -- Logical Unit Number assigned to the module. Negative in case
 *                 of driver simulator
 * @param chanN -- Minor Device Number. There can be several entry points for
 *                 current Logical Unit Number (ChannelNumber).
 *
 * @return Open file decriptor (normally >= 3) - if success
 * @return -1                                  - if fails
 *                                               Error message is printing out.
 */
int <sys_lower>EnableAccess(int lun, int chanN)
{
	int  fd;			/* open file descriptor */
	char fileName[0x100];		/* device file name */
	char *tmp;

	if (!MODULE_NAME_OK(_DRVR_NM_)) {
		fprintf(stderr, "Spurious Module Name '%s'.\n"
			"Normally _should not_ contain any lowercase"
			" letters!\n",  _DRVR_NM_);
		return -1;
	}

	tmp = _ncf(_DRVR_NM_);
	sprintf(fileName, "/dev/" NODE_NAME_FMT,
		tmp, (lun < 0)?_SIML_:_DRVR_, abs(lun), chanN);
	free(tmp);
	if ((fd = open(fileName, O_RDWR)) < 0) { /* error */
		perror(NULL);
		fprintf(stderr, "Error [%s] in <sys_lower>EnableAccess()"
			" while opening '%s' file.\nCheck if '%s' module is"
			" installed.\n", strerror(errno), fileName, _DRVR_NM_);
		return -1;
	}

	return fd;
}

/**
 * @brief  Close driver file descriptor.
 *
 * @param fd -- open file descriptor, retuned by <sys_lower>EnableAccess call
 *
 * @return void
 */
void <sys_lower>DisableAccess(int fd)
{
	close(fd);
}

