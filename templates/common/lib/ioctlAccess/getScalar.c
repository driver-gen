/**
 * @brief
 *
 * @param fd     -- driver node descriptor
 * @param result -- buffer to put results
 *
 * @return  0 - on success.
 * @return -1 - if error occurs. errno is set appropriately.
 */
int <sys_lower>Get<reg_name>(
			     int fd,
			     unsigned <reg_type> *result)
{
	unsigned long arguments[3];

	/* pack ioctl args */
	arguments[0] = (unsigned long) result; /* where to put results */
	arguments[1] = 1;		       /* number of elements to read */
	arguments[2] = 0;		       /* element index */

	/* driver call */
	if (ioctl(fd, <ioctl_const>, (char*)arguments))
		return -1;

	/* handle endianity */
	matchEndian((char*)result, sizeof(unsigned <reg_type>), 0);

	return 0;
}

