/**
 * @brief
 *
 * @param fd     -- driver node descriptor
 * @param elOffs -- element offset (expressed in elements)
 * @param depth  -- number of elemets to get
 * @param result -- buffer for the results
 *
 * @return  0 - on success.
 * @return -1 - if error occurs. errno is set appropriately.
 */
int <sys_lower>GetLastHisWindow<reg_name>(
					  int fd,
					  unsigned int elOffs,
					  unsigned int depth,
					  unsigned <reg_type> *result)
{
	unsigned long arguments[3];

	/* pack ioctl args */
	arguments[0] = (unsigned long) result;
	arguments[1] = depth;
	arguments[2] = elOffs;

	/* driver call */
	if (ioctl(fd, <ioctl_const>, (char*)arguments))
		return -1;

	/* handle endianity */
	matchEndian((char*)result, sizeof(unsigned <reg_type>), <reg_depth>);

	return 0;
}

/**
 * @brief
 *
 * @param fd     -- driver node descriptor
 * @param result -- buffer to put results
 *
 * @return  0 - on success.
 * @return -1 - if error occurs. errno is set appropriately.
 */
int <sys_lower>GetLastHis<reg_name>(
				    int fd,
				    unsigned <reg_type> result[<reg_depth>])
{
	return <sys_lower>GetLastHisWindow<reg_name>(fd, 0, <reg_depth>, result);
}

