/**
 * @brief
 *
 * @param fd  -- driver node descriptor
 * @param arg -- new values
 *
 * @return  0 - on success.
 * @return -1 - if error occurs. errno is set appropriately.
 */
int <sys_lower>Set<reg_name>(
			     int fd,
			     unsigned <reg_type> arg)
{
	unsigned long arguments[3];

	/* pack ioctl args */
	arguments[0] = (unsigned long) &arg; /* where to take data from */
	arguments[1] = 1;		     /* number of elements write */
	arguments[2] = 0;		     /* element index */

	/* handle endianity */
	matchEndian((char*)&arg, sizeof(unsigned <reg_type>), 0);

	return ioctl(fd, <ioctl_const>, (char *)arguments);

}

