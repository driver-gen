
#ifdef __cplusplus
extern "C" {
#endif

/* see <sys_lower>IoctlAccess.c for precise parameter description */

int  <sys_lower>EnableAccess(int, int); /* open  Device driver */
void <sys_lower>DisableAccess(int);     /* close Device driver */

