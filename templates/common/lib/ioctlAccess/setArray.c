/**
 * @brief
 *
 * @param fd     -- driver node descriptor
 * @param elOffs -- element offset (expressed in elements)
 * @param depth  -- number of elemets to set
 * @param arg    -- buffer holds new values
 *
 * @return  0 - on success.
 * @return -1 - if error occurs. errno is set appropriately.
 */
int <sys_lower>SetWindow<reg_name>(
				   int fd,
				   unsigned int elOffs,
				   unsigned int depth,
				   unsigned <reg_type> *arg)
{
	unsigned long arguments[3];

	/* pack ioctl args */
	arguments[0] = (unsigned long) arg; /* where to take data from */
	arguments[1] = depth;               /* number of elements write */
	arguments[2] = elOffs;              /* element index */

	/* handle endianity */
	matchEndian((char*)arg, sizeof(unsigned <reg_type>), <reg_depth>);

	/* driver call */
	return ioctl(fd, <ioctl_const>, (char *)arguments);
}

/**
 * @brief
 *
 * @param fd  -- driver node descriptor
 * @param arg -- buffer holds new values
 *
 * @return  0 - on success.
 * @return -1 - if error occurs. errno is set appropriately.
 */
int <sys_lower>Set<reg_name>(
			     int fd,
			     unsigned <reg_type> arg[<reg_depth>])
{
	return <sys_lower>SetWindow<reg_name>(fd, 0, <reg_depth>, arg);
}


