#include "<sys_lower>UserDefinedAccess.h"

/*
   If you want to implement your own library for module accessing, than you
   should do this in this file. API function prototypes should be declared
   in <sys_lower>UserDefinedAccess.h file.
*/
