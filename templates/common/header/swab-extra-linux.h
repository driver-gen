/**
 * @file swab-extra-linux.h
 *
 * @brief Extra swapping functions.
 *
 * @author Copyright (C) 2009 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 18/09/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifndef _SWAB_EXTRA_LINUX_H_INCLUDE_
#define _SWAB_EXTRA_LINUX_H_INCLUDE_

#include <linux/version.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,29)
/* extra swap operations defined here */
#include <linux/byteorder/swabb.h>
#endif

/**
 * __swahl64 - return a long dword-swapped 64-bit value
 * @x: value to long-dwordswap
 *
 * __swahl64(0x1234 5678 12ab cdef) is 0x12ab cdef 1234 5678
 */
#define __swahl64(x) ((__u64)(					\
        (((__u64)(x) & (__u64)0x00000000ffffffffULL) << 32) |	\
        (((__u64)(x) & (__u64)0xffffffff00000000ULL) >> 32)))

/**
 * __swahw64 - return a dword-swapped 64-bit value
 * @x: value to dwordswap
 *
 * __swahw64(0x1234 5678 12ab cdef) is 0xcdef 12ab 5678 1234
 */
#define __swahw64(x) ((__u64)(					\
        (((__u64)(x) & (__u64)0x000000000000ffffULL) << 48) |	\
        (((__u64)(x) & (__u64)0x00000000ffff0000ULL) << 32) |	\
        (((__u64)(x) & (__u64)0x0000ffff00000000ULL) >> 32) |	\
        (((__u64)(x) & (__u64)0xffff000000000000ULL) >> 48)))


static inline __u64 __swahl64p(const __u64 *p)
{ return __swahl64(*p); }

static inline __u64 __swahw64p(const __u64 *p)
{ return __swahw64(*p); }

/**
 * swahl64s - dwordswap a 64-bit value in-place
 * @p: pointer to a naturally-aligned 64-bit value
 *
 * See __swahl64() for details of high and low dword swapping
 */
static inline void swahl64s(__u64 *p)
{ *p = __swahl64p(p); }

/**
 * swahw64s - wordswap a 64-bit value in-place
 * @p: pointer to a naturally-aligned 64-bit value
 *
 * See __swahw64() for details of word swapping
 */
static inline void swahw64s(__u64 *p)
{ *p = __swahw64p(p); }

#endif	/* _SWAB_EXTRA_LINUX_H_INCLUDE_ */
