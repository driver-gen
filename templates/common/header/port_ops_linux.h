/**
 * @file port_ops_linux.h
 *
 * @brief Lynx port_ops_ppc.h emulation.
 *
 * @b NOTE, that all VME devices are considered to be BE only!
 *
 * This file is used by kernel and user space.
 * User space is mmap reading hw registers.
 *
 * @author Copyright (C) 2009 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 29/05/2009
 */
#ifndef _PORT_OPS_LINUX_H_INCLUDE_
#define _PORT_OPS_LINUX_H_INCLUDE_

#ifdef __KERNEL__

#include <asm-generic/iomap.h> 	/* for ioread/iowrite[8,16,32][be, le] */
#include <linux/delay.h>	/* for udelay() */
typedef void __iomem* __port_addr_t;

#else  /* user space needs it */

#include <elf.h>    /* for endianity business */

/*
 * From Linux Kernel Source's compiler.h -- GPL.
 * Generic compiler-dependent macros.
 */
#if __GNUC__ > 3
# define likely(x) __builtin_expect(!!(x), 1)
# define unlikely(x) __builtin_expect(!!(x), 0)
#else
# define likely(x) (x)
# define unlikely(x) (x)
#endif
#define udelay usleep
/* TODO Support 64bit addresses */
typedef unsigned long* __port_addr_t;

//!< swap bytes
static __inline__ void __endian(const void *src, void *dest, unsigned int size)
{
	unsigned int i;
	for (i = 0; i < size; i++)
		((unsigned char*)dest)[i] = ((unsigned char*)src)[size - i - 1];
}

/**
 * @brief To find out current endianity.
 *
 * @param none
 *
 * @return current endianity - in case of success,
 *         or generate an abnormal process abort - in case of failure.
 */
static inline int __my_endian(void)
{
	static int my_endian = ELFDATANONE;
	union { short s; char c[2]; } endian_test;

	if (my_endian != ELFDATANONE)	/* already defined */
		return my_endian;

	endian_test.s = 1;
	if (endian_test.c[1] == 1) my_endian = ELFDATA2MSB;
	else if (endian_test.c[0] == 1) my_endian = ELFDATA2LSB;
	else abort();

	return my_endian;
}

/*! @name BE <-> LE convertion
 */
//@{
#define _ENDIAN(x)				\
({						\
  typeof(x) __x;				\
  typeof(x) __val = (x);			\
    __endian(&(__val), &(__x), sizeof(__x));	\
  __x;						\
})
//@}

#endif	/* user space defs */

/*
  These are prototypes and inline functions that operate on ports on the PPC

  __inb			read a byte from a port
  __inw			read a word from a port
  __inl			read a long from a port
  __outb		write a byte to a port
  __outw		write a word to a port
  __outl		write a long to a port
  __rep_inb		read multiple bytes from a port into a buffer
  __rep_inw		read multiple words from a port into a buffer
  __rep_inl		read multiple longs from a port into a buffer
  __rep_outb		write multiple bytes to a port from a buffer
  __rep_outw		write multiple words to a port from a buffer
  __rep_outl		write multiple longs to a port from a buffer
  __port_orb		read a byte, or it with a byte, and write it
  __port_orw		read a word, or it with a word, and write it
  __port_orl		read a long, or it with a long, and write it
  __port_andb		read a byte, and it with a byte, and write it
  __port_andw		read a word, and it with a word, and write it
  __port_andl		read a long, and it with a long, and write it

  Following extra functions are defined (DMA can be used to r/w HW):
  ------------------------------------------------------------------
  __inb_delay	-- read single byte from a single port with delay
  __inw_delay	-- read single word from a single port with delay
  __inl_delay	-- read single long from a single port with delay
  __outb_delay	-- write single byte to a single port with delay
  __outw_delay	-- write single word to a single port with delay
  __outl_delay	-- write single long to a single port with delay

  __rep_inb_delay  -- read multiple bytes from a single port with delay
  __rep_inw_delay  -- read multiple words from a single port with delay
  __rep_inl_delay  -- read multiple longs from a single port with delay
  __rep_outb_delay -- write multiple bytes to a single port with delay
  __rep_outw_delay -- write multiple words to a single port with delay
  __rep_outl_delay -- write multiple longs to a single port with delay

  __rep_inb_delay_shift  -- read bytes from consecutive ports with delay
  __rep_inw_delay_shift  -- read words from consecutive ports with delay
  __rep_inl_delay_shift  -- read longs from consecutive ports with delay
  __rep_outb_delay_shift -- write bytes to consecutive ports with delay
  __rep_outw_delay_shift -- write words to consecutive ports with delay
  __rep_outl_delay_shift -- write longs to consecutive ports with delay
*/

static unsigned char  __inb	(__port_addr_t);
static unsigned short __inw	(__port_addr_t);
static unsigned long  __inl	(__port_addr_t);
static void __outb		(__port_addr_t, unsigned char);
static void __outw		(__port_addr_t, unsigned short);
static void __outl		(__port_addr_t, unsigned long);
static void __rep_inb		(__port_addr_t, unsigned char *, int);
static void __rep_inw		(__port_addr_t, unsigned short *, int);
static void __rep_inl		(__port_addr_t, unsigned long *, int);
static void __rep_outb		(__port_addr_t, unsigned char *, int);
static void __rep_outw		(__port_addr_t, unsigned short *, int);
static void __rep_outl		(__port_addr_t, unsigned long *, int);
static void __port_orb		(__port_addr_t, unsigned char);
static void __port_orw		(__port_addr_t, unsigned short);
static void __port_orl		(__port_addr_t, unsigned long);
static void __port_andb		(__port_addr_t, unsigned char);
static void __port_andw		(__port_addr_t, unsigned short);
static void __port_andl		(__port_addr_t, unsigned long);

static __inline__ unsigned char
__inb(__port_addr_t port)
{
#ifdef _DG_SIM
	unsigned char value;
	value = *((unsigned char *)(port));

#ifndef __KERNEL__
	value = _ENDIAN(value);
#endif
	return value;
#else
	return (unsigned char)ioread8(port);
#endif
}

static __inline__ unsigned short
__inw(__port_addr_t port)
{
#ifdef _DG_SIM
	unsigned short value;
	value = *((unsigned short *)(port));

#ifndef __KERNEL__
	value = _ENDIAN(value);
#endif

	return value;
#else
	return (unsigned short)ioread16be(port);
#endif
}

static __inline__ unsigned long
__inl(__port_addr_t port)
{
#ifdef _DG_SIM
	unsigned long value;
	value = *((unsigned long *)(port));

#ifndef __KERNEL__
	value = _ENDIAN(value);
#endif

	return value;
#else
	return (unsigned long)ioread32be(port);
#endif
}

static __inline__ void
__outb(__port_addr_t port, unsigned char value)
{
#ifdef _DG_SIM

#ifndef __KERNEL__
	value = _ENDIAN(value);
#endif

	*((unsigned char *)(port)) = (value);
#else
	iowrite8((u8)value, port);
#endif
}

static __inline__ void
__outw(__port_addr_t port, unsigned short value)
{
#ifdef _DG_SIM

#ifndef __KERNEL__
	value = _ENDIAN(value);
#endif

	*((unsigned short *)(port)) = (value);
#else
	iowrite16be((u16)value, port);
#endif
}

static __inline__ void
__outl(__port_addr_t port, unsigned long value)
{
#ifdef _DG_SIM

#ifndef __KERNEL__
	value = _ENDIAN(value);
#endif

	*((unsigned long *)(port)) = (value);
#else
	iowrite32be((u32)value, port);
#endif
}

static __inline__ void
__rep_inb(__port_addr_t port, unsigned char *buffer, int count)
{
#ifdef _DG_SIM

#ifndef __KERNEL__
	int _cntr = count;
	unsigned char *_buf = buffer;
#endif
	while (count--) *buffer++ = *((unsigned char *)(port));
#ifndef __KERNEL__
	while (_cntr--) { *_buf = _ENDIAN(*_buf); _buf++; }
#endif

#else
	while (count--) *buffer++ = ioread8(port);
#endif
}

static __inline__ void
__rep_inw(__port_addr_t port, unsigned short *buffer, int count)
{
#ifdef _DG_SIM
#ifndef __KERNEL__
	int _cntr = count;
	unsigned short *_buf = buffer;
#endif
	while (count--) *buffer++ = *((unsigned short *)(port));
#ifndef __KERNEL__
	while (_cntr--) { *_buf = _ENDIAN(*_buf); _buf++; }
#endif

#else
	while (count--) *buffer++ = ioread16be(port);
#endif
}

static __inline__ void
__rep_inl(__port_addr_t port, unsigned long *buffer, int count)
{
#ifdef _DG_SIM
#ifndef __KERNEL__
	int _cntr = count;
	unsigned long *_buf = buffer;
#endif
	while (count--) *buffer++ = *((unsigned long *)(port));
#ifndef __KERNEL__
	while (_cntr--) { *_buf = _ENDIAN(*_buf); _buf++; }
#endif

#else
	while (count--) *buffer++ = ioread32be(port);
#endif
}

static __inline__ void
__rep_outb(__port_addr_t port, unsigned char *buffer, int count)
{
#ifdef _DG_SIM

#ifndef __KERNEL__
	unsigned char lbuf;
#endif
	while (count--) {

#ifndef __KERNEL__
		lbuf = _ENDIAN(*buffer);
		*((unsigned char *)(port)) = lbuf;
		buffer++;
#else
		*((unsigned char *)(port)) = *buffer++;
#endif
	}

#else
	while (count--) iowrite8((u8)*buffer++, port);
#endif
}

static __inline__ void
__rep_outw(__port_addr_t port, unsigned short *buffer, int count)
{
#ifdef _DG_SIM

#ifndef __KERNEL__
	unsigned short lbuf;
#endif
	while (count--) {
#ifndef __KERNEL__
		lbuf = _ENDIAN(*buffer);
		*((unsigned short *)(port)) = lbuf;
		buffer++;
#else
		*((unsigned short *)(port)) = *buffer++;
#endif
	}

#else
	while (count--) iowrite16be((u16)*buffer++, port);
#endif
}

static __inline__ void
__rep_outl(__port_addr_t port, unsigned long *buffer, int count)
{
#ifdef _DG_SIM

#ifndef __KERNEL__
	unsigned long lbuf;
#endif
	while (count--) {
#ifndef __KERNEL__
		lbuf = _ENDIAN(*buffer);
		*((unsigned long *)(port)) = lbuf;
		buffer++;
#else
		*((unsigned long *)(port)) = *buffer++;
#endif
	}

#else
	while (count--) iowrite32be((u32)*buffer++, port);
#endif
}

static __inline__ void
__port_orb(__port_addr_t port, unsigned char value)
{
#ifdef _DG_SIM
	*((unsigned char *)(port)) |= (value);
#else
	u8 reg = ioread8(port);
	reg |= (value);
	iowrite8(reg, port);
#endif
}

static __inline__ void
__port_orw(__port_addr_t port, unsigned short value)
{
#ifdef _DG_SIM
	*((unsigned short *)(port)) |= (value);
#else
	u16 reg = ioread16be(port);
	reg |= (value);
	iowrite16be(reg, port);
#endif
}

static __inline__ void
__port_orl(__port_addr_t port, unsigned long value)
{
#ifdef _DG_SIM
	*((unsigned long *)(port)) |= (value);
#else
	u32 reg = ioread32be(port);
	reg |= (value);
	iowrite32be(reg, port);
#endif
}

static __inline__ void
__port_andb(__port_addr_t port, unsigned char value)
{
#ifdef _DG_SIM
	*((unsigned char *)(port)) &= (value);
#else
	u8 reg = ioread8(port);
	reg &= (value);
	iowrite8(reg, port);
#endif
}

static __inline__ void
__port_andw(__port_addr_t port, unsigned short value)
{
#ifdef _DG_SIM
	*((unsigned short *)(port)) &= (value);
#else
	u16 reg = ioread16be(port);
	reg &= (value);
	iowrite16be(reg, port);
#endif
}

static __inline__ void
__port_andl(__port_addr_t port, unsigned long value)
{
#ifdef _DG_SIM
	*((unsigned long *)(port)) &= (value);
#else
	u32 reg = ioread32be(port);
	reg &= (value);
	iowrite32be(reg, port);
#endif
}

/**
 * @brief Read from single device port with delay
 *
 * Read single byte/wordslong from single device port into a local buffer with
 * delay, expressed in us. Call to usec_sleep() will be done to do a busy loop
 * for at least the requested number of microseconds.
 *
 * @param port   -- first device address to get data from
 * @param buffer -- obtained data goes here
 * @param delay  -- time delay in microseconds
 */
#define __in_delay(_r_type, _r_r)					\
static __inline__ void __in##_r_r##_delay(__port_addr_t port,		\
					  unsigned _r_type *buffer,	\
					  unsigned long delay)		\
{									\
	*buffer = __in##_r_r(port);					\
	if (unlikely(delay))						\
		udelay(delay); /* read delay time loop in us */		\
}
__in_delay(char,  b); /* __inb_delay() function definition */
__in_delay(short, w); /* __inw_delay() function definition */
__in_delay(long,  l); /* __inl_delay() function definition */

/**
 * @brief Write into single device port with delay
 *
 * Write single byte/word/long to a single device port from a local buffer with
 * delay, expressed in us. Call to usec_sleep() will be done to do a busy loop
 * for at least the requested number of microseconds.
 *
 * @param port   -- first device address to put data into
 * @param buffer -- data is taken from here
 * @param delay  -- time delay in microseconds
 */
#define __out_delay(_r_type, _r_r)					\
static __inline__ void __out##_r_r##_delay(__port_addr_t port,		\
					   unsigned _r_type *buffer,	\
					   unsigned long delay)		\
{									\
	__out##_r_r(port, *buffer);					\
	if (unlikely(delay))						\
		udelay(delay); /* write delay time loop in us */	\
}
__out_delay(char,  b); /* __outb_delay() function definition */
__out_delay(short, w); /* __outw_delay() function definition */
__out_delay(long,  l); /* __outl_delay() function definition */

/**
 * @brief FIFO. Read from single device port with delay
 *
 * Read bytes/words/longs from single device port into a local buffer with
 * delay, expressed in us. Call to usec_sleep() will be done to do a busy loop
 * for at least the requested number of microseconds.
 *
 * @param port   -- first device address to get data from
 * @param buffer -- obtained data goes here
 * @param count  -- how many times to read from a port
 * @param delay  -- time delay in microseconds
 */
#define __rep_in_delay(_r_type, _r_r)					\
static __inline__ void __rep_in##_r_r##_delay(__port_addr_t port,	\
						    unsigned _r_type *buffer, \
						    int count,		\
						    unsigned long delay) \
{									\
	if (likely(!delay)) {						\
		while (count--) {					\
			*buffer++ = __in##_r_r(port);			\
		}							\
	} else {							\
		while (count--) {					\
			*buffer++ = __in##_r_r(port);			\
			udelay(delay); /* read delay time loop in us */	\
		}							\
	}								\
}
__rep_in_delay(char,  b); /* __rep_inb_delay() function definition */
__rep_in_delay(short, w); /* __rep_inw_delay() function definition */
__rep_in_delay(long,  l); /* __rep_inl_delay() function definition */

/**
 * @brief FIFO. Write into single device port with delay
 *
 * Write bytes/words/longs to a single device port from a local buffer with
 * delay, expressed in us. Call to usec_sleep() will be done to do a busy loop
 * for at least the requested number of microseconds.
 *
 * @param port   -- first device address to put data into
 * @param buffer -- data is taken from here
 * @param count  -- how many times to write into a port
 * @param delay  -- time delay in microseconds
 */
#define __rep_out_delay(_r_type, _r_r)					\
static __inline__ void __rep_out##_r_r##_delay(__port_addr_t port,	\
						     unsigned _r_type *buffer, \
						     int count,		\
						     unsigned long delay) \
{									\
	if (likely(!delay)) {						\
		while (count--) {					\
			__out##_r_r(port, *buffer++);			\
		}							\
	} else {							\
		while (count--) {					\
			__out##_r_r(port, *buffer++);			\
			udelay(delay); /* write delay time loop in us */ \
		}							\
	}								\
}
__rep_out_delay(char,  b); /* __rep_outb_delay() function definition */
__rep_out_delay(short, w); /* __rep_outw_delay() function definition */
__rep_out_delay(long,  l); /* __rep_outl_delay() function definition */

/**
 * @brief Read multiple device ports
 *
 * Read multiple byte/word/long from device ports into a local buffer with
 * delay, expressed in us. Call to udelay() will be done to do a busy loop
 * for at least the requested number of microseconds.
 *
 * @param port   -- first device address to get data from
 * @param buffer -- obtained data goes here
 * @param count  -- port counter
 * @param delay  -- time delay in us
 */
#define __rep_in_delay_shift(_r_type, _r_r)				\
static __inline__ void __rep_in##_r_r##_delay_shift(__port_addr_t port,	\
						    unsigned _r_type *buffer, \
						    int count,		\
						    unsigned long delay) \
{									\
	if (likely(!delay)) {						\
		while (count--) {					\
			*buffer++ = __in##_r_r(port);			\
			/* TODO Support 64bit addresses */		\
			port = (__port_addr_t) ((ulong)port + sizeof(_r_type));	\
		}							\
	} else {							\
		while (count--) {					\
			*buffer++ = __in##_r_r(port);			\
			/* TODO Support 64bit addresses */		\
			port = (__port_addr_t) ((ulong)port + sizeof(_r_type));	\
			udelay(delay); /* read delay time loop in us */	\
		}							\
	}								\
}
__rep_in_delay_shift(char,  b); /* __rep_inb_delay_shift() function definition */
__rep_in_delay_shift(short, w); /* __rep_inw_delay_shift() function definition */
__rep_in_delay_shift(long,  l); /* __rep_inl_delay_shift() function definition */

/**
 * @brief Write multiple device ports
 *
 * Write multiple bytes/words/longs to a device ports from a local buffer with
 * delay, expressed in us. Call to udelay() will be done to do a busy loop
 * for at least the requested number of microseconds.
 *
 * @param port   -- first module address to put data into
 * @param buffer -- data is taken from here
 * @param count  -- port counter
 * @param delay  -- time delay in us
 */
#define __rep_out_delay_shift(_r_type, _r_r)				\
static __inline__ void __rep_out##_r_r##_delay_shift(__port_addr_t port, \
						     unsigned _r_type *buffer, \
						     int count,		\
						     unsigned long delay) \
{									\
	if (likely(!delay)) {						\
		while (count--) {					\
			__out##_r_r(port, *buffer++);			\
			/* TODO Support 64bit addresses */		\
			port = (__port_addr_t) ((ulong)port + sizeof(_r_type));	\
		}							\
	} else {							\
		while (count--) {					\
			__out##_r_r(port, *buffer++);			\
			/* TODO Support 64bit addresses */		\
			port = (__port_addr_t) ((ulong)port + sizeof(_r_type));	\
			udelay(delay); /* write delay time loop in us */ \
		}							\
	}								\
}
__rep_out_delay_shift(char,  b); /* __rep_outb_delay_shift() function definition */
__rep_out_delay_shift(short, w); /* __rep_outw_delay_shift() function definition */
__rep_out_delay_shift(long,  l); /* __rep_outl_delay_shift() function definition */

#endif	/* _PORT_OPS_LINUX_H_INCLUDE_ */
