#ifndef _REGDESC_H_INCLUDE_
#define _REGDESC_H_INCLUDE_

#include "GeneralHeader.h"

/* register access rights */
typedef enum _tagARIGHTS {
  AMRD = 2, /* register can be read */
  AMWR = 1, /* register can be write */
  AMEX = 4  /* extraneous register (can be read and write) */
} ARIGHTS;

/* register element size */
typedef enum _tagREGSIZE {
	SZCHAR  = 1,	/* char  */
	SZSHORT = 2,	/* short */
	SZLONG  = 4,	/* long  */
	SZ64    = 8,	/* long long */
} REGSIZE;

/* possible register types */
typedef enum _tagREGTYPE {
  RT_REAL = 1,	/* real register */
  RT_EXTR = 2,	/* extraneous register */
  RT_SRVS = 4	/* service register */
} reg_t;


/**
 * @brief register description
 *
 * @param regId        -- register ID
 * @param regName      -- register name
 * @param busType      -- bus type (VME, PCI, ...)
 * @param reg_regType  -- register type
 * @param regSize      -- size of each register element in bytes
 *                        (should be one of REGSIZE)
 * @param b_a          -- base address - 'addr1' or 'addr2'
 * @param bid          -- block ID (starting from zero), to which block
 *                        register belongs
 * @param regOffset    -- reg offset in bytes from the block base addr
 *                        Note, that block can have an offset itself from
 *                        his address space!
 * @param regDepth     -- register depth.
			  '-1'      => FIFO register
			  '0'       => scalar i.e. single register
			  any other => number of elements in register
 * @param regtl        -- specify a timing loop for register accessing.
			  A value of 0 disables the timing loop
 * @param regar        -- register access rights (rwe)
 * @param rwIoctlOpNum -- Each register can be read/write. ioctl
			  numbers for this operations are stored
			  here. They are masked:
			  & 0xffff0000 'read'  (or GET) ioctl num,
			  & 0x0000ffff 'write' (or SET) ioctl num.
			  IOCTL_BASE should be added after extraction.
 */
typedef struct _tagREGDESC {
	int     regId;
	char    regName[NAME_LEN];
	char    busType[MIN_STR];
	reg_t   regType;
	int     regSize;
	char    b_a[MIN_STR];
	short   bid;
	int     regOffset;
	int     regDepth;
	int     regtl;
	ARIGHTS regar;
	int     rwIoctlOpNum;
} __attribute__((packed)) REGDESC;

#endif /* _REGDESC_H_INCLUDE_ */
