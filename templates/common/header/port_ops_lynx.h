/**
 * @file port_ops_lynx.h
 *
 * @brief Extra port operations required for Lynx.
 *
 * @author Copyright (C) 2009 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
 *
 * @date Created on 23/11/2009
 *
 * @section license_sec License
 *          Released under the GPL
 */
#ifndef _PORT_OPS_LYNX_H_INCLUDE_
#define _PORT_OPS_LYNX_H_INCLUDE_

/* Definitions from this file are used by both - the driver && usr space
   (ioconfiglib part of DAL, doing dma on device) */

#include <port_ops_ppc.h> /* for __inb etc... */

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))
#endif


#ifdef __LYNXOS
extern void usec_sleep _AP((unsigned long));
#else
#include <types.h>
static inline void usec_sleep(unsigned useconds)
{
	usleep(useconds);
}
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*
 * From Linux Kernel Source's compiler.h -- GPL.
 * Generic compiler-dependent macros.
 */
#if __GNUC__ > 3
# define likely(x) __builtin_expect(!!(x), 1)
# define unlikely(x) __builtin_expect(!!(x), 0)
#else
# define likely(x) (x)
# define unlikely(x) (x)
#endif

/*
  Following extra functions are defined:
  --------------------------------------
  __inb_delay	-- read single byte from a single port with delay
  __inw_delay	-- read single word from a single port with delay
  __inl_delay	-- read single long from a single port with delay
  __outb_delay	-- write single byte to a single port with delay
  __outw_delay	-- write single word to a single port with delay
  __outl_delay	-- write single long to a single port with delay

  __rep_inb_delay  -- read multiple bytes from a single port with delay
  __rep_inw_delay  -- read multiple words from a single port with delay
  __rep_inl_delay  -- read multiple longs from a single port with delay
  __rep_outb_delay -- write multiple bytes to a single port with delay
  __rep_outw_delay -- write multiple words to a single port with delay
  __rep_outl_delay -- write multiple longs to a single port with delay

  __rep_inb_delay_shift  -- read bytes from consecutive ports with delay
  __rep_inw_delay_shift  -- read words from consecutive ports with delay
  __rep_inl_delay_shift  -- read longs from consecutive ports with delay
  __rep_outb_delay_shift -- write bytes to consecutive ports with delay
  __rep_outw_delay_shift -- write words to consecutive ports with delay
  __rep_outl_delay_shift -- write longs to consecutive ports with delay
 */

/**
 * @brief Read from single device port with delay
 *
 * Read single byte/word/long from single device port into a local buffer with
 * delay, expressed in us. Call to usec_sleep() will be done to do a busy loop
 * for at least the requested number of microseconds.
 *
 * @param port   -- first device address to get data from
 * @param buffer -- obtained data goes here
 * @param delay  -- time delay in microseconds
 */
#define __in_delay(_r_type, _r_r)					\
static __inline__ void __in##_r_r##_delay(__port_addr_t port,		\
					  unsigned _r_type *buffer,	\
					  unsigned long delay)		\
{									\
	volatile unsigned _r_type *paddr =				\
		(unsigned _r_type *)port; /* register to get */		\
	*buffer = *paddr;						\
	if (unlikely(delay))						\
		usec_sleep(delay); /* read delay time loop in us */	\
	PPC_EIEIO_INSN;							\
}
__in_delay(char,  b); /* __inb_delay() function definition */
__in_delay(short, w); /* __inw_delay() function definition */
__in_delay(long,  l); /* __inl_delay() function definition */

/**
 * @brief Write into single device port with delay
 *
 * Write single byte/word/long to a single device port from a local buffer with
 * delay, expressed in us. Call to usec_sleep() will be done to do a busy loop
 * for at least the requested number of microseconds.
 *
 * @param port   -- first device address to put data into
 * @param buffer -- data is taken from here
 * @param delay  -- time delay in microseconds
 */
#define __out_delay(_r_type, _r_r)					\
static __inline__ void __out##_r_r##_delay(__port_addr_t port,		\
					   unsigned _r_type *buffer,	\
					   unsigned long delay)		\
{									\
	volatile unsigned _r_type *paddr =				\
		(unsigned _r_type *)port; /* register to set */		\
	*paddr = *buffer;						\
	if (unlikely(delay))						\
		usec_sleep(delay);  /* write delay time loop in us */	\
	PPC_EIEIO_INSN;							\
}
__out_delay(char,  b); /* __outb_delay() function definition */
__out_delay(short, w); /* __outw_delay() function definition */
__out_delay(long,  l); /* __outl_delay() function definition */

/**
 * @brief FIFO. Read from single device port with delay
 *
 * Read bytes/words/longs from single device port into a local buffer with
 * delay, expressed in us. Call to usec_sleep() will be done to do a busy loop
 * for at least the requested number of microseconds.
 *
 * @param port   -- first device address to get data from
 * @param buffer -- obtained data goes here
 * @param count  -- how many times to read from a port
 * @param delay  -- time delay in microseconds
 */
#define __rep_in_delay(_r_type, _r_r)					\
static __inline__ void __rep_in##_r_r##_delay(__port_addr_t port,	\
					      unsigned _r_type *buffer, \
					      int count,		\
					      unsigned long delay)	\
{									\
	volatile unsigned _r_type *paddr =				\
		(unsigned _r_type *)port; /* register to get */		\
	if (likely(!delay)) {						\
		while (count--)						\
			*buffer++ = *paddr;				\
	} else {							\
		while (count--) {					\
			*buffer++ = *paddr;				\
			usec_sleep(delay); /* read delay time loop in us */ \
		}							\
	}								\
	PPC_EIEIO_INSN;							\
}
__rep_in_delay(char,  b); /* __rep_inb_delay() function definition */
__rep_in_delay(short, w); /* __rep_inw_delay() function definition */
__rep_in_delay(long,  l); /* __rep_inl_delay() function definition */

/**
 * @brief FIFO. Write into single device port with delay
 *
 * Write bytes/words/longs to a single device port from a local buffer with
 * delay, expressed in us. Call to usec_sleep() will be done to do a busy loop
 * for at least the requested number of microseconds.
 *
 * @param port   -- first device address to put data into
 * @param buffer -- data is taken from here
 * @param count  -- how many times to write into a port
 * @param delay  -- time delay in microseconds
 */
#define __rep_out_delay(_r_type, _r_r)					\
static __inline__ void __rep_out##_r_r##_delay(__port_addr_t port,	\
					       unsigned _r_type *buffer, \
					       int count,		\
					       unsigned long delay)	\
{									\
	volatile unsigned _r_type *paddr =				\
		(unsigned _r_type *)port; /* register to set */		\
	if (likely(!delay)) {						\
		while (count--)						\
			*paddr = *buffer++;				\
	} else {							\
		while (count--) {					\
			*paddr = *buffer++;				\
			usec_sleep(delay);  /* write delay time loop in us */ \
		}							\
	}								\
	PPC_EIEIO_INSN;							\
}
__rep_out_delay(char,  b); /* __rep_outb_delay() function definition */
__rep_out_delay(short, w); /* __rep_outw_delay() function definition */
__rep_out_delay(long,  l); /* __rep_outl_delay() function definition */

/**
 * @brief Read multiple device ports
 *
 * Read multiple byte/word/long from device ports into a local buffer with
 * delay, expressed in us. Call to usec_sleep() will be done to do a busy loop
 * for at least the requested number of microseconds.
 *
 * @param port   -- first device address to get data from
 * @param buffer -- obtained data goes here
 * @param count  -- port counter
 * @param delay  -- time delay in microseconds
 */
#define __rep_in_delay_shift(_r_type, _r_r)				\
static __inline__ void __rep_in##_r_r##_delay_shift(__port_addr_t port,	\
						    unsigned _r_type *buffer, \
						    int count,		\
						    unsigned long delay) \
{									\
	volatile unsigned _r_type *paddr =				\
		(unsigned _r_type *)port; /* first register to get */	\
	if (likely(!delay)) {						\
		while (count--)						\
			*buffer++ = *paddr++;				\
	} else {							\
		while (count--) {					\
			*buffer++ = *paddr++;				\
			usec_sleep(delay); /* read delay time loop in us */ \
		}							\
	}								\
	PPC_EIEIO_INSN;							\
}
__rep_in_delay_shift(char,  b);	/* __rep_inb_delay_shift() function definition */
__rep_in_delay_shift(short, w);	/* __rep_inw_delay_shift() function definition */
__rep_in_delay_shift(long,  l);	/* __rep_inl_delay_shift() function definition */

/**
 * @brief Write multiple device ports
 *
 * Write multiple bytes/words/longs to a device ports from a local buffer with
 * delay, expressed in us. Call to usec_sleep() will be done to do a busy loop
 * for at least the requested number of microseconds.
 *
 * @param port   -- first device address to put data into
 * @param buffer -- data is taken from here
 * @param count  -- port counter
 * @param delay  -- time delay in microseconds
 */
#define __rep_out_delay_shift(_r_type, _r_r)				\
static __inline__ void __rep_out##_r_r##_delay_shift(__port_addr_t port, \
						     unsigned _r_type *buffer, \
						     int count,		\
						     unsigned long delay) \
{									\
	volatile unsigned _r_type *paddr =				\
		(unsigned _r_type *)port; /* first register to set */	\
	if (likely(!delay)) {						\
		while (count--)						\
			*paddr++ = *buffer++;				\
	} else {							\
		while (count--) {					\
			*paddr++ = *buffer++;				\
			usec_sleep(delay);  /* write delay time loop in us */ \
		}							\
	}								\
	PPC_EIEIO_INSN;							\
}
__rep_out_delay_shift(char,  b); /* __rep_outb_delay_shift() function definition */
__rep_out_delay_shift(short, w); /* __rep_outw_delay_shift() function definition */
__rep_out_delay_shift(long,  l); /* __rep_outl_delay_shift() function definition */


#ifdef __cplusplus
}
#endif

#endif /* _PORT_OPS_LYNX_H_INCLUDE_ */
