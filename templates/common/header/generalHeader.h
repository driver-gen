#ifndef _GENERAL_HEADER_H_INCLUDE_
#define _GENERAL_HEADER_H_INCLUDE_

#ifdef __Lynx__
#include <time.h>
#include <ctype.h> /* for 'islower' */

/* some typedefs */
#ifndef __cplusplus
typedef unsigned int bool;
#endif
typedef unsigned int uint;
#endif	/* __Lynx__ */

#if defined(__linux__) && defined (__KERNEL__) && !defined(__CDCM__)
/* only for Linux kernel */
#include <cdcm/cdcm.h> /* screw up cdcm */
#endif

#ifndef FALSE
#define FALSE	0
#endif

#ifndef TRUE
#define TRUE	1
#endif

/* min */
#ifdef MIN
#undef MIN
#endif
#define MIN(X,Y) (((X) < (Y)) ? (X) : (Y))

/* max */
#ifdef MAX
#undef MAX
#endif
#define MAX(X,Y) (((X) < (Y)) ? (Y) : (X))

/* macro that returns true if MIN <= ARG <= MAX */
#define WITHIN_RANGE(MIN,ARG,MAX) ( ((MAX) >= (ARG)) && ((MIN) <= (ARG)) )

/* macro that limits a value between two bounds.
   !NOTE! Value will be changed! */
#define LIMIT(LOWER_LIMIT,X,UPPER_LIMIT) ((X) = MAX((LOWER_LIMIT),MIN((UPPER_LIMIT),(X))))

/* DataBase module name can consists of uppercase letters _ONLY_ (well at least
   for now). This macro is used to check if provided module name satisfy the
   above condition.
   If OK (i.e. only uppercase) - then nonzero,
   and a zero value if not (i.e. provided module name is spurious) */
#define MODULE_NAME_OK(name)			\
({						\
  __label__ done;				\
  int value = 1;				\
  int i = 0, j = 0;				\
  while (name[i]) {				\
    j = name[i];				\
    if (islower(j))				\
      { value = 0; goto done; }			\
    i++;					\
  }						\
  done:						\
  value;					\
})


/* Here comes some coloring */
#define RED_CLR		"\033[0;31m"
#define WHITE_CLR	"\033[1;37m"
#define END_CLR		"\033[m"

#define ERR_MSG	    "\033[0;31mERROR->\033[m"
#define WARNING_MSG "\033[0;31mWARNING->\033[m"

/* part of Driver name */
#define _DRVR_ "D"

/* part of Driver Simulator name */
#define _SIML_ "S"

/*
   Node Name format string: lowercase DB module name, drvr/sim, LUN, channel
   Example:
   TRIC        --> TricD.l01.c05
   SHARED_CIBC --> SharedCibcS.l03.c10
 */
#define NODE_NAME_FMT "%s%s.l%2.2d.c%2.2d"

/* driver name format string:
   [1] lowercase DB module name
   [2] (D)drvr/(S)sim

   Examples:
   TRIC        --> TricD.ko
   SHARED_CIBC --> SharedCibcS.ko
   RF_IRQ_TEST --> RfIrqTestD.ko
   RF_IRQ_TEST --> RfIrqTestS.ko
 */
#define DRVR_NAME_FMT "%s%s.ko"

/* device name format string: lowercase DB module name, drvr/sim, LUN
   Example:
   TRIC        --> TricD01.info
   SHARED_CIBC --> SharedCibcS03.info
 */
#define DEVICE_NAME_FMT "%s%s%02d.info"

/* Default shift of ioctl driver calls. This is because of LynxOSX.X:
   Ioctl calls less than 32 are vulnerable, and can be called spuriously.
   I.e. user should use ioctl numbers starting from 0x20.
   The way to bypass this is to shift (in this paticular case by 8) the
   value of all user-defined ioctl function in order not to collide with
   low value of the standard system ioctl code. */
#define IOCTL_BASE 0x100

/* Max string length */
#define MAX_STR <precision>

/* Min string length */
#define MIN_STR <ddd_num>

/* Max name length */
#define NAME_LEN <plain_num>

/* Max register amount that module can have */
#define MAX_REG <reg_depth>

/* Max block amount that module can have */
#define MAX_BLK <int_num>

/* indicate address space absence */
#define ADCG__NO_ADDRESS <extra_num>

/* indicate that Logical Unit Number is not defined */
#define NO_LUN <hex_num>

#if !defined(__LYNXOS) && !defined (__KERNEL__)
/* Naming Convention. For user space only */
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <ctype.h>

static char* str2lower(char *s) {
	char *__tmp = s; /* save pointer */
	while (*s) {
		if (isalpha(*s)) *s = tolower(*s);
		s++;
	}
	return __tmp;
}

static char* str2upper(char *s) {
        char *__tmp = s; /* save pointer */
        while (*s) {
                if (isalpha(*s)) *s = toupper(*s);
                s++;
        }
        return __tmp;
}

/**
 * @brief Naming convetion factory
 *
 * @param nm -- module name in 2 possible formats:
 *              1. Exactly as in the DB. Ex: RF_IRQ_TEST
 *              2. DG format.            Ex: RfIrqTest
 *
 * @b NOTE. @ref nm paramter @e should comply to one of formats!
 *
 * Naming convention is the following:
 * TST_CIBC        <--> TstCibc
 * A4032VXIVME     <--> A4032vxivme
 * SHARED_CIBC_TST <--> SharedCibcTst
 * RF_IRQ_TEST     <--> RfIrqTest
 * etc...
 *
 * If @ref nm is in [2]DG format -- then it will be converted in [1]DB
 * format, and vise versa:
 * If @ref nm is in [1]DB format -- it will be converted in [2]DG format.
 *
 * @b NOTE returned pointer should be freed by the caller
 *
 * @return converted module name
 */
static __attribute__((unused)) char *_ncf(char *nm)
{
	char *buf, *bptr; /* will hold new converted name */
	int i;
	char *p, fmt = 1;	/* DB format by default */
	char *tmp = nm;


	/* guess parameter format */
	for (i = 0; i < (int)strlen(nm); i++) {
		if (islower(*tmp++)) {
			fmt = 2; /* this is DB format */
			break;
		}
	}

	tmp = nm;

	if (fmt == 1) {	/* DB format (i.e. RF_IRQ_TEST form) */
		i = 0;
		/* count uderscores */
		while ((tmp = index(tmp, (int)'_'))) {
			++i; ++tmp;
		}

		buf = (char *)calloc(strlen(nm)-i+1, 1);
		bptr = buf;

		tmp = strdup(nm); /* prevent corruption */
		str2lower(tmp);
		p = strtok(tmp, "_");
		while (p) {
			*p = toupper(*p);
			strcpy(bptr, p);
			bptr += strlen(p);
			p = strtok(NULL, "_");
		}
		free(tmp);
	} else { /* DG format (i.e. RfIrqTest form) */
		fmt = 0;
		/* count upper letters */
		/* should have at least one uppercase letter */
		for (i = 0; i < (int)strlen(nm); i++) {
			if (isupper(*tmp++))
				++fmt;
		}
		--fmt; /* don't take into account first Upper letter */
		buf = (char *)calloc(strlen(nm)+fmt+1, 1);
		bptr = buf;

		/* pass through all letters of the provided name */
		*bptr++ = nm[0];
		tmp = &nm[1];
		for (i = 1; i < (int)strlen(nm); i++) {
			if (isupper(*tmp))
				*bptr++ = '_';
			*bptr++ = *tmp++;
		}

		str2upper(buf);
	}

	return buf;
}
#endif

#endif /* _GENERAL_HEADER_H_INCLUDE_ */
