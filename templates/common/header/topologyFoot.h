} <mod_name>Topology_t;

/* compiler should know variable type. So get rid of incomplete type */
typedef struct <mod_name>UserStatics_t <mod_name>UserStatics_t;

/* <mod_name> statics data table */
typedef struct {
  <mod_name>Topology_t *card; /* hardware module topology */
  DevInfo_t *info; /* device information table */
  <mod_name>UserStatics_t *usrst; /* user-defined statics data table */
} <mod_name>Statics_t;

#endif /* _<mod_name>_<drvr_type>_H_INCLUDE_ */
