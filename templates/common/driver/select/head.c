
/**
 * @brief Entry point function. Supports I/O polling or multiplexing.
 *
 * @param statPtr -- statics table pointer
 * @param flp     -- file pointer. Lynx/Linux specific.
 *                   See (sys/file.h) for Lynx and (linux/fs.h) for Linux.
 * @param which   -- condition to monitor. Not valid in case of Linux!\n
 *                   <b> SREAD, SWRITE, SEXCEPT </b> in case of Lynx.
 * @param ffs     -- select data structure in case of Lynx.\n
 *                   struct poll_table_struct in case of Linux.
 *
 * @return OK     - if succeed.
 * @return SYSERR - in case of failure.
 */
int <sys_lower>_select(register <mod_name>Statics_t *statPtr,
		       struct file *flp, int which, struct sel *ffs)
{
	int minN;    /* minor device number */
	int proceed; /* if standard code execution should be proceed after
			call to user entry point function  */
	int usrcc; /* completion code of user entry point function */

#ifdef __Lynx__
	minN = minor(flp->dev);
#else  /* __linux__ */
	minN = iminor(flp->f_path.dentry->d_inode);
#endif

	/* user entry point function call */
	usrcc = <sys_lower>UserSelect(&proceed, statPtr, flp, which, ffs);
	if (!proceed) /* all done by user */
		return usrcc;

	return OK; /* 0 */
}

