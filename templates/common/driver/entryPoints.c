
/* <mod_name> entry points */
#ifdef __linux__
struct dldd entry_points = {
	(int(*)(void*, int, struct cdcm_file*))
	/* open      */ <sys_lower>_open,
	 (int(*)(void*, struct cdcm_file*))
	/* close     */ <sys_lower>_close,
	 (int(*)(void*, struct cdcm_file*, char*, int))
	/* read      */ <sys_lower>_read,
	 (int(*)(void*, struct cdcm_file*, char*, int))
	/* write     */ <sys_lower>_write,
	 (int(*)(void*, struct cdcm_file*, int, struct cdcm_sel*))
	/* select    */ <sys_lower>_select,
	 (int(*)(void*, struct cdcm_file*, int, char*))
	/* ioctl     */ <sys_lower>_ioctl,
	 (char* (*)(void*))
	/* install   */ <sys_lower>_install,
	 (int(*)(void*))
	/* uninstall */ <sys_lower>_uninstall,
	/* memory-mapped devices access */ NULL
};
#else
struct dldd entry_points = {
  /* open      */ <sys_lower>_open,
  /* close     */ <sys_lower>_close,
  /* read      */ <sys_lower>_read,
  /* write     */ <sys_lower>_write,
  /* select    */ <sys_lower>_select,
  /* ioctl     */ <sys_lower>_ioctl,
  /* install   */ <sys_lower>_install,
  /* uninstall */ <sys_lower>_uninstall,
  /* memory-mapped devices access */ NULL
};
#endif

