

/**
 * @brief Entry point function.Called to close open file descriptor.
 *
 * @param statPtr -- statics table pointer
 * @param flp     -- file pointer. Lynx/Linux specific.
 *                   See (sys/file.h) for Lynx and (linux/fs.h) for Linux.
 *
 * @return OK     - if succeed.
 * @return SYSERR - in case of failure.
 */
int <sys_lower>_close(register <mod_name>Statics_t *statPtr, struct file *flp)
{
	int minN;    /* minor device number */
	int proceed; /* if standard code execution should be proceed
			after call to user entry point function  */
	int usrcoco; /* completion code of user entry point function */

#ifdef __Lynx__
	minN = minor(flp->dev);
#else
	minN = iminor(flp->f_path.dentry->d_inode);
#endif

	/* user entry point function call */
	usrcoco = <sys_lower>UserClose(&proceed, statPtr, flp);
	if (!proceed) /* all done by user */
		return usrcoco;

	return OK; /* 0 */
}

