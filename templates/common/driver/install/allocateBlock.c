	kkprintf("<sys_lower>: Block <plain_num> allocation - ");
	if ( !(statPtr->card->block<fancy_num> = (<mod_name>Block<fancy_num>_t *)sysbrk((long)sizeof(<mod_name>Block<fancy_num>_t))) ) {
		/* Horrible, impossible failure. */
		kkprintf("FAILED\n");
		pseterr(ENOMEM);
		INST_FAIL();
		return((char*)SYSERR); /* -1 */
	}

	bzero((char*)statPtr->card->block<fancy_num>, sizeof(<mod_name>Block<fancy_num>_t));
	kkprintf("OK\n");

