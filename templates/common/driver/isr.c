
#if defined (__LYNXOS)
/* from linux/irqreturn.h */
/**
 * enum irqreturn
 * @IRQ_NONE            interrupt was not from this device
 * @IRQ_HANDLED         interrupt was handled by this device
 * @IRQ_WAKE_THREAD     handler requests to wake the handler thread
 */
enum irqreturn {
	IRQ_NONE,
	IRQ_HANDLED,
	IRQ_WAKE_THREAD,
};
#endif
/**
 * @brief Interrupt Service Routine.
 *
 * @param stPtr -- Statics table pointer
 *
 * Implementation is @b completely up to the user.
 *
 * @return IRQ_NONE        --  interrupt was not from this device
 * @return IRQ_HANDLED     --  interrupt was handled by this device
 * @return IRQ_WAKE_THREAD --  handler requests to wake the handler thread
 */
static int __attribute__((unused)) <sys_lower>ISR(<mod_name>Statics_t *ptr)
{
	<mod_name>Statics_t *stPtr;
	stPtr = (<mod_name>Statics_t *)ptr;

	return IRQ_HANDLED;
}

