#ifndef _<mod_name>_USER_DEFINED_<drvr_type>_H_INCLUDE_
#define _<mod_name>_USER_DEFINED_<drvr_type>_H_INCLUDE_

#include "<sys_lower><fancy_str>.h"

struct sel; /* preliminary structure declaration to supress warnings during
	       user code compilation */

/* user-defined statics data table for <mod_name> module */
struct <mod_name>UserStatics_t {
  /*
    +=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+
    |                     insert your new members here                        |
    | NOTE! All that will be placed here should be managed by you, and you    |
    | alone!!! All the pointers-related memory allocation and deallocation    |
    | should be done by you. It will NOT be done automatically! If the        |
    | pointer is declared - it should be initialized properly by allocating   |
    | a new memory (using sysbrk) or by assigning it a correct and valid      |
    | address. Don't forget to free allocated memory (if any) in the          |
    | uninstallation procedure! Have fun (-;                                  |
    +=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+
  */
};



/*
  +-------------------------------+
  | INSERT USER-DEFINED CODE HERE |
  +-------------------------------+
*/


int <sys_lower>UserOpen(int*, register <mod_name>Statics_t*, int, struct file*);
int <sys_lower>UserClose(int*, register <mod_name>Statics_t*, struct file*);
int <sys_lower>UserRead(int*, register <mod_name>Statics_t*, struct file*, char*, int);
int <sys_lower>UserWrite(int*, register <mod_name>Statics_t*, struct file*, char*, int);
int <sys_lower>UserSelect(int*, register <mod_name>Statics_t*, struct file*, int, struct sel*);
int <sys_lower>UserIoctl(int*, register <mod_name>Statics_t*, struct file*, int, int, char*);
char* <sys_lower>UserInst(int*, register DevInfo_t*, register <mod_name>Statics_t*);
int <sys_lower>UserUnInst(int*, <mod_name>Statics_t*);

#endif /* _<mod_name>_USER_DEFINED_<drvr_type>_H_INCLUDE_ */
