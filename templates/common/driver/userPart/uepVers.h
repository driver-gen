#ifndef _<mod_name>_VERSION_<drvr_type>_H_INCLUDE_
#define _<mod_name>_VERSION_<drvr_type>_H_INCLUDE_

/*
   Use this definition to set current <fancy_str> source code version.

   WARNING! Use _ONLY_ decimal numbers.
   No floating point or strings allowed!
   (i.e. 2, 3, 4, 5, 6 ..., etc)

   NOTE. If 'cpUsrPt' script is used to copy user-defined source code
   from one version to another - then current <fancy_str> version will be
   automatically increased by one.
*/
#define <mod_name>_<drvr_type>_CURRENT_VERSION 1

#endif /* _<mod_name>_VERSION_<drvr_type>_H_INCLUDE_ */
