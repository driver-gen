#include "<sys_lower>UserDefined<fancy_str>.h"

#ifdef __powerpc__
#include <vme.h>
#endif

/* for __inb, __outb etc... */
#ifdef __Lynx__
#include "dg/port_ops_lynx.h"
#else  /* __linux__ */
#include "dg/port_ops_linux.h"
#include "dg/swab-extra-linux.h"
#endif


/*
  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  User Entry Points into Driver/Simulator are located here.
  This functions are called in an appropriate driver/simulator routines.
  All user-defined driver/simulator code should be added in this functions.
  Also Interrupt Service Routine is located here, as it's implementation is
  _completely_ up to the user.
  -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
*/


