
/**
 * @brief Entry point function. Reads data from the device.
 *
 * @param statPtr -- statics table pointer
 * @param flp     -- file pointer
 * @param buff    -- character buffer pointer
 * @param count   -- number of bytes to copy
 *
 * @return number of bytes actually copied, including zero - if succeed.
 * @return SYSERR                                          - if fails.
 */
int <sys_lower>_read(register <mod_name>Statics_t *statPtr,
		     struct file *flp, char *buff, int count)
{
	int minN;    /* minor device number */
	int proceed; /* if standard code execution should be proceed
			after call to user entry point function  */
	int usrcoco; /* completion code of user entry point function */

#ifdef __Lynx__
	minN = minor(flp->dev);
#else
	minN = iminor(flp->f_path.dentry->d_inode);
#endif

	/* user entry point function call */
	usrcoco = <sys_lower>UserRead(&proceed, statPtr, flp, buff, count);
	if (!proceed) /* all done by user */
		return usrcoco;

	return usrcoco; /* number of read bytes */
}

