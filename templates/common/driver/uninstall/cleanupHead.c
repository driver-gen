

/**
 * @brief Cleanup and deallocation of the <drvr_type> statics table.
 *
 * @param statPtr - deallocate this
 *
 * @return void
 */
static void <sys_lower>MemCleanup(<mod_name>Statics_t *statPtr)
{
  DevInfo_t *dip = statPtr->info; /* device info pointer */

  if (statPtr->card != NULL) {
    kkprintf("<sys_lower>: Cleaning up Logical Unit %d\n", dip->mlun);

    if (statPtr->card->wo != NULL) {
      kkprintf("<sys_lower>: Writeonly structure deallocation - ");
      sysfree((char*)statPtr->card->wo, (long)sizeof(<mod_name>Writeonly_t));
      kkprintf("OK\n");
    }

    if (statPtr->card->ex != NULL) {
      kkprintf("<sys_lower>: Extraneous structure deallocation - ");
      sysfree((char*)statPtr->card->ex, (long)sizeof(<mod_name>Extraneous_t));
      kkprintf("OK\n");
    }
