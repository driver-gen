    kkprintf("<sys_lower>: Topology structure deallocation - ");
    sysfree((char*)statPtr->card, (long)sizeof(<mod_name>Topology_t));
    kkprintf("OK\n");

    kkprintf("<sys_lower>: All Logical Unit specific resources have been deallocated.\n");
  }

  if (statPtr->usrst != NULL) {
    kkprintf("<sys_lower>: User-defined statics data table deallocation - ");
    sysfree((char*)statPtr->usrst, (long)sizeof(<mod_name>UserStatics_t));
    kkprintf("OK\n");
  }

  kkprintf("<sys_lower>: Device info table structure deallocation - ");
  sysfree((char*)statPtr->info, (long)sizeof(DevInfo_t));
  kkprintf("OK\n");

  kkprintf("<sys_lower>: Statics structure deallocation - ");
  sysfree((char*)statPtr, (long)sizeof(<mod_name>Statics_t));
  kkprintf("OK\n");
}


