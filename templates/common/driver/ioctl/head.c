
/**
 * @brief <mod_name> ioctl entry point
 *
 * @param s   -- statics table
 * @param f   -- file pointer. Lynx/Linux specific.
 *               See (sys/file.h) for Lynx and (linux/fs.h) for Linux.
 * @param com -- ioctl number
 * @param arg -- arguments
 *
 * By default - we do check r/w bounds while accessing the register.
 *
 * @return OK     - if succeed.
 * @return SYSERR - in case of failure.
 */
int <sys_lower>_ioctl(register <mod_name>Statics_t *s,
		      struct file *f, int com, char *arg)
{
	static int c_rwb = 1; /* check r/w bounds (1 - yes, 0 - no)
				 Valid _ONLY_ for Lynx! */
	int minN;     /* minor device number (LUN) */
	int proceed;  /* if standard code execution should be proceed after
			 call to user entry point function  */
	int rc;  /* return code */
	int r_rw = 0; /* repetitive r/w (1 - yes, 0 - no) */

	START_TIME_STAT(); /* timing measurements */

#ifdef __Lynx__
	minN = minor(f->dev);
#else
	minN = iminor(f->f_path.dentry->d_inode);
#endif

	/* user entry point function call */
	rc = <sys_lower>UserIoctl(&proceed, s, f, minN, com, arg);
	if (!proceed) /* all done by user */
		goto out_ioctl;

 rep_ioctl:
	switch (com) { /* default 'ioctl' driver operations */
