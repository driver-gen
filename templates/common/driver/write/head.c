
/**
 * @brief Entry point function. Sends data to the device.
 *
 * @param statPtr -- statics table pointer
 * @param flp     -- file pointer. Lynx/Linux specific.
 *                   See (sys/file.h) for Lynx and (linux/fs.h) for Linux.
 * @param which   -- condition to monitor. Not valid in case of Linux!\n
 * @param buff    -- char buffer pointer
 * @param count   -- the number of bytes to copy
 *
 * @return number of bytes actually copied, including zero - if succeed.
 * @return SYSERR                                          - if fails.
 */
int <sys_lower>_write(register <mod_name>Statics_t *statPtr,
		      struct file *flp, char *buff, int count)
{
	int minN; /* minor device number */
	int proceed;	/* if standard code execution should be proceed after
			   call to user entry point function  */
	int usrcoco;	/* completion code of user entry point function */

#ifdef __Lynx__
	minN = minor(flp->dev);
#else  /* __linux__ */
	minN = iminor(flp->f_path.dentry->d_inode);
#endif

	/* user entry point function call */
	usrcoco = <sys_lower>UserWrite(&proceed, statPtr, flp, buff, count);
	if (!proceed) /* all done by user */
		return usrcoco;

	return usrcoco; /* number of written bytes */
}

