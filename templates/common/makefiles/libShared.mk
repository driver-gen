###############################################################################
# @file libShared.mk
#
# @brief Driver Access Library compilation
#
# Automatically generated by driver-gen
#
# @author Copyright (C) 2003-2010 CERN. Yury GEORGIEVSKIY <ygeorgie@cern.ch>
#
# @date Created on 29/06/2009
#
# @section license_sec License
#          Released under the GPL
###############################################################################

include ../Makefile.base

# TODO. Use deliver.mk to do CERN delivery
INSTDIR = /acc/local/$(CPU)/dg/dg

# Local header files are here
INCDIRS = ../dg

ADDINCLUDES = $(KERN_INCLUDES)

#  For Position-Independent-Code (to be able to use from .so libraries)
#+ and for r/w port operations
ADDCFLAGS  = -fPIC -DNO_CSP_MODEL

# set good delivery place for compiled files
OUTPUT_OPTION +=; mv $*.$(CPU).o $(OBJDIR)/$@

###############################################################################
#   Here comes all that needs to build-up 'Driver Access Library' (aka DAL)   #
###############################################################################
DA_LIB_OBJS = \
	$(addsuffix $(EXTOBJ), $(notdir $(basename $(patsubst %.c, %.o, $(wildcard *.c)))))

libdal.$(CPU).a: $(DA_LIB_OBJS)
	$(Q)$(AR) -rv $@ $(addprefix $(OBJDIR)/, $^)
	$(Q)$(RANLIB) $@

LIBRARIES += libdal.$(CPU).a
###############################################################################

_build: $(OBJDIR) $(LIBRARIES)

linux:
	$(Q)$(MAKE) _build CPU=L865

lynx:
	$(Q)$(MAKE) _build CPU=ppc4

all:
	$(Q)$(MAKE) _build CPU=L865
	$(Q)$(MAKE) _build CPU=ppc4

# installation of supplementary scripts
scripts:
	$(CP) -p ../cpUsrPt ../../../../scripts/

_deliver:
	dsc_install $(LIBRARIES) $(INSTDIR)
deliver:
	$(Q)$(MAKE) _deliver CPU=L865
	$(Q)$(MAKE) _deliver CPU=ppc4

cleanloc clearloc:: abort
	@ if [ -n "$(ISP)" ]; then \
		rm -rf $(ISP)* ; \
	fi
	find ./ -name '*~' | xargs rm -f
	-rm lib*.a
