
	for (;;) {
		printf("%s%s %s <V. %d> Test Program - Main Menu\n[%s]\n",
		       ClearScreen, curModName,
		       (g_isDrvr)?"Driver":"Simulator", g_drvrVers,
		       g_drvrVersInfo);
		for (i = 0; i < screenWidth(); i++)
			printf("-");

		printf("\n\n");
		printf("01 - Exit\n");
		printf("02 - User Defined Menu\n");
		printf("03 - Service Registers\n");
		printf("04 - Extraneous Registers\n");
		printf("05 - WriteOnly Registers history buffer"
		       " (last written values)\n");
