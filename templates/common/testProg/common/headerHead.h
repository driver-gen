#ifndef _TEST_H_INCLUDE_
#define _TEST_H_INCLUDE_

#ifdef __linux__
#include <curses.h>
#include <term.h>
#else
extern int  tgetent(char*, char*);
extern char *tgetstr(char*, char**);
#endif

#ifndef OK
#define OK 0	/* return code */
#endif

#include "<dg_free_fmt>/dal.h"
#include "<sys_lower>UserDefinedAccess.h"
#include "<sys_lower>RegId.h"

#define WHITE_CLR	"\033[1;37m"
#define END_CLR		"\033[m"

