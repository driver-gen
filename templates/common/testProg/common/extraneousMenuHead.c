

/**
 * @brief
 *
 * @param handle -- lib handle, ret. by @e DaEnableAccess() call
 *
 * @return
 */
int Extraneous(HANDLE handle)
{
	int		 i, j;
	int		 choice;
	int		 result;
	int		 count;
	char		 format[32];
	int		 upper, lower;
	unsigned char  charResult;
	unsigned short shortResult;
	unsigned long  longResult;
	unsigned char  *charPointer;
	unsigned short *shortPointer;
	unsigned long  *longPointer;

	/* just to suppress 'unused variable' warnings */
	i=j=choice=result=count=upper=lower=charResult=shortResult=longResult=0;
	charPointer  = NULL;
	shortPointer = NULL;
	longPointer  = NULL;
	format[0]    = 0;

	for (;;) {
		printf("%s%s %s <V. %d> Test Program - Extraneous"
		       " Registers\n[%s]\n", ClearScreen, curModName,
		       (g_isDrvr)?"Driver":"Simulator",
		       g_drvrVers, g_drvrVersInfo);
		for (i = 0; i < screenWidth(); i++)
			printf("-");

		printf("\n\n");

