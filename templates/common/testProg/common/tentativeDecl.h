
#define EXTRANEOUS_NUM_REGISTERS <plain_num>
#define SERVICE_NUM_REGISTERS    <reg_depth>
#define WRITEONLY_NUM_REGISTERS  <precision>

/* <sys_lower>TestUser.c */
int UserDefinedMenu(HANDLE, int);

/* <sys_lower>Test.c */
extern char *ClearScreen;
extern const char *g_drvrVersInfo;
extern int g_drvrVers;
int         screenHeight();
int         screenWidth();
