#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/file.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <time.h>
#include "<sys_lower>Test.h"

#ifdef __linux__
#define STDIN   0
#define STDOUT  1
#define STDERR  2
#endif

/* to suppress 'implicit declaration' warnings, and 'undefined vars' errors */
extern char *optarg;

/*==========================Global data=====================================*/
static char *curModName;	 /* Module Name exactly as in the DataBase */
METHOD      access_mode = IOCTL; /* default access method */
char        *ClearScreen;	 /* terminal clear screen esc sequence */
const char  *g_drvrVersInfo;     /* Module version info string */
int         g_drvrVers;		 /* Module driver version */
int         g_isDrvr;		 /* What is currently testing,
				    driver or simulator */
