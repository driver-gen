/**
 * @brief
 *
 * @param handle -- lib handle, ret. by @e DaEnableAccess() call
 *
 * @return
 */
int ServiceRegsOps(HANDLE handle)
{
	int		 i, j;
	int		 choice;
	int		 result;
	int		 count;
	char		 format[32];
	unsigned char  charResult;
	unsigned short shortResult;
	unsigned long  longResult;

	/* just to suppress 'unused variable' warnings */
	i=j=choice=result=count=charResult=shortResult=longResult=0;
	format[0] = 0;

	for (;;) {
		printf("%s%s %s <V. %d> Test Program - Service"
		       " Registers\n[%s]\n", ClearScreen, curModName,
		       (g_isDrvr)?"Driver":"Simulator", g_drvrVers,
		       g_drvrVersInfo);
		for (i = 0; i < screenWidth(); i++)
			printf("-");

		printf("\n\n");
