/**
 * @file utilities.c
 *
 * @brief Handy DriverGen functions are located here.
 *
 * @author Copyright (C) 2004 - 2010 CERN. Georgievskiy Yury <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */
#include <ctype.h>
#include <sys/mman.h> /* for mmap cruft */
#include "driverGen.h"
#include "dit.h"

/**
 * @brief  Constructs a string representation of an integer.
 *
 * @param val  -- value to convert
 * @param base -- base of the value [2 - 16]
 *
 * Different based are possible (at least was tested for 2, 10, 16).
 *
 * @return pointer to the string with converted value - if succeed.
 * @return NULL                                       - if fails.
 */
char *itoa(int val, int base)
{
	static char buf[34] = { 0 };
	char *out = &buf[33];
	int sign = (val < 0) ? sign = -val : val;

	/* check if the base is valid */
	if (base < 2 || base > 16)
		return (NULL);

	/* base 16 and base 2 cases */
	if (base == 16 || base == 2) {
		unsigned int hval = val;
		unsigned int hbase = base;
		do {
			*out = "0123456789abcdef"[hval % hbase];
			--out;
			hval /= hbase;
		} while (hval);

		if (base == 16)	/* apply 0x prefix */
			*out-- = 'x', *out = '0';
		else
			++out;

		return (out);
	}

	/* for all remaining bases */
	do {
		*out = "0123456789abcdef"[sign % base];
		--out;
		sign /= base;
	} while (sign);

	if (val < 0 && base == 10) /* apply negative sign only for base 10 */
		*out = '-';
	else
		++out;

	return (out);
}

/**
 * @brief Convert a string to lower case.
 *
 * @param string -- string to convert.
 *
 * @return void
 */
void StrToLower(char *string)
{
	int i = -1;

	while (string[++i] != '\0')
		string[i] = (char)tolower(string[i]);
}

/**
 * @brief Convert a string to upper case.
 *
 * @param string -- string to convert
 *
 * @return void
 */
void StrToUpper(char *string)
{
	int i = -1;

	while (string[++i] != '\0')
		string[i] = (char)toupper(string[i]);
}

/**
 * @brief Service function for debug purposes.
 *
 * @param reginfo -- register description
 * @param regNum  -- register amount
 *
 * @return void
 */
void PrintoutRegInfo(RegisterDef_t * reginfo, int regNum)
{
	int cntr;

	for (cntr = 0; cntr < regNum; cntr++) {
		printf("----- [%03d] %s -----\n\tblock %d\n\toffset %#x\n"
		       "\tdepth %d (%#x)\n--------------------\n",
		       cntr, reginfo[cntr].name, reginfo[cntr].blockP->blockID,
		       reginfo[cntr].offset, reginfo[cntr].depth,
		       reginfo[cntr].depth);
	}

}

/**
 * @brief How many registers does block has
 *
 * @param reg  -- first register in the block
 *
 * @return register amount
 */
static int calc_block_reg_amount(RegisterDef_t *reg)
{
	int cntr = 0;
	int bid = reg->blockP->blockID;

	while (bid == reg->blockP->blockID) {
		++cntr;
		if (reg->last)
			break;
		else
			++reg;
	}

	return cntr;
}

/**
 * @brief Get block size in bytes.
 *
 * @param reg  -- first register in the block
 * @param type -- driver (DRIVER_FT) or simulator (SIM_FT)
 *
 * Calculate block size, depending on the type (driver/simulator).
 * If driver -- gaps are taken into account.
 * If simulator -- there will be no gaps.
 *
 * @return block size in bytes
 */
int calc_block_size(RegisterDef_t *reg, int type)
{
	long bsz = 0;
	int i;
	int ram = calc_block_reg_amount(reg);

	/* for simulator -- count size without gaps */
	if (type == SIM_FT) {
		for (i = 0; i < ram; i++) {
			bsz += ((reg[i].depth) ? reg[i].depth : 1) *
				reg[i].regSize;
		}
	} else { /* count gaps only for real driver */
		reg = &reg[ram-1]; /* last block register */
		bsz = reg->offset + ((reg->depth) ? reg->depth : 1)
			* reg->regSize;
	}

	return bsz;
}


/**
 * @brief Find out how many gaps does block has.
 *
 * @param reg -- first register in the block
 *
 * @return gap amount
 */
int calc_block_gap_amount(RegisterDef_t *reg)
{
	int gap = 0;
	int i, addr;
	RegisterDef_t *preg;
	int ram = calc_block_reg_amount(reg);

	if (reg->offset)
		++gap;

	for (i = 1, preg = &reg[i-1], reg = &reg[1]; i < ram;
	     i++, reg++, preg++) {
		addr = preg->offset + ((preg->depth) ? preg->depth : 1)
			* preg->regSize;
		if (reg->offset - addr)
			++gap;
	}

	return gap;
}

/**
 * @brief Set block size and block register amount information.
 *
 * @param regs -- register descr
 *
 * This function is called after we get and check all info from the DB
 * in order to set missing information blocks, namely:
 * 1. block size for the real driver (gap sizes are taken into account)
 * 2. block size for driver simulator (gaps are not considered)
 * 3. number of registers in the block.
 */
void set_extra_block_data(RegisterDef_t *regs)
{
	int ram = 0;
	RegisterDef_t *ptr;

	do {
		ram = calc_block_reg_amount(regs);
		ptr = &regs[ram-1]; /* 'last register' detection */
		regs->blockP->blksz_drvr = calc_block_size(regs, DRIVER_FT);
		regs->blockP->blksz_sim  = calc_block_size(regs, SIM_FT);
		regs->blockP->reg_am     = ram;
		regs += ram;	/* move to the first register of next block */
	} while (!ptr->last);
}

/*! @name BE <-> LE convertion
 */
//@{
#define _ENDIAN(x)				\
({						\
  typeof(x) __x;				\
  typeof(x) __val = (x);			\
    __endian(&(__val), &(__x), sizeof(__x));	\
  __x;						\
})
//@}

/**
 * @brief Converts Device Info Table from BE to LE
 *
 * @param src -- source. Device info table to convert
 * @param dst -- destanation. If not NULL - converted table goes here.
 *               If NULL - @ref src will be used as destanation.
 *
 * Info file stores all data in the BE format. It should be converted it LE
 * in order to be able to use it.
 *
 * @e NOTE
 * If dsc is NULL - current info table (in @ref src parameter) will be
 * overwritten!
 *
 * @return pointer to LE-converted info table
 */
DevInfo_t* convert_dit(DevInfo_t *src, DevInfo_t *dsc)
{
	int i;
	DevInfo_t *res = (dsc)?dsc:src; /* where results will go */

	/* addr1 member */
	res->addr1.baseAddr  = _ENDIAN(src->addr1.baseAddr);
	res->addr1.range     = _ENDIAN(src->addr1.range);
	res->addr1.increment = _ENDIAN(src->addr1.increment);
	res->addr1.dpSize    = _ENDIAN(src->addr1.dpSize);
	res->addr1.addrModif = _ENDIAN(src->addr1.addrModif);

	/* addr2 member */
	res->addr2.baseAddr  = _ENDIAN(src->addr2.baseAddr);
	res->addr2.range     = _ENDIAN(src->addr2.range);
	res->addr2.increment = _ENDIAN(src->addr2.increment);
	res->addr2.dpSize    = _ENDIAN(src->addr2.dpSize);
	res->addr2.addrModif = _ENDIAN(src->addr2.addrModif);

	res->mtn        = _ENDIAN(src->mtn);
	res->mlun       = _ENDIAN(src->mlun);
	res->debugFlag  = _ENDIAN(src->debugFlag);
	/* 'opaque' is char */
	res->iLevel     = _ENDIAN(src->iLevel);
	res->iVector    = _ENDIAN(src->iVector);
	res->iVectorInc = _ENDIAN(src->iVectorInc);
	res->chan       = _ENDIAN(src->chan);
	res->chrindex   = _ENDIAN(src->chrindex);
	res->gen_date   = _ENDIAN(src->gen_date);
	/* 'dg_vers' is char */
	res->regAmount  = _ENDIAN(src->regAmount);

	for (i = 0; i < res->regAmount; i++) {
	res->regDesc[i].regId        = _ENDIAN(src->regDesc[i].regId);
	/* 'regName' is char */
	/* 'busType' is char */
	res->regDesc[i].regType      = _ENDIAN(src->regDesc[i].regType);
	res->regDesc[i].regSize      = _ENDIAN(src->regDesc[i].regSize);
	/* 'b_a' is char */
	res->regDesc[i].bid          = _ENDIAN(src->regDesc[i].bid);
	res->regDesc[i].regOffset    = _ENDIAN(src->regDesc[i].regOffset);
	res->regDesc[i].regDepth     = _ENDIAN(src->regDesc[i].regDepth);
	res->regDesc[i].regtl        = _ENDIAN(src->regDesc[i].regtl);
	res->regDesc[i].regar        = _ENDIAN(src->regDesc[i].regar);
	res->regDesc[i].rwIoctlOpNum = _ENDIAN(src->regDesc[i].rwIoctlOpNum);
	}

	res->maxRegSz   = _ENDIAN(src->maxRegSz);
	res->blkAmount  = _ENDIAN(src->blkAmount);

	for (i = 0; i < res->blkAmount; i++) {
	res->blkDesc[i].block       = _ENDIAN(src->blkDesc[i].block);
	res->blkDesc[i].blkBaseAddr = _ENDIAN(src->blkDesc[i].blkBaseAddr);
	res->blkDesc[i].offset      = _ENDIAN(src->blkDesc[i].offset);
	res->blkDesc[i].blksz_drvr  = _ENDIAN(src->blkDesc[i].blksz_drvr);
	res->blkDesc[i].blksz_sim   = _ENDIAN(src->blkDesc[i].blksz_sim);
	res->blkDesc[i].reg_am      = _ENDIAN(src->blkDesc[i].reg_am);
	}

	/* 'checksum' is char */

	return res;
}

/**
 * @brief Open info file with DataBase module description.
 *
 * Returned pointer should be freed by the caller afterwards!
 *
 * @param fNm -- info file name to open
 *               Note, that info file is a Big Endian encoded.
 *
 * @return pointer  -- if everything is OK.
 *                     NOTE. Should be freed by the caller afterwards.
 * @return NULL     -- failed
 */
DevInfo_t* read_info_file(char *fNm)
{
	int fd, fSz;
	struct stat fSt;
	DevInfo_t *devD, *dit;	/* device data from the info file */

	if ((fd = open(fNm, O_RDONLY)) < 0) {
		perror("open()");
		return NULL;
	}

	fstat(fd, &fSt);
	fSz = fSt.st_size;

	devD = mmap(0, fSz, PROT_READ, MAP_SHARED, fd, 0);
	if (devD == MAP_FAILED) {
		perror("mmap()");
		close(fd);
		return NULL;
	}

	close(fd); /* we don't need it anymore */

	dit = calloc(1, sizeof(*dit));
	/* copy it locally */
	memmove(dit, devD, sizeof(*devD));
	munmap(devD, fSz);

#ifdef __linux__
	convert_dit(dit, NULL); /* convert to BE */
#endif
	return dit;
}

/**
 * @brief Exploit driver .info file
 *
 * @param info table to printout.
 *
 * Useful for debug purposes. User can check if info file contains
 * correct data.
 *
 * @return void
 */

void exploit_info_file(DevInfo_t * info)
{
	int cntr;
	AddrInfo_t *aiptr = &(info->addr1);
	REGDESC *rdp = info->regDesc;
	BLKDESC *bdp = info->blkDesc;

	printf("\n%s.info file data:\n\n", TranslationGetSysLower());
	/* general info */
	printf("+---- general info -----------\n");
	printf("| 'mtn'        => %d\n",   info->mtn);
	printf("| 'mlun'       => 0x%x\n", info->mlun);
	printf("| 'debugFlag'  => 0x%X\n", info->debugFlag);
	printf("| 'opaque'     => '%s'\n", info->opaque);
	printf("| 'iLevel'     => %d\n",   info->iLevel);
	printf("| 'iVector'    => %d\n",   info->iVector);
	printf("| 'iVectorInc' => %d\n",   info->iVectorInc);
	printf("| 'chan'       => %d\n",   info->chan);
	printf("| 'chrindex'   => %d\n",   info->chrindex);
	printf("| 'gen_date'   => %ld\n",  info->gen_date);
	printf("| 'dg_vers'    => '%s'\n", info->dg_vers);
	printf("| 'regAmount'  => %d\n",   info->regAmount);
	printf("| 'maxRegSz'   => %d\n",   info->maxRegSz);
	printf("| 'blkAmount'  => %d\n",   info->blkAmount);
	printf("| 'checksum'   => %d\n",   info->checksum);
	printf("+-----------------------------\n\n");

	/* 2 address space info */
	for (cntr = 0; cntr < 2; cntr++) {
		printf("+-----------------------------\n");
		printf("| Addr[%d].baseAddr  => 0x%lx\n", cntr,
		       aiptr->baseAddr);
		printf("| Addr[%d].range     => 0x%x\n", cntr, aiptr->range);
		printf("| Addr[%d].increment => 0x%x\n", cntr,
		       aiptr->increment);
		printf("| Addr[%d].dpSize    => %d\n", cntr, aiptr->dpSize);
		printf("| Addr[%d].addrModif => 0x%x\n", cntr,
		       aiptr->addrModif);
		printf("+-----------------------------\n\n");
		aiptr++;
	}

	/* all block info */
	for (cntr = 0; cntr < info->blkAmount; cntr++) {
		printf("+---------------------------------------------\n");
		printf("| Blk[%d].block       => %d\n", cntr, bdp[cntr].block);
		printf("| Blk[%d].blkBaseAddr => %d\n", cntr,
		       bdp[cntr].blkBaseAddr);
		printf("| Blk[%d].offset      => 0x%lX\n", cntr,
		       bdp[cntr].offset);
		printf("| Blk[%d].blksz_drvr  => %d bytes\n", cntr,
		       bdp[cntr].blksz_drvr);
		printf("| Blk[%d].blksz_sim   => %d bytes\n", cntr,
		       bdp[cntr].blksz_sim);
		printf("| Blk[%d].reg_am      => %d\n", cntr,
		       bdp[cntr].reg_am);

		printf("+---------------------------------------------\n\n");
	}

	/* all registers info */
	for (cntr = 0; cntr < info->regAmount; cntr++) {
		printf("+---------------------------------------------\n");
		printf("| Reg[%d].regId        => %d\n", cntr, rdp[cntr].regId);
		printf("| Reg[%d].regName      => %s\n", cntr,
		       rdp[cntr].regName);
		printf("| Reg[%d].busType      => %s\n", cntr,
		       rdp[cntr].busType);
		printf("| Reg[%d].regType      => %s (%d)\n", cntr,
		       (rdp[cntr].regType ==
			RT_REAL) ? "RT_REAL" : (rdp[cntr].regType ==
						RT_EXTR) ? "RT_EXTR" :
		       "RT_SRVS", rdp[cntr].regType);
		printf("| Reg[%d].regSize      => %d\n", cntr,
		       rdp[cntr].regSize);
		printf("| Reg[%d].b_a          => %s\n", cntr, rdp[cntr].b_a);
		printf("| Reg[%d].bid          => %d\n", cntr, rdp[cntr].bid);
		printf("| Reg[%d].regOffset    => 0x%X\n", cntr,
		       rdp[cntr].regOffset);
		printf("| Reg[%d].regDepth     => %d\n", cntr,
		       rdp[cntr].regDepth);
		printf("| Reg[%d].regtl        => %d\n", cntr, rdp[cntr].regtl);
		printf("| Reg[%d].regar        => %d\n", cntr, rdp[cntr].regar);
		printf("| Reg[%d].rwIoctlOpNum => 0x%x\n", cntr,
		       rdp[cntr].rwIoctlOpNum);
		printf("+---------------------------------------------\n\n");
	}
}


char* get_info_fn(void)
{
	static char ifn[MAX_STR] = { 0 };

	if (ifn[0]) /* already initialized */
		return ifn;

	snprintf(ifn, sizeof(ifn), "%s/object_%s/%s.info", get_msd(NULL),
		TranslationGetSysLower(), TranslationGetSysLower());

	return ifn;
}

char* get_xml_fn(void)
{
	static char xfn[MAX_STR] = { 0 };

	if (xfn[0]) /* already initialized */
		return xfn;

	snprintf(xfn, sizeof(xfn), "%s/object_%s/%s.xml", get_msd(NULL),
		TranslationGetSysLower(), TranslationGetModName());

	return xfn;
}
