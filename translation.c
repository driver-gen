/**
 * @file translation.c
 *
 * @brief Template translation functions are here.
 *
 * @author Copyright (C) 2002 CERN. Stuart Baird
 * @author Copyright (C) 2003 - 2010 CERN. Georgievskiy Yury <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */
#define _GNU_SOURCE /* asprintf rocks */
#include "driverGen.h"
#include "utilities.h"
#include "translation.h"

#define FIND    0
#define REPLACE 1
#define INT_LEN 16

/* Here comes all tokens, that are used in code generation */
typedef enum tag_Tokens {
	/* indexes */
	SYS_LOWER = 0,		/* 00 */
	MOD_NAME,		/* 01 */
	BUS_UPPER,		/* 02 */
	INT_NUM,		/* 03 */
	PLAIN_NUM,		/* 04 */
	HEX_NUM,		/* 05 */
	FANCY_NUM,		/* 06 */
	EXTRA_NUM,		/* 07 */
	DEC_NUM,		/* 08 */
	DDD_NUM,		/* 09 */
	REG_NAME,		/* 10 */
	REG_COMM,		/* 11 */
	REG_TYPE,		/* 12 */
	REG_DEPTH,		/* 13 */
	REG_LOOP,		/* 14 */
	REG_ID,			/* 15 */
	IOCTL_CONST,		/* 16 */
	PRECISION,		/* 17 */
	BASE_ADDR,		/* 18 */
	FANCY_STR,		/* 19 */
	DG_DUMMY_STR,		/* 20 */
	DRVR_TYPE,		/* 21 */
	DG_COMMENT,		/* 22 */
	DG_FREE_FMT,		/* 23 */
	DG_CHAR,		/* 24 */
	DG_STRING,		/* 25 */
	DG_REG_SZ,		/* 26 */
	NUM_TOKENS		/* add new tokens BEFORE this */
} Tokens;

static char *tokenNames[NUM_TOKENS] = {
	[SYS_LOWER]    = "<sys_lower>",
	[MOD_NAME]     = "<mod_name>",
	[BUS_UPPER]    = "<bus_upper>",
	[INT_NUM]      = "<int_num>",
	[PLAIN_NUM]    = "<plain_num>",
	[HEX_NUM]      = "<hex_num>",
	[FANCY_NUM]    = "<fancy_num>",
	[EXTRA_NUM]    = "<extra_num>",
	[DEC_NUM]      = "<dec_num>",
	[DDD_NUM]      = "<ddd_num>",
	[REG_NAME]     = "<reg_name>",
	[REG_COMM]     = "<reg_comment>",
	[REG_TYPE]     = "<reg_type>",
	[REG_DEPTH]    = "<reg_depth>",
	[REG_LOOP]     = "<reg_loop>",
	[REG_ID]       = "<reg_id>",
	[IOCTL_CONST]  = "<ioctl_const>",
	[PRECISION]    = "<precision>",
	[BASE_ADDR]    = "<base_addr>",
	[FANCY_STR]    = "<fancy_str>",
	[DG_DUMMY_STR] = "<dg_dummy_str>",
	[DRVR_TYPE]    = "<drvr_type>",
	[DG_COMMENT]   = "<dg_comment>",
	[DG_FREE_FMT]  = "<dg_free_fmt>",
	[DG_CHAR]      = "<dg_char>",
	[DG_STRING]    = "<dg_string>",
	[DG_REG_SZ]    = "<dg_reg_sz>"
};

static int ModNameLen;
static char ***GetFindReplace();
static int *GetFindLengths();
static void GlobalReplace(FILE *, FILE *);

/**
 * @brief find/replace table container.
 *
 * @param none
 *
 * All possible templates are checked here for validity.
 *
 * @return find/replace table pointer
 */
static char ***GetFindReplace(void)
{
	static char*** findReplace = NULL;

	if (!findReplace) {	/* init only once */

		findReplace = (char ***)calloc(2, sizeof(char **));

		findReplace[FIND] =
			(char **)calloc(NUM_TOKENS, sizeof(char *));
		findReplace[REPLACE] =
			(char **)calloc(NUM_TOKENS, sizeof(char *));
	}

	return findReplace;
}

/**
 * @brief Token names length container
 *
 * @param none
 *
 * @return pointer to token length container
 */
static int *GetFindLengths()
{
	static int findLengths[NUM_TOKENS] = { 0 };
	int i;

	if (!findLengths[0]) {
		for (i = 0; i < NUM_TOKENS; i++)
			findLengths[i] = strlen((const char *)tokenNames[i]);
	}

	return findLengths;
}

/**
 * @brief Search for a template in the text and replace it with current
 *        template value.
 *
 * @param template - template file
 * @param output   - results goes here
 *
 * @return void
 */
static void GlobalReplace(FILE * template, FILE * output)
{
	char line[2048];
	char *location, *start;
	int swapped, i;

	while (fgets(line, 2048, template) != NULL) {
		start = location = line;

		while ((location = (char *)index(start, '<')) != NULL) {

			/* Dump the characters parsed so far */
			swapped = 0;
			fwrite(start, 1, (int)(location - start), output);
			start = location + 1;

			/* Check if the '<' character found is an interesting
			   token.  If so replace it with the appropriate string
			   and skip the input pointer to just past the token. */
			for (i = 0; i < NUM_TOKENS; i++) {
				if (GetFindReplace()[FIND][i] != NULL) {
					if (strncmp
					    (location,
					     GetFindReplace()[FIND][i],
					     GetFindLengths()[i]) == 0) {
						fprintf(output, "%s",
							(char *)
							GetFindReplace()
							[REPLACE][i]);
						start =
						    location +
						    GetFindLengths()[i];
						swapped = 1;
						break;
					}
				}
			}

			/* No tokens were found so dump the '<' */
			if (!swapped)
				fputc('<', output);
		}
		/* Dump the remaining characters */
		fprintf(output, "%s", start);
	}
}

/**
 * @brief Naming convetion factory
 *
 * @param nm -- module name _exactly_ as in the DB
 *
 * Naming convention is the following:
 *
 * TST_CIBC        --> TstCibc
 * A4032VXIVME     --> A4032vxivme
 * SHARED_CIBC_TST --> SharedCibcTst
 * RF_IRQ_TEST     --> RfIrqTest
 * etc...
 *
 * @b NOTE returned pointer should be freed by the caller
 *
 * @return converted module name
 */
char *naming_convention(char *nm)
{
	char *buf, *bptr;
	char *p, i = 0;
	char *tmp = nm;

	/* count uderscores */
	while ((tmp = index(tmp, (int)'_'))) {
		++i; ++tmp;
	}

	/* will hold new name */
	buf = calloc(strlen(nm)-i + 1, 1);
	bptr = buf;

	tmp = strdup(nm); /* prevent corruption */
	StrToLower(tmp);
	p = strtok(tmp, "_");
	while (p) {
		*p = toupper(*p);
		strcpy(bptr, p);
		bptr += strlen(p);
		p = strtok(NULL, "_");
	}
	free(tmp);
	return buf;
}

/**
 * @brief Template table initialization.
 *
 * @param moduleName -- Module Name _exactly_ as in the DataBase
 * @param bus        -- @b pci or @b vme buses are supproted
 *
 * @return void
 */
void TranslationInit(char *moduleName, char *bus)
{
	char ***fr = GetFindReplace(); /* find&&replace */
	ModNameLen = strlen(moduleName);

	fr[REPLACE][MOD_NAME]     = (char *)strdup(moduleName);
	fr[REPLACE][BUS_UPPER]    = (char *)strdup(bus);
	fr[REPLACE][SYS_LOWER]    = naming_convention(moduleName);
	fr[REPLACE][INT_NUM]      = (char *)calloc(INT_LEN, sizeof(char));
	fr[REPLACE][PLAIN_NUM]    = (char *)calloc(INT_LEN, sizeof(char));
	fr[REPLACE][HEX_NUM]      = (char *)calloc(INT_LEN, sizeof(char));
	fr[REPLACE][FANCY_NUM]    = (char *)calloc(INT_LEN, sizeof(char));
	fr[REPLACE][EXTRA_NUM]    = (char *)calloc(INT_LEN, sizeof(char));
	fr[REPLACE][DEC_NUM]      = (char *)calloc(INT_LEN, sizeof(char));
	fr[REPLACE][DDD_NUM]      = (char *)calloc(INT_LEN, sizeof(char));
	fr[REPLACE][REG_DEPTH]    = (char *)calloc(INT_LEN, sizeof(char));
	fr[REPLACE][REG_LOOP]     = (char *)calloc(INT_LEN, sizeof(char));
	fr[REPLACE][REG_ID]       = (char *)calloc(NAME_LEN + 3, sizeof(char));
	fr[REPLACE][PRECISION]    = (char *)calloc(INT_LEN, sizeof(char));
	fr[REPLACE][IOCTL_CONST]  = (char *)calloc(NAME_LEN + ModNameLen, sizeof(char));
	fr[REPLACE][REG_NAME]     = NULL;
	fr[REPLACE][REG_TYPE]     = NULL;
	fr[REPLACE][REG_COMM]     = NULL;
	fr[REPLACE][BASE_ADDR]    = NULL;
	fr[REPLACE][FANCY_STR]    = NULL;
	fr[REPLACE][DG_DUMMY_STR] = NULL;
	fr[REPLACE][DRVR_TYPE]    = NULL;
	fr[REPLACE][DG_COMMENT]   = NULL;
	fr[REPLACE][DG_FREE_FMT]  = NULL;
	fr[REPLACE][DG_CHAR]      = (char *)calloc(2, sizeof(char));
	fr[REPLACE][DG_STRING]    = NULL;
	fr[REPLACE][DG_REG_SZ]    = (char *)calloc(INT_LEN, sizeof(char));

	/* <bus_upper> */
	StrToUpper(fr[REPLACE][BUS_UPPER]);

	TranslationReset();
}

/**
 * @brief Reset some translation table values to default one.
 *
 * @param none
 *
 * @return void
 */
void TranslationReset()
{
	int i;

	for (i = 0; i < NUM_TOKENS; i++)
		GetFindReplace()[FIND][i] = NULL;

	GetFindReplace()[FIND][SYS_LOWER] = (char *)tokenNames[SYS_LOWER];
	GetFindReplace()[FIND][MOD_NAME] = (char *)tokenNames[MOD_NAME];
	GetFindReplace()[FIND][BUS_UPPER] = (char *)tokenNames[BUS_UPPER];
}

/*---------------- GET and SET access functions -----------------------------*/

/* Returns modified module name Example: SHARED_CIBC => SharedCibc */
char *TranslationGetSysLower()
{
	return GetFindReplace()[REPLACE][SYS_LOWER];
}

/* Returns module name EXACTLY as in the dataBase */
char *TranslationGetModName()
{
	return GetFindReplace()[REPLACE][MOD_NAME];
}

/* Returns bus (VME or PCI) */
char *TranslationGetBus()
{
	return GetFindReplace()[REPLACE][BUS_UPPER];
}

/* <int_num> */
char *TranslationGetIntNum()
{
	return GetFindReplace()[REPLACE][INT_NUM];
}
void TranslationSetIntNum(int newNum)
{
	GetFindReplace()[FIND][INT_NUM] = (char *)tokenNames[INT_NUM];
	snprintf(GetFindReplace()[REPLACE][INT_NUM], INT_LEN, "%d", newNum);
}

/* <plain_num> */
char *TranslationGetPlainNum()
{
	return GetFindReplace()[REPLACE][PLAIN_NUM];
}
void TranslationSetPlainNum(int newNum)
{
	GetFindReplace()[FIND][PLAIN_NUM] = (char *)tokenNames[PLAIN_NUM];
	snprintf(GetFindReplace()[REPLACE][PLAIN_NUM], INT_LEN, "%d", newNum);
}

/* <hex_num> */
char *TranslationGetHexNum()
{
	return GetFindReplace()[REPLACE][HEX_NUM];
}
void TranslationSetHexNum(int newNum)
{
	GetFindReplace()[FIND][HEX_NUM] = (char *)tokenNames[HEX_NUM];
	snprintf(GetFindReplace()[REPLACE][HEX_NUM], INT_LEN, "0x%x", newNum);
}

/* <fancy_num> */
char *TranslationGetFancyNum()
{
	return GetFindReplace()[REPLACE][FANCY_NUM];
}
void TranslationSetFancyNum(int newNum)
{
	GetFindReplace()[FIND][FANCY_NUM] = (char *)tokenNames[FANCY_NUM];
	snprintf(GetFindReplace()[REPLACE][FANCY_NUM], INT_LEN, "%02d", newNum);
}

/* <extra_num> */
char *TranslationGetExtraNum()
{
	return GetFindReplace()[REPLACE][EXTRA_NUM];
}
void TranslationSetExtraNum(int newNum)
{
	GetFindReplace()[FIND][EXTRA_NUM] = (char *)tokenNames[EXTRA_NUM];
	snprintf(GetFindReplace()[REPLACE][EXTRA_NUM], INT_LEN, "0x%X", newNum);
}

/* <dec_num> */
char *TranslationGetDecNum()
{
	return GetFindReplace()[REPLACE][DEC_NUM];
}
void TranslationSetDecNum(int newNum)
{
	GetFindReplace()[FIND][DEC_NUM] = (char *)tokenNames[DEC_NUM];
	snprintf(GetFindReplace()[REPLACE][DEC_NUM], INT_LEN, "%d", newNum);
}

/* <ddd_num> */
char *TranslationGetDDDNum()
{
	return GetFindReplace()[REPLACE][DDD_NUM];
}
void TranslationSetDDDNum(int newNum)
{
	GetFindReplace()[FIND][DDD_NUM] = (char *)tokenNames[DDD_NUM];
	snprintf(GetFindReplace()[REPLACE][DDD_NUM], INT_LEN, "%d", newNum);
}

/* <reg_loop> */
char *TranslationGetRegLoop()
{
	return GetFindReplace()[REPLACE][REG_LOOP];
}
void TranslationSetRegLoop(int newRegLoop)
{
	GetFindReplace()[FIND][REG_LOOP] = (char *)tokenNames[REG_LOOP];
	snprintf(GetFindReplace()[REPLACE][REG_LOOP], INT_LEN, "%d",
		 newRegLoop);
}

/* <reg_id> */
char *TranslationGetRegId()
{
	return GetFindReplace()[REPLACE][REG_ID];
}
void TranslationSetRegId(char *newToken)
{
	GetFindReplace()[FIND][REG_ID] = (char *)tokenNames[REG_ID];
	snprintf(GetFindReplace()[REPLACE][REG_ID], NAME_LEN, "%s_ID",
		 newToken);
}

/* <reg_depth> */
char *TranslationGetRegDepth()
{
	return GetFindReplace()[REPLACE][REG_DEPTH];
}
void TranslationSetRegDepth(int newRegDepth)
{
	GetFindReplace()[FIND][REG_DEPTH] = (char *)tokenNames[REG_DEPTH];
	snprintf(GetFindReplace()[REPLACE][REG_DEPTH], INT_LEN, "%d",
		 newRegDepth);
}

/* <reg_name> */
char *TranslationGetRegName()
{
	return GetFindReplace()[REPLACE][REG_NAME];
}
void TranslationSetRegName(char *newToken)
{
	GetFindReplace()[FIND][REG_NAME] = (char *)tokenNames[REG_NAME];
	GetFindReplace()[REPLACE][REG_NAME] = newToken;
}

/* <reg_comment> */
char *TranslationGetRegComm()
{
	return GetFindReplace()[REPLACE][REG_COMM];
}
void TranslationSetRegComm(char *newToken)
{
	GetFindReplace()[FIND][REG_COMM] = (char *)tokenNames[REG_COMM];
	GetFindReplace()[REPLACE][REG_COMM] = newToken;
}

/*  <reg_type> */
char *TranslationGetRegType()
{
	return GetFindReplace()[REPLACE][REG_TYPE];
}
void TranslationSetRegType(char *newToken)
{
	GetFindReplace()[FIND][REG_TYPE] = (char *)tokenNames[REG_TYPE];
	GetFindReplace()[REPLACE][REG_TYPE] = newToken;
}

/* <dg_reg_sz> */
char* TranslationGetRegSize()
{
	return GetFindReplace()[REPLACE][DG_REG_SZ];
}
void  TranslationSetRegSize(int newRegSz)
{
	GetFindReplace()[FIND][DG_REG_SZ] = (char *)tokenNames[DG_REG_SZ];
	snprintf(GetFindReplace()[REPLACE][DG_REG_SZ], INT_LEN, "%d", newRegSz);
}

/* <ioctl_const> */
char *TranslationGetIoctlConst()
{
	return GetFindReplace()[REPLACE][IOCTL_CONST];
}
void TranslationSetIoctlConst(char *token, char *regName)
{
	GetFindReplace()[FIND][IOCTL_CONST] = (char *)tokenNames[IOCTL_CONST];
	if (!strncmp(regName, SRVREGPREF, strlen(SRVREGPREF)))	/* service register */
		snprintf(GetFindReplace()[REPLACE][IOCTL_CONST],
			 NAME_LEN + ModNameLen, "SRV_%s_%s", token,
			 regName + strlen(SRVREGPREF));
	else			/* normal module register */
		snprintf(GetFindReplace()[REPLACE][IOCTL_CONST],
			 NAME_LEN + ModNameLen, "%s_%s_%s",
			 TranslationGetModName(), token, regName);
}

/* <precision> */
char *TranslationGetPrecision()
{
	return GetFindReplace()[REPLACE][PRECISION];
}
void TranslationSetPrecision(int newPrecision)
{
	GetFindReplace()[FIND][PRECISION] = (char *)tokenNames[PRECISION];
	snprintf(GetFindReplace()[REPLACE][PRECISION], INT_LEN, "%d",
		 newPrecision);
}

/* <base_addr> */
char *TranslationGetBaseAddr()
{
	return GetFindReplace()[REPLACE][BASE_ADDR];
}
void TranslationSetBaseAddr(char *newToken)
{
	GetFindReplace()[FIND][BASE_ADDR] = (char *)tokenNames[BASE_ADDR];
	GetFindReplace()[REPLACE][BASE_ADDR] = newToken;
}

/* <fancy_str> */
char *TranslationGetFancyString()
{
	return GetFindReplace()[REPLACE][FANCY_STR];
}
void TranslationSetFancyString(char *newToken)
{
	GetFindReplace()[FIND][FANCY_STR] = (char *)tokenNames[FANCY_STR];
	GetFindReplace()[REPLACE][FANCY_STR] = newToken;
}

/* <dg_dummy_str> */
char *TranslationGetDummyString()
{
	return GetFindReplace()[REPLACE][DG_DUMMY_STR];
}
void TranslationSetDummyString(char *newToken)
{
	GetFindReplace()[FIND][DG_DUMMY_STR] = (char *)tokenNames[DG_DUMMY_STR];
	GetFindReplace()[REPLACE][DG_DUMMY_STR] = newToken;
}

/* <drvr_type> */
char *TranslationGetDriverType()
{
	return GetFindReplace()[REPLACE][DRVR_TYPE];
}
void TranslationSetDriverType(char *newToken)
{
	GetFindReplace()[FIND][DRVR_TYPE] = (char *)tokenNames[DRVR_TYPE];
	GetFindReplace()[REPLACE][DRVR_TYPE] = newToken;
}

/* <dg_comment> */
char *TranslationGetComment()
{
	return GetFindReplace()[REPLACE][DG_COMMENT];
}
void TranslationSetComment(char *newToken)
{
	GetFindReplace()[FIND][DG_COMMENT] = (char *)tokenNames[DG_COMMENT];
	GetFindReplace()[REPLACE][DG_COMMENT] = newToken;
}

/* <dg_free_fmt> */
char *TranslationGetFreeFormat()
{
	return GetFindReplace()[REPLACE][DG_FREE_FMT];
}
void TranslationSetFreeFormat(char *newToken, ...)
{
	va_list ap;
	static char formatStr[MAX_STR];

	memset(formatStr, 0, sizeof(formatStr));

	GetFindReplace()[FIND][DG_FREE_FMT] = (char *)tokenNames[DG_FREE_FMT];

	va_start(ap, newToken);
	vsnprintf((char *)formatStr, MAX_STR, newToken, ap);
	va_end(ap);

	GetFindReplace()[REPLACE][DG_FREE_FMT] = (char *)formatStr;
}

/* <dg_char> */
char *TranslationGetChar()
{
	return GetFindReplace()[REPLACE][DG_CHAR];
}
void TranslationSetChar(char newToken)
{
	GetFindReplace()[FIND][DG_CHAR] = (char *)tokenNames[DG_CHAR];
	snprintf(GetFindReplace()[REPLACE][DG_CHAR], 2, "%c", newToken);
}

/* <dg_string> */
char *TranslationGetString()
{
	return GetFindReplace()[REPLACE][DG_STRING];
}
void TranslationSetString(char *newToken)
{
	GetFindReplace()[FIND][DG_STRING] = (char *)tokenNames[DG_STRING];
	GetFindReplace()[REPLACE][DG_STRING] = newToken;
}

/*----------------- END of get and set access functions ---------------------*/

/**
 * @brief Template translation is performed in this function.
 *
 * @param output           - where to put
 * @param path             - template file path directory in the template
 *                           root directory
 * @param templateFileName - template
 * @param ...              -
 *
 * @return void
 */
void Translate(FILE * output, char *path, char *templateFileName, ...)
{
	va_list ap;
	char inputName[MAXPATHLEN];
	char *bufferStart;
	int templateRootLen;
	FILE *template;		/* template file */

	templateRootLen =
	    snprintf(inputName, MAXPATHLEN, "%s/%s/", TEMPLATE_ROOT, path);
	bufferStart = inputName + templateRootLen;

	va_start(ap, templateFileName);
	vsnprintf(bufferStart, MAXPATHLEN - templateRootLen, templateFileName,
		  ap);
	va_end(ap);

	template = fopen(inputName, "r");
	if (template == NULL) {
		fprintf(stderr,
			"%s Couldn't open file \"%s\" for translation.\n",
			ERROR_MSG, inputName);
		return;
	}

	GlobalReplace(template, output);
	fclose(template);
}
