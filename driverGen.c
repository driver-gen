/**
 * @file driverGen.c
 *
 * @brief Pulling everything together.
 *
 * Device Driver Generator (aka DriverGen) tool designed to produce
 * device drivers for a given architecture based on an abstract description
 * of a piece of hardware.
 *
 * @author Copyright (C) 2002 CERN. Stuart Baird
 * @author Copyright (C) 2003 CERN. Alain Gagnaire
 * @author Copyright (C) 2003 - 2010 CERN. Georgievskiy Yury <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */
#define _GNU_SOURCE /* asprintf rocks */
#include <getopt.h>
#include "driverGen.h"
#include "utilities.h"
#include "serviceOptions.h"
#include "dg-version.h"
#include "dg-git-lib.h"

struct mdescr dgmd = { 0 }; /* Driver-Gen Module Description */

bool verboseMode;		/* denotes verbose driver generation */
static dgopt genOpt;		/* possible command line options */
static srvarg glob_srv_arg;	/* possible service option arguments */
char *driverName;		/* Driver Name. Exactly as in the DataBase */
char *busType;			/* bus type. 'vme' or 'pci' */
time_t mod_creation_time;	/* driver creation time of the currently
				   processed module */

/* supported buses */
static char *allBuses[] = {
	"vme",
	"VME",
	"pci",
	"PCI",
	NULL
};

/* supported command line options */
static struct option allLongOptions[] = {
	{ "git",     no_argument,       NULL, 'g' },
	{ "gall",    no_argument,       NULL, 'a' },
	{ "ginfo",   no_argument,       NULL, 'i' },
	{ "general", required_argument, NULL, 'e' },
	{ "verbose", no_argument,       NULL, 'n' },
	{ "srv",     required_argument, NULL, 's' }, /* see 'serviceOptions.h'
							for more information */
	{ "dir",     required_argument, NULL, 'd' },
	{ "version", no_argument,       NULL, 'v' },
	{ "help",    no_argument,       NULL, 'h' },
	{
		.name    = 0,
		.has_arg = no_argument,	/* 0 */
		.flag    = 0,
		.val     = 0
	}
};

static int  CheckAndPrepareRegs(RegisterDef_t *, int, VmeInfo_t *);
static int  CheckAndPrepareBlocks(BlockDef_t *, int);
static void SortRegisters(RegisterDef_t *, VmeInfo_t *, int, int);
static int  LessThan(RegisterDef_t *, RegisterDef_t *);
static int  GreaterThan(RegisterDef_t *, RegisterDef_t *);
static void SortBlocks(BlockDef_t *, int, int);
static bool CheckBusType(char *);
static void DisplayUsage(char *);
static void ParseProgArgs(int, char *[], struct mdescr *);
static void free_md(struct mdescr *);

int main(int argc, char *argv[], char *envp[])
{
	int numRegisters;
	int numBlocks;
	rstat result;
	RegisterDef_t registers[MAX_REG] = { {0} };
	BlockDef_t blocks[MAX_BLK] = { {0} };
	PciInfo_t pciInfo;
	VmeInfo_t vmeInfo;

	time(&mod_creation_time);
	printf("%s <vers. %s> started on %s\n", basename(argv[0]), dg_version,
	       ctime(&mod_creation_time));

	/* Init default parameters */
	vmeInfo.CheckRegister = -1;	/* no checking register */
	verboseMode = FALSE;	/* by default - no verbose mode */

	ParseProgArgs(argc, argv, &dgmd); /* check if provided params are valid */

	/* init global template values for further use */
	TranslationInit(driverName, busType);

	/* setup Master installation directory */
	get_mid(&dgmd);

	/* if only general shared code should be build */
	if (genOpt == OPT_G_SHARED) {
		BuildGeneralCode();
		printf("%s: All done.\n", basename(argv[0]));
		exit(EXIT_SUCCESS);
	}

	/* if it's one of the service calls */
	if (genOpt == OPT_G_SERVICE) {
		if (ProcessSrvOpt(glob_srv_arg, basename(argv[0]))) {
			printf("%s: service option processing fails\n",
			       argv[0]);
			exit(EXIT_FAILURE);
		} else {
			printf("%s: service option processing done.\n",
			       argv[0]);
			exit(EXIT_SUCCESS);
		}
	}

	/* get block configuration from database */
	if (!(numBlocks = GetBlockConfig(driverName, blocks, argv[0]))) {
		fprintf(stderr, "%s No Blocks found for %s Module\n", ERROR_MSG,
			driverName);
		exit(EXIT_FAILURE);
	}

	/* check if block configuration is correct */
	if (CheckAndPrepareBlocks(blocks, numBlocks) != DRIVER_GEN_OK)
		exit(EXIT_FAILURE);

	/* get register configuration from database */
	numRegisters = GetRegisterConfig(driverName, registers, blocks,
					 numBlocks, argv[0]);
	if (!numRegisters) {
		fprintf(stderr, "%s No Registers found for %s Module\n",
			ERROR_MSG, driverName);
		exit(EXIT_FAILURE);
	}

	/* check if register configuration is correct */
	if (CheckAndPrepareRegs(registers, numRegisters, &vmeInfo) !=
	    DRIVER_GEN_OK)
		exit(EXIT_FAILURE);

	/* we need to compute block sizes and their register amount */
	set_extra_block_data(registers);

	/* get general board information from the database */
	if (!strcmp(TranslationGetBus(), "VME"))
		result = GetVmeInfo(driverName, &vmeInfo);
	else
		result = GetPciInfo(driverName, &pciInfo);

	if (result)
		exit(EXIT_FAILURE);	/* 1 */

	/* setup Module source directory */
	get_msd(&dgmd);

	/* now we can start Driver/Simulator code generation */
	if (!strcmp(TranslationGetBus(), "VME")) {	/* we need VME driver */
		switch (genOpt) {
		case OPT_G_IT:
			if (do_git_vme_driver(driverName, &dgmd, registers, numRegisters,
					      blocks, numBlocks, &vmeInfo))
				exit(EXIT_FAILURE);
			break;
		case OPT_G_ALL:	/* generate all, include user-defined files */
			printf("Creating   %s source code directory.\n", driverName);
			GenerateVmeDriver(registers, numRegisters, blocks,
					  numBlocks, &vmeInfo);
			break;
		case OPT_G_INFO:	/* generate only info files */
			printf("Creating   %s source code directory.\n", driverName);
			GenerateVmeInfoFile(registers, numRegisters, blocks,
					    numBlocks, &vmeInfo);
			break;
		default:
			FATAL_ERR();
			break;	/* not reached */
		}
	} else { /* we need PCI driver */
		switch (genOpt) {
		case OPT_G_IT:
			/* TODO */
			do_git_pci_driver(driverName, &dgmd, registers, numRegisters,
					  blocks, numBlocks, &pciInfo);
			break;
		case OPT_G_ALL:	/* generate all, include user-defined files */
			printf("Creating   %s source code directory.\n", driverName);
			GenerateDrmDriver(registers, numRegisters, blocks,
					  numBlocks, &pciInfo);
			break;
		case OPT_G_INFO:	/* generate only install files */
			printf("Creating   %s source code directory.\n", driverName);
			/* TODO. Should be removed. */
			GenerateDrmInstallFilesOLD(&pciInfo, blocks, numBlocks);
			break;
		default:
			FATAL_ERR();
			break;	/* not reached */
		}
	}

	free_md(&dgmd);
	printf("All done.\n");
	exit(EXIT_SUCCESS);	/* 0 */
}

/**
 * @brief Checks if module configuration is correct and supported.
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param vmeInfo      -- @b VME description
 *
 * @return DRIVER_GEN_OK  - if configuration is ok.
 * @return DRIVER_GEN_BAD - othervise.
 */
static int CheckAndPrepareRegs(RegisterDef_t * registers, int numRegisters,
			       VmeInfo_t * vmeInfo)
{
	int i, j;
	int badConfig = DRIVER_GEN_OK;
	int mappedAddr = 0;	/* checked address range */
	int regDepth;		/* register depth, in elements */

	for (i = 0; i < numRegisters; i++) { /* Sanitise register definition */
		StrToLower(registers[i].size);
		StrToLower(registers[i].mode);
		registers[i].rar = 0;
		for (j = 0; j < strlen(registers[i].mode); j++) {
			if (registers[i].mode[j] == 'w') {
				registers[i].rar |= AMWR;
			} else if (registers[i].mode[j] == 'r') {
				registers[i].rar |= AMRD;
			} else if (registers[i].mode[j] == 'e') {
				registers[i].rar |= AMEX;
			} else if (registers[i].mode[j] == 'c') {
				vmeInfo->CheckRegister = i;
			} else {
				badConfig = DRIVER_GEN_BAD;
				fprintf(stderr, "%s Register '%s' has unknown"
					" access mode '%c'\n",
					ERROR_MSG, registers[i].name,
					registers[i].mode[j]);
			}
		}

		strcpy(registers[i].upperName, registers[i].name);
		StrToUpper(registers[i].upperName);

		if (!strcmp(registers[i].size, "char"))
			registers[i].regSize = SZCHAR;
		else if (!strcmp(registers[i].size, "short"))
			registers[i].regSize = SZSHORT;
		else if (!strcmp(registers[i].size, "long"))
			registers[i].regSize = SZLONG;
		else {
			badConfig = DRIVER_GEN_BAD;
			fprintf(stderr, "%s Register '%s' has incorrect size"
				" name '%s'\n",
				ERROR_MSG, registers[i].name,
				registers[i].size);
		}

		/* Check if the data is valid */
		if (registers[i].timeLoop < 0) {
			badConfig = DRIVER_GEN_BAD;
			fprintf(stderr, "%s Register '%s' has a negative"
				" timeloop %d\n",
				ERROR_MSG, registers[i].name,
				registers[i].timeLoop);
		}

		for (j = 0; j < strlen(registers[i].name); j++)
			if (isspace(registers[i].name[j]))
				/* replace wite spaces with underscore */
				registers[i].name[j] = '_';

		for (j = i + 1; j < numRegisters; j++)
			if (!strcmp(registers[i].name, registers[j].name)) {
				badConfig = DRIVER_GEN_BAD;
				fprintf(stderr, "%s Register definitions %d"
					" and %d share the name \"%s\"\n",
					ERROR_MSG, i, j, registers[i].name);
			}

		if (registers[i].rar > AMEX) {
			fprintf(stderr, "%s Register '%s' has a conflicting"
				" access mode '%s'\n",
				ERROR_MSG, registers[i].name,
				registers[i].mode);
			sprintf(registers[i].mode, "e");
			registers[i].rar = AMEX;
		}

		if ((!registers[i].blockP)) {
			badConfig = DRIVER_GEN_BAD;
			fprintf(stderr, "%s Register '%s' doesn't belong to"
				" any block\n",
				ERROR_MSG, registers[i].name);
		}

		if (registers[i].offset < 0) {
			badConfig = DRIVER_GEN_BAD;
			fprintf(stderr, "%s Register '%s' has negative offset"
				" value '0x%x'\n",
				ERROR_MSG, registers[i].name,
				registers[i].offset);
		}
	}			/* end of 'for' */

	if (badConfig == DRIVER_GEN_BAD)
		return badConfig;

	/* sort registers by their address */
	SortRegisters(registers, vmeInfo, 0, numRegisters - 1);

	for (i = 0; i < numRegisters - 1; i++) {

		if (registers[i].depth <= 0)
			regDepth = 1;
		else
			regDepth = registers[i].depth;

		/* check registers that belong to the same block only */
		if (registers[i].blockP->blockID ==
		    registers[i + 1].blockP->blockID) {
			/* check if there is duplicate register addresses
			   in the same block */
			if (registers[i].offset == registers[i + 1].offset) {
				badConfig = DRIVER_GEN_BAD;
				fprintf(stderr, "%s Registers '%s' and '%s' in"
					" Block#%d have the same offset %#x\n",
					ERROR_MSG, registers[i].name,
					registers[i + 1].name,
					registers[i].blockP->blockID,
					registers[i].offset);
			}
			mappedAddr =
			    registers[i].offset +
			    registers[i].regSize * regDepth;
			if (mappedAddr > registers[i + 1].offset) {
				fprintf(stderr, "%s Overlapping between '%s'"
					" and '%s' register massives\n"
					"\tdetected in Block#%d. Incorrect "
					"device memory mapping. Try to "
					"change\n\tmemory layout in the Data"
					" Base.\n",
					ERROR_MSG, registers[i].name,
					registers[i + 1].name,
					registers[i].blockP->blockID);
				badConfig = DRIVER_GEN_BAD;
			}
		} else {
			mappedAddr = 0;
		}
	}

	if (badConfig == DRIVER_GEN_BAD)
		return badConfig;

	/* Check if device checking address is provided */
	if (vmeInfo->CheckRegister == -1) {
		fprintf(stderr, "%sChecking Register is NOT provided!\n"
			"         Module availability will NOT be checked"
			" during device driver\n         installation!\n\n",
			WARNING_MSG);
		sleep(2);	/* let user see it */
	}

	return badConfig;
}

/**
 * @brief Checks if block configuration is correct and supported.
 *
 * @param blocks    -- block description
 * @param numBlocks -- block amount
 *
 * @return DRIVER_GEN_OK  - if configuration is ok.
 * @return DRIVER_GEN_BAD - othervise.
 */
static int CheckAndPrepareBlocks(BlockDef_t * blocks, int numBlocks)
{
	int i;
	int configState = DRIVER_GEN_OK;

	for (i = 0; i < numBlocks; i++) {
		if (blocks[i].blockID < 0) {
			fprintf(stderr, "%s GetBlockConfig() has returned a"
				" record with a negative block number\n",
				ERROR_MSG);
			configState = DRIVER_GEN_BAD;
		}

		if ((blocks[i].blkBaseAddr < 1) ||
		    (blocks[i].blkBaseAddr > 2)) {
			fprintf(stderr, "%s Block#%d is associated with"
				" invalid base address value '%d'\n",
				ERROR_MSG, blocks[i].blockID,
				blocks[i].blkBaseAddr);
			configState = DRIVER_GEN_BAD;
		}

		if (blocks[i].offset < 0) {
			fprintf(stderr, "%s Block#%d has a negative offset"
				" value\n",
				ERROR_MSG, blocks[i].blockID);
			configState = DRIVER_GEN_BAD;
		}
	}

	if (configState == DRIVER_GEN_OK) {
		SortBlocks(blocks, 0, numBlocks - 1);
		for (i = 0; i < numBlocks - 1; i++)
			if (blocks[i].blockID == blocks[i + 1].blockID) {
				fprintf(stderr, "%s GetBlockConfig() has"
					" returned two records with block ID's"
					" equals to %d\n",
					ERROR_MSG, blocks[i].blockID);
				configState = DRIVER_GEN_BAD;
			}
	}

	return configState;
}

/**
 * @brief Sort registers by their block indexes && addresses.
 *
 * @param registers -- register description table
 * @param vmeInfo   -- VME description
 * @param low       -- lowest index
 * @param high      -- highest index
 */
static void SortRegisters(RegisterDef_t *registers, VmeInfo_t *vmeInfo,
			  int low, int high)
{
	int left, right;
	RegisterDef_t buffer;
	RegisterDef_t *median;

	if (high > low) {
		left = low;
		right = high;
		median = &registers[low];

		while (right >= low) {
			while (LessThan(&(registers[left]), median))
				left++;

			while (GreaterThan(&(registers[right]), median))
				right--;

			if (left > right)
				break;

			buffer = registers[left];
			registers[left] = registers[right];
			registers[right] = buffer;

			/* move checking register index */
			if (vmeInfo->CheckRegister == left)
				vmeInfo->CheckRegister = right;
			else if (vmeInfo->CheckRegister == right)
				vmeInfo->CheckRegister = left;

			left++;
			right--;
		}

		SortRegisters(registers, vmeInfo, low, right);
		SortRegisters(registers, vmeInfo, left, high);
	}
}

/**
 * @brief Check if register a is smaller then register b
 *
 * @param a -- reg descr
 * @param b -- reg descr
 *
 * @return 1 - if first regID is smaller then the second one.
 * @return 0 - otherwise.
 */
static int LessThan(RegisterDef_t *a, RegisterDef_t *b)
{
	if (a->blockP->blockID < b->blockP->blockID)
		return 1;
	else if ((a->blockP->blockID == b->blockP->blockID) &&
		 (a->offset < b->offset))
		return 1;
	else
		return 0;
}

/**
 * @brief Check if register a is greater then register b
 *
 * @param a -- reg descr
 * @param b -- reg descr
 *
 * @return 1 - if first regID is greater then the second one.
 * @return 0 - otherwise.
 */
static int GreaterThan(RegisterDef_t *a, RegisterDef_t *b)
{
	if ((a->blockP->blockID == b->blockP->blockID)
	    && (a->offset == b->offset))
		return 0;
	else
		return (!LessThan(a, b));
}

/**
 * @brief Sort blocks by their index
 *
 * @param blocks -- block description
 * @param low    -- low block idx
 * @param high   -- high block idx
 *
 * @return void
 */
static void SortBlocks(BlockDef_t *blocks, int low, int high)
{
	int left, right;
	BlockDef_t buffer;
	BlockDef_t *median;

	if (high > low) {
		left = low;
		right = high;
		median = &(blocks[low]);

		while (right >= low) {
			while (blocks[left].blockID < median->blockID)
				left++;

			while (blocks[right].blockID > median->blockID)
				right--;

			if (left > right)
				break;

			buffer = blocks[left];
			blocks[left] = blocks[right];
			blocks[right] = buffer;
			left++;
			right--;
		}

		SortBlocks(blocks, low, right);
		SortBlocks(blocks, left, high);
	}
}

/**
 * @brief Checks if the bus type parameter is correct.
 *
 * If it is not, than error is printed out on stderr.
 *
 * @param str -- string to check
 *
 * @return TRUE - if parameter is valid.
 * @return FALSE - otherwise.
 */
static bool CheckBusType(char *str)
{
	bool res = FALSE;
	char **ptr, errMsg[64];

	ptr = allBuses;
	snprintf(errMsg, sizeof(errMsg), "Bus type \'%s\' not supported.\n",
		 str);

	while (*ptr) {
		if (!(strcmp(*ptr, str))) {	/* option matches */
			res = TRUE;
			break;
		}
		ptr++;		/* move pointer */
	}

	if (!res)
		fprintf(stderr, errMsg);	/* printout error message. */

	return (res);
}

/**
 * @brief Educate user
 *
 * @param progName -- program name
 *
 * @return void
 */
static void DisplayUsage(char *progName)
{
	progName = basename(progName);
	printf("%s %s\n\n", progName, dg_version);
	printf("Usage: %s <module_name> <bus> -g[it | all | info] "
	       "<-verbose> -d <root_dir>\n", progName);
	printf("       %s <-general> <bus>\n", progName);
	printf("       %s <-h> <--version> (to get help && version info)\n",
	       progName);
	printf("where:\n");

	/* module-name */
	printf(" <module_name> %sCompulsory%s\n", WHITE_CLR, END_CLR);
	printf(" It is the name of the module for which Driver/Simulator "
	       "should be generated.\n");
	printf(" Should be exactly as in the DataBase. See DataBase for"
	       " more info about\n Module Names.\n\n");

	/* bus */
	printf(" <bus> %sCompulsory%s\n", WHITE_CLR, END_CLR);
	printf(" Select the bus architecture supported by the card.\n");
	printf(" Supported buses are 'pci' or 'vme'.\n\n");

	/* '-g' mode description */
	printf(" -g %sOptional%s\n"
	       "    What to generate (if not provided -- default will be"
	       " used):\n", WHITE_CLR, END_CLR);
	printf("    -g[all]\n"
	       "      Generate complete driver framework\n");
	printf("    -g[it] %sDefault%s\n"
	       "      Generate git-controlled Driver, i.e. user-defined files"
	       " are generated\n"
	       "      only if driver directory does not yet exist (otherwise"
	       " user-defined files\n"
	       "      left intact), driver dir name doesn't have time prefix,"
	       " git-tag is\n"
	       "      performed. (All files, that've got \"UserDefined\""
	       " subword in their\n"
	       "      name -- are considered to be user-defined files)\n",
	       WHITE_CLR, END_CLR);

	printf("    -g[info]\n"
	       "      Generate only info file for current module.\n\n");

	/* '-verbose' mode description */
	printf(" <-verbose> %sOptional%s\n"
	       " This enables verbose driver/simulator source code"
	       " generation.\n"
	       " Useful for debugging purposes.\n\n", WHITE_CLR, END_CLR);

	/* '-dir' mode description */
	printf(" <-d> %sOptional%s\n"
	       " Change root directory, where generated driver source code"
	       " will reside.\n"
	       " Default one is 'genDriverDeposit'\n\n",
	       WHITE_CLR, END_CLR);

	/* '-general' mode description */
	printf(" <-general> %sOptional%s\n", WHITE_CLR, END_CLR);
	printf(" Normal users should ignore this option.\n If all that you"
	       " want - is to generate a Driver/Simulator framework for a\n"
	       " given module, and you didn't make any changes in"
	       " installation procedure\n and library code (that is usially"
	       " the case), than you should ignore\n this option. Only"
	       " general purpose files (i.e. identical for all types of \n"
	       " Driver/Simulator) will be generated. These are module"
	       " Driver/Simulator\n installation and unistallaion programs,"
	       " 'DriverAccess' library and general\n header files. Normally,"
	       " general purpose part should be rebuild by 'driverGen'\n"
	       " software developer, and only in case of it's modification."
	       " Bus type (pci or\n vme) should be provided.\n\n");

	/* show some examples examples */
	printf("EXAMPLES: %s BNLDSP vme -gall\n"
	       "          %s SHARED_CIBC vme\n\n",
	       progName, progName);
}

#define OPT_COLLIDE(__opt)			\
({						\
  int __val;					\
  if (genOpt & (~__opt)) {			\
    genOpt = OPT_COLLIDE;			\
    __val = 1;					\
  } else					\
    __val = 0;					\
  __val;					\
})
#define MAX_NON_OPT_ARGS 3	/* max non-option arguments amount */

/**
 * @brief Parses command line arguments.
 *
 * @param argc -- command line argument count
 * @param argv -- command line argument array
 * @param md   -- module description
 *
 * @return void
 */
static void ParseProgArgs(int argc, char *argv[], struct mdescr *md)
{
	int cur_opt; /* currently parsed option */
	char arg_mas[MAX_NON_OPT_ARGS][NAME_LEN]; /* hold all non-option
						     arguments */
	int arg_cntr = 0;	/* non-option arguments counter */
	int expected_arg_amount = 0;

	memset(arg_mas, 0, sizeof(arg_mas));
	while (((cur_opt =
		 getopt_long_only(argc, argv, "-hv", allLongOptions,
				  NULL)) != EOF) && (genOpt != OPT_COLLIDE)
	       && (glob_srv_arg != SRV_ARG_ERROR) && (arg_cntr != 27051977)) {
		switch (cur_opt) {
		case 1:	/* this is non-option arg */
			if (arg_cntr == MAX_NON_OPT_ARGS) {
				arg_cntr = 27051977;
				break;
			}
			strncpy(arg_mas[arg_cntr], optarg, NAME_LEN);
			arg_mas[arg_cntr][NAME_LEN - 1] = 0; /* safety
								terminate */
			++arg_cntr;
			break;
		case 'h':	/* user need help */
			DisplayUsage(argv[0]);
			exit(EXIT_SUCCESS);	/* 0 */
			break;
		case 'g': /* generate driver, controlled by git */
			if (OPT_COLLIDE(OPT_G_IT))
				break;
			genOpt = OPT_G_IT;
			expected_arg_amount = 2;
			md->isgit = 1;
			break;
		case 'a':	/* generate full driver (include user part) */
			if (OPT_COLLIDE(OPT_G_ALL))
				break;
			genOpt = OPT_G_ALL;
			expected_arg_amount = 2;
			break;
		case 'i':	/* only info file */
			if (OPT_COLLIDE(OPT_G_INFO))
				break;
			genOpt = OPT_G_INFO;
			expected_arg_amount = 2;
			break;
		case 'e':	/* general part only (-general) */
			if (OPT_COLLIDE(OPT_G_SHARED))
				break;
			genOpt = OPT_G_SHARED;
			expected_arg_amount = 0;
			busType = strdup(optarg);	/* set bus type */
			driverName = strdup("GENERAL");	/* set name for
							   general part */
			break;
		case 's':	/* we have service option */
			/* see serviceOptions.h for more info */
			if (OPT_COLLIDE(OPT_G_SERVICE))
				break;
			genOpt = OPT_G_SERVICE;
			/* they've got non-option arguments. set their amount */
			if (!strcmp(optarg, "drvrvers")) {
				expected_arg_amount = 3;
				glob_srv_arg = SRV_VF_DRVR; /* Verion File for
							       the driver */
			} else if (!strcmp(optarg, "simvers")) {
				expected_arg_amount = 3;
				glob_srv_arg = SRV_VF_SIM; /* Version File for
							      the simulator */
			} else {
				expected_arg_amount = 0;
				glob_srv_arg = SRV_ARG_ERROR;
			}
			break;
		case 'n': /* verbose printout during code generation */
			verboseMode = TRUE;
			break;
		case 'd': /* root directory, where driver directory will be created */
			asprintf(&md->mdn, "%s", optarg);
			break;
		case 'v': /* version string */
			printf("%s [Compiled on %s %s]\n", dg_version,
			       __DATE__, __TIME__);
			exit(EXIT_SUCCESS);
			break;
		case '?': /* wrong option */
		default:
			fprintf(stderr, "Wrong option.\n");
			fprintf(stderr, "Try \'%s -h\' for more information.\n",
				argv[0]);
			exit(EXIT_FAILURE);	/* 1 */
			break;
		}
	}

	if (genOpt == OPT_COLLIDE) {	/* check for option colliding */
		fprintf(stderr, "Option collision detected!\n");
		fprintf(stderr, "Try \'%s -h\' for more information.\n",
			argv[0]);
		exit(EXIT_FAILURE);	/* 1 */
	}

	if (!genOpt) {	/* user didn't provide command line
			   option -- set default */
		genOpt = OPT_G_IT; /* by default create git-controlled driver */
		md->isgit = 1;
		expected_arg_amount = 2;
	}

	if (glob_srv_arg == SRV_ARG_ERROR) { /* check service option argument */
		fprintf(stderr, "Wrong service option argument.\n");
		fprintf(stderr, "Try \'%s -h\' for more information.\n",
			argv[0]);
		exit(EXIT_FAILURE);	/* 1 */
	}

	if (arg_cntr != expected_arg_amount) {	/* check for argument amount */
		fprintf(stderr, "Wrong arguments.\n");
		fprintf(stderr, "Try \'%s -h\' for more information.\n",
			argv[0]);
		exit(EXIT_FAILURE);	/* 1 */
	}

	/* now we handle all non-option arguments
	   (they are in 'arg_mas' massive) */
	switch (genOpt) {
	case OPT_G_IT:
		if (git_check_version())
			exit(EXIT_FAILURE);
	case OPT_G_ALL:
	case OPT_G_INFO:
		driverName = strdup(arg_mas[0]); /* set DB driver name */
		busType = strdup(arg_mas[1]);	/* set bus type */
		break;
	case OPT_G_SHARED:
		break;		/* nothing to do */
	case OPT_G_SERVICE:
		switch (glob_srv_arg) {
		case SRV_VF_DRVR:
		case SRV_VF_SIM:
			driverName = strdup(arg_mas[0]); /* set DB driver
							    name */
			busType = strdup(arg_mas[1]);	/* set bus type */
			SetSrvOptArgs(glob_srv_arg, arg_mas[2]);
			break;
		default:
			break;
		}
	default:
		break;
	}

	if (!CheckBusType(busType)) {	/* check 'Bus Type' */
		fprintf(stderr, "Try \'%s -h\' for more information.\n",
			argv[0]);
		exit(EXIT_FAILURE);	/* 1 */
	}

	if (!md->mdn)
		asprintf(&md->mdn, "%s", DEFAULT_MASTER_DIR);
}

/**
 * @brief Free allocated resources of module description.
 *
 * @param md -- module description.
 *
 */
static void free_md(struct mdescr *md)
{
	if (md->mdn)
		free(md->mdn);
}
