/**
 * @file commonGeneration.c
 *
 * @brief Generates driver files that are common for both, VME and PCI modules.
 *
 * @author Copyright (C) 2002 CERN. Stuart Baird
 * @author Copyright (C) 2003 CERN. Alain Gagnaire
 * @author Copyright (C) 2003 - 2010 CERN. Georgievskiy Yury <ygeorgie@cern.ch>
 *
 * @date Created on 27/02/2004
 *
 * @section license_sec License
 *          Released under the GPL
 */
#include <sys/types.h>
#include <sys/stat.h>
#include "driverGen.h"
#include "utilities.h"

static void  BuildVersionFiles(FILETYPE);
static char *GetGeneralSrcDir();
static char *GetLocalDrvrDir();
static char *GetLocalFcltyDir();
static char *GetLocalInclDir();
static char *GetLocalLibDir();
static char *GetLocalInstDir();
static void  BuildTestProgHFile(RegisterDef_t *, int);
static void  BuildTestProgConsts(RegisterDef_t *, int, FILE *);
static void  BuildTestProgSubMenus(RegisterDef_t *, BlockDef_t *, int, FILE *);
static void  BuildGeneralHeaderFiles();
static void  BuildGeneralLibraries();
static void  BuildGeneralInstallProgram();
static void  BuildGeneralMakefiles();
static void  BuildGeneralScripts();
static int   HasExtraneous(RegisterDef_t *, int);
static int   HasWriteOnly(RegisterDef_t *, int);

/**
 * @brief TODO. REMOVE! (Still used by drmGeneration.c module)
 *
 * @param numDevs --
 *
 * @return void
 */
void BuildDescsOLD(int numDevs)
{
	FILE *devDescFile;
	FILE *simDescFile;
	FILE *opDescFile;
	int i;

	devDescFile = OpenFile(DRIVER_FT, LOC_MODSRC, "INST_BI/Dev.desc");
	simDescFile = OpenFile(SIM_FT, LOC_MODSRC, "INST_BI/Dev.desc");
	opDescFile = OpenFile(DRIVER_FT, LOC_MODSRC, "INST_BI/.desc");

	Translate(devDescFile, "common", "descs/devHead.desc");
	Translate(simDescFile, "common", "descs/simHead.desc");
	Translate(opDescFile, "common", "descs/opHead.desc");

	for (i = 0; i < numDevs; i++) {
		TranslationSetFancyNum(i);
		TranslationSetPlainNum(i);
		Translate(devDescFile, "common", "descs/devDef.desc");
		Translate(simDescFile, "common", "descs/devDef.desc");
		Translate(opDescFile, "common", "descs/devDef.desc");
	}

	fclose(devDescFile);
	fclose(simDescFile);
	fclose(opDescFile);
}

/**
 * @brief Create and open for editing file with the specified name <b>name</b>
 *
 * @param type -- @b DRIVER_FT, @b SIM_FT, @b COMMON_FT or @b EXACT_FT
 * @param loc  -- file path
 * @param name -- filename
 * @param ...  -- extra parameters in case if filename @e name contains
 *                conversion specification(s)
 *
 * - 0x0. Parameter 'name' can consist of conversion
 * specification (e.g. '%s'), that begins with the character '%'.
 * In this case one should provide additional parameters for each
 * specification
 * - 0x1. This function takes into account currently processed
 * module type (driver or simulator). If it's a driver, than
 * suffix 'Drvr' will be added to the generated file name. If it's
 * simulator, than suffix 'Sim' will be added. It also can be
 * so-called 'EXACT_FT' type In this case file is created with
 * exact given name, without adding any specific driver or
 * simulator prefixes or suffixes. If type is COMMON_FT than
 * only 'driver name' preffix is added (e.g. SharedCibc), but not
 * driver or simulator suffixes.
 * - 0x2. Nested directories, where file should be located are
 * created also. To denote the fact that nested dir(s) are
 * needed - one should provide their names in the 'name' parameter
 * (e.g. 'subdir0/subdir1.../subdirN/filename.c'). Directories
 * subdir0, subdir1, ... subdirN will be created.
 * - 0x3. Parameter 'loc' points out initial directory, where
 * file should be located. See definition of type 'localtion' for
 * more information.
 *
 * @e Example:\n\n
 * Consider that driver is <b>SHARED_CIBC</b>. This means that
 * <sys_lower> is "SharedCibc". Also consider that file name that
 * you want to generate is 'My.c' (@e name parameter):\n
 * type == DRIVER_FT. <filename> will be 'SharedCibcMyDrvr.c'\n
 * type == SIM_FT.	   <filename> will be 'SharedCibcMySim.c'\n
 * type == COMMON_FT. <filename> will be 'SharedCibcMy.c\n
 * type == EXACT_FT.  <filename> will be 'My.c'\n\n
 * Depending on 'loc' (location) param, file will be build in
 * different directories:\n
 * <I>LOC_MASTER   ~/\$(MASTER_DIR)/<filename></I>\n
 * <I>LOC_GENERAL  ~/\$(MASTER_DIR)/\$(GENERAL_DIR)/<filename></I>\n
 * <I>LOC_MODSRC   ~/\$(MASTER_DIR)/\$(DRIVER_DIRECTORY)/<filename></I>\n
 * <I>LOC_DRVR     ~/\$(MASTER_DIR)/\$(DRIVER_DIRECTORY)/driver/<filename></I>\n
 * <I>LOC_FCLTY    ~/\$(MASTER_DIR)/\$(DRIVER_DIRECTORY)/test/<filename></I>\n
 * <I>LOC_INCL     ~/\$(MASTER_DIR)/\$(DRIVER_DIRECTORY)/include/<filename></I>\n
 * <I>LOC_LIB      ~/\$(MASTER_DIR)/\$(DRIVER_DIRECTORY)/lib/<filename></I>\n
 * <I>LOC_INST     ~/\$(MASTER_DIR)/\$(DRIVER_DIRECTORY)/install/<filename></I>\n
 * <I>LOC_EXPLICIT ~/\$(name) - NOT IMPLEMENTED YET!!! TODO
 *
 * @note In case of <b>LOC_EXPLICIT</b> you should provide complete directory
 *       path in the <b>name</b> parameter. Afterwards, based on the
 *       <b>type</b> parameter filename will be build.
 *
 * @return FILE pointer - if succeed.
 * @return NULL         - if error occurs.
 */
FILE *OpenFile(FILETYPE type, location loc, char *name, ...)
{
	va_list ap;
	char inputName[MAXPATHLEN];
	char *suffix = NULL, *prefix = NULL, *ptr;
	int sz;
	char *curDir;
	char dirPath[4096];
	char *dpPtr = dirPath;
	char *nestDir;
	FILE *retFd;

	/* build-up the name first */
	va_start(ap, name);
	vsnprintf(inputName, sizeof(inputName), name, ap);
	va_end(ap);

	memset(dirPath, 0, sizeof(dirPath));

	if (type == EXACT_FT) {
		prefix = strdup(inputName);
	} else {
		if (!(suffix = rindex(inputName, '.')))
			return (NULL);	/* wrong name */

		sz = strlen(inputName) - strlen(suffix);
		prefix = (char *)calloc((sz + 1), sizeof(char));
		strncpy(prefix, inputName, sz);
	}

	switch (loc) {
	case LOC_MASTER:
		ptr = get_mid(NULL);
		break;
	case LOC_GENERAL:
		ptr = GetGeneralSrcDir();
		break;
	case LOC_MODSRC:
		ptr = get_msd(NULL);
		break;
	case LOC_DRVR:
		ptr = GetLocalDrvrDir();
		break;
	case LOC_FCLTY:
		ptr = GetLocalFcltyDir();
		break;
	case LOC_INCL:
		ptr = GetLocalInclDir();
		break;
	case LOC_LIB:
		ptr = GetLocalLibDir();
		break;
	case LOC_INST:
		ptr = GetLocalInstDir();
		break;
	case LOC_EXPLICIT:	/* TODO */
		break;
	default:
		return NULL;	/* error */
		break;
	}
	strncpy(dirPath, ptr, strlen(ptr));

	/* Check if nested directories are required */
	if ((ptr = strrchr(prefix, '/'))) {
		char tmpBuf[2048];
		*ptr = 0;
		nestDir = strdup(prefix); /* copy, to prevent corruption */
		ptr++;
		strcpy(tmpBuf, ptr);
		prefix = realloc(prefix, strlen(ptr) + 1);
		strcpy(prefix, tmpBuf);

		dpPtr += strlen(dirPath);
		*dpPtr = '/';
		dpPtr++;

		curDir = strtok(nestDir, "/");
		while (curDir) {	/* create new directories */
			strcpy(dpPtr, curDir);
			/* TODO. no buffer overflow check! */
			dpPtr += strlen(curDir);
			*dpPtr = '/';
			dpPtr++;
			if (MakeSafeDir(dirPath) == -DRIVER_GEN_BAD)
				return NULL;
			curDir = strtok(NULL, "/");
		}
		free(nestDir);
	}

	{
		struct stat status;
		char *final_fn = GenerateFilename(type, dirPath, prefix,
						  suffix, NULL);

		if (!stat(final_fn, &status))
			/* filename is already exist. We should check
			   access permission rights and change them if
			   needed (as we'll need 'w' access right) */
			chmod(final_fn, (status.st_mode | S_IWUSR | S_IWGRP));

		retFd = fopen(final_fn, "w");
		free(prefix);
	}

	return retFd;
}

/**
 * @brief Generate driver IOCTL numbers
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param headerFile   -- open header file descriptor
 *
 * @return void
 */
void BuildIoctlConsts(RegisterDef_t * registers, int numRegisters,
		      FILE * headerFile)
{
	int cntr;
	int constCounter = 0;	/* driver ioctl index */

	TranslationReset();

	/* Set first allowed ioctl number for the driver ioctl numbers
	   Keep the name in accordance with
	   templates/common/header/srvFoot.h! */
	TranslationSetFancyString("_FIRST__IOCTL_");

	for (cntr = 0; cntr < numRegisters; cntr++) {
		switch (registers[cntr].rar) {
		case AMRD:	/* 'r' */
			TranslationSetPlainNum(constCounter++);
			TranslationSetIoctlConst("GET",
						 registers[cntr].upperName);
			Translate(headerFile, "common", "header/ioctlConst.h");
			break;
		case AMWR:	/* 'w' */
			TranslationSetPlainNum(constCounter++);
			TranslationSetIoctlConst("GET_HISTORY",
						 registers[cntr].upperName);
			Translate(headerFile, "common", "header/ioctlConst.h");

			TranslationSetPlainNum(constCounter++);
			TranslationSetIoctlConst("SET",
						 registers[cntr].upperName);
			Translate(headerFile, "common", "header/ioctlConst.h");
			break;
		case (AMWR | AMRD):	/* 'rw' */
		case AMEX:	/* 'e'  */
			TranslationSetPlainNum(constCounter++);
			TranslationSetIoctlConst("GET",
						 registers[cntr].upperName);
			Translate(headerFile, "common", "header/ioctlConst.h");

			TranslationSetPlainNum(constCounter++);
			TranslationSetIoctlConst("SET",
						 registers[cntr].upperName);
			Translate(headerFile, "common", "header/ioctlConst.h");
			break;
		default:
			FATAL_ERR();
			break;
		}
	}			/* end of 'for' */

	/* set first allowed user-defined ioctl number */
	Translate(headerFile, "common", "dummyTemplates/1NewLine");
	TranslationSetComment("First allowed number for user-defined ioctl");
	Translate(headerFile, "common", "dummyTemplates/comment");

	TranslationSetPlainNum(constCounter);
	TranslationSetIoctlConst("FIRST_USR", "IOCTL");
	Translate(headerFile, "common", "header/ioctlConst.h");
}

/**
 * @brief Generate module topology.
 *
 * @param type         -- driver (DRIVER_FT) or simulator (SIM_FT)
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param headerFile   -- open header file descriptor
 *
 * Generate a history structure to store values that were written to writeonly
 * registers, an extraneous structure for extra driver variables, defined by
 * the user and a structure for each block that has been defined in the design.
 *
 * @return void
 */
void BuildCommonBlocks(int type, RegisterDef_t * registers, int numRegisters,
		       FILE * headerFile)
{
	int i;
	int theGap;	  /* memory gap size betveen the registers */
	int endOfBlock;
	int prevFreeAddr; /* for memory gap computing */

	TranslationReset();

  /*--------------------------------------------------------------
    00. Generate the driver's writeonly structure.
    --------------------------------------------------------------*/
	TranslationSetFreeFormat
	    ("keeps last written value of the 'write only' registers");
	TranslationSetDummyString("");
	Translate(headerFile, "common", "header/structHead.h");
	for (i = 0; i < numRegisters; i++) {
		if (registers[i].rar == AMWR) {	/* Is the register writeonly? */
			TranslationSetRegDepth(registers[i].depth);
			TranslationSetRegType(registers[i].size);
			TranslationSetRegName(registers[i].name);
			if (registers[i].depth <= 1) {	/* one register */
				TranslationSetHexNum(registers[i].offset);
				TranslationSetFreeFormat("last written value");
				Translate(headerFile, "common",
					  "header/recordDefScalar.h");
			} else {	/* massive */
				TranslationSetFreeFormat("last written values");
				Translate(headerFile, "common",
					  "header/recordDefArray.h");
			}
		}
	}
	Translate(headerFile, "common", "header/endWriteonly.h");

  /*-----------------------------------------------------------------
    01. Generate the driver's extraneous variables structure.
    -----------------------------------------------------------------*/
	TranslationSetFreeFormat("user-defined extraneous registers");
	TranslationSetDummyString("");
	Translate(headerFile, "common", "header/structHead.h");
	for (i = 0; i < numRegisters; i++) {
		if (registers[i].rar == AMEX) {	/* is it is extraneous? */
			TranslationSetRegDepth(registers[i].depth);
			TranslationSetRegType(registers[i].size);
			TranslationSetRegName(registers[i].name);
			if (registers[i].depth <= 1) {	/* one register */
				TranslationSetFreeFormat("user-def reg");
				Translate(headerFile, "common",
					  "header/recordDefScalar.h");
			} else {	/* massive */
				TranslationSetFreeFormat("user-def regs");
				Translate(headerFile, "common",
					  "header/recordDefArray.h");
			}
		}
	}
	Translate(headerFile, "common", "header/endExtraneous.h");

  /*------------------------------------------------------
    02. Generate device memory map topology structures.
    ------------------------------------------------------*/
	endOfBlock = TRUE;
	for (i = 0; i < numRegisters; i++) {
		if (registers[i].rar != AMEX) {
			/* exclude extraneous registers */
			if (endOfBlock) {
				/* If we just ended a block then we must be
				   starting a new one */
				TranslationSetFreeFormat
				    ("Blk[#%d]@addr[#%d] Offs 0x%x."
				     " Sz %d bytes. %d reg(s). %d gap(s)",
				     registers[i].blockP->blockID,
				     registers[i].blockP->blkBaseAddr,
				     registers[i].blockP->offset,
				     (type == SIM_FT) ?
				     registers[i].blockP->blksz_sim :
				     registers[i].blockP->blksz_drvr,
				     registers[i].blockP->reg_am,
				     calc_block_gap_amount(&registers[i]));
				TranslationSetDummyString("volatile");
				Translate(headerFile, "common",
					  "header/structHead.h");
			}

			/*
			   If this is the first register in a block, check that
			   it's offset is 0. If not then insert some dummy
			   records into the structure to take up the spare
			   space.
			 */

			if (type == DRIVER_FT) {
				/* we need to set gaps only for real driver */
				if (endOfBlock) {
					/* If we just ended a block then we
					   must be starting a new one */
					prevFreeAddr = 0;
					theGap = registers[i].offset;
				} else {
					prevFreeAddr =
					    registers[i - 1].offset +
					    ((registers[i - 1].depth) ?
					     registers[i - 1].depth :
					     1) * registers[i - 1].regSize;
					theGap =
					    registers[i].offset - prevFreeAddr;
				}

				if (theGap > 0) {
					TranslationSetFancyNum(i);
					TranslationSetRegDepth(theGap);
					TranslationSetFreeFormat("0x%x - 0x%x",
								 prevFreeAddr,
								 registers[i].
								 offset);
					Translate(headerFile, "common",
						  "header/dummyRecord.h");
				}
			}

			/* Produce a record in the structure for our register */
			TranslationSetRegType(registers[i].size);
			TranslationSetRegName(registers[i].name);
			TranslationSetRegDepth(registers[i].depth);
			if (registers[i].depth <= 1) {	/* one register */
				TranslationSetFreeFormat("0x%x",
							 registers[i].offset);
				Translate(headerFile, "common",
					  "header/recordDefScalar.h");
			} else { /* massive */
				/* register offset (i.e. Base Address) */
				unsigned int regBA = registers[i].offset;

				/* last valid massive address (i.e. address
				   of the last register in massive) */
				unsigned int regLA = regBA +
					(registers[i].depth - 1) *
					registers[i].regSize;
				TranslationSetFreeFormat("0x%x - 0x%x", regBA,
							 regLA);
				Translate(headerFile, "common",
					  "header/recordDefArray.h");
			}

			/* Determine whether we should terminate this
			   structure */
			if (i == numRegisters - 1)
				endOfBlock = TRUE;
			else if (registers[i].blockP->blockID !=
				 registers[i + 1].blockP->blockID)
				endOfBlock = TRUE;
			else
				endOfBlock = FALSE;

			if (endOfBlock) {
				TranslationSetFancyNum(registers[i].blockP->
						       blockID);
				if (type == DRIVER_FT)
					Translate(headerFile, "common",
						  "header/endDrvrBlock.h");
				else
					Translate(headerFile, "common",
						  "header/endSimBlock.h");
			}
		}		/* end of 'rar != AMEX' */
	}			/* end of for */
}

/**
 * @brief Generate @b DEPRECATED @e IoctlAccess library.
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param blocks       -- block description
 * @param numBlocks    -- block amount
 *
 * This library enables user to use old-style ioctl calls to access the driver.
 *
 * @return void
 */
void BuildIoctlLibrary(RegisterDef_t * registers, int numRegisters,
		       BlockDef_t * blocks, int numBlocks)
{
	int cntr;
	char accessType[8];
	FILE *libraryFile;
	FILE *headerFile;
	char *bus = TranslationGetBus();	/* 'VME' or 'DRM' */

	TranslationReset();
	libraryFile = OpenFile(COMMON_FT, LOC_LIB, "IoctlAccess.c");
	headerFile = OpenFile(COMMON_FT, LOC_INCL, "IoctlAccess.h");

	Translate(libraryFile, bus, "lib/IoctlAccess/head.c");
	Translate(libraryFile, "common", "lib/ioctlAccess/enableAccess.c");

	TranslationSetFreeFormat("%s", DG_INC_DIR);
	Translate(headerFile, bus, "lib/IoctlAccess/headerHead.h");
	Translate(headerFile, "common", "lib/ioctlAccess/enableAccess.h");

	/* Generate the get and set functions for each register and write to
	   the library source file, and generate function prototypes for each
	   get and set function and write to the library's header file. */
	for (cntr = 0; cntr < numRegisters; cntr++) {
		TranslationSetRegName(registers[cntr].name);
		TranslationSetRegType(registers[cntr].size);
		TranslationSetRegDepth(registers[cntr].depth);

		if (registers[cntr].depth <= 1)
			strcpy(accessType, "Scalar");
		else
			strcpy(accessType, "Array");

		switch (registers[cntr].rar) {
		case AMRD:	/* 'r'  */
			TranslationSetIoctlConst("GET",
						 registers[cntr].upperName);
			Translate(libraryFile, "common",
				  "lib/ioctlAccess/get%s.c", accessType);
			Translate(headerFile, "common",
				  "lib/ioctlAccess/get%s.h", accessType);
			break;
		case AMWR:	/* 'w'  */
			TranslationSetIoctlConst("GET_HISTORY",
						 registers[cntr].upperName);
			Translate(libraryFile, "common",
				  "lib/ioctlAccess/getLastHisWo%s.c",
				  accessType);
			Translate(headerFile, "common",
				  "lib/ioctlAccess/getLastHisWo%s.h",
				  accessType);

			TranslationSetIoctlConst("SET",
						 registers[cntr].upperName);
			Translate(libraryFile, "common",
				  "lib/ioctlAccess/set%s.c", accessType);
			Translate(headerFile, "common",
				  "lib/ioctlAccess/set%s.h", accessType);
			break;
		case (AMWR | AMRD):	/* 'rw' */
		case AMEX:	/* 'e'  */
			TranslationSetIoctlConst("GET",
						 registers[cntr].upperName);
			Translate(libraryFile, "common",
				  "lib/ioctlAccess/get%s.c", accessType);
			Translate(headerFile, "common",
				  "lib/ioctlAccess/get%s.h", accessType);

			TranslationSetIoctlConst("SET",
						 registers[cntr].upperName);
			Translate(libraryFile, "common",
				  "lib/ioctlAccess/set%s.c", accessType);
			Translate(headerFile, "common",
				  "lib/ioctlAccess/set%s.h", accessType);
			break;
		default:
			FATAL_ERR();
			break;
		}
	}
	Translate(headerFile, "common", "lib/ioctlAccess/headerFoot.h");

	fclose(libraryFile);
	fclose(headerFile);
}

/**
 * @brief Builds test program that user will use to test driver/simulator.
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param blocks       -- block description
 * @param numBlocks    -- block amount
 *
 * @return void
 */
void BuildTestProgram(RegisterDef_t * registers, int numRegisters,
		      BlockDef_t * blocks, int numBlocks)
{
	FILE *tstFileC = OpenFile(COMMON_FT, LOC_FCLTY, "Test.c");
	char *bus = TranslationGetBus();	/* 'VME' or 'DRM' */
	unsigned int freeEntry = TST_PROG_FIRST_FREE_ENTRY;	/*  */
	int count;
	int i;

	TranslationReset();
	/* Generate the needed block size consts and register
	   description strings */
	BuildTestProgHFile(registers, numRegisters);
	BuildTestProgConsts(registers, numRegisters, tstFileC);

	/* Generate the remainder of the head and the start of main */
	Translate(tstFileC, "common", "testProg/common/testMainBegin.c");
	Translate(tstFileC, "common", "testProg/testMainMenu.c");

	/* For each block that's been defined, generate an entry in the
	   program's main menu. */
	for (count = freeEntry, i = 0; i < numBlocks; i++, count++) {
		TranslationSetFancyNum(count);
		TranslationSetPlainNum(blocks[i].blockID);
		Translate(tstFileC, "common",
			  "testProg/common/mainMenuEntry.c");
	}

	/* Add the PCI config menu item (in case of DRM usage),
	   and the other common options. */
	Translate(tstFileC, bus, "testProg/mainMenuFoot.c");
	Translate(tstFileC, "common", "testProg/mainCaseHead.c");

	/* Generate the case statements for each of the main menu entries. */
	for (count = freeEntry, i = 0; i < numBlocks; i++, count++) {
		TranslationSetFancyNum(blocks[i].blockID);
		TranslationSetPlainNum(count);
		Translate(tstFileC, "common", "testProg/common/case.c");
	}

	TranslationSetPlainNum(count);
	Translate(tstFileC, bus, "testProg/mainCaseFoot.c");

	BuildTestProgSubMenus(registers, blocks, numRegisters, tstFileC);

	fclose(tstFileC);
}

/**
 * @brief Generates the makefiles that are used to build the generated
 *        suite of code.
 *
 * @param none
 *
 * @return void
 */
void BuildMakefiles()
{
	FILE *mkfd;
	char dirPath[MAXPATHLEN] = { 0 };

	TranslationReset();

	/* Base Makefile */
	mkfd = OpenFile(EXACT_FT, LOC_MODSRC, "Makefile.base");
	Translate(mkfd, "common", "makefiles/Makefile.base");
	fclose(mkfd);

	/* Specific Makefile */
	mkfd = OpenFile(EXACT_FT, LOC_MODSRC, "Makefile.specific");
	Translate(mkfd, "common", "makefiles/Makefile.specific");
	fclose(mkfd);

	/* "Which dirs to compile" Makefile */
	mkfd = OpenFile(EXACT_FT, LOC_MODSRC, "Makefile");
	Translate(mkfd, "common", "makefiles/Makefile");
	fclose(mkfd);

	/* delivery */
	mkfd = OpenFile(EXACT_FT, LOC_MODSRC, "deliver.mk");
	Translate(mkfd, "common", "makefiles/deliver.mk");
	fclose(mkfd);

	/* Library Makefile */
	mkfd = OpenFile(EXACT_FT, LOC_LIB, "Makefile");
	Translate(mkfd, "common", "makefiles/libModule.mk");
	fclose(mkfd);

	/* Driver Makefiles */
	mkfd = OpenFile(EXACT_FT, LOC_DRVR, "makefiles/drvr-linux.mk");
	Translate(mkfd, "common", "makefiles/drvr-linux.mk");
	fclose(mkfd);

	mkfd = OpenFile(EXACT_FT, LOC_DRVR, "makefiles/drvr-lynx.mk");
	Translate(mkfd, "common", "makefiles/drvr-lynx.mk");
	fclose(mkfd);

	mkfd = OpenFile(EXACT_FT, LOC_DRVR, "makefiles/Kbuild");
	Translate(mkfd, "common", "makefiles/Kbuild");
	fclose(mkfd);

	mkfd = OpenFile(EXACT_FT, LOC_DRVR, "makefiles/drvr-paramount.mk");
	Translate(mkfd, "common", "makefiles/drvr-paramount.mk");
	fclose(mkfd);

	mkfd = OpenFile(EXACT_FT, LOC_DRVR, "Makefile");
	Translate(mkfd, "common", "makefiles/Makefile.drvr");
	fclose(mkfd);

	mkfd = OpenFile(EXACT_FT, LOC_DRVR, "compiledrvr");
	Translate(mkfd, "common", "makefiles/compiledrvr");
	fchmod(fileno(mkfd),
	       S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP | S_IROTH);
	fclose(mkfd);

	/* we need this symlink for Kbuild to work */
	sprintf(dirPath, "%s%s", GetLocalDrvrDir(), "/Kbuild");
	symlink("makefiles/Kbuild", dirPath);

	/* Facility (test program) Makefile */
	mkfd = OpenFile(EXACT_FT, LOC_FCLTY, "Makefile");
	TranslationSetFreeFormat("%s", DAL_DIR);
	Translate(mkfd, "common", "makefiles/test.mk");
	fclose(mkfd);

	/* Include Makefile */
	mkfd = OpenFile(EXACT_FT, LOC_INCL, "Makefile");
	Translate(mkfd, "common", "makefiles/include.mk");
	fclose(mkfd);

	/* Install program entry point Makefile */
	mkfd = OpenFile(EXACT_FT, LOC_INST, "Makefile");
	TranslationSetFreeFormat("%s", DAL_DIR);
	Translate(mkfd, "common", "makefiles/installso.mk");
	fclose(mkfd);
}

/**
 * @brief Create (if it was not done yet) main installation directory, where
 *        newly created drivers are located.
 *
 * @param  mdn -- Module Description
 *                If NULL, current master directory name will be returned.
 *
 * Will be created only *once*. Creates all general-purpose files that should
 * present in the master diretory (user-part source copy utility for example).
 *
 * If directory is created already, than no action perfomed exept of returning
 * the directory name.
 *
 * @return Main Installation Directory name
 */
char *get_mid(struct mdescr *md)
{
	static char *mainDir = NULL; /* container */
	char *cwd = ""; /* current working directory */
	rstat ret;

	if (mainDir) /* master dir was already created */
		return mainDir;

	/* master dir is not yet created */
	mainDir = (char *)calloc((strlen(cwd) + strlen(md->mdn) + 2), sizeof(char));
	sprintf(mainDir, "%s%s", cwd, md->mdn);

	ret = MakeSafeDir(mainDir);
	if (ret == -DRIVER_GEN_BAD)
		exit(EXIT_FAILURE);	/* 1 */

	/* make symlinks to the general utilities
	   (script for copy user-part src code) */
	{
		struct stat status;
		char loc_link[128];	/* local utility symlink */
		char glob_link[128];	/* global utility symlink */
		char gen_dir[128];

		sprintf(loc_link, "%s/%s", mainDir, UTIL_CP_USR_PT_LOC);
		sprintf(glob_link, "%s/%s", mainDir, UTIL_CP_USR_PT_GLOB);
		sprintf(gen_dir, "%s/%s", mainDir, GENERAL_DIR);

		if (lstat(glob_link, &status))	/* global symlink doesn't
						   exist */
			symlink(GenerateFilename(EXACT_FT, DG_DIR_PATH,
				 "scripts/" UTIL_CP_USR_PT_GLOB, NULL, NULL),
				glob_link);

		if (!stat(gen_dir, &status))
			/* we have 'general' dir locally, so create local
			   symlink */
			if (lstat(loc_link, &status))	/* local symlink doesn't
							   exist */
				symlink(GenerateFilename
					(EXACT_FT, GENERAL_DIR,
					 UTIL_CP_USR_PT_GLOB, NULL, NULL),
					loc_link);
	}

	return (mainDir);
}

/**
 * @brief Get Module Source code Directory.
 *
 * @param md -- module description
 *
 * Creates directory, where Driver/Simulator source code of a currently
 * processed module will be located. If directory is already created, than no
 * action perfomed exept of returning the directory name.
 *
 * @return directory name for driver src code.
 */
char *get_msd(struct mdescr *md)
{
	static char *sourceDir = NULL; /* container */
	time_t theTime;
	struct tm *tmPtr;
	char timeStr[MAX_STR];
	char *tFmt;

	if (sourceDir) /* driver source dir is already created */
		return sourceDir;

	/* Driver directory is not created yet */
	time(&theTime);
	tmPtr = localtime(&theTime);

	/* obfuscate 'gcc' to bypass warnings about format string */
	tFmt = "%d%b%G.%Hh%Mm%Ss";

	strftime(timeStr, sizeof(timeStr), tFmt, tmPtr);

	sourceDir = (char *)calloc(MAXPATHLEN, sizeof(char));
	if (md->isgit)
		snprintf(sourceDir, MAXPATHLEN, "%s/%s",
			 get_mid(NULL), TranslationGetModName());
	else
		snprintf(sourceDir, MAXPATHLEN, "%s/%s.%s",
			 get_mid(NULL), TranslationGetModName(), timeStr);

	if (MakeSafeDir(sourceDir) == -DRIVER_GEN_BAD)
		exit(EXIT_FAILURE);	/* 1 */

	return sourceDir;
}

/**
 * @brief Build the driver's Ioctl routine.
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param driverFile   -- open file descriptor
 */
void BuildDrvrSimIoctl(RegisterDef_t * registers, int numRegisters,
		       FILE * driverFile)
{
	int cntr;

	TranslationReset();
	Translate(driverFile, "common", "driver/ioctl/head.c");

	/* first, build 'service' registers ioctl */
	TranslationSetBaseAddr("");
	for (cntr = 0; cntr < GetSrvRegNum(); cntr++) {
		TranslationSetRegName(srv_ioctl[cntr].name);
		switch (srv_ioctl[cntr].rar) {
		case AMRD:	/* 'r'  */
			TranslationSetIoctlConst("GET", srv_ioctl[cntr].name);
			TranslationSetRegType("get");
			Translate(driverFile, "common",
				  "driver/ioctl/getSetReg.c");
			break;
		case AMWR:	/* 'w'  */
			TranslationSetIoctlConst("SET", srv_ioctl[cntr].name);
			TranslationSetRegType("set");
			Translate(driverFile, "common",
				  "driver/ioctl/getSetReg.c");
			break;
		case (AMWR | AMRD):	/* 'rw' */
			TranslationSetIoctlConst("GET", srv_ioctl[cntr].name);
			TranslationSetRegType("get");
			Translate(driverFile, "common",
				  "driver/ioctl/getSetReg.c");

			TranslationSetIoctlConst("SET", srv_ioctl[cntr].name);
			TranslationSetRegType("set");
			Translate(driverFile, "common",
				  "driver/ioctl/getSetReg.c");
			break;
		default:
			TranslationSetIoctlConst("", srv_ioctl[cntr].name);
			TranslationSetRegType("srv_func");
			Translate(driverFile, "common",
				  "driver/ioctl/getSetReg.c");
			break;
		}
	}

	/* now build-up r/w operations for the normal registers */
	for (cntr = 0; cntr < numRegisters; cntr++) {
		TranslationSetRegName(registers[cntr].name);
		switch (registers[cntr].rar) {
		case AMRD:	/* 'r' read-only reg */
			TranslationSetIoctlConst("GET",
						 registers[cntr].upperName);
			TranslationSetBaseAddr("");
			TranslationSetRegType("get");
			Translate(driverFile, "common",
				  "driver/ioctl/getSetReg.c");
			break;
		case AMWR:	/* 'w' write-only reg */
			TranslationSetIoctlConst("GET_HISTORY",
						 registers[cntr].upperName);
			TranslationSetBaseAddr("wo_");
			TranslationSetRegType("get");
			Translate(driverFile, "common",
				  "driver/ioctl/getSetReg.c");

			TranslationSetIoctlConst("SET",
						 registers[cntr].upperName);
			TranslationSetBaseAddr("");
			TranslationSetRegType("set");
			Translate(driverFile, "common",
				  "driver/ioctl/getSetReg.c");
			break;
		case (AMWR | AMRD):	/* 'rw' read/write reg */
			TranslationSetIoctlConst("GET",
						 registers[cntr].upperName);
			TranslationSetBaseAddr("");
			TranslationSetRegType("get");
			Translate(driverFile, "common",
				  "driver/ioctl/getSetReg.c");

			TranslationSetIoctlConst("SET",
						 registers[cntr].upperName);
			TranslationSetBaseAddr("");
			TranslationSetRegType("set");
			Translate(driverFile, "common",
				  "driver/ioctl/getSetReg.c");
			break;
		case AMEX:	/* 'e' extraneous (i.e. w/r) reg */
			TranslationSetIoctlConst("GET",
						 registers[cntr].upperName);
			TranslationSetBaseAddr("ex_");
			TranslationSetRegType("get");
			Translate(driverFile, "common",
				  "driver/ioctl/getSetReg.c");

			TranslationSetIoctlConst("SET",
						 registers[cntr].upperName);
			TranslationSetBaseAddr("ex_");
			TranslationSetRegType("set");
			Translate(driverFile, "common",
				  "driver/ioctl/getSetReg.c");
			break;
		default:
			FATAL_ERR();
			break;
		}
	}
	Translate(driverFile, "common", "driver/ioctl/foot.c");
}

/**
 * @brief Generate the get and set (or r/w) functions
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param vmeInfo      -- @e vme description
 *
 * Header file with their declaration is also created. This functions are used
 * by the driver ioctl routine to access hardware registers. @b CH in the name
 * stands for @e c source file and @e h header file.
 *
 * @return void
 */
void BuildGetSetRegCH(RegisterDef_t * registers, int numRegisters,
		      VmeInfo_t * vmeInfo)
{
	FILE *c_fd = OpenFile(COMMON_FT, LOC_DRVR, "GetSetReg.inc.c");
	FILE *h_fd = OpenFile(COMMON_FT, LOC_INCL, "GetSetReg.h");
	char *bus = TranslationGetBus();	/* 'VME' or 'DRM' */
	char accessType[MIN_STR], *strDps, singularity[16], *rsz;
	int cntr;
	long dps;		/* address space data port size */
	char singular_comment_str[256];
	char *cmt =
		" These functions are used to deliver register values directly"
		" to the user\n  space.\n\n"
		"  API is the following:\n"
		"  1 param -- statics table\n\n"
		"  2 param -- ioctl argument in predefined format:\n"
		"             Massive of 3 elements, each is 4 bytes long.\n"
		"             [0] - user-space address\n"
		"             [1] - number of elements to r/w\n"
		"             [2] - element index, starting from zero\n\n"
		"             In case of service registers -- ioctl arguments"
		" can vary.\n"
		"             Their amount depends on specific ioctl number.\n"
		"             See service routines (those are with __SRV__"
		" subword)\n"
		"             for more details on parameter amount.\n\n"
		"             For example, if this is a repetitive r/w"
		" request\n"
		"             (ioctl number is SRV__REP_REG_RW) then we should"
		" have 4 arguments,\n"
		"             that are packed as follows:\n\n"
		"             [0] -- ioctl number\n"
		"             [1] -- user-space address\n"
		"             [2] -- number of elements to r/w\n"
		"             [3] -- element index, starting from zero\n\n"
		"  3 param -- check r/w bounds (1 - yes, 0 - no)\n"
		"             valid only in case of Lynx\n"
		"  4 param -- repeatedly read register (1 - yes, 0 - no)\n\n\n"
		"  Bear in mind, that r/w operation results goes diretly to"
		" the user space.\n"
		"  If you want to operate on the HW registers inside the"
		" driver -- use\n"
		"  low-level port operation functions from"
		" port_ops_[linux/lynx].h like:\n"
		"  __inb      -- read a byte from a port\n"
		"  __inw      -- read a word from a port\n"
		"  __in       -- lread a long from a port\n"
		"  __outb     -- write a byte to a port\n"
		"  __outw     -- write a word to a port\n"
		"  __outl     -- write a long to a port\n"
		"  __rep_inb  -- read multiple bytes from a port into a"
		" buffer\n"
		"  __rep_inw  -- read multiple words from a port into a"
		" buffer\n"
		"  __rep_inl  -- read multiple longs from a port into a"
		" buffer\n"
		"  __rep_outb -- write multiple bytes to a port from a buffer\n"
		"  __rep_outw -- write multiple words to a port from a buffer\n"
		"  __rep_outl -- write multiple longs to a port from a"
		" buffer\n\n"
		"  These functions are used to r/w HW registers inside the"
		" driver.\n"
		"  Never access registers directly. Use this function to do"
		" this.";

	Translate(c_fd, "common", "driver/getSetReg/funcDefs.c");

	/* ----------> first handle service registers <------------------- */

	/* C file */
	TranslationSetIntNum(sizeof(time_t));
	TranslationSetRegType(srv_ioctl[3].type); /* size of DAL_CONSISTENT
						     register */
	Translate(c_fd, bus, "driver/getSetReg/srvRegOps.c");

	/* header file starts */
	TranslationSetFancyString("GET_SET_REG");
	Translate(h_fd, "common", "header/headerStartEnd/hsModNmStr.h");
	TranslationSetComment(cmt);

	Translate(h_fd, "common", "dummyTemplates/comment-multiline");

	/* srv regs functions declaration */
	Translate(h_fd, "common", "dummyTemplates/1NewLine");
	TranslationSetComment("Service register operations");
	Translate(h_fd, "common", "dummyTemplates/comment");
	TranslationSetBaseAddr("");
	for (cntr = 0; cntr < GetSrvRegNum(); cntr++) {
		TranslationSetRegName(srv_ioctl[cntr].name);
		if (srv_ioctl[cntr].rar & AMRD) { /* get register function */
			TranslationSetRegType("get");
			Translate(h_fd, "common",
				  "driver/getSetReg/declGetSet.h");
		}
		if (srv_ioctl[cntr].rar & AMWR) { /* set register function */
			TranslationSetRegType("set");
			Translate(h_fd, "common",
				  "driver/getSetReg/declGetSet.h");
		}
	}

	/* ---------------> now handle all module registers <------------- */
	for (cntr = 0; cntr < numRegisters; cntr++) {
		TranslationSetRegName(registers[cntr].name);
		TranslationSetRegType(registers[cntr].size);
		TranslationSetRegComm(registers[cntr].comment);
		TranslationSetRegLoop(registers[cntr].timeLoop);
		TranslationSetFreeFormat("block%02d",
					 registers[cntr].blockP->blockID);

		switch (registers[cntr].regSize) {
		case 1:
			rsz = "b";
			TranslationSetRegSize(8);
			break;
		case 2:
			rsz = "w";
			TranslationSetRegSize(16);
			break;
		case 4:
			rsz = "l";
			TranslationSetRegSize(32);
			break;
		case 8:
			rsz = "ll";
			TranslationSetRegSize(64);
			break;
		default:
			FATAL_ERR();
			break;
		}

		TranslationSetChar(*rsz); /* set read/write port
					     operation suffix */

		/* manage dataport size */
		if (registers[cntr].blockP->blkBaseAddr == 1)
			dps = vmeInfo->addr1.dpSize;
		else
			dps = vmeInfo->addr2.dpSize;

		/* set max allowed r/w size */
		switch (dps) {
		case 8:
			strDps = "char";
			rsz = "b";
			break;
		case 16:
			strDps = "short";
			rsz = "w";
			break;
		case 32:
			strDps = "long";
			rsz = "l";
			break;
		case 64:
			strDps = "long long";
			rsz = "ll";
			break;
		default:
			fprintf(stderr, "%s Data Port Size is NOT provided in"
				" the DB.\n", ERROR_MSG);
			FATAL_ERR();
			break;
		}

		/* singularity comment string format */
#define COMMENT_SINGULAR \
" *\n * @warning Element size of the current register is %d bytes,\n *          which is @b BIGGER than Addr[%d] DataPort Size (%d bytes).\n *          Will be read/written in parts!\n *"

		/* if register size is bigger than Data Port Size, then
		   register is considered to be singular and will be
		   read/written in parts. In this case we should adjust
		   register type and r/w port suffix to the max allowed
		   (i.e. the data port size) */
		if (dps / 8 /*convert to bytes */  < registers[cntr].regSize) {
			/* reg type (char, short, long) */
			TranslationSetRegType(strDps);
			/* read/write port operation suffix */
			TranslationSetChar(*rsz);
		}

		/* if register size is bigger than MAX data width */
		if (registers[cntr].rar & AMEX) {
			/* ignore fake external registers */
			strcpy(singularity, "Extraneous");
			TranslationSetComment(" *");
		} else {
			if (registers[cntr].regSize * 8 > dps) {
				/* build-up singularitiy comment string
				   for the user */
				snprintf(singular_comment_str,
					 sizeof(singular_comment_str),
					 COMMENT_SINGULAR,
					 registers[cntr].regSize,
					 registers[cntr].blockP->blkBaseAddr,
					 (int)dps / 8);
				strcpy(singularity, "Singular");
				TranslationSetComment(singular_comment_str);
			} else {
				strcpy(singularity, "Normal");
				TranslationSetComment(" *");
			}
		}

		TranslationSetFancyString(singularity);

		/* if this register is a scalar, fifo or an array */
		if (registers[cntr].depth <= 1)
			strcpy(accessType, "scalar");
		else
			strcpy(accessType, "array");

		/* generate get/set methods depending on the register's
		   description */
		switch (registers[cntr].rar) {
		case AMWR:	/* 'w' write-only reg */
			/* c file */
			Translate(c_fd, "common",
				  "driver/getSetReg/func_comment.c");
			Translate(c_fd, "common", "driver/getSetReg/wo_%s.c",
				  accessType);

			/* header file */
			Translate(h_fd, "common", "dummyTemplates/1NewLine");
			TranslationSetComment(registers[cntr].comment);
			Translate(h_fd, "common", "dummyTemplates/comment");
			TranslationSetBaseAddr("wo_");
			TranslationSetRegType("get");
			Translate(h_fd, "common",
				  "driver/getSetReg/declGetSet.h");
			TranslationSetBaseAddr("");
			TranslationSetRegType("set");
			Translate(h_fd, "common",
				  "driver/getSetReg/declGetSet.h");
			break;
		case AMRD:	/* 'r' read-only reg */
			/* c file */
			Translate(c_fd, "common",
				  "driver/getSetReg/func_comment.c");
			Translate(c_fd, "common", "driver/getSetReg/ro_%s.c",
				  accessType);

			/* header file */
			Translate(h_fd, "common", "dummyTemplates/1NewLine");
			TranslationSetComment(registers[cntr].comment);
			Translate(h_fd, "common", "dummyTemplates/comment");
			TranslationSetBaseAddr("");
			TranslationSetRegType("get");
			Translate(h_fd, "common",
				  "driver/getSetReg/declGetSet.h");
			break;
		case (AMWR | AMRD):	/* 'wr' read/write reg */
			/* c file */
			Translate(c_fd, "common",
				  "driver/getSetReg/func_comment.c");
			Translate(c_fd, "common", "driver/getSetReg/rw_%s.c",
				  accessType);

			/* header file */
			Translate(h_fd, "common", "dummyTemplates/1NewLine");
			TranslationSetComment(registers[cntr].comment);
			Translate(h_fd, "common", "dummyTemplates/comment");
			TranslationSetBaseAddr("");
			TranslationSetRegType("get");
			Translate(h_fd, "common",
				  "driver/getSetReg/declGetSet.h");
			TranslationSetRegType("set");
			Translate(h_fd, "common",
				  "driver/getSetReg/declGetSet.h");
			break;
		case AMEX: /* 'e' extraneous reg. They are always r/w */
			/* c file */
			Translate(c_fd, "common",
				  "driver/getSetReg/func_comment.c");
			Translate(c_fd, "common", "driver/getSetReg/ex_%s.c",
				  accessType);

			/* header file */
			Translate(h_fd, "common", "dummyTemplates/1NewLine");
			TranslationSetComment(registers[cntr].comment);
			Translate(h_fd, "common", "dummyTemplates/comment");
			TranslationSetBaseAddr("ex_");
			TranslationSetRegType("get");
			Translate(h_fd, "common",
				  "driver/getSetReg/declGetSet.h");
			TranslationSetRegType("set");
			Translate(h_fd, "common",
				  "driver/getSetReg/declGetSet.h");
			break;
		default:
			break;
		}
	} /* end of 'for(cntr = 0; cntr < numRegisters; cntr++)' */

	/* header ends */
	TranslationSetFancyString("GET_SET_REG");
	Translate(h_fd, "common", "header/headerStartEnd/heModNmStr.h");

	fclose(c_fd);
	fclose(h_fd);
}

/**
 * @brief Generates src code and header files that are common to all
 *        Module drivers.
 *
 * @param none
 *
 * It includes header files, libraries and installation/deinstallation
 * programs.
 *
 * @return void
 */
void BuildGeneralCode()
{
	char *bus = TranslationGetBus();	/* 'VME' or 'DRM' */

	printf("Building general header files for '%s' modules.\n", bus);
	BuildGeneralHeaderFiles();

	printf("Building general libraries for '%s' modules.\n", bus);
	BuildGeneralLibraries();

	printf("Building general install program for '%s' modules.\n", bus);
	BuildGeneralInstallProgram();

	printf("Building general Makefiles for '%s' modules.\n", bus);
	BuildGeneralMakefiles();

	/* instantiate scripts */
	printf("Instantiate supplementary scripts.\n");
	BuildGeneralScripts();
}

/**
 * @brief Generates files and directories, intended for user intervention.
 *
 * @param none
 *
 * These are so-called <E>user entry points</E>. This is a user part of the
 * driver. User is allowed to modify this code if he wants to implement his
 * own code. There are several entry points. They are in the driver/simulator,
 * in the test program, in the library and in the installation program.
 *
 * @return void
 */
void BuildUserPartDrvrCode()
{
	FILE *file;
	char *system;

	system = TranslationGetModName();

	/* 0x0 build user entry points and header files for the driver */
	/* c-file for real driver */
	//printf("Building %s driver's user-defined entry poins.\n", system);
	TranslationSetDriverType("DRVR");
	TranslationSetFancyString("Drvr");
	TranslationSetChar('D');
	file = OpenFile(DRIVER_FT, LOC_DRVR, "%s/%s.c", LOCAL_USER_DIR,
			USER_SUBWORD_LEXEME);
	Translate(file, "common", "driver/userPart/uepHead.c");
	Translate(file, "common", "driver/isr.c");	/* generate empty ISR */
	Translate(file, "common", "driver/userPart/uepFoot.c");
	fclose(file);

	/* h-file for real driver */
	file = OpenFile(DRIVER_FT, LOC_INCL, "%s.h", USER_SUBWORD_LEXEME);
	Translate(file, "common", "driver/userPart/uep.h");
	fclose(file);

	/* c-file for driver simulator */
	//printf("Building %s simulator's user-defined entry poins.\n", system);
	TranslationSetDriverType("SIM");
	TranslationSetFancyString("Sim");
	TranslationSetChar('S');
	file =OpenFile(SIM_FT, LOC_DRVR, "%s/%s.c", LOCAL_USER_DIR,
		       USER_SUBWORD_LEXEME);
	Translate(file, "common", "driver/userPart/uepHead.c");
	Translate(file, "common", "driver/isr.c");	/* generate empty ISR */
	Translate(file, "common", "driver/userPart/uepFoot.c");
	fclose(file);

	/* h-file for driver simulator */
	file = OpenFile(SIM_FT, LOC_INCL, "%s.h", USER_SUBWORD_LEXEME);
	Translate(file, "common", "driver/userPart/uep.h");
	fclose(file);

	/* add 'readme' file */
	file = OpenFile(EXACT_FT, LOC_DRVR, "%s/README.1ST", LOCAL_USER_DIR);
	Translate(file, "common", "driver/userPart/warnTemplate.txt");
	fclose(file);

	/* Driver/Simulator version files */
	BuildVersionFiles(DRIVER_FT | SIM_FT);

	/* 0x1 now build user entry points in the test programm */
	//printf("Building %s files for user-defined test program.\n", system);
	file = OpenFile(COMMON_FT, LOC_FCLTY, "%s/%sTest.c", LOCAL_USER_DIR,
			USER_SUBWORD_LEXEME);
	Translate(file, "common", "testProg/common/userTest.c");
	fclose(file);

	file = OpenFile(COMMON_FT, LOC_FCLTY, "%s/%sTest.h", LOCAL_USER_DIR,
			USER_SUBWORD_LEXEME);
	Translate(file, "common", "testProg/common/userTest.h");
	fclose(file);

	/* 0x2 now build user-defined library framework */
	//printf("Building %s files for user-defined access library.\n", system);
	file = OpenFile(COMMON_FT, LOC_LIB, "%s/%sAccess.c", LOCAL_USER_DIR,
			USER_SUBWORD_LEXEME);
	Translate(file, "common", "lib/usrAccess/userDefinedAccess.c");
	fclose(file);

	file = OpenFile(COMMON_FT, LOC_INCL, "%sAccess.h", USER_SUBWORD_LEXEME);
	Translate(file, "common", "lib/usrAccess/userDefinedAccess.h");
	fclose(file);

	/* 0x3 Install program entry point */
	file = OpenFile(EXACT_FT, LOC_INST, "%s/install-so.c", LOCAL_USER_DIR);
	Translate(file, "common", "install/usr-install-so.c");
	fclose(file);
}

/**
 * @brief Generates filename based on directory name, filename itself and file
 *        extension.
 *
 * @param type -- Driver, Simulator, common or exact
 * @param root -- full directory path
 * @param name -- filename
 * @param ext  -- file extension (i.e. '.h' '.c' etc.)
 * @param ret  -- where to put the results (if not NULL)
 *
 * It also takes in account if it is a driver or driver simulator.\n
 * For example if @e type is @b SIM_FT, @b root is @b $HOME/tmp, @e name is
 * @b Myname and @e ext is @b .h, than generated filename will be
 * @b "$HOME/tmp/<sys_lower>MynameSim.h".\n
 * If @e type is @b EXACT_FT than it just generates file with name @b name
 * without taking in account if this is a drvr or sim.
 *
 * @return Generated filename.
 */
char *GenerateFilename(FILETYPE type, char *root, char *name, char *ext,
		       char *ret)
{
	static char fn[MAXPATHLEN];

	if (type == DRIVER_FT)	/* driver */
		snprintf(fn, MAXPATHLEN, "%s/%s%sDrvr%s", root,
			 TranslationGetSysLower(), name, ext);
	else if (type == SIM_FT)	/* driver simulator */
		snprintf(fn, MAXPATHLEN, "%s/%s%sSim%s", root,
			 TranslationGetSysLower(), name, ext);
	else if (type == COMMON_FT)	/* common for driver and simulator */
		snprintf(fn, MAXPATHLEN, "%s/%s%s%s", root,
			 TranslationGetSysLower(), name, ext);
	else			/* file with exact given name
				   (i.e. type == EXACT_FT) */
		snprintf(fn, MAXPATHLEN, "%s/%s", root, name);

#ifdef _VERBOSE_
	printf("%s(): Generated filename is-> %s\n", __FUNCTION__, fn);
#endif

	if (ret) {
		strcpy(ret, fn);
		return ret;
	} else {
		return fn;
	}
}

/**
 * @brief Create version files for real and simulator driver.
 *
 * @param ft -- what to build
 *
 * @return void
 */
static void BuildVersionFiles(FILETYPE ft)
{
	FILE *file;

	if (ft & DRIVER_FT) {
		TranslationSetDriverType("DRVR");
		TranslationSetFancyString("Driver");
		file = OpenFile(DRIVER_FT, LOC_INCL, "%s.h", "Version");
		Translate(file, "common", "driver/userPart/uepVers.h");
		fclose(file);
	}

	if (ft & SIM_FT) {
		TranslationSetDriverType("SIM");
		TranslationSetFancyString("Simulator");
		file = OpenFile(SIM_FT, LOC_INCL, "%s.h", "Version");
		Translate(file, "common", "driver/userPart/uepVers.h");
		fclose(file);
	}
}

/**
 * @brief Returns directory name for shared source code of VME/PCI buses.
 *
 * @param none
 *
 * @return Shared source code directory name (GENERAL_DIR).
 */
static char *GetGeneralSrcDir()
{
	static char *shDir = NULL;
	char *mid;		/* master install dir */

	if (shDir)		/* directory is already created, so return */
		return (shDir);

	mid = get_mid(NULL);
	shDir = (char *)calloc((strlen(mid) + strlen(GENERAL_DIR) + 2),
			       sizeof(char));
	sprintf(shDir, "%s/%s", mid, GENERAL_DIR);

	if (MakeSafeDir(shDir) == -DRIVER_GEN_BAD)
		exit(EXIT_FAILURE);	/* 1 */

	return shDir;
}

/**
 * @brief local driver diectory name container.
 *
 * @param none
 *
 * @return local driver diectory name (LOCAL_DRVR_DIR).
 */
static char *GetLocalDrvrDir()
{
	static char *drvrDir = NULL;
	char *cmd;		/* current module directory */

	if (drvrDir)		/* directory is already created, so return */
		return (drvrDir);

	/* directory is not created yet. So DO it. */
	cmd = get_msd(NULL);
	drvrDir = (char *)calloc((strlen(cmd) + strlen(LOCAL_DRVR_DIR) + 2),
				 sizeof(char));
	sprintf(drvrDir, "%s/%s", cmd, LOCAL_DRVR_DIR);

	if (MakeSafeDir(drvrDir) == -DRIVER_GEN_BAD)
		exit(EXIT_FAILURE);	/* 1 */

	return drvrDir;
}

/**
 * @brief local facility directory name container.
 *
 * @param none
 *
 * @return local facility directory name (LOCAL_FCLTY_DIR).
 */
static char *GetLocalFcltyDir()
{
	static char *fcltyDir = NULL;
	char *cmd;		/* current module directory */

	if (fcltyDir)		/* directory is already created, so return */
		return (fcltyDir);

	/* directory is not created yet. So DO it. */
	cmd = get_msd(NULL);
	fcltyDir = (char *)calloc((strlen(cmd) + strlen(LOCAL_FCLTY_DIR) + 2),
				  sizeof(char));
	sprintf(fcltyDir, "%s/%s", cmd, LOCAL_FCLTY_DIR);

	if (MakeSafeDir(fcltyDir) == -DRIVER_GEN_BAD)
		exit(EXIT_FAILURE);	/* 1 */

	return fcltyDir;
}

/**
 * @brief local include directory name container.
 *
 * @param none
 *
 * @return local include directory name (LOCAL_INCL_DIR).
 */
static char *GetLocalInclDir()
{
	static char *inclDir = NULL;
	char *cmd;		/* current module directory */

	if (inclDir)		/* directory is already created, so return */
		return (inclDir);

	/* directory is not created yet. So DO it. */
	cmd = get_msd(NULL);
	inclDir = (char *)calloc((strlen(cmd) + strlen(LOCAL_INCL_DIR) + 2),
				 sizeof(char));
	sprintf(inclDir, "%s/%s", cmd, LOCAL_INCL_DIR);

	if (MakeSafeDir(inclDir) == -DRIVER_GEN_BAD)
		exit(EXIT_FAILURE);	/* 1 */

	return (inclDir);
}

/**
 * @brief local library directory name container.
 *
 * @param none
 *
 * @return local library directory name (LOCAL_LIB_DIR).
 */
static char *GetLocalLibDir()
{
	static char *libDir = NULL;
	char *cmd;		/* current module directory */

	if (libDir)		/* directory is already created, so return */
		return (libDir);

	/* directory is not created yet. So DO it. */
	cmd = get_msd(NULL);
	libDir = (char *)calloc((strlen(cmd) + strlen(LOCAL_LIB_DIR) + 2),
				sizeof(char));
	sprintf(libDir, "%s/%s", cmd, LOCAL_LIB_DIR);

	if (MakeSafeDir(libDir) == -DRIVER_GEN_BAD)
		exit(EXIT_FAILURE);	/* 1 */

	return libDir;
}

/**
 * @brief installation entry point directory name container.
 *
 * @param none
 *
 * @return installation directory name (LOCAL_INST_DIR).
 */
static char *GetLocalInstDir()
{
	static char *instDir = NULL;
	char *cmd;		/* current module directory */

	if (instDir)		/* directory is already created, so return */
		return instDir;

	/* directory is not created yet. So DO it. */
	cmd = get_msd(NULL);
	instDir = (char *)calloc((strlen(cmd) + strlen(LOCAL_INST_DIR) + 2),
				 sizeof(char));
	sprintf(instDir, "%s/%s", cmd, LOCAL_INST_DIR);

	if (MakeSafeDir(instDir) == -DRIVER_GEN_BAD)
		exit(EXIT_FAILURE);	/* 1 */

	return instDir;
}

/**
 * @brief Creates directory named @e dirName
 *
 * @param dirName -- directory name
 *
 * @return -DRIVER_GEN_BAD   - in case of error.
 * @return  DRIVER_GEN_OK    - if directory is created.
 * @return  DRIVER_GEN_EXIST - if directory already exist.
 */
rstat MakeSafeDir(char *dirName)
{
	struct stat status;
	rstat ret;

	if (!dirName)
		return DRIVER_GEN_BAD;

	if (stat(dirName, &status)) { /* directory doesn't exist - so create */
		umask(0002);	/* set tmp mask for dir creation */
		if (mkdir(dirName, 0777)) {
			ERR_POS("mkdir");
			return DRIVER_GEN_BAD;
		}
		ret = DRIVER_GEN_OK;
	} else			/* directory already exist */
		ret = DRIVER_GEN_EXIST;

#ifdef _VERBOSE_
	printf("%s() Created directory name is: %s\n", __func__, dirName);
#endif

	return ret;
}

/**
 * @brief
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 *
 * @return void
 */
static void BuildTestProgHFile(RegisterDef_t * registers, int numRegisters)
{
	FILE *tstFileH;
	int endOfBlock = FALSE;
	int i, count = 0, blockcntr = 0, exNum = 0, woNum = 0;

	TranslationReset();
	tstFileH = OpenFile(COMMON_FT, LOC_FCLTY, "Test.h");

	/* Generate the first portion of the test program */
	TranslationSetFreeFormat("%s", DG_INC_DIR);
	Translate(tstFileH, "common", "testProg/common/headerHead.h");

	/* Generate the '*NUM_REGISTERS' constants, there
	   should be one for each block that has been defined. */
	count = 0;
	for (i = 0; i < numRegisters; i++) {
		count++;
		if (i == numRegisters - 1)
			endOfBlock = TRUE;
		else if (registers[i].blockP->blockID !=
			 registers[i + 1].blockP->blockID)
			endOfBlock = TRUE;
		else
			endOfBlock = FALSE;

		if (endOfBlock) {
			blockcntr++;	/* block counter */
			TranslationSetFancyNum(registers[i].blockP->blockID);
			TranslationSetPlainNum(count);
			Translate(tstFileH, "common",
				  "testProg/common/numReg.h");
			count = 0;
		}
		if (registers[i].rar == AMEX)
			exNum++;
		if (registers[i].rar == AMWR)
			woNum++;
	}

	/* Generate tentative function decls. to suppess any
	   possible warnings */
	TranslationSetPlainNum(exNum);
	TranslationSetRegDepth(GetSrvRegNum());
	TranslationSetPrecision(woNum);
	Translate(tstFileH, "common", "testProg/common/tentativeDecl.h");
	Translate(tstFileH, "common", "testProg/serviceMenuHead.h");
	Translate(tstFileH, "common", "testProg/common/extraneousMenuHead.h");
	Translate(tstFileH, "common", "testProg/writeOnlyMenuHead.h");
	for (i = 0; i < blockcntr; i++) {
		TranslationSetFancyNum(i);
		Translate(tstFileH, "common",
			  "testProg/common/blockMenuHead.h");
	}

	Translate(tstFileH, "common", "testProg/common/headerFoot.h");
	fclose(tstFileH);
}

/**
 * @brief Generates the first portion of the driver's test program.
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 * @param testFile     -- open file descriptor
 *
 * Namely the #includes, #defines and the arrays of strings containing
 * register comments.
 *
 * @return void
 */
static void BuildTestProgConsts(RegisterDef_t * registers, int numRegisters,
				FILE * testFile)
{
	int endOfBlock = FALSE;
	int i;

	TranslationReset();
	Translate(testFile, "common", "testProg/common/testHead.c");

  /*------------------------------------------------------------------
    00. Generate the register comments string arrays. There should be
    an array of string for each defined block
    -----------------------------------------------------------------*/
	endOfBlock = TRUE;
	for (i = 0; i < numRegisters; i++)
		if (registers[i].rar != AMEX) {
			/* exclude extraneous registers */
			TranslationSetFancyNum(registers[i].blockP->blockID);
			TranslationSetRegName(registers[i].name);
			TranslationSetRegComm(registers[i].comment);

			/* If we've just ended a block then this must be
			   the start of a new one */
			if (endOfBlock)
				Translate(testFile, "common",
					  "testProg/common/blockDefHead.c");
			Translate(testFile, "common",
				  "testProg/common/regData.c");

			/* Is this the end of a block? */
			if (i == numRegisters - 1)
				endOfBlock = TRUE;
			else if (registers[i].blockP->blockID !=
				 registers[i + 1].blockP->blockID)
				endOfBlock = TRUE;
			else
				endOfBlock = FALSE;

			if (endOfBlock)
				Translate(testFile, "common",
					  "dummyTemplates/endOfBraces");
		}

  /*----------------------------------------------------------------
    01. Generate the comments string for the extraneous registers.
    ----------------------------------------------------------------*/
	Translate(testFile, "common", "testProg/common/extraneousDefHead.c");
	for (i = 0; i < numRegisters; i++)
		if (registers[i].rar == AMEX) {	/* is it is extraneous? */
			TranslationSetRegName(registers[i].name);
			TranslationSetRegComm(registers[i].comment);
			Translate(testFile, "common",
				  "testProg/common/regData.c");
		}
	Translate(testFile, "common", "dummyTemplates/endOfBraces");

  /*----------------------------------------------------------------
    02. Generate the comments string for the writeOnly registers.
    ----------------------------------------------------------------*/
	Translate(testFile, "common", "testProg/writeOnlyDefHead.c");
	for (i = 0; i < numRegisters; i++)
		if (registers[i].rar == AMWR) {	/* is it is writeOnly? */
			TranslationSetRegName(registers[i].name);
			TranslationSetRegComm(registers[i].comment);
			Translate(testFile, "common",
				  "testProg/common/regData.c");
		}
	Translate(testFile, "common", "dummyTemplates/endOfBraces");

  /*-------------------------------------------------------------
    03. Generate the comments string for Service Registers.
    -------------------------------------------------------------*/
	Translate(testFile, "common", "testProg/serviceDefHead.c");
	for (i = 0; i < GetSrvRegNum(); i++) {
		TranslationSetRegName(srv_ioctl[i].name);
		TranslationSetRegComm(srv_ioctl[i].comment);
		Translate(testFile, "common", "testProg/common/regData.c");
	}
	Translate(testFile, "common", "dummyTemplates/endOfBraces");
}

/**
 * @brief Generate the last portion of the driver's test program.
 *
 * @param registers    -- register description
 * @param blocks       -- block description
 * @param numRegisters -- register amount
 * @param testFile     -- open file descriptor
 *
 * It produces a sub menu for each of the defined blocks and for the extraneous
 * variables block.
 *
 * @return void
 */
static void BuildTestProgSubMenus(RegisterDef_t * registers,
				  BlockDef_t * blocks, int numRegisters,
				  FILE * testFile)
{
	int i, j, count, blockStart, endOfBlock;

	TranslationReset();

  /*=================00. Generate WriteOnly Registers sub menu.==============*/
	Translate(testFile, "common", "testProg/writeOnlyMenuHead.c");
	if (!HasWriteOnly(registers, numRegisters))
		Translate(testFile, "common", "testProg/writeOnlyNO.c");
	else {
		/* Add a menu item for each extraneous variable that's
		   been defined */
		Translate(testFile, "common", "testProg/common/menuHead.c");
		count = TST_PROG_SUB_MENU_FIRST_ENTRY;

		for (i = 0; i < numRegisters; i++)
			if (registers[i].rar == AMWR) {
				/* is it is writeOnly reg? */
				TranslationSetRegName(registers[i].name);

				/* read */
				TranslationSetPlainNum(count++);
				Translate(testFile, "common",
					  "testProg/common/menuReadEntry.c");
			}

		Translate(testFile, "common", "testProg/common/menuFoot.c");
		Translate(testFile, "common", "testProg/writeOnlyCaseHead.c");
		count = TST_PROG_SUB_MENU_FIRST_ENTRY;

		for (i = 0; i < numRegisters; i++)
			if (registers[i].rar == AMWR) {
				/* is it is writeOnly? */
				TranslationSetRegId(registers[i].upperName);

				/* read */
				TranslationSetPlainNum(count++);
				TranslationSetFancyString("Get");
				Translate(testFile, "common",
					  "testProg/common/caseGetSet.c");
			}
		Translate(testFile, "common", "testProg/common/caseEnd.c");
	}

  /*=================01. Generate Extraneous Variable sub menu.==============*/
	Translate(testFile, "common", "testProg/common/extraneousMenuHead.c");
	if (!HasExtraneous(registers, numRegisters))
		/* If there were not any extraneous variables then generate
		   code to alert the test program's user that there aren't any
		   extra variables.  If there were extraneous variables then
		   produce the case statments that should accompany the menu
		   items. */
		Translate(testFile, "common", "testProg/common/extraneousNO.c");
	else {
		/* Add a menu item for each extraneous variable that's been
		   defined */
		Translate(testFile, "common", "testProg/common/menuHead.c");
		count = TST_PROG_SUB_MENU_FIRST_ENTRY;

		for (i = 0; i < numRegisters; i++)
			if (registers[i].rar == AMEX) {
				/* is it is extraneous? */
				TranslationSetRegName(registers[i].name);

				/* read */
				TranslationSetPlainNum(count++);
				Translate(testFile, "common",
					  "testProg/common/menuReadEntry.c");

				/* write */
				TranslationSetPlainNum(count++);
				Translate(testFile, "common",
					  "testProg/common/menuWriteEntry.c");
			}
		Translate(testFile, "common", "testProg/common/menuFoot.c");
		Translate(testFile, "common",
			  "testProg/common/extraneousCaseHead.c");
		count = TST_PROG_SUB_MENU_FIRST_ENTRY;

		for (i = 0; i < numRegisters; i++)
			if (registers[i].rar == AMEX) {
				/* is it is extraneous? */
				TranslationSetRegId(registers[i].upperName);

				/* read */
				TranslationSetPlainNum(count++);
				TranslationSetFancyString("Get");
				Translate(testFile, "common",
					  "testProg/common/caseGetSet.c");

				/* write */
				TranslationSetPlainNum(count++);
				TranslationSetFancyString("Set");
				Translate(testFile, "common",
					  "testProg/common/caseGetSet.c");
			}
		Translate(testFile, "common", "testProg/common/caseEnd.c");
	}

  /*=========02. Generate Service Registers sub menu.====================*/
	Translate(testFile, "common", "testProg/serviceMenuHead.c");
	Translate(testFile, "common", "testProg/common/menuHead.c");
	count = TST_PROG_SUB_MENU_FIRST_ENTRY;

	for (i = 0; i < GetSrvRegNum(); i++) {
		TranslationSetRegName(srv_ioctl[i].name);
		switch (srv_ioctl[i].rar) {
		case AMRD:	/* 'r' */
			TranslationSetPlainNum(count++);
			Translate(testFile, "common",
				  "testProg/common/menuReadEntry.c");
			break;
		case AMWR:	/* 'w' */
			TranslationSetPlainNum(count++);
			Translate(testFile, "common",
				  "testProg/common/menuWriteEntry.c");
			break;
		case (AMRD | AMWR):	/* 'rw' */
			TranslationSetPlainNum(count++);
			Translate(testFile, "common",
				  "testProg/common/menuReadEntry.c");
			TranslationSetPlainNum(count++);
			Translate(testFile, "common",
				  "testProg/common/menuWriteEntry.c");
			break;
		default:
			/* it's not an error, just this service register
			   is not r/w */
			break;
		}
	}
	Translate(testFile, "common", "testProg/common/menuFoot.c");
	Translate(testFile, "common", "testProg/serviceCaseHead.c");
	count = TST_PROG_SUB_MENU_FIRST_ENTRY;

	for (i = 0; i < GetSrvRegNum(); i++) {
		TranslationSetRegId(srv_ioctl[i].name);
		switch (srv_ioctl[i].rar) {
		case AMRD:	/* 'r' */
			TranslationSetPlainNum(count++);
			TranslationSetFancyString("Get");
			Translate(testFile, "common",
				  "testProg/common/caseGetSet.c");
			break;
		case AMWR:	/* 'w' */
			TranslationSetPlainNum(count++);
			TranslationSetFancyString("Set");
			Translate(testFile, "common",
				  "testProg/common/caseGetSet.c");
			break;
		case (AMRD | AMWR):	/* 'rw' */
			TranslationSetPlainNum(count++);
			TranslationSetFancyString("Get");
			Translate(testFile, "common",
				  "testProg/common/caseGetSet.c");

			TranslationSetPlainNum(count++);
			TranslationSetFancyString("Set");
			Translate(testFile, "common",
				  "testProg/common/caseGetSet.c");
			break;
		default:
			/* it's not an error, just this service register
			   is not r/w */
			break;
		}
	}
	Translate(testFile, "common", "testProg/common/caseEnd.c");

  /*=====03. Now generate sub menus for each of the defined blocks.==========*/
	endOfBlock = TRUE;

	for (i = 0; i < numRegisters; i++)
		if (registers[i].rar != AMEX) {	/* skip extraneous registers */
			if (endOfBlock) {
				TranslationSetFancyNum(registers[i].blockP->
						       blockID);
				TranslationSetPlainNum(registers[i].blockP->
						       blockID);
				Translate(testFile, "common",
					  "testProg/common/blockMenuHead.c");
				Translate(testFile, "common",
					  "testProg/common/menuHead.c");
				count = TST_PROG_SUB_MENU_FIRST_ENTRY;
				blockStart = i;
			}
			TranslationSetRegName(registers[i].name);
			switch (registers[i].rar) {
			case AMRD:	/* 'r' */
				TranslationSetPlainNum(count++);
				Translate(testFile, "common",
					  "testProg/common/menuReadEntry.c");
				break;
			case AMWR:	/* 'w' */
				TranslationSetPlainNum(count++);
				Translate(testFile, "common",
					  "testProg/common/menuWriteEntry.c");
				break;
			case (AMWR | AMRD):	/* 'rw' */
				TranslationSetPlainNum(count++);
				Translate(testFile, "common",
					  "testProg/common/menuReadEntry.c");

				TranslationSetPlainNum(count++);
				Translate(testFile, "common",
					  "testProg/common/menuWriteEntry.c");
				break;
			default:
				FATAL_ERR();
				break;
			}

			if (i == numRegisters - 1)
				endOfBlock = TRUE;
			else if (registers[i].blockP->blockID !=
				 registers[i + 1].blockP->blockID)
				endOfBlock = TRUE;
			else
				endOfBlock = FALSE;

			if (endOfBlock) {	/* now 'switch' can be generated */
				Translate(testFile, "common",
					  "testProg/common/menuFoot.c");
				Translate(testFile, "common",
					  "testProg/common/blockCaseHead.c");
				count = TST_PROG_SUB_MENU_FIRST_ENTRY;

				for (j = blockStart; j <= i; j++) {
					TranslationSetRegId(registers[j].
							    upperName);
					switch (registers[j].rar) {
					case AMRD:	/* 'r' */
						TranslationSetPlainNum(count++);
						TranslationSetFancyString
						    ("Get");
						Translate(testFile, "common",
							  "testProg/common/"
							  "caseGetSet.c");
						break;
					case AMWR:	/* 'w' */
						TranslationSetPlainNum(count++);
						TranslationSetFancyString
						    ("Set");
						Translate(testFile, "common",
							  "testProg/common/"
							  "caseGetSet.c");
						break;
					case (AMWR | AMRD):	/* 'rw' */
						TranslationSetPlainNum(count++);
						TranslationSetFancyString
						    ("Get");
						Translate(testFile, "common",
							  "testProg/common/"
							  "caseGetSet.c");

						TranslationSetPlainNum(count++);
						TranslationSetFancyString
						    ("Set");
						Translate(testFile, "common",
							  "testProg/common/"
							  "caseGetSet.c");
						break;
					default:
						FATAL_ERR();
						break;
					}
				}	/* end of for */
				Translate(testFile, "common",
					  "testProg/common/caseEnd.c");
			}	/* end of if */
		}		/* end of for */
}

/**
 * @brief Create major header files that are common for all VME/DRM drivers.
 */
static void BuildGeneralHeaderFiles(void)
{
	int cntr;
	int constCounter;	/*  */
	FILE *fp;
	char *bus = TranslationGetBus();	/* 'VME' or 'DRM' */

	/* set general constants */
	fp = OpenFile(EXACT_FT, LOC_GENERAL, "%s/GeneralHeader.h", DG_INC_DIR);
	TranslationSetPrecision(MAX_STR);
	TranslationSetDDDNum(MIN_STR);
	TranslationSetPlainNum(NAME_LEN);
	TranslationSetRegDepth(MAX_REG);
	TranslationSetIntNum(MAX_BLK);
	TranslationSetExtraNum(NO_ADDRESS);
	TranslationSetHexNum(NO_LUN);
	Translate(fp, "common", "header/generalHeader.h");
	fclose(fp);

	/* header for driver and simulator. Differs for VME/PCI buses. */
	fp = OpenFile(EXACT_FT, LOC_GENERAL, "%s/ModuleHeader.h", DG_INC_DIR);
	Translate(fp, bus, "header/generalHeader.h");
	fclose(fp);

	/* register description is placed here */
	fp = OpenFile(EXACT_FT, LOC_GENERAL, "%s/RegDesc.h", DG_INC_DIR);
	Translate(fp, "common", "header/regDesc.h");
	fclose(fp);

	/* seed Linux Kernel List */
	fp = OpenFile(EXACT_FT, LOC_GENERAL, "%s/lklist.h", DG_INC_DIR);
	Translate(fp, "common", "header/lklist.h");
	fclose(fp);

	/* VMEbus Address Modifier Codes */
	fp = OpenFile(EXACT_FT, LOC_GENERAL, "%s/vme_am.h", DG_INC_DIR);
	Translate(fp, "../include", "vme_am.h");
	fclose(fp);

	/* port operations for Lynx */
	fp = OpenFile(EXACT_FT, LOC_GENERAL, "%s/port_ops_lynx.h", DG_INC_DIR);
	Translate(fp, "common", "header/port_ops_lynx.h");
	fclose(fp);

	/* port operations for Linux */
	fp = OpenFile(EXACT_FT, LOC_GENERAL, "%s/port_ops_linux.h", DG_INC_DIR);
	Translate(fp, "common", "header/port_ops_linux.h");
	fclose(fp);

	/* extra swap operations for Linux */
	fp = OpenFile(EXACT_FT, LOC_GENERAL, "%s/swab-extra-linux.h",
		      DG_INC_DIR);
	Translate(fp, "common", "header/swab-extra-linux.h");
	fclose(fp);

	/* service registers ioctl numbers */
	fp = OpenFile(EXACT_FT, LOC_GENERAL, "%s/ServiceRegIoctl.h",
		      DG_INC_DIR);
	Translate(fp, "common", "header/srvHead.h");
	TranslationSetFancyString("IOCTL_BASE"); /* setup base number for the
						    service ioctl numbers */

	for (cntr = 0, constCounter = 0; cntr < GetSrvRegNum(); cntr++) {
		switch (srv_ioctl[cntr].rar) {
		case AMRD:	/* 'r'  */
			TranslationSetPlainNum(constCounter++);
			TranslationSetIoctlConst("GET", srv_ioctl[cntr].name);
			Translate(fp, "common", "header/ioctlConst.h");
			break;
		case AMWR:	/* 'w'  */
			TranslationSetPlainNum(constCounter++);
			TranslationSetIoctlConst("SET", srv_ioctl[cntr].name);
			Translate(fp, "common", "header/ioctlConst.h");
			break;
		case (AMWR | AMRD):	/* 'rw' */
			TranslationSetPlainNum(constCounter++);
			TranslationSetIoctlConst("GET", srv_ioctl[cntr].name);
			Translate(fp, "common", "header/ioctlConst.h");

			TranslationSetPlainNum(constCounter++);
			TranslationSetIoctlConst("SET", srv_ioctl[cntr].name);
			Translate(fp, "common", "header/ioctlConst.h");
			break;
		default:
			/* special srv register that has ioctl entry,
			   but it's not a 'r/w' register */
			TranslationSetPlainNum(constCounter++);
			TranslationSetIoctlConst("", srv_ioctl[cntr].name);
			Translate(fp, "common", "header/ioctlConst.h");
			break;
		}
	}

	TranslationSetPlainNum(constCounter);
	Translate(fp, "common", "header/srvFoot.h");
	fclose(fp);

	/* service registers ID enum */
	fp = OpenFile(EXACT_FT, LOC_GENERAL, "%s/ServiceRegId.h", DG_INC_DIR);
	Translate(fp, "common", "header/srvIdHead.h");

	for (cntr = 0, constCounter = 0; cntr < GetSrvRegNum(); cntr++) {
		TranslationSetRegId(srv_ioctl[cntr].name);
		TranslationSetRegComm(srv_ioctl[cntr].comment);
		TranslationSetFancyString(rar2mode(srv_ioctl[cntr].rar));
		TranslationSetDummyString(itoa(constCounter++, 10));
		Translate(fp, "common", "header/regIdEnumIndexed.h");
	}

	/* set last service regID description */
	TranslationSetRegName(srv_ioctl[cntr].name);
	TranslationSetRegComm(srv_ioctl[cntr].comment);
	TranslationSetDummyString(itoa(constCounter, 10));
	Translate(fp, "common", "header/srvIdFoot.h");
	fclose(fp);
}

/**
 * @brief Generate general libraries source code, wich are @b DriverAccess,
 *        @b Ioconfig and @b extra-lynx
 *
 * @param none
 *
 * DriverAccess library enables user to use @e DaGetElement(),
 * @e DaSetElement() and many other functions for register accessing.
 *
 * Ioconfig library enables user to access driver by means of @e ioconfig
 * calls, that uses a direct memory access mechanism for accessing.
 *
 * xtra-lynx -- imports such handy functions as vsnprintf, asprintf etc...
 * into user-space apps.
 *
 * @return void
 */
static void BuildGeneralLibraries()
{
	FILE *fd;
	char *bus = TranslationGetBus();	/* 'VME' or 'DRM' */
	char *tstr = ctime(&mod_creation_time);

	tstr[strlen(tstr) - 1] = 0;	/* get rid of '\n' */

	/* driverAccess library source code */
	/* generate '.h' file */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/dal.h", DG_INC_DIR);
	Translate(fd, bus, "lib/drvrAccess/DrvrAccess.h");
	fclose(fd);

	/* generate '.c' files */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/dal.c", DAL_DIR);

	/* set DAL generation time */
	TranslationSetHexNum(mod_creation_time);
	TranslationSetComment(tstr);

	/* DAL mainframe */
	Translate(fd, bus, "lib/drvrAccess/DrvrAccess.c");
	fclose(fd);

	/* Linux DMA support */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/dma.c", DAL_DIR);
	Translate(fd, bus, "lib/drvrAccess/dma.c");
	fclose(fd);

	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/swa.h", DG_INC_DIR);
	Translate(fd, bus, "lib/drvrAccess/swa.h");
	fclose(fd);


	/* mmap access for Lynx */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/IoconfAccess.c", DAL_DIR);
	Translate(fd, bus, "lib/drvrAccess/ioconf.c");
	fclose(fd);

	/* xtra-lynx library -- handy function for lynx user-space */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/lynx-extra.c", DAL_DIR);
	Translate(fd, bus, "lib/drvrAccess/lynx-extra.c");
	fclose(fd);
}

/**
 * @brief Build Device driver installation/uninstallation program source code.
 *
 * @param none
 *
 * @return void
 */
static void BuildGeneralInstallProgram()
{
	FILE *fd;
	char *bus = TranslationGetBus();	/* 'VME' or 'DRM' */

	/* generate install program '.h' file */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/modinst.h", DG_INC_DIR);
	Translate(fd, bus, "installProg/modinst.h");
	fclose(fd);

	/* generate install program '.c' file */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/modinst.c", INST_RM_DIR);
	Translate(fd, bus, "installProg/modinst.c");
	fclose(fd);

	/* generate deinstall program '.h' file */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/moduninst.h", DG_INC_DIR);
	Translate(fd, bus, "installProg/moduninst.h");
	fclose(fd);

	/* generate deinstall program '.c' file */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/moduninst.c", INST_RM_DIR);
	Translate(fd, bus, "installProg/moduninst.c");
	fclose(fd);

	/* add utilities */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/utils.c", INST_RM_DIR);
	Translate(fd, bus, "installProg/utils.c");
	fclose(fd);

	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/utils.h", DG_INC_DIR);
	Translate(fd, bus, "installProg/utils.h");
	fclose(fd);

	/* Lynx-specific utils */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/utils-lynx.c", INST_RM_DIR);
	Translate(fd, bus, "installProg/utils-lynx.c");
	fclose(fd);

	/* Linux-specific utils */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/utils-linux.c", INST_RM_DIR);
	Translate(fd, bus, "installProg/utils-linux.c");
	fclose(fd);

	/* Lynx API, needed for linux */
	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/extra-linux.c", INST_RM_DIR);
	Translate(fd, bus, "installProg/extra-linux.c");
	fclose(fd);

	fd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/extra-linux.h", DG_INC_DIR);
	Translate(fd, bus, "installProg/extra-linux.h");
	fclose(fd);
}

/**
 * @brief Build-up makefiles that are located in @e general directory.
 *
 * @param none
 *
 * They contains install/uninstall programs, DALib and header files that are
 * common to all generated drivers.
 *
 * @return void
 */
static void BuildGeneralMakefiles()
{
	FILE *mkfd;

	/* Base Makefile */
	mkfd = OpenFile(EXACT_FT, LOC_GENERAL, "Makefile.base");
	Translate(mkfd, "common", "makefiles/Makefile.base");
	fclose(mkfd);

	/* "Which dirs to compile" Makefile */
	mkfd = OpenFile(EXACT_FT, LOC_GENERAL, "Makefile");
	TranslationSetFreeFormat("%s", DG_INC_DIR);
	TranslationSetString(DAL_DIR);
	TranslationSetComment(INST_RM_DIR);
	Translate(mkfd, "common", "makefiles/MakefileShared");
	fclose(mkfd);

	/* Include Makefile  */
	mkfd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/Makefile", DG_INC_DIR);
	Translate(mkfd, "common", "makefiles/includeGeneral.mk");
	fclose(mkfd);

	/* Libraries Makefile */
	mkfd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/Makefile", DAL_DIR);

	Translate(mkfd, "common", "makefiles/libShared.mk");
	fclose(mkfd);

	/* Install program Makefile */
	mkfd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/Makefile", INST_RM_DIR);
	TranslationSetString(DG_INC_DIR);
	Translate(mkfd, "common", "makefiles/instProg.mk");
	fclose(mkfd);

	mkfd = OpenFile(EXACT_FT, LOC_GENERAL, "%s/deliver.mk", INST_RM_DIR);
	Translate(mkfd, "common", "makefiles/deliver.mk");
	fclose(mkfd);
}

/**
 * @brief
 *
 * @param none
 *
 * @return void
 */
static void BuildGeneralScripts(void)
{
	FILE *mkfd;

	mkfd = OpenFile(EXACT_FT, LOC_GENERAL, UTIL_CP_USR_PT_GLOB);
	Translate(mkfd, "common", UTIL_CP_USR_PT_GLOB);
	fchmod(fileno(mkfd), 0755);	/* set new access rights */
	fclose(mkfd);
}

/**
 * @brief Checks if register description obtained from the data base contains
 *        any extraneous registers.
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 *
 * @return amount of extraneous registers
 */
static int HasExtraneous(RegisterDef_t * registers, int numRegisters)
{
	int amount = 0;
	int cntr;

	for (cntr = 0; cntr < numRegisters; cntr++)
		if (registers[cntr].rar == AMEX)
			amount++;

	return (amount);
}

/**
 * @brief Checks if register description obtained from the data base contains
 *        any writeOnly registers.
 *
 * @param registers    -- register description
 * @param numRegisters -- register amount
 *
 * @return amount of writeonly registers.
 */
static int HasWriteOnly(RegisterDef_t * registers, int numRegisters)
{
	static int amount = -1;
	int i;

	if (amount == -1) {
		amount++;
		for (i = 0; i < numRegisters; i++)
			if (registers[i].rar == AMWR)
				amount++;
	}

	return (amount);
}
