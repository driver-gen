/**
 * @file xmlAccess.c
 *
 * @brief xml file access to get hw module description is here.
 *
 * @author Copyright (C) 2009 - 2010 GSI. Braeuning, Harald
 * @author Copyright (C) 2010 CERN. Georgievskiy Yury <ygeorgie@gmail.com>
 *
 * @date Created on 14/12/2009
 */
#define _GNU_SOURCE /* asprintf rocks */

#include <stdio.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include "driverGen.h"
#include "utilities.h"

#define ROOT_TAG                   "DriverGen"
#define MODULE_TAG                 "Module"
#define BLOCK_TAG                  "Block"
#define BLOCK_BASE_TAG             "Base"
#define BLOCK_OFFSET_TAG           "Offset"
#define REGISTER_TAG               "Register"
#define REGISTER_BLOCK_TAG         "Block"
#define REGISTER_OFFSET_TAG        "Offset"
#define REGISTER_DEPTH_TAG         "Depth"
#define REGISTER_MODE_TAG          "Mode"
#define REGISTER_SIZE_TAG          "Size"
#define REGISTER_TIMELOOP_TAG      "TimeLoop"
#define REGISTER_COMMENT_TAG       "Comment"
#define PCI_TAG                    "PCI"
#define PCI_VENDOR_TAG             "Vendor"
#define PCI_DEVICE_TAG             "Device"
#define VME_TAG                    "VME"
#define VME_ADDRESS_TAG            "Address"
#define VME_ADDRESS_RANGE_TAG      "Range"
#define VME_ADDRESS_INCREMENT_TAG  "Increment"
#define VME_ADDRESS_AM_TAG         "AM"
#define VME_ADDRESS_SIZE_TAG       "Size"
#define VME_TYPE_TAG               "Type"
#define VME_CHANNELS_TAG           "Channels"
#define VME_IRQ_TAG                "Irq"
#define VME_IRQVECTOR_TAG          "IrqVector"
#define VME_IRQVECTORINC_TAG       "IrqVectorInc"



#define NODE_ROOT      "DriverGen"
#define NODE_MODULE    "module"
#define NODE_VME       "vme_space"
#define NODE_BLK       "block"
#define NODE_REG       "register"

#define PROP_NAME      "name"
#define PROP_BA        "base"
#define PROP_ADDR_INCR "addrinc"
#define PROP_AM        "am"
#define PROP_RANGE     "range"
#define PROP_DPS       "dps"
#define PROP_OFFSET    "offset"
#define PROP_DEPTH     "depth"
#define PROP_MODE      "mode"
#define PROP_SIZE      "size"
#define PROP_TLOOP     "timeloop"
#define PROP_COMMENT   "comment"
#define PROP_ILEVEL    "ilevel"
#define PROP_IVEC      "ivec"
#define PROP_IVEC_INCR "ivecinc"

static xmlDoc *doc = NULL;
static xmlNode *rootElement = NULL;
static xmlNode *moduleElement = NULL;

static void PrintOutVmeInfo(VmeInfo_t *);
static void ReadXMLFile(char *moduleName);

/**
 * @brief Get register description from the DataBase.
 *
 * @param moduleName -- DataBase Module Name
 * @param defs       -- register description to put
 * @param blks       -- block description
 * @param numBlks    -- number of blocks
 * @param prog       -- program name
 *
 * Registers are prepared for the further use. If MAX allowed register amount
 * reached - printout error message and exits.
 *
 * @return number of registers, defined in the DB.
 */
int XMLGetRegisterConfig(char *moduleName, RegisterDef_t *defs,
		      BlockDef_t *blks, int numBlks, char *prog)
{
	int idx = 0;		/* register counter */
	int maxReg;		/*  */
	int cntr;		/*  */

	if (doc == NULL) ReadXMLFile(moduleName);

	maxReg = MAX_REG - GetSrvRegNum(); /* how many reg descr we can have */

	xmlNode *n = moduleElement->children;
	while (n != NULL)
	{
		if (n->type == XML_ELEMENT_NODE && strcasecmp((char*)n->name,REGISTER_TAG) == 0)
		{
			if (idx < maxReg) {
				char *p = (char*)xmlGetProp(n,(xmlChar*)"name");
				if (p != NULL)
				{
					snprintf(defs[idx].name,NAME_LEN,"%s",p);
					xmlFree(p);
				}
				xmlNode *b = n->children;
				while (b != NULL)
				{
					if (strcasecmp((char*)b->name,REGISTER_BLOCK_TAG) == 0)
					{
						char *str = (char*)xmlNodeListGetString(doc,b->children,1);
						short blockno = (short)strtoul(str,NULL,0);
						for (cntr = 0; cntr < numBlks; cntr++) {
							if (blks[cntr].blockID == blockno) {
								defs[idx].blockP = &blks[cntr];
								break;
							}
						}
						xmlFree(str);
					}
					else if (strcasecmp((char*)b->name,REGISTER_TIMELOOP_TAG) == 0)
					{
						char *str = (char*)xmlNodeListGetString(doc,b->children,1);
						defs[idx].timeLoop = strtoul(str,NULL,0);
						xmlFree(str);
					}
					else if (strcasecmp((char*)b->name,REGISTER_OFFSET_TAG) == 0)
					{
						char *str = (char*)xmlNodeListGetString(doc,b->children,1);
						defs[idx].offset = strtoul(str,NULL,0);
						xmlFree(str);
					}
					else if (strcasecmp((char*)b->name,REGISTER_DEPTH_TAG) == 0)
					{
						char *str = (char*)xmlNodeListGetString(doc,b->children,1);
						defs[idx].depth = strtoul(str,NULL,0);
						xmlFree(str);
					}
					else if (strcasecmp((char*)b->name,REGISTER_SIZE_TAG) == 0)
					{
						char *str = (char*)xmlNodeListGetString(doc,b->children,1);
						snprintf(defs[idx].size, SIZE_LEN, "%s", str);
						xmlFree(str);
					}
					else if (strcasecmp((char*)b->name,REGISTER_MODE_TAG) == 0)
					{
						char *str = (char*)xmlNodeListGetString(doc,b->children,1);
						snprintf(defs[idx].mode, MODE_LEN, "%s", str);
						xmlFree(str);
					}
					else if (strcasecmp((char*)b->name,REGISTER_COMMENT_TAG) == 0)
					{
						char *str = (char*)xmlNodeListGetString(doc,b->children,1);
						snprintf(defs[idx].comment, COMMENT_LEN, "%s", str);
						xmlFree(str);
					}
					b = b->next;
				}
			}
			idx++;		/* increase register counter */
		}
		n = n->next;
	}


	if (idx >= maxReg) {	/* check if within range */
		fprintf(stderr,
			"%sFATAL%s %s Module %s has too many registers.\n",
			RED_CLR, END_CLR, ERROR_MSG, moduleName);
		fprintf(stderr, "\t'%s' supports MAX %d registers (including"
			" service registers).\n\tYour module has %d register"
			" definitions (plus %d service registers)\n\t'MAX_REG'"
			" should be changed.\n\tPlease report this problem to"
			" the DriverGen support!\n",
			prog, MAX_REG, idx, GetSrvRegNum());
		exit(EXIT_FAILURE);	/* 1 */
	}

	defs[idx-1].last = 1;	/* mark last register */
	return idx;
}

/**
 * @brief Get block configuration from the DataBase.
 *
 * @param moduleName -- DataBase Module Name
 * @param defs       -- results will go here
 * @param prog       -- program name
 *
 * prepare results for the further use. If MAX allowed block amount
 * reached - printout error message and exits.
 *
 * @return number of defined blocks
 */
int XMLGetBlockConfig(char *moduleName, BlockDef_t * defs, char *prog)
{
	int idx = 0;		/* block counter */

	if (doc == NULL) ReadXMLFile(moduleName);
	xmlNode *n = moduleElement->children;
	while (n != NULL)
	{
		if (n->type == XML_ELEMENT_NODE && strcasecmp((char*)n->name,BLOCK_TAG) == 0)
		{
			if (idx < MAX_BLK) {
				defs[idx].blockID = -1;
				char *p = (char*)xmlGetProp(n,(xmlChar*)"id");
				if (p != NULL)
				{
					defs[idx].blockID = (short)atoi(p);
					xmlFree(p);
				}
				xmlNode *b = n->children;
				while (b != NULL)
				{
					if (strcasecmp((char*)b->name,BLOCK_BASE_TAG) == 0)
					{
						char *str = (char*)xmlNodeListGetString(doc,b->children,1);
						defs[idx].blkBaseAddr = (short)strtoul(str,NULL,0);
						xmlFree(str);
					}
					else if (strcasecmp((char*)b->name,BLOCK_OFFSET_TAG) == 0)
					{
						char *str = (char*)xmlNodeListGetString(doc,b->children,1);
						defs[idx].offset = strtoul(str,NULL,0);
						xmlFree(str);
					}
					b = b->next;
				}
			}
			idx++;	/* increase block counter */
		}
		n = n->next;
	}

	if (idx >= MAX_BLK) {	/* check if within range */
		fprintf(stderr, "%sFATAL%s %s Module %s has too many blocks.\n",
			RED_CLR, END_CLR, ERROR_MSG, moduleName);
		fprintf(stderr, "\t'%s' supports MAX %d blocks.\n\tYour module"
			" has %d block definitions\n\t'MAX_BLK' should be"
			" changed.\n\tPlease report this problem to the"
			" DriverGen support!\n", prog, MAX_BLK, idx);
		exit(EXIT_FAILURE);	/* 1 */
	}

	return idx;
}

/**
 * @brief Get PCI board information from the database.
 *
 * @param moduleName -- DataBase Module Name
 * @param pciInfo    -- results will go here
 *
 * @return DRIVER_GEN_OK  - if PCI board info is obtained.
 * @return DRIVER_GEN_BAD - othervise.
 */
rstat XMLGetPciInfo(char *moduleName, PciInfo_t * pciInfo)
{
	if (doc == NULL) ReadXMLFile(moduleName);
	xmlNode *n = moduleElement->children;
	while (n != NULL)
	{
		if (n->type == XML_ELEMENT_NODE && strcasecmp((char*)n->name,PCI_TAG) == 0)
		{
			xmlNode *b = n->children;
			while (b != NULL)
			{
				if (strcasecmp((char*)b->name,PCI_VENDOR_TAG) == 0)
				{
					char *str = (char*)xmlNodeListGetString(doc,b->children,1);
					pciInfo->vendorId = strtoul(str,NULL,0);
					xmlFree(str);
				}
				else if (strcasecmp((char*)b->name,PCI_DEVICE_TAG) == 0)
				{
					char *str = (char*)xmlNodeListGetString(doc,b->children,1);
					pciInfo->deviceId = strtoul(str,NULL,0);
					xmlFree(str);
				}
				b = b->next;
			}
			pciInfo->vendorId = ASSERT_MSB(pciInfo->vendorId);
			pciInfo->deviceId = ASSERT_MSB(pciInfo->deviceId);
			return DRIVER_GEN_OK;
		}
		n = n->next;
	}
	return DRIVER_GEN_BAD;
}

/**
 * @brief Get VME board information from the database.
 *
 * @param moduleName -- DataBase Module Name
 * @param vmeInfo    -- results will go here
 *
 * @return DRIVER_GEN_OK  - if VME board info is obtained.
 * @return DRIVER_GEN_BAD - othervise.
 */
rstat XMLGetVmeInfo(char *moduleName, VmeInfo_t *vmeInfo)
{
	int addrSpace = 0;

	if (doc == NULL) ReadXMLFile(moduleName);
	xmlNode *n = moduleElement->children;
	while (n != NULL)
	{
		if (n->type == XML_ELEMENT_NODE && strcasecmp((char*)n->name,VME_TAG) == 0)
		{
			vmeInfo->addr1.baseAddr = NO_ADDRESS;
			vmeInfo->addr2.baseAddr = NO_ADDRESS;
			xmlNode *b = n->children;
			while (b != NULL)
			{
				if (strcasecmp((char*)b->name,VME_ADDRESS_TAG) == 0)
				{
					addrSpace++;
					VmeAddrInfo_t *addr = &vmeInfo->addr1;
					if (addrSpace == 2) addr = &vmeInfo->addr2;
					char *p = (char*)xmlGetProp(b,(xmlChar*)"base");
					if (p != NULL)
					{
						addr->baseAddr = strtoul(p,NULL,0);
						xmlFree(p);
					}
					xmlNode *a = b->children;
					while (a != NULL)
					{
						if (strcasecmp((char*)a->name,VME_ADDRESS_AM_TAG) == 0)
						{
							char *str = (char*)xmlNodeListGetString(doc,a->children,1);
							if (strcasecmp(str, "SH") == 0)
								addr->addressModifier = DG_AM_SH;
							else if (strcasecmp(str,"ST") == 0)
								addr->addressModifier = DG_AM_ST;
							else if (strcasecmp(str,"EX") == 0)
								addr->addressModifier = DG_AM_EX;
							else if (strcasecmp(str,"CR") == 0)
								addr->addressModifier = DG_AM_CR;
							else {
								fprintf(stderr, "Unsupported AM (%s) detected for addr%d. Check xml file!\n",
												str,addrSpace);
								return DRIVER_GEN_BAD;
							}
							xmlFree(str);
						}
						else if (strcasecmp((char*)a->name,VME_ADDRESS_INCREMENT_TAG) == 0)
						{
							char *str = (char*)xmlNodeListGetString(doc,a->children,1);
							addr->increment = strtol(str,NULL,0);
							xmlFree(str);
						}
						else if (strcasecmp((char*)a->name,VME_ADDRESS_RANGE_TAG) == 0)
						{
							char *str = (char*)xmlNodeListGetString(doc,a->children,1);
							addr->range = strtoul(str,NULL,0);
							xmlFree(str);
						}
						else if (strcasecmp((char*)a->name,VME_ADDRESS_SIZE_TAG) == 0)
						{
							char *str = (char*)xmlNodeListGetString(doc,a->children,1);
							addr->dpSize = strtol(str,NULL,0);
							xmlFree(str);
						}
						a = a->next;
					}
				}
				else if (strcasecmp((char*)b->name,VME_CHANNELS_TAG) == 0)
				{
					char *str = (char*)xmlNodeListGetString(doc,b->children,1);
					vmeInfo->chCntr = strtol(str,NULL,0);
					xmlFree(str);
				}
				else if (strcasecmp((char*)b->name,VME_TYPE_TAG) == 0)
				{
					char *str = (char*)xmlNodeListGetString(doc,b->children,1);
					vmeInfo->mtn = strtol(str,NULL,0);
					xmlFree(str);
				}
				else if (strcasecmp((char*)b->name,VME_IRQ_TAG) == 0)
				{
					char *str = (char*)xmlNodeListGetString(doc,b->children,1);
					vmeInfo->irq = strtol(str,NULL,0);
					xmlFree(str);
				}
				else if (strcasecmp((char*)b->name,VME_IRQVECTOR_TAG) == 0)
				{
					char *str = (char*)xmlNodeListGetString(doc,b->children,1);
					vmeInfo->vector = strtol(str,NULL,0);
					xmlFree(str);
				}
				else if (strcasecmp((char*)b->name,VME_IRQVECTORINC_TAG) == 0)
				{
					char *str = (char*)xmlNodeListGetString(doc,b->children,1);
					vmeInfo->vectorInc = strtol(str,NULL,0);
					xmlFree(str);
				}
				b = b->next;
			}
			if (verboseMode) /* verbose driverGen */
				PrintOutVmeInfo(vmeInfo);
			return DRIVER_GEN_OK;
		}
		n = n->next;
	}
	return DRIVER_GEN_BAD;
}

/**
 * @brief Printout VME configuration info.
 *
 * @param vmeInfo -- data to print
 *
 * @return void
 */
static void PrintOutVmeInfo(VmeInfo_t * vmeInfo)
{
	int cntr;
	VmeAddrInfo_t *ptr;

	/* general info */
	printf("General information\n");
	printf("-------------------\n");
	printf("Module type number                  => %d\n", vmeInfo->mtn);
	printf("Number of data channels             => %s\n",
	       (vmeInfo->chCntr == -1) ? "NOT_DEFINED" : itoa(vmeInfo->chCntr,
							      10));
	printf("Interrupt processing hardware level => %s\n",
	       (vmeInfo->irq == -1) ? "NOT_DEFINED" : itoa(vmeInfo->irq, 10));
	printf("Interrupt vector                    => %s\n",
	       (vmeInfo->vector == -1) ? "NOT_DEFINED" : itoa(vmeInfo->vector,
							      10));
	printf("Interrupt vector increment          => %ld\n",
	       vmeInfo->vectorInc);
	printf("\n");

	/* address space specific information */
	printf("Address space information\n");
	printf("-------------------------\n");
	ptr = &(vmeInfo->addr1);
	for (cntr = 0; cntr < 2; cntr++, ptr++) {
		if (ptr->baseAddr == NO_ADDRESS) {
			printf("\nAddress space 'Addr%d' not defined.\n",
			       cntr + 1);
			continue;
		} else {
			printf("Address space 'Addr%d' info:\n", cntr + 1);
			printf("\tBase address      => 0x%lx\n", ptr->baseAddr);
			printf("\tAddress range     => 0x%x\n", ptr->range);
			printf("\tAddress increment => 0x%lx\n",
			       ptr->increment);
			printf("\tDataport size     => %ld\n", ptr->dpSize);
			printf("\tAddress modifier  => %s\n",
			       (ptr->addressModifier == DG_AM_SH) ? "SH - short" :
						 (ptr->addressModifier == DG_AM_ST) ? "ST - standard" :
					 	 (ptr->addressModifier == DG_AM_EX) ? "EX - extended" :
			       "CR - configuration");
		}
	}
}

static void ReadXMLFile(char *moduleName)
{
	char fileName[512];

	/*
	 * this initialize the library and check potential ABI mismatches
	 * between the version it was compiled for and the actual shared
	 * library used.
	 */
	LIBXML_TEST_VERSION

	/*parse the file and get the DOM */
	sprintf(fileName,"%s.xml",moduleName);
	doc = xmlReadFile(fileName, NULL, 0);

	if (doc == NULL) {
		fprintf(stderr,
			"%sFATAL%s %s Could not parse file: %s.\n",
			RED_CLR, END_CLR, ERROR_MSG, fileName);
		exit(EXIT_FAILURE);	/* 1 */
	}

	/*Get the root element node */
	rootElement = xmlDocGetRootElement(doc);
	if (strcasecmp((char*)rootElement->name,ROOT_TAG) != 0)
	{
		fprintf(stderr,
			"%sFATAL%s %s Illegal DriverGen xml file: %s.\n",
			RED_CLR, END_CLR, ERROR_MSG, fileName);
		exit(EXIT_FAILURE);	/* 1 */
	}
	moduleElement = rootElement->children;
	while (moduleElement != NULL)
	{
		if (moduleElement->type == XML_ELEMENT_NODE && strcasecmp((char*)moduleElement->name,MODULE_TAG) == 0)
		{
			xmlChar *p = xmlGetProp(moduleElement,(xmlChar*)"name");
			if (p != NULL && strcmp((char*)p,moduleName) == 0) break;
		}
		moduleElement = moduleElement->next;
	}
	if (moduleElement == NULL)
	{
		fprintf(stderr,
			"%sFATAL%s %s No valid module entry found for: %s.\n",
			RED_CLR, END_CLR, ERROR_MSG, moduleName);
		exit(EXIT_FAILURE);	/* 1 */
	}
}

/**
 * @brief Converts CERN-specific DB info table into xml module description
 *
 * @param ifn -- .info file to convert to .xml
 *
 * CERN Data Base has a form with a predefined module description layout.
 * Info file generated from it has a fixed format of DevInfo_t type.
 * Number of max allowed address spaces is fixed (2).
 * Number of blocks and registers is not allocated dynamically and fixed
 * in DevInfo_t structure.
 * This information is stored in the file and read by the driver
 * during installation.
 *
 * New driver installation schema -- is to pass the user-space address
 * where device info table is located. Driver will get info table from the user
 * space instead of a file during installation.
 * This apporach is much more flexible, as it allows to have variable amount of
 * address spaces, registers and blocks.
 *
 * @return
 */
int dgxml_cerndb_info2xml(char *ifn)
{
	xmlDocPtr doc = NULL; /* document pointer */
	xmlNodePtr root_node, module, space, block, reg; /* node pointers */
	DevInfo_t *dit;
	AddrInfo_t *aip;
	int ma, ba, ra; /* module/block/register amount */
	int offst;	/* address space offset from the module base address */
	int regar;	/* register access rights (rwec) */
	char *c;

	if (!ifn)
		ifn = get_info_fn();

	dit = read_info_file(ifn);

	if (!dit)
		return -1;

	aip = &dit->addr1; /* initialize address info pointer */

	doc = xmlNewDoc(BAD_CAST "1.0");
	root_node = xmlNewNode(NULL, BAD_CAST NODE_ROOT);
	xmlDocSetRootElement(doc, root_node);

	/* set general module description */
	module = xmlNewChild(root_node, NULL, BAD_CAST NODE_MODULE, NULL);
	xmlNewProp(module, BAD_CAST PROP_NAME, BAD_CAST TranslationGetModName());

	/*
	  ----------------------------- N.B. -----------------------------------
	  Base Address can actually be set to zero by the user in the DB.
	  But as it is set to NO_ADDRESS by DBGetVmeInfo() in case if base
	  address is 0 -- rall it back to actual value.

	  It is done in such a way, because if no BaseAddress is provided in the
	  dataBase by the user -- it is set to zero by dbrt library.
	  ----------------------------------------------------------------------
	*/
	if (dit->addr1.baseAddr == NO_ADDRESS)
		asprintf(&c, "0x%x", 0);
	else
		asprintf(&c, "%p", (void*) dit->addr1.baseAddr);
	xmlNewProp(module, BAD_CAST PROP_BA, BAD_CAST c);
	free(c);

	/* module base address increment. Next module base address will
	   be (base_address + increment) */
	asprintf(&c, "%#x", dit->addr1.increment);
	xmlNewProp(module, BAD_CAST PROP_ADDR_INCR, BAD_CAST c);
	free(c);

	/* interrupt level, vector and vector increment */
	if (dit->iLevel != -1) {
		/* int level (only if defined) */
		asprintf(&c, "%d", dit->iLevel);
		xmlNewProp(module, BAD_CAST PROP_ILEVEL, BAD_CAST c);
		free(c);
	}

	if (dit->iVector != -1) {
		/* int vector (only if defined) */
		asprintf(&c, "%d", dit->iVector);
		xmlNewProp(module, BAD_CAST PROP_IVEC, BAD_CAST c);
		free(c);

		/* int vector increment (only if defined) */
		asprintf(&c, "%d", dit->iVectorInc);
		xmlNewProp(module, BAD_CAST PROP_IVEC_INCR, BAD_CAST c);
		free(c);
	}

	/* pass through all address spaces */
	for (ma = 0; ma < DBMAM; ma++) {
		if (!aip[ma].dpSize)
			continue; /* not defined */

		if (aip[ma].baseAddr == NO_ADDRESS)
			/* BA can actually be 0.
			   But as it is set to NO_ADDRESS by DBGetVmeInfo()
			   in case if base address is 0 -- rall it back to
			   actual value.
			   NOTE, that if no BaseAddress is provided in the
			   dataBase -- it is set to zero by dbrt library */
			aip[ma].baseAddr = 0;

		space = xmlNewChild(module, NULL, BAD_CAST NODE_VME, NULL);

		asprintf(&c, "%d", ma);
		xmlNewProp(space, BAD_CAST PROP_NAME, BAD_CAST c);
		free(c);

		asprintf(&c, "%#x", aip[ma].addrModif);
		xmlNewProp(space, BAD_CAST PROP_AM, BAD_CAST c);
		free(c);

		asprintf(&c, "%d", aip[ma].dpSize);
		xmlNewProp(space, BAD_CAST PROP_DPS, BAD_CAST c);
		free(c);

		asprintf(&c, "%#x", aip[ma].range);
		xmlNewProp(space, BAD_CAST PROP_RANGE, BAD_CAST c);
		free(c);

		offst = aip[ma].baseAddr - aip[0].baseAddr;
		if (offst)
			asprintf(&c, "%p", (void*) offst);
		else
			asprintf(&c, "0x%x", offst);
		xmlNewProp(space, BAD_CAST PROP_OFFSET, BAD_CAST c);
		free(c);

		/* pass through all the blocks */
		for (ba = 0; ba < dit->blkAmount; ba++) {
			if (dit->blkDesc[ba].blkBaseAddr != ma+1)
				/* block doesn't belong to
				   current Address Space */
				continue;

			block = xmlNewChild(space, NULL, BAD_CAST
					    NODE_BLK, NULL);

			/* block ID */
			asprintf(&c, "%d", dit->blkDesc[ba].block);
			xmlNewProp(block, BAD_CAST PROP_NAME, BAD_CAST c);
			free(c);

			/* offset from the Address Space, to which it belongs */
			offst = dit->blkDesc[ba].offset;
			if (offst)
				asprintf(&c, "%p", (void*) offst);
			else
				asprintf(&c, "0x%x", offst);
			xmlNewProp(block, BAD_CAST PROP_OFFSET, BAD_CAST c);
			free(c);

			/* pass through all the registers */
			for (ra = 0; ra < dit->regAmount; ra++) {
				if (dit->regDesc[ra].bid != ba)
					/* register doesn't belong
					   to current block */
					continue;

				reg = xmlNewChild(block, NULL, BAD_CAST
						  NODE_REG, NULL);

				xmlNewProp(reg, BAD_CAST PROP_NAME,
					   BAD_CAST dit->regDesc[ra].regName);

				/* offset from the Block, to which it belongs */
				offst = dit->regDesc[ra].regOffset;
				if (offst)
					asprintf(&c, "%p", (void*) offst);
				else
					asprintf(&c, "0x%x", offst);
				xmlNewProp(reg, BAD_CAST PROP_OFFSET,
					   BAD_CAST c);
				free(c);

				/* register depth */
				asprintf(&c, "0x%x",
					 dit->regDesc[ra].regDepth);
				xmlNewProp(reg, BAD_CAST PROP_DEPTH,
					   BAD_CAST c);
				free(c);

				/* reg access mode */
				regar = dit->regDesc[ra].regar;
				{
					int i = 0;
					char am[4];

					if (regar & (1 << 0))
						am[i++] = 'w';

					if (regar & (1 << 1))
						am[i++] = 'r';

					if (regar & (1 << 2)) {
						i = 0;
						am[i++] = 'e';
					}

					if (dit->chrindex == ra)
						am[i++] = 'c';
					am[i] = 0;
					xmlNewProp(reg, BAD_CAST PROP_MODE,
						   BAD_CAST am);
				}

				/* register size */
				asprintf(&c, "%d",
					 dit->regDesc[ra].regSize);
				xmlNewProp(reg, BAD_CAST PROP_SIZE,
					   BAD_CAST c);
				free(c);

				/* access timeloop */
				if (!dit->regDesc[ra].regtl)
					continue;

				asprintf(&c, "%d", dit->regDesc[ra].regtl);
				xmlNewProp(reg, BAD_CAST PROP_TLOOP,
					   BAD_CAST c);
			}
		}
	}

	/* save it to the file */
	ifn = get_xml_fn();
	xmlSaveFormatFileEnc(ifn, doc, "UTF-8", 1);

	xmlFreeDoc(doc);
	xmlCleanupParser();

	free(dit);
	return 0;
}

void dgxml_info2xml(void)
{}
void dgxml_xml2info(void)
{}
