/**
 * @file access.c
 *
 * @brief DataBase access to get hw module description is here.
 *
 * @author Copyright (c) 2003 - 2010 CERN. Georgievskiy Yury
 *
 * @date Created on 14/12/2009
 */
#include <stdio.h>
#include "driverGen.h"
#include "utilities.h"

/* dbrtAccess.c */
extern int   DBGetRegisterConfig(char*, RegisterDef_t*, BlockDef_t*, int, char*);
extern int   DBGetBlockConfig(char*, BlockDef_t*, char*);
extern rstat DBGetPciInfo(char*, PciInfo_t*);
extern rstat DBGetVmeInfo(char*, VmeInfo_t*);

/* xmlAccess.c */
extern int   XMLGetRegisterConfig(char*, RegisterDef_t*, BlockDef_t*, int, char*);
extern int   XMLGetBlockConfig(char*, BlockDef_t*, char*);
extern rstat XMLGetPciInfo(char*, PciInfo_t*);
extern rstat XMLGetVmeInfo(char*, VmeInfo_t*);

/**
 * @brief Get register description from the DataBase.
 *
 * @param moduleName -- DataBase Module Name
 * @param defs       -- register description to put
 * @param blks       -- block description
 * @param numBlks    -- number of blocks
 * @param prog       -- program name
 *
 * Registers are prepared for the further use. If MAX allowed register amount
 * reached - printout error message and exits.
 *
 * @return number of registers, defined in the DB.
 */
int GetRegisterConfig(char *moduleName, RegisterDef_t *defs,
		      BlockDef_t *blks, int numBlks, char *prog)
{
#ifdef ACCESS_DB
	return DBGetRegisterConfig(moduleName, defs, blks, numBlks, prog);
#else
	return XMLGetRegisterConfig(moduleName, defs, blks, numBlks, prog);
#endif
}

/**
 * @brief Get block configuration from the DataBase.
 *
 * @param moduleName -- DataBase Module Name
 * @param defs       -- results will go here
 * @param prog       -- program name
 *
 * prepare results for the further use. If MAX allowed block amount
 * reached - printout error message and exits.
 *
 * @return number of defined blocks
 */
int GetBlockConfig(char *moduleName, BlockDef_t * defs, char *prog)
{
#ifdef ACCESS_DB
	return DBGetBlockConfig(moduleName, defs, prog);
#else
	return XMLGetBlockConfig(moduleName, defs, prog);
#endif
}

/**
 * @brief Get PCI board information from the database.
 *
 * @param moduleName -- DataBase Module Name
 * @param pciInfo    -- results will go here
 *
 * @return DRIVER_GEN_OK  - if PCI board info is obtained.
 * @return DRIVER_GEN_BAD - othervise.
 */
rstat GetPciInfo(char *moduleName, PciInfo_t * pciInfo)
{
#ifdef ACCESS_DB
	return DBGetPciInfo(moduleName, pciInfo);
#else
	return XMLGetPciInfo(moduleName, pciInfo);
#endif
}

/**
 * @brief Get VME board information from the database.
 *
 * @param moduleName -- DataBase Module Name
 * @param vmeInfo    -- results will go here
 *
 * @return DRIVER_GEN_OK  - if VME board info is obtained.
 * @return DRIVER_GEN_BAD - othervise.
 */
rstat GetVmeInfo(char *moduleName, VmeInfo_t *vmeInfo)
{
#ifdef ACCESS_DB
	return DBGetVmeInfo(moduleName, vmeInfo);
#else
	return XMLGetVmeInfo(moduleName, vmeInfo);
#endif
}
